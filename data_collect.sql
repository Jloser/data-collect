/*
 Navicat Premium Data Transfer

 Source Server         : mysql3306-docker
 Source Server Type    : MySQL
 Source Server Version : 50735
 Source Host           : localhost:3306
 Source Schema         : data_collect

 Target Server Type    : MySQL
 Target Server Version : 50735
 File Encoding         : 65001

 Date: 27/12/2021 10:43:39
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for InverterData_Curr
-- ----------------------------
DROP TABLE IF EXISTS `InverterData_Curr`;
CREATE TABLE `InverterData_Curr`  (
  `IID` bigint(20) NOT NULL,
  `ClltIDDay` int(11) NOT NULL,
  `ClltDaytime` datetime(0) NULL,
  `DeviceIndex` int(11) NOT NULL,
  `DayTimes` int(11) NOT NULL,
  `RegionID` int(11) NOT NULL,
  `ProjectID` int(11) NOT NULL,
  `SectionID` int(11) NOT NULL,
  `CollectID` int(11) NOT NULL,
  `ComID` int(11) NOT NULL,
  `Address` int(11) NOT NULL,
  `SN` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `StatusInt` int(11) NOT NULL,
  `StatusDesc` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `PVVoltage` float NOT NULL,
  `PVCurrent` float NOT NULL,
  `PVOutputPower` float NOT NULL,
  `PVVoltageB` float NOT NULL,
  `PVCurrentB` float NOT NULL,
  `PVOutputPowerB` float NOT NULL,
  `PVVoltageC` float NOT NULL,
  `PVCurrentC` float NOT NULL,
  `PVOutputPowerC` float NOT NULL,
  `PVTotalOutputPower` float NOT NULL,
  `VoltageA` float NOT NULL,
  `CurrentA` float NOT NULL,
  `FrequencyA` float NOT NULL,
  `OutputPowerA` float NOT NULL,
  `ROutputPowerA` float NOT NULL,
  `PowerFactorA` float NOT NULL,
  `VoltageB` float NOT NULL,
  `CurrentB` float NOT NULL,
  `FrequencyB` float NOT NULL,
  `OutputPowerB` float NOT NULL,
  `ROutputPowerB` float NOT NULL,
  `PowerFactorB` float NOT NULL,
  `VoltageC` float NOT NULL,
  `CurrentC` float NOT NULL,
  `FrequencyC` float NOT NULL,
  `OutputPowerC` float NOT NULL,
  `ROutputPowerC` float NOT NULL,
  `PowerFactorC` float NOT NULL,
  `TotalOutputPower` float NOT NULL,
  `RTotalOutputPower` float NOT NULL,
  `TotalPowerFactor` float NOT NULL,
  `TodayEnergy` float NOT NULL,
  `TotalEnergy` float NOT NULL,
  `NTodayEnergy` float NOT NULL,
  `NTotalEnergy` float NOT NULL,
  `RTodayEnergy` float NOT NULL,
  `RTotalEnergy` float NOT NULL,
  `NRTodayEnergy` float NOT NULL,
  `NRTotalEnergy` float NOT NULL,
  `BtryVoltage` float NOT NULL,
  `BtryCurrent` float NOT NULL,
  `BtryOutputPower` float NOT NULL,
  `TemperatureInverter` float NOT NULL,
  `TemperatureBooster` float NOT NULL,
  `TemperatureSink` float NOT NULL,
  `TodayHour` float NOT NULL,
  `TotalHour` float NOT NULL,
  `AGCSetting` float NOT NULL,
  `AVCSetting` float NOT NULL,
  `GroundResistance` float NOT NULL,
  `LeakCurrent` float NOT NULL,
  `StringVoltage1` float NOT NULL,
  `StringCurrent1` float NOT NULL,
  `StringVoltage2` float NOT NULL,
  `StringCurrent2` float NOT NULL,
  `StringVoltage3` float NOT NULL,
  `StringCurrent3` float NOT NULL,
  `StringVoltage4` float NOT NULL,
  `StringCurrent4` float NOT NULL,
  `StringVoltage5` float NOT NULL,
  `StringCurrent5` float NOT NULL,
  `StringVoltage6` float NOT NULL,
  `StringCurrent6` float NOT NULL,
  `StringVoltage7` float NOT NULL,
  `StringCurrent7` float NOT NULL,
  `StringVoltage8` float NOT NULL,
  `StringCurrent8` float NOT NULL,
  `StringVoltage9` float NOT NULL,
  `StringCurrent9` float NOT NULL,
  `StringVoltage10` float NOT NULL,
  `StringCurrent10` float NOT NULL,
  `StringVoltage11` float NOT NULL,
  `StringCurrent11` float NOT NULL,
  `StringVoltage12` float NOT NULL,
  `StringCurrent12` float NOT NULL,
  `StringVoltage13` float NOT NULL,
  `StringCurrent13` float NOT NULL,
  `StringVoltage14` float NOT NULL,
  `StringCurrent14` float NOT NULL,
  `StringVoltage15` float NOT NULL,
  `StringCurrent15` float NOT NULL,
  `StringVoltage16` float NOT NULL,
  `StringCurrent16` float NOT NULL,
  `StringVoltage17` float NOT NULL,
  `StringCurrent17` float NOT NULL,
  `StringVoltage18` float NOT NULL,
  `StringCurrent18` float NOT NULL,
  `StringVoltage19` float NOT NULL,
  `StringCurrent19` float NOT NULL,
  `StringVoltage20` float NOT NULL,
  `StringCurrent20` float NOT NULL,
  `StringVoltage21` float NOT NULL,
  `StringCurrent21` float NOT NULL,
  `StringVoltage22` float NOT NULL,
  `StringCurrent22` float NOT NULL,
  `StringVoltage23` float NOT NULL,
  `StringCurrent23` float NOT NULL,
  `StringVoltage24` float NOT NULL,
  `StringCurrent24` float NOT NULL,
  `MotherLineVoltage` float NOT NULL,
  `GroudVoltage` float NOT NULL,
  `NGroudVoltage` float NOT NULL,
  `DesignPower` float NOT NULL,
  `VoltageAB` float NOT NULL,
  `VoltageBC` float NOT NULL,
  `VoltageCA` float NOT NULL,
  `SystemTime` int(11) NOT NULL,
  `Efficiency` int(11) NOT NULL,
  `PowerOnSecond` int(11) NOT NULL,
  `PowerOffSecond` int(11) NOT NULL,
  `MPPT1` float NOT NULL,
  `MPPT2` float NOT NULL,
  `MPPT3` float NOT NULL,
  `MPPT4` float NOT NULL,
  PRIMARY KEY (`IID`) USING BTREE,
  INDEX `index_InverterData_Curr`(`ProjectID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for InverterData_CurrYear
-- ----------------------------
DROP TABLE IF EXISTS `InverterData_CurrYear`;
CREATE TABLE `InverterData_CurrYear`  (
  `IID` bigint(20) NOT NULL,
  `ClltIDDay` int(11) NOT NULL,
  `ClltDaytime` datetime(0) NULL,
  `DeviceIndex` int(11) NOT NULL,
  `DayTimes` int(11) NOT NULL,
  `RegionID` int(11) NOT NULL,
  `ProjectID` int(11) NOT NULL,
  `SectionID` int(11) NOT NULL,
  `CollectID` int(11) NOT NULL,
  `ComID` int(11) NOT NULL,
  `Address` int(11) NOT NULL,
  `SN` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `StatusInt` int(11) NOT NULL,
  `StatusDesc` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `PVVoltage` float NOT NULL,
  `PVCurrent` float NOT NULL,
  `PVOutputPower` float NOT NULL,
  `PVVoltageB` float NOT NULL,
  `PVCurrentB` float NOT NULL,
  `PVOutputPowerB` float NOT NULL,
  `PVVoltageC` float NOT NULL,
  `PVCurrentC` float NOT NULL,
  `PVOutputPowerC` float NOT NULL,
  `PVTotalOutputPower` float NOT NULL,
  `VoltageA` float NOT NULL,
  `CurrentA` float NOT NULL,
  `FrequencyA` float NOT NULL,
  `OutputPowerA` float NOT NULL,
  `ROutputPowerA` float NOT NULL,
  `PowerFactorA` float NOT NULL,
  `VoltageB` float NOT NULL,
  `CurrentB` float NOT NULL,
  `FrequencyB` float NOT NULL,
  `OutputPowerB` float NOT NULL,
  `ROutputPowerB` float NOT NULL,
  `PowerFactorB` float NOT NULL,
  `VoltageC` float NOT NULL,
  `CurrentC` float NOT NULL,
  `FrequencyC` float NOT NULL,
  `OutputPowerC` float NOT NULL,
  `ROutputPowerC` float NOT NULL,
  `PowerFactorC` float NOT NULL,
  `TotalOutputPower` float NOT NULL,
  `RTotalOutputPower` float NOT NULL,
  `TotalPowerFactor` float NOT NULL,
  `TodayEnergy` float NOT NULL,
  `TotalEnergy` float NOT NULL,
  `NTodayEnergy` float NOT NULL,
  `NTotalEnergy` float NOT NULL,
  `RTodayEnergy` float NOT NULL,
  `RTotalEnergy` float NOT NULL,
  `NRTodayEnergy` float NOT NULL,
  `NRTotalEnergy` float NOT NULL,
  `BtryVoltage` float NOT NULL,
  `BtryCurrent` float NOT NULL,
  `BtryOutputPower` float NOT NULL,
  `TemperatureInverter` float NOT NULL,
  `TemperatureBooster` float NOT NULL,
  `TemperatureSink` float NOT NULL,
  `TodayHour` float NOT NULL,
  `TotalHour` float NOT NULL,
  `AGCSetting` float NOT NULL,
  `AVCSetting` float NOT NULL,
  `GroundResistance` float NOT NULL,
  `LeakCurrent` float NOT NULL,
  `StringVoltage1` float NOT NULL,
  `StringCurrent1` float NOT NULL,
  `StringVoltage2` float NOT NULL,
  `StringCurrent2` float NOT NULL,
  `StringVoltage3` float NOT NULL,
  `StringCurrent3` float NOT NULL,
  `StringVoltage4` float NOT NULL,
  `StringCurrent4` float NOT NULL,
  `StringVoltage5` float NOT NULL,
  `StringCurrent5` float NOT NULL,
  `StringVoltage6` float NOT NULL,
  `StringCurrent6` float NOT NULL,
  `StringVoltage7` float NOT NULL,
  `StringCurrent7` float NOT NULL,
  `StringVoltage8` float NOT NULL,
  `StringCurrent8` float NOT NULL,
  `StringVoltage9` float NOT NULL,
  `StringCurrent9` float NOT NULL,
  `StringVoltage10` float NOT NULL,
  `StringCurrent10` float NOT NULL,
  `StringVoltage11` float NOT NULL,
  `StringCurrent11` float NOT NULL,
  `StringVoltage12` float NOT NULL,
  `StringCurrent12` float NOT NULL,
  `StringVoltage13` float NOT NULL,
  `StringCurrent13` float NOT NULL,
  `StringVoltage14` float NOT NULL,
  `StringCurrent14` float NOT NULL,
  `StringVoltage15` float NOT NULL,
  `StringCurrent15` float NOT NULL,
  `StringVoltage16` float NOT NULL,
  `StringCurrent16` float NOT NULL,
  `StringVoltage17` float NOT NULL,
  `StringCurrent17` float NOT NULL,
  `StringVoltage18` float NOT NULL,
  `StringCurrent18` float NOT NULL,
  `StringVoltage19` float NOT NULL,
  `StringCurrent19` float NOT NULL,
  `StringVoltage20` float NOT NULL,
  `StringCurrent20` float NOT NULL,
  `StringVoltage21` float NOT NULL,
  `StringCurrent21` float NOT NULL,
  `StringVoltage22` float NOT NULL,
  `StringCurrent22` float NOT NULL,
  `StringVoltage23` float NOT NULL,
  `StringCurrent23` float NOT NULL,
  `StringVoltage24` float NOT NULL,
  `StringCurrent24` float NOT NULL,
  `MotherLineVoltage` float NOT NULL,
  `GroudVoltage` float NOT NULL,
  `NGroudVoltage` float NOT NULL,
  `DesignPower` float NOT NULL,
  `VoltageAB` float NOT NULL,
  `VoltageBC` float NOT NULL,
  `VoltageCA` float NOT NULL,
  `SystemTime` int(11) NOT NULL,
  `Efficiency` int(11) NOT NULL,
  `PowerOnSecond` int(11) NOT NULL,
  `PowerOffSecond` int(11) NOT NULL,
  `MPPT1` float NOT NULL,
  `MPPT2` float NOT NULL,
  `MPPT3` float NOT NULL,
  `MPPT4` float NOT NULL,
  PRIMARY KEY (`IID`) USING BTREE,
  INDEX `index_InverterData_CurrYear`(`ClltIDDay`, `ProjectID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for InverterRpt_CurrYear
-- ----------------------------
DROP TABLE IF EXISTS `InverterRpt_CurrYear`;
CREATE TABLE `InverterRpt_CurrYear`  (
  `IID` bigint(20) NOT NULL,
  `ClltIDDay` int(11) NOT NULL,
  `DeviceIndex` int(11) NOT NULL,
  `ClltIDYear` int(11) NOT NULL,
  `ClltIDMonth` int(11) NOT NULL,
  `ClltIDDayIndex` int(11) NOT NULL,
  `RegionID` int(11) NOT NULL,
  `ProjectID` int(11) NOT NULL,
  `SectionID` int(11) NOT NULL,
  `CollectID` int(11) NOT NULL,
  `ComID` int(11) NOT NULL,
  `Address` int(11) NOT NULL,
  `TodayEnergy` float NOT NULL,
  `TotalEnergy` float NOT NULL,
  PRIMARY KEY (`IID`) USING BTREE,
  INDEX `index_InverterRpt_CurrYear`(`ClltIDDay`, `ProjectID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for request
-- ----------------------------
DROP TABLE IF EXISTS `request`;
CREATE TABLE `request`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `rec_data` varchar(2048) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '收包数据',
  `rec_data_len` int(11) NULL DEFAULT NULL COMMENT '接收数据的长度',
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `deal` bit(1) NULL DEFAULT NULL COMMENT '是否已经转换存储',
  `intver_data_id` bigint(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;

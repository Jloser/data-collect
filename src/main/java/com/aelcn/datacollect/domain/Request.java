package com.aelcn.datacollect.domain;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import lombok.Data;

/**
 * 
 * @TableName request
 */
@TableName(value ="request")
@Data
public class Request implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 收包数据
     */
    private String recData;

    private Integer recDataLen;

    /**
     * 
     */
    @TableField(fill = FieldFill.INSERT)
    private Timestamp createTime;

    /**
     * 
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Timestamp updateTime;

    /**
     * 是否已经转换存储
     */
    private Boolean deal;

    private Long intverDataId;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

}
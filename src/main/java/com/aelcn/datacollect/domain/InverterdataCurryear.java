package com.aelcn.datacollect.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * @TableName InverterData_CurrYear
 */
@TableName(value ="InverterData_CurrYear")
@Data
public class InverterdataCurryear implements Serializable {
    /**
     * 
     */
    @TableId
    private Long iid;

    /**
     * 
     */
    private Integer clltidday;

    /**
     * 
     */
    private Date clltdaytime;

    /**
     * 
     */
    private Integer deviceindex;

    /**
     * 
     */
    private Integer daytimes;

    /**
     * 
     */
    private Integer regionid;

    /**
     * 
     */
    private Integer projectid;

    /**
     * 
     */
    private Integer sectionid;

    /**
     * 
     */
    private Integer collectid;

    /**
     * 
     */
    private Integer comid;

    /**
     * 
     */
    private Integer address;

    /**
     * 
     */
    private String sn;

    /**
     * 
     */
    private Integer statusint;

    /**
     * 
     */
    private String statusdesc;

    /**
     * 
     */
    private Double pvvoltage;

    /**
     * 
     */
    private Double pvcurrent;

    /**
     * 
     */
    private Double pvoutputpower;

    /**
     * 
     */
    private Double pvvoltageb;

    /**
     * 
     */
    private Double pvcurrentb;

    /**
     * 
     */
    private Double pvoutputpowerb;

    /**
     * 
     */
    private Double pvvoltagec;

    /**
     * 
     */
    private Double pvcurrentc;

    /**
     * 
     */
    private Double pvoutputpowerc;

    /**
     * 
     */
    private Double pvtotaloutputpower;

    /**
     * 
     */
    private Double voltagea;

    /**
     * 
     */
    private Double currenta;

    /**
     * 
     */
    private Double frequencya;

    /**
     * 
     */
    private Double outputpowera;

    /**
     * 
     */
    private Double routputpowera;

    /**
     * 
     */
    private Double powerfactora;

    /**
     * 
     */
    private Double voltageb;

    /**
     * 
     */
    private Double currentb;

    /**
     * 
     */
    private Double frequencyb;

    /**
     * 
     */
    private Double outputpowerb;

    /**
     * 
     */
    private Double routputpowerb;

    /**
     * 
     */
    private Double powerfactorb;

    /**
     * 
     */
    private Double voltagec;

    /**
     * 
     */
    private Double currentc;

    /**
     * 
     */
    private Double frequencyc;

    /**
     * 
     */
    private Double outputpowerc;

    /**
     * 
     */
    private Double routputpowerc;

    /**
     * 
     */
    private Double powerfactorc;

    /**
     * 
     */
    private Double totaloutputpower;

    /**
     * 
     */
    private Double rtotaloutputpower;

    /**
     * 
     */
    private Double totalpowerfactor;

    /**
     * 
     */
    private Double todayenergy;

    /**
     * 
     */
    private Double totalenergy;

    /**
     * 
     */
    private Double ntodayenergy;

    /**
     * 
     */
    private Double ntotalenergy;

    /**
     * 
     */
    private Double rtodayenergy;

    /**
     * 
     */
    private Double rtotalenergy;

    /**
     * 
     */
    private Double nrtodayenergy;

    /**
     * 
     */
    private Double nrtotalenergy;

    /**
     * 
     */
    private Double btryvoltage;

    /**
     * 
     */
    private Double btrycurrent;

    /**
     * 
     */
    private Double btryoutputpower;

    /**
     * 
     */
    private Double temperatureinverter;

    /**
     * 
     */
    private Double temperaturebooster;

    /**
     * 
     */
    private Double temperaturesink;

    /**
     * 
     */
    private Double todayhour;

    /**
     * 
     */
    private Double totalhour;

    /**
     * 
     */
    private Double agcsetting;

    /**
     * 
     */
    private Double avcsetting;

    /**
     * 
     */
    private Double groundresistance;

    /**
     * 
     */
    private Double leakcurrent;

    /**
     * 
     */
    private Double stringvoltage1;

    /**
     * 
     */
    private Double stringcurrent1;

    /**
     * 
     */
    private Double stringvoltage2;

    /**
     * 
     */
    private Double stringcurrent2;

    /**
     * 
     */
    private Double stringvoltage3;

    /**
     * 
     */
    private Double stringcurrent3;

    /**
     * 
     */
    private Double stringvoltage4;

    /**
     * 
     */
    private Double stringcurrent4;

    /**
     * 
     */
    private Double stringvoltage5;

    /**
     * 
     */
    private Double stringcurrent5;

    /**
     * 
     */
    private Double stringvoltage6;

    /**
     * 
     */
    private Double stringcurrent6;

    /**
     * 
     */
    private Double stringvoltage7;

    /**
     * 
     */
    private Double stringcurrent7;

    /**
     * 
     */
    private Double stringvoltage8;

    /**
     * 
     */
    private Double stringcurrent8;

    /**
     * 
     */
    private Double stringvoltage9;

    /**
     * 
     */
    private Double stringcurrent9;

    /**
     * 
     */
    private Double stringvoltage10;

    /**
     * 
     */
    private Double stringcurrent10;

    /**
     * 
     */
    private Double stringvoltage11;

    /**
     * 
     */
    private Double stringcurrent11;

    /**
     * 
     */
    private Double stringvoltage12;

    /**
     * 
     */
    private Double stringcurrent12;

    /**
     * 
     */
    private Double stringvoltage13;

    /**
     * 
     */
    private Double stringcurrent13;

    /**
     * 
     */
    private Double stringvoltage14;

    /**
     * 
     */
    private Double stringcurrent14;

    /**
     * 
     */
    private Double stringvoltage15;

    /**
     * 
     */
    private Double stringcurrent15;

    /**
     * 
     */
    private Double stringvoltage16;

    /**
     * 
     */
    private Double stringcurrent16;

    /**
     * 
     */
    private Double stringvoltage17;

    /**
     * 
     */
    private Double stringcurrent17;

    /**
     * 
     */
    private Double stringvoltage18;

    /**
     * 
     */
    private Double stringcurrent18;

    /**
     * 
     */
    private Double stringvoltage19;

    /**
     * 
     */
    private Double stringcurrent19;

    /**
     * 
     */
    private Double stringvoltage20;

    /**
     * 
     */
    private Double stringcurrent20;

    /**
     * 
     */
    private Double stringvoltage21;

    /**
     * 
     */
    private Double stringcurrent21;

    /**
     * 
     */
    private Double stringvoltage22;

    /**
     * 
     */
    private Double stringcurrent22;

    /**
     * 
     */
    private Double stringvoltage23;

    /**
     * 
     */
    private Double stringcurrent23;

    /**
     * 
     */
    private Double stringvoltage24;

    /**
     * 
     */
    private Double stringcurrent24;

    /**
     * 
     */
    private Double motherlinevoltage;

    /**
     * 
     */
    private Double groudvoltage;

    /**
     * 
     */
    private Double ngroudvoltage;

    /**
     * 
     */
    private Double designpower;

    /**
     * 
     */
    private Double voltageab;

    /**
     * 
     */
    private Double voltagebc;

    /**
     * 
     */
    private Double voltageca;

    /**
     * 
     */
    private Integer systemtime;

    /**
     * 
     */
    private Integer efficiency;

    /**
     * 
     */
    private Integer poweronsecond;

    /**
     * 
     */
    private Integer poweroffsecond;

    /**
     * 
     */
    private Double mppt1;

    /**
     * 
     */
    private Double mppt2;

    /**
     * 
     */
    private Double mppt3;

    /**
     * 
     */
    private Double mppt4;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        InverterdataCurryear other = (InverterdataCurryear) that;
        return (this.getIid() == null ? other.getIid() == null : this.getIid().equals(other.getIid()))
            && (this.getClltidday() == null ? other.getClltidday() == null : this.getClltidday().equals(other.getClltidday()))
            && (this.getClltdaytime() == null ? other.getClltdaytime() == null : this.getClltdaytime().equals(other.getClltdaytime()))
            && (this.getDeviceindex() == null ? other.getDeviceindex() == null : this.getDeviceindex().equals(other.getDeviceindex()))
            && (this.getDaytimes() == null ? other.getDaytimes() == null : this.getDaytimes().equals(other.getDaytimes()))
            && (this.getRegionid() == null ? other.getRegionid() == null : this.getRegionid().equals(other.getRegionid()))
            && (this.getProjectid() == null ? other.getProjectid() == null : this.getProjectid().equals(other.getProjectid()))
            && (this.getSectionid() == null ? other.getSectionid() == null : this.getSectionid().equals(other.getSectionid()))
            && (this.getCollectid() == null ? other.getCollectid() == null : this.getCollectid().equals(other.getCollectid()))
            && (this.getComid() == null ? other.getComid() == null : this.getComid().equals(other.getComid()))
            && (this.getAddress() == null ? other.getAddress() == null : this.getAddress().equals(other.getAddress()))
            && (this.getSn() == null ? other.getSn() == null : this.getSn().equals(other.getSn()))
            && (this.getStatusint() == null ? other.getStatusint() == null : this.getStatusint().equals(other.getStatusint()))
            && (this.getStatusdesc() == null ? other.getStatusdesc() == null : this.getStatusdesc().equals(other.getStatusdesc()))
            && (this.getPvvoltage() == null ? other.getPvvoltage() == null : this.getPvvoltage().equals(other.getPvvoltage()))
            && (this.getPvcurrent() == null ? other.getPvcurrent() == null : this.getPvcurrent().equals(other.getPvcurrent()))
            && (this.getPvoutputpower() == null ? other.getPvoutputpower() == null : this.getPvoutputpower().equals(other.getPvoutputpower()))
            && (this.getPvvoltageb() == null ? other.getPvvoltageb() == null : this.getPvvoltageb().equals(other.getPvvoltageb()))
            && (this.getPvcurrentb() == null ? other.getPvcurrentb() == null : this.getPvcurrentb().equals(other.getPvcurrentb()))
            && (this.getPvoutputpowerb() == null ? other.getPvoutputpowerb() == null : this.getPvoutputpowerb().equals(other.getPvoutputpowerb()))
            && (this.getPvvoltagec() == null ? other.getPvvoltagec() == null : this.getPvvoltagec().equals(other.getPvvoltagec()))
            && (this.getPvcurrentc() == null ? other.getPvcurrentc() == null : this.getPvcurrentc().equals(other.getPvcurrentc()))
            && (this.getPvoutputpowerc() == null ? other.getPvoutputpowerc() == null : this.getPvoutputpowerc().equals(other.getPvoutputpowerc()))
            && (this.getPvtotaloutputpower() == null ? other.getPvtotaloutputpower() == null : this.getPvtotaloutputpower().equals(other.getPvtotaloutputpower()))
            && (this.getVoltagea() == null ? other.getVoltagea() == null : this.getVoltagea().equals(other.getVoltagea()))
            && (this.getCurrenta() == null ? other.getCurrenta() == null : this.getCurrenta().equals(other.getCurrenta()))
            && (this.getFrequencya() == null ? other.getFrequencya() == null : this.getFrequencya().equals(other.getFrequencya()))
            && (this.getOutputpowera() == null ? other.getOutputpowera() == null : this.getOutputpowera().equals(other.getOutputpowera()))
            && (this.getRoutputpowera() == null ? other.getRoutputpowera() == null : this.getRoutputpowera().equals(other.getRoutputpowera()))
            && (this.getPowerfactora() == null ? other.getPowerfactora() == null : this.getPowerfactora().equals(other.getPowerfactora()))
            && (this.getVoltageb() == null ? other.getVoltageb() == null : this.getVoltageb().equals(other.getVoltageb()))
            && (this.getCurrentb() == null ? other.getCurrentb() == null : this.getCurrentb().equals(other.getCurrentb()))
            && (this.getFrequencyb() == null ? other.getFrequencyb() == null : this.getFrequencyb().equals(other.getFrequencyb()))
            && (this.getOutputpowerb() == null ? other.getOutputpowerb() == null : this.getOutputpowerb().equals(other.getOutputpowerb()))
            && (this.getRoutputpowerb() == null ? other.getRoutputpowerb() == null : this.getRoutputpowerb().equals(other.getRoutputpowerb()))
            && (this.getPowerfactorb() == null ? other.getPowerfactorb() == null : this.getPowerfactorb().equals(other.getPowerfactorb()))
            && (this.getVoltagec() == null ? other.getVoltagec() == null : this.getVoltagec().equals(other.getVoltagec()))
            && (this.getCurrentc() == null ? other.getCurrentc() == null : this.getCurrentc().equals(other.getCurrentc()))
            && (this.getFrequencyc() == null ? other.getFrequencyc() == null : this.getFrequencyc().equals(other.getFrequencyc()))
            && (this.getOutputpowerc() == null ? other.getOutputpowerc() == null : this.getOutputpowerc().equals(other.getOutputpowerc()))
            && (this.getRoutputpowerc() == null ? other.getRoutputpowerc() == null : this.getRoutputpowerc().equals(other.getRoutputpowerc()))
            && (this.getPowerfactorc() == null ? other.getPowerfactorc() == null : this.getPowerfactorc().equals(other.getPowerfactorc()))
            && (this.getTotaloutputpower() == null ? other.getTotaloutputpower() == null : this.getTotaloutputpower().equals(other.getTotaloutputpower()))
            && (this.getRtotaloutputpower() == null ? other.getRtotaloutputpower() == null : this.getRtotaloutputpower().equals(other.getRtotaloutputpower()))
            && (this.getTotalpowerfactor() == null ? other.getTotalpowerfactor() == null : this.getTotalpowerfactor().equals(other.getTotalpowerfactor()))
            && (this.getTodayenergy() == null ? other.getTodayenergy() == null : this.getTodayenergy().equals(other.getTodayenergy()))
            && (this.getTotalenergy() == null ? other.getTotalenergy() == null : this.getTotalenergy().equals(other.getTotalenergy()))
            && (this.getNtodayenergy() == null ? other.getNtodayenergy() == null : this.getNtodayenergy().equals(other.getNtodayenergy()))
            && (this.getNtotalenergy() == null ? other.getNtotalenergy() == null : this.getNtotalenergy().equals(other.getNtotalenergy()))
            && (this.getRtodayenergy() == null ? other.getRtodayenergy() == null : this.getRtodayenergy().equals(other.getRtodayenergy()))
            && (this.getRtotalenergy() == null ? other.getRtotalenergy() == null : this.getRtotalenergy().equals(other.getRtotalenergy()))
            && (this.getNrtodayenergy() == null ? other.getNrtodayenergy() == null : this.getNrtodayenergy().equals(other.getNrtodayenergy()))
            && (this.getNrtotalenergy() == null ? other.getNrtotalenergy() == null : this.getNrtotalenergy().equals(other.getNrtotalenergy()))
            && (this.getBtryvoltage() == null ? other.getBtryvoltage() == null : this.getBtryvoltage().equals(other.getBtryvoltage()))
            && (this.getBtrycurrent() == null ? other.getBtrycurrent() == null : this.getBtrycurrent().equals(other.getBtrycurrent()))
            && (this.getBtryoutputpower() == null ? other.getBtryoutputpower() == null : this.getBtryoutputpower().equals(other.getBtryoutputpower()))
            && (this.getTemperatureinverter() == null ? other.getTemperatureinverter() == null : this.getTemperatureinverter().equals(other.getTemperatureinverter()))
            && (this.getTemperaturebooster() == null ? other.getTemperaturebooster() == null : this.getTemperaturebooster().equals(other.getTemperaturebooster()))
            && (this.getTemperaturesink() == null ? other.getTemperaturesink() == null : this.getTemperaturesink().equals(other.getTemperaturesink()))
            && (this.getTodayhour() == null ? other.getTodayhour() == null : this.getTodayhour().equals(other.getTodayhour()))
            && (this.getTotalhour() == null ? other.getTotalhour() == null : this.getTotalhour().equals(other.getTotalhour()))
            && (this.getAgcsetting() == null ? other.getAgcsetting() == null : this.getAgcsetting().equals(other.getAgcsetting()))
            && (this.getAvcsetting() == null ? other.getAvcsetting() == null : this.getAvcsetting().equals(other.getAvcsetting()))
            && (this.getGroundresistance() == null ? other.getGroundresistance() == null : this.getGroundresistance().equals(other.getGroundresistance()))
            && (this.getLeakcurrent() == null ? other.getLeakcurrent() == null : this.getLeakcurrent().equals(other.getLeakcurrent()))
            && (this.getStringvoltage1() == null ? other.getStringvoltage1() == null : this.getStringvoltage1().equals(other.getStringvoltage1()))
            && (this.getStringcurrent1() == null ? other.getStringcurrent1() == null : this.getStringcurrent1().equals(other.getStringcurrent1()))
            && (this.getStringvoltage2() == null ? other.getStringvoltage2() == null : this.getStringvoltage2().equals(other.getStringvoltage2()))
            && (this.getStringcurrent2() == null ? other.getStringcurrent2() == null : this.getStringcurrent2().equals(other.getStringcurrent2()))
            && (this.getStringvoltage3() == null ? other.getStringvoltage3() == null : this.getStringvoltage3().equals(other.getStringvoltage3()))
            && (this.getStringcurrent3() == null ? other.getStringcurrent3() == null : this.getStringcurrent3().equals(other.getStringcurrent3()))
            && (this.getStringvoltage4() == null ? other.getStringvoltage4() == null : this.getStringvoltage4().equals(other.getStringvoltage4()))
            && (this.getStringcurrent4() == null ? other.getStringcurrent4() == null : this.getStringcurrent4().equals(other.getStringcurrent4()))
            && (this.getStringvoltage5() == null ? other.getStringvoltage5() == null : this.getStringvoltage5().equals(other.getStringvoltage5()))
            && (this.getStringcurrent5() == null ? other.getStringcurrent5() == null : this.getStringcurrent5().equals(other.getStringcurrent5()))
            && (this.getStringvoltage6() == null ? other.getStringvoltage6() == null : this.getStringvoltage6().equals(other.getStringvoltage6()))
            && (this.getStringcurrent6() == null ? other.getStringcurrent6() == null : this.getStringcurrent6().equals(other.getStringcurrent6()))
            && (this.getStringvoltage7() == null ? other.getStringvoltage7() == null : this.getStringvoltage7().equals(other.getStringvoltage7()))
            && (this.getStringcurrent7() == null ? other.getStringcurrent7() == null : this.getStringcurrent7().equals(other.getStringcurrent7()))
            && (this.getStringvoltage8() == null ? other.getStringvoltage8() == null : this.getStringvoltage8().equals(other.getStringvoltage8()))
            && (this.getStringcurrent8() == null ? other.getStringcurrent8() == null : this.getStringcurrent8().equals(other.getStringcurrent8()))
            && (this.getStringvoltage9() == null ? other.getStringvoltage9() == null : this.getStringvoltage9().equals(other.getStringvoltage9()))
            && (this.getStringcurrent9() == null ? other.getStringcurrent9() == null : this.getStringcurrent9().equals(other.getStringcurrent9()))
            && (this.getStringvoltage10() == null ? other.getStringvoltage10() == null : this.getStringvoltage10().equals(other.getStringvoltage10()))
            && (this.getStringcurrent10() == null ? other.getStringcurrent10() == null : this.getStringcurrent10().equals(other.getStringcurrent10()))
            && (this.getStringvoltage11() == null ? other.getStringvoltage11() == null : this.getStringvoltage11().equals(other.getStringvoltage11()))
            && (this.getStringcurrent11() == null ? other.getStringcurrent11() == null : this.getStringcurrent11().equals(other.getStringcurrent11()))
            && (this.getStringvoltage12() == null ? other.getStringvoltage12() == null : this.getStringvoltage12().equals(other.getStringvoltage12()))
            && (this.getStringcurrent12() == null ? other.getStringcurrent12() == null : this.getStringcurrent12().equals(other.getStringcurrent12()))
            && (this.getStringvoltage13() == null ? other.getStringvoltage13() == null : this.getStringvoltage13().equals(other.getStringvoltage13()))
            && (this.getStringcurrent13() == null ? other.getStringcurrent13() == null : this.getStringcurrent13().equals(other.getStringcurrent13()))
            && (this.getStringvoltage14() == null ? other.getStringvoltage14() == null : this.getStringvoltage14().equals(other.getStringvoltage14()))
            && (this.getStringcurrent14() == null ? other.getStringcurrent14() == null : this.getStringcurrent14().equals(other.getStringcurrent14()))
            && (this.getStringvoltage15() == null ? other.getStringvoltage15() == null : this.getStringvoltage15().equals(other.getStringvoltage15()))
            && (this.getStringcurrent15() == null ? other.getStringcurrent15() == null : this.getStringcurrent15().equals(other.getStringcurrent15()))
            && (this.getStringvoltage16() == null ? other.getStringvoltage16() == null : this.getStringvoltage16().equals(other.getStringvoltage16()))
            && (this.getStringcurrent16() == null ? other.getStringcurrent16() == null : this.getStringcurrent16().equals(other.getStringcurrent16()))
            && (this.getStringvoltage17() == null ? other.getStringvoltage17() == null : this.getStringvoltage17().equals(other.getStringvoltage17()))
            && (this.getStringcurrent17() == null ? other.getStringcurrent17() == null : this.getStringcurrent17().equals(other.getStringcurrent17()))
            && (this.getStringvoltage18() == null ? other.getStringvoltage18() == null : this.getStringvoltage18().equals(other.getStringvoltage18()))
            && (this.getStringcurrent18() == null ? other.getStringcurrent18() == null : this.getStringcurrent18().equals(other.getStringcurrent18()))
            && (this.getStringvoltage19() == null ? other.getStringvoltage19() == null : this.getStringvoltage19().equals(other.getStringvoltage19()))
            && (this.getStringcurrent19() == null ? other.getStringcurrent19() == null : this.getStringcurrent19().equals(other.getStringcurrent19()))
            && (this.getStringvoltage20() == null ? other.getStringvoltage20() == null : this.getStringvoltage20().equals(other.getStringvoltage20()))
            && (this.getStringcurrent20() == null ? other.getStringcurrent20() == null : this.getStringcurrent20().equals(other.getStringcurrent20()))
            && (this.getStringvoltage21() == null ? other.getStringvoltage21() == null : this.getStringvoltage21().equals(other.getStringvoltage21()))
            && (this.getStringcurrent21() == null ? other.getStringcurrent21() == null : this.getStringcurrent21().equals(other.getStringcurrent21()))
            && (this.getStringvoltage22() == null ? other.getStringvoltage22() == null : this.getStringvoltage22().equals(other.getStringvoltage22()))
            && (this.getStringcurrent22() == null ? other.getStringcurrent22() == null : this.getStringcurrent22().equals(other.getStringcurrent22()))
            && (this.getStringvoltage23() == null ? other.getStringvoltage23() == null : this.getStringvoltage23().equals(other.getStringvoltage23()))
            && (this.getStringcurrent23() == null ? other.getStringcurrent23() == null : this.getStringcurrent23().equals(other.getStringcurrent23()))
            && (this.getStringvoltage24() == null ? other.getStringvoltage24() == null : this.getStringvoltage24().equals(other.getStringvoltage24()))
            && (this.getStringcurrent24() == null ? other.getStringcurrent24() == null : this.getStringcurrent24().equals(other.getStringcurrent24()))
            && (this.getMotherlinevoltage() == null ? other.getMotherlinevoltage() == null : this.getMotherlinevoltage().equals(other.getMotherlinevoltage()))
            && (this.getGroudvoltage() == null ? other.getGroudvoltage() == null : this.getGroudvoltage().equals(other.getGroudvoltage()))
            && (this.getNgroudvoltage() == null ? other.getNgroudvoltage() == null : this.getNgroudvoltage().equals(other.getNgroudvoltage()))
            && (this.getDesignpower() == null ? other.getDesignpower() == null : this.getDesignpower().equals(other.getDesignpower()))
            && (this.getVoltageab() == null ? other.getVoltageab() == null : this.getVoltageab().equals(other.getVoltageab()))
            && (this.getVoltagebc() == null ? other.getVoltagebc() == null : this.getVoltagebc().equals(other.getVoltagebc()))
            && (this.getVoltageca() == null ? other.getVoltageca() == null : this.getVoltageca().equals(other.getVoltageca()))
            && (this.getSystemtime() == null ? other.getSystemtime() == null : this.getSystemtime().equals(other.getSystemtime()))
            && (this.getEfficiency() == null ? other.getEfficiency() == null : this.getEfficiency().equals(other.getEfficiency()))
            && (this.getPoweronsecond() == null ? other.getPoweronsecond() == null : this.getPoweronsecond().equals(other.getPoweronsecond()))
            && (this.getPoweroffsecond() == null ? other.getPoweroffsecond() == null : this.getPoweroffsecond().equals(other.getPoweroffsecond()))
            && (this.getMppt1() == null ? other.getMppt1() == null : this.getMppt1().equals(other.getMppt1()))
            && (this.getMppt2() == null ? other.getMppt2() == null : this.getMppt2().equals(other.getMppt2()))
            && (this.getMppt3() == null ? other.getMppt3() == null : this.getMppt3().equals(other.getMppt3()))
            && (this.getMppt4() == null ? other.getMppt4() == null : this.getMppt4().equals(other.getMppt4()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getIid() == null) ? 0 : getIid().hashCode());
        result = prime * result + ((getClltidday() == null) ? 0 : getClltidday().hashCode());
        result = prime * result + ((getClltdaytime() == null) ? 0 : getClltdaytime().hashCode());
        result = prime * result + ((getDeviceindex() == null) ? 0 : getDeviceindex().hashCode());
        result = prime * result + ((getDaytimes() == null) ? 0 : getDaytimes().hashCode());
        result = prime * result + ((getRegionid() == null) ? 0 : getRegionid().hashCode());
        result = prime * result + ((getProjectid() == null) ? 0 : getProjectid().hashCode());
        result = prime * result + ((getSectionid() == null) ? 0 : getSectionid().hashCode());
        result = prime * result + ((getCollectid() == null) ? 0 : getCollectid().hashCode());
        result = prime * result + ((getComid() == null) ? 0 : getComid().hashCode());
        result = prime * result + ((getAddress() == null) ? 0 : getAddress().hashCode());
        result = prime * result + ((getSn() == null) ? 0 : getSn().hashCode());
        result = prime * result + ((getStatusint() == null) ? 0 : getStatusint().hashCode());
        result = prime * result + ((getStatusdesc() == null) ? 0 : getStatusdesc().hashCode());
        result = prime * result + ((getPvvoltage() == null) ? 0 : getPvvoltage().hashCode());
        result = prime * result + ((getPvcurrent() == null) ? 0 : getPvcurrent().hashCode());
        result = prime * result + ((getPvoutputpower() == null) ? 0 : getPvoutputpower().hashCode());
        result = prime * result + ((getPvvoltageb() == null) ? 0 : getPvvoltageb().hashCode());
        result = prime * result + ((getPvcurrentb() == null) ? 0 : getPvcurrentb().hashCode());
        result = prime * result + ((getPvoutputpowerb() == null) ? 0 : getPvoutputpowerb().hashCode());
        result = prime * result + ((getPvvoltagec() == null) ? 0 : getPvvoltagec().hashCode());
        result = prime * result + ((getPvcurrentc() == null) ? 0 : getPvcurrentc().hashCode());
        result = prime * result + ((getPvoutputpowerc() == null) ? 0 : getPvoutputpowerc().hashCode());
        result = prime * result + ((getPvtotaloutputpower() == null) ? 0 : getPvtotaloutputpower().hashCode());
        result = prime * result + ((getVoltagea() == null) ? 0 : getVoltagea().hashCode());
        result = prime * result + ((getCurrenta() == null) ? 0 : getCurrenta().hashCode());
        result = prime * result + ((getFrequencya() == null) ? 0 : getFrequencya().hashCode());
        result = prime * result + ((getOutputpowera() == null) ? 0 : getOutputpowera().hashCode());
        result = prime * result + ((getRoutputpowera() == null) ? 0 : getRoutputpowera().hashCode());
        result = prime * result + ((getPowerfactora() == null) ? 0 : getPowerfactora().hashCode());
        result = prime * result + ((getVoltageb() == null) ? 0 : getVoltageb().hashCode());
        result = prime * result + ((getCurrentb() == null) ? 0 : getCurrentb().hashCode());
        result = prime * result + ((getFrequencyb() == null) ? 0 : getFrequencyb().hashCode());
        result = prime * result + ((getOutputpowerb() == null) ? 0 : getOutputpowerb().hashCode());
        result = prime * result + ((getRoutputpowerb() == null) ? 0 : getRoutputpowerb().hashCode());
        result = prime * result + ((getPowerfactorb() == null) ? 0 : getPowerfactorb().hashCode());
        result = prime * result + ((getVoltagec() == null) ? 0 : getVoltagec().hashCode());
        result = prime * result + ((getCurrentc() == null) ? 0 : getCurrentc().hashCode());
        result = prime * result + ((getFrequencyc() == null) ? 0 : getFrequencyc().hashCode());
        result = prime * result + ((getOutputpowerc() == null) ? 0 : getOutputpowerc().hashCode());
        result = prime * result + ((getRoutputpowerc() == null) ? 0 : getRoutputpowerc().hashCode());
        result = prime * result + ((getPowerfactorc() == null) ? 0 : getPowerfactorc().hashCode());
        result = prime * result + ((getTotaloutputpower() == null) ? 0 : getTotaloutputpower().hashCode());
        result = prime * result + ((getRtotaloutputpower() == null) ? 0 : getRtotaloutputpower().hashCode());
        result = prime * result + ((getTotalpowerfactor() == null) ? 0 : getTotalpowerfactor().hashCode());
        result = prime * result + ((getTodayenergy() == null) ? 0 : getTodayenergy().hashCode());
        result = prime * result + ((getTotalenergy() == null) ? 0 : getTotalenergy().hashCode());
        result = prime * result + ((getNtodayenergy() == null) ? 0 : getNtodayenergy().hashCode());
        result = prime * result + ((getNtotalenergy() == null) ? 0 : getNtotalenergy().hashCode());
        result = prime * result + ((getRtodayenergy() == null) ? 0 : getRtodayenergy().hashCode());
        result = prime * result + ((getRtotalenergy() == null) ? 0 : getRtotalenergy().hashCode());
        result = prime * result + ((getNrtodayenergy() == null) ? 0 : getNrtodayenergy().hashCode());
        result = prime * result + ((getNrtotalenergy() == null) ? 0 : getNrtotalenergy().hashCode());
        result = prime * result + ((getBtryvoltage() == null) ? 0 : getBtryvoltage().hashCode());
        result = prime * result + ((getBtrycurrent() == null) ? 0 : getBtrycurrent().hashCode());
        result = prime * result + ((getBtryoutputpower() == null) ? 0 : getBtryoutputpower().hashCode());
        result = prime * result + ((getTemperatureinverter() == null) ? 0 : getTemperatureinverter().hashCode());
        result = prime * result + ((getTemperaturebooster() == null) ? 0 : getTemperaturebooster().hashCode());
        result = prime * result + ((getTemperaturesink() == null) ? 0 : getTemperaturesink().hashCode());
        result = prime * result + ((getTodayhour() == null) ? 0 : getTodayhour().hashCode());
        result = prime * result + ((getTotalhour() == null) ? 0 : getTotalhour().hashCode());
        result = prime * result + ((getAgcsetting() == null) ? 0 : getAgcsetting().hashCode());
        result = prime * result + ((getAvcsetting() == null) ? 0 : getAvcsetting().hashCode());
        result = prime * result + ((getGroundresistance() == null) ? 0 : getGroundresistance().hashCode());
        result = prime * result + ((getLeakcurrent() == null) ? 0 : getLeakcurrent().hashCode());
        result = prime * result + ((getStringvoltage1() == null) ? 0 : getStringvoltage1().hashCode());
        result = prime * result + ((getStringcurrent1() == null) ? 0 : getStringcurrent1().hashCode());
        result = prime * result + ((getStringvoltage2() == null) ? 0 : getStringvoltage2().hashCode());
        result = prime * result + ((getStringcurrent2() == null) ? 0 : getStringcurrent2().hashCode());
        result = prime * result + ((getStringvoltage3() == null) ? 0 : getStringvoltage3().hashCode());
        result = prime * result + ((getStringcurrent3() == null) ? 0 : getStringcurrent3().hashCode());
        result = prime * result + ((getStringvoltage4() == null) ? 0 : getStringvoltage4().hashCode());
        result = prime * result + ((getStringcurrent4() == null) ? 0 : getStringcurrent4().hashCode());
        result = prime * result + ((getStringvoltage5() == null) ? 0 : getStringvoltage5().hashCode());
        result = prime * result + ((getStringcurrent5() == null) ? 0 : getStringcurrent5().hashCode());
        result = prime * result + ((getStringvoltage6() == null) ? 0 : getStringvoltage6().hashCode());
        result = prime * result + ((getStringcurrent6() == null) ? 0 : getStringcurrent6().hashCode());
        result = prime * result + ((getStringvoltage7() == null) ? 0 : getStringvoltage7().hashCode());
        result = prime * result + ((getStringcurrent7() == null) ? 0 : getStringcurrent7().hashCode());
        result = prime * result + ((getStringvoltage8() == null) ? 0 : getStringvoltage8().hashCode());
        result = prime * result + ((getStringcurrent8() == null) ? 0 : getStringcurrent8().hashCode());
        result = prime * result + ((getStringvoltage9() == null) ? 0 : getStringvoltage9().hashCode());
        result = prime * result + ((getStringcurrent9() == null) ? 0 : getStringcurrent9().hashCode());
        result = prime * result + ((getStringvoltage10() == null) ? 0 : getStringvoltage10().hashCode());
        result = prime * result + ((getStringcurrent10() == null) ? 0 : getStringcurrent10().hashCode());
        result = prime * result + ((getStringvoltage11() == null) ? 0 : getStringvoltage11().hashCode());
        result = prime * result + ((getStringcurrent11() == null) ? 0 : getStringcurrent11().hashCode());
        result = prime * result + ((getStringvoltage12() == null) ? 0 : getStringvoltage12().hashCode());
        result = prime * result + ((getStringcurrent12() == null) ? 0 : getStringcurrent12().hashCode());
        result = prime * result + ((getStringvoltage13() == null) ? 0 : getStringvoltage13().hashCode());
        result = prime * result + ((getStringcurrent13() == null) ? 0 : getStringcurrent13().hashCode());
        result = prime * result + ((getStringvoltage14() == null) ? 0 : getStringvoltage14().hashCode());
        result = prime * result + ((getStringcurrent14() == null) ? 0 : getStringcurrent14().hashCode());
        result = prime * result + ((getStringvoltage15() == null) ? 0 : getStringvoltage15().hashCode());
        result = prime * result + ((getStringcurrent15() == null) ? 0 : getStringcurrent15().hashCode());
        result = prime * result + ((getStringvoltage16() == null) ? 0 : getStringvoltage16().hashCode());
        result = prime * result + ((getStringcurrent16() == null) ? 0 : getStringcurrent16().hashCode());
        result = prime * result + ((getStringvoltage17() == null) ? 0 : getStringvoltage17().hashCode());
        result = prime * result + ((getStringcurrent17() == null) ? 0 : getStringcurrent17().hashCode());
        result = prime * result + ((getStringvoltage18() == null) ? 0 : getStringvoltage18().hashCode());
        result = prime * result + ((getStringcurrent18() == null) ? 0 : getStringcurrent18().hashCode());
        result = prime * result + ((getStringvoltage19() == null) ? 0 : getStringvoltage19().hashCode());
        result = prime * result + ((getStringcurrent19() == null) ? 0 : getStringcurrent19().hashCode());
        result = prime * result + ((getStringvoltage20() == null) ? 0 : getStringvoltage20().hashCode());
        result = prime * result + ((getStringcurrent20() == null) ? 0 : getStringcurrent20().hashCode());
        result = prime * result + ((getStringvoltage21() == null) ? 0 : getStringvoltage21().hashCode());
        result = prime * result + ((getStringcurrent21() == null) ? 0 : getStringcurrent21().hashCode());
        result = prime * result + ((getStringvoltage22() == null) ? 0 : getStringvoltage22().hashCode());
        result = prime * result + ((getStringcurrent22() == null) ? 0 : getStringcurrent22().hashCode());
        result = prime * result + ((getStringvoltage23() == null) ? 0 : getStringvoltage23().hashCode());
        result = prime * result + ((getStringcurrent23() == null) ? 0 : getStringcurrent23().hashCode());
        result = prime * result + ((getStringvoltage24() == null) ? 0 : getStringvoltage24().hashCode());
        result = prime * result + ((getStringcurrent24() == null) ? 0 : getStringcurrent24().hashCode());
        result = prime * result + ((getMotherlinevoltage() == null) ? 0 : getMotherlinevoltage().hashCode());
        result = prime * result + ((getGroudvoltage() == null) ? 0 : getGroudvoltage().hashCode());
        result = prime * result + ((getNgroudvoltage() == null) ? 0 : getNgroudvoltage().hashCode());
        result = prime * result + ((getDesignpower() == null) ? 0 : getDesignpower().hashCode());
        result = prime * result + ((getVoltageab() == null) ? 0 : getVoltageab().hashCode());
        result = prime * result + ((getVoltagebc() == null) ? 0 : getVoltagebc().hashCode());
        result = prime * result + ((getVoltageca() == null) ? 0 : getVoltageca().hashCode());
        result = prime * result + ((getSystemtime() == null) ? 0 : getSystemtime().hashCode());
        result = prime * result + ((getEfficiency() == null) ? 0 : getEfficiency().hashCode());
        result = prime * result + ((getPoweronsecond() == null) ? 0 : getPoweronsecond().hashCode());
        result = prime * result + ((getPoweroffsecond() == null) ? 0 : getPoweroffsecond().hashCode());
        result = prime * result + ((getMppt1() == null) ? 0 : getMppt1().hashCode());
        result = prime * result + ((getMppt2() == null) ? 0 : getMppt2().hashCode());
        result = prime * result + ((getMppt3() == null) ? 0 : getMppt3().hashCode());
        result = prime * result + ((getMppt4() == null) ? 0 : getMppt4().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", iid=").append(iid);
        sb.append(", clltidday=").append(clltidday);
        sb.append(", clltdaytime=").append(clltdaytime);
        sb.append(", deviceindex=").append(deviceindex);
        sb.append(", daytimes=").append(daytimes);
        sb.append(", regionid=").append(regionid);
        sb.append(", projectid=").append(projectid);
        sb.append(", sectionid=").append(sectionid);
        sb.append(", collectid=").append(collectid);
        sb.append(", comid=").append(comid);
        sb.append(", address=").append(address);
        sb.append(", sn=").append(sn);
        sb.append(", statusint=").append(statusint);
        sb.append(", statusdesc=").append(statusdesc);
        sb.append(", pvvoltage=").append(pvvoltage);
        sb.append(", pvcurrent=").append(pvcurrent);
        sb.append(", pvoutputpower=").append(pvoutputpower);
        sb.append(", pvvoltageb=").append(pvvoltageb);
        sb.append(", pvcurrentb=").append(pvcurrentb);
        sb.append(", pvoutputpowerb=").append(pvoutputpowerb);
        sb.append(", pvvoltagec=").append(pvvoltagec);
        sb.append(", pvcurrentc=").append(pvcurrentc);
        sb.append(", pvoutputpowerc=").append(pvoutputpowerc);
        sb.append(", pvtotaloutputpower=").append(pvtotaloutputpower);
        sb.append(", voltagea=").append(voltagea);
        sb.append(", currenta=").append(currenta);
        sb.append(", frequencya=").append(frequencya);
        sb.append(", outputpowera=").append(outputpowera);
        sb.append(", routputpowera=").append(routputpowera);
        sb.append(", powerfactora=").append(powerfactora);
        sb.append(", voltageb=").append(voltageb);
        sb.append(", currentb=").append(currentb);
        sb.append(", frequencyb=").append(frequencyb);
        sb.append(", outputpowerb=").append(outputpowerb);
        sb.append(", routputpowerb=").append(routputpowerb);
        sb.append(", powerfactorb=").append(powerfactorb);
        sb.append(", voltagec=").append(voltagec);
        sb.append(", currentc=").append(currentc);
        sb.append(", frequencyc=").append(frequencyc);
        sb.append(", outputpowerc=").append(outputpowerc);
        sb.append(", routputpowerc=").append(routputpowerc);
        sb.append(", powerfactorc=").append(powerfactorc);
        sb.append(", totaloutputpower=").append(totaloutputpower);
        sb.append(", rtotaloutputpower=").append(rtotaloutputpower);
        sb.append(", totalpowerfactor=").append(totalpowerfactor);
        sb.append(", todayenergy=").append(todayenergy);
        sb.append(", totalenergy=").append(totalenergy);
        sb.append(", ntodayenergy=").append(ntodayenergy);
        sb.append(", ntotalenergy=").append(ntotalenergy);
        sb.append(", rtodayenergy=").append(rtodayenergy);
        sb.append(", rtotalenergy=").append(rtotalenergy);
        sb.append(", nrtodayenergy=").append(nrtodayenergy);
        sb.append(", nrtotalenergy=").append(nrtotalenergy);
        sb.append(", btryvoltage=").append(btryvoltage);
        sb.append(", btrycurrent=").append(btrycurrent);
        sb.append(", btryoutputpower=").append(btryoutputpower);
        sb.append(", temperatureinverter=").append(temperatureinverter);
        sb.append(", temperaturebooster=").append(temperaturebooster);
        sb.append(", temperaturesink=").append(temperaturesink);
        sb.append(", todayhour=").append(todayhour);
        sb.append(", totalhour=").append(totalhour);
        sb.append(", agcsetting=").append(agcsetting);
        sb.append(", avcsetting=").append(avcsetting);
        sb.append(", groundresistance=").append(groundresistance);
        sb.append(", leakcurrent=").append(leakcurrent);
        sb.append(", stringvoltage1=").append(stringvoltage1);
        sb.append(", stringcurrent1=").append(stringcurrent1);
        sb.append(", stringvoltage2=").append(stringvoltage2);
        sb.append(", stringcurrent2=").append(stringcurrent2);
        sb.append(", stringvoltage3=").append(stringvoltage3);
        sb.append(", stringcurrent3=").append(stringcurrent3);
        sb.append(", stringvoltage4=").append(stringvoltage4);
        sb.append(", stringcurrent4=").append(stringcurrent4);
        sb.append(", stringvoltage5=").append(stringvoltage5);
        sb.append(", stringcurrent5=").append(stringcurrent5);
        sb.append(", stringvoltage6=").append(stringvoltage6);
        sb.append(", stringcurrent6=").append(stringcurrent6);
        sb.append(", stringvoltage7=").append(stringvoltage7);
        sb.append(", stringcurrent7=").append(stringcurrent7);
        sb.append(", stringvoltage8=").append(stringvoltage8);
        sb.append(", stringcurrent8=").append(stringcurrent8);
        sb.append(", stringvoltage9=").append(stringvoltage9);
        sb.append(", stringcurrent9=").append(stringcurrent9);
        sb.append(", stringvoltage10=").append(stringvoltage10);
        sb.append(", stringcurrent10=").append(stringcurrent10);
        sb.append(", stringvoltage11=").append(stringvoltage11);
        sb.append(", stringcurrent11=").append(stringcurrent11);
        sb.append(", stringvoltage12=").append(stringvoltage12);
        sb.append(", stringcurrent12=").append(stringcurrent12);
        sb.append(", stringvoltage13=").append(stringvoltage13);
        sb.append(", stringcurrent13=").append(stringcurrent13);
        sb.append(", stringvoltage14=").append(stringvoltage14);
        sb.append(", stringcurrent14=").append(stringcurrent14);
        sb.append(", stringvoltage15=").append(stringvoltage15);
        sb.append(", stringcurrent15=").append(stringcurrent15);
        sb.append(", stringvoltage16=").append(stringvoltage16);
        sb.append(", stringcurrent16=").append(stringcurrent16);
        sb.append(", stringvoltage17=").append(stringvoltage17);
        sb.append(", stringcurrent17=").append(stringcurrent17);
        sb.append(", stringvoltage18=").append(stringvoltage18);
        sb.append(", stringcurrent18=").append(stringcurrent18);
        sb.append(", stringvoltage19=").append(stringvoltage19);
        sb.append(", stringcurrent19=").append(stringcurrent19);
        sb.append(", stringvoltage20=").append(stringvoltage20);
        sb.append(", stringcurrent20=").append(stringcurrent20);
        sb.append(", stringvoltage21=").append(stringvoltage21);
        sb.append(", stringcurrent21=").append(stringcurrent21);
        sb.append(", stringvoltage22=").append(stringvoltage22);
        sb.append(", stringcurrent22=").append(stringcurrent22);
        sb.append(", stringvoltage23=").append(stringvoltage23);
        sb.append(", stringcurrent23=").append(stringcurrent23);
        sb.append(", stringvoltage24=").append(stringvoltage24);
        sb.append(", stringcurrent24=").append(stringcurrent24);
        sb.append(", motherlinevoltage=").append(motherlinevoltage);
        sb.append(", groudvoltage=").append(groudvoltage);
        sb.append(", ngroudvoltage=").append(ngroudvoltage);
        sb.append(", designpower=").append(designpower);
        sb.append(", voltageab=").append(voltageab);
        sb.append(", voltagebc=").append(voltagebc);
        sb.append(", voltageca=").append(voltageca);
        sb.append(", systemtime=").append(systemtime);
        sb.append(", efficiency=").append(efficiency);
        sb.append(", poweronsecond=").append(poweronsecond);
        sb.append(", poweroffsecond=").append(poweroffsecond);
        sb.append(", mppt1=").append(mppt1);
        sb.append(", mppt2=").append(mppt2);
        sb.append(", mppt3=").append(mppt3);
        sb.append(", mppt4=").append(mppt4);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}
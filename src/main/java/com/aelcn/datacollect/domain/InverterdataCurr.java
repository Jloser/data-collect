package com.aelcn.datacollect.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * 
 * @TableName InverterData_Curr
 */
@TableName(value ="InverterData_Curr")
@Getter
@Setter
public class InverterdataCurr implements Serializable {
    /**
     *
     */
    @TableId
    private Long iid;

    /**
     *
     */
    private Integer clltidday;

    /**
     *
     */
    private Date clltdaytime;

    /**
     *
     */
    private Integer deviceindex;

    /**
     *
     */
    private Integer daytimes;

    /**
     *
     */
    private Integer regionid;

    /**
     *
     */
    private Integer projectid;

    /**
     *
     */
    private Integer sectionid;

    /**
     *
     */
    private Integer collectid;

    /**
     *
     */
    private Integer comid;

    /**
     *
     */
    private Integer address;

    /**
     *
     */
    private String sn;

    /**
     *
     */
    private Integer statusint;

    /**
     *
     */
    private String statusdesc;

    /**
     *
     */
    private Double pvvoltage;

    /**
     *
     */
    private Double pvcurrent;

    /**
     *
     */
    private Double pvoutputpower;

    /**
     *
     */
    private Double pvvoltageb;

    /**
     *
     */
    private Double pvcurrentb;

    /**
     *
     */
    private Double pvoutputpowerb;

    /**
     *
     */
    private Double pvvoltagec;

    /**
     *
     */
    private Double pvcurrentc;

    /**
     *
     */
    private Double pvoutputpowerc;

    /**
     *
     */
    private Double pvtotaloutputpower;

    /**
     *
     */
    private Double voltagea;

    /**
     *
     */
    private Double currenta;

    /**
     *
     */
    private Double frequencya;

    /**
     *
     */
    private Double outputpowera;

    /**
     *
     */
    private Double routputpowera;

    /**
     *
     */
    private Double powerfactora;

    /**
     *
     */
    private Double voltageb;

    /**
     *
     */
    private Double currentb;

    /**
     *
     */
    private Double frequencyb;

    /**
     *
     */
    private Double outputpowerb;

    /**
     *
     */
    private Double routputpowerb;

    /**
     *
     */
    private Double powerfactorb;

    /**
     *
     */
    private Double voltagec;

    /**
     *
     */
    private Double currentc;

    /**
     *
     */
    private Double frequencyc;

    /**
     *
     */
    private Double outputpowerc;

    /**
     *
     */
    private Double routputpowerc;

    /**
     *
     */
    private Double powerfactorc;

    /**
     *
     */
    private Double totaloutputpower;

    /**
     *
     */
    private Double rtotaloutputpower;

    /**
     *
     */
    private Double totalpowerfactor;

    /**
     *
     */
    private Double todayenergy;

    /**
     *
     */
    private Double totalenergy;

    /**
     *
     */
    private Double ntodayenergy;

    /**
     *
     */
    private Double ntotalenergy;

    /**
     *
     */
    private Double rtodayenergy;

    /**
     *
     */
    private Double rtotalenergy;

    /**
     *
     */
    private Double nrtodayenergy;

    /**
     *
     */
    private Double nrtotalenergy;

    /**
     *
     */
    private Double btryvoltage;

    /**
     *
     */
    private Double btrycurrent;

    /**
     *
     */
    private Double btryoutputpower;

    /**
     *
     */
    private Double temperatureinverter;

    /**
     *
     */
    private Double temperaturebooster;

    /**
     *
     */
    private Double temperaturesink;

    /**
     *
     */
    private Double todayhour;

    /**
     *
     */
    private Double totalhour;

    /**
     *
     */
    private Double agcsetting;

    /**
     *
     */
    private Double avcsetting;

    /**
     *
     */
    private Double groundresistance;

    /**
     *
     */
    private Double leakcurrent;

    /**
     *
     */
    private Double stringvoltage1;

    /**
     *
     */
    private Double stringcurrent1;

    /**
     *
     */
    private Double stringvoltage2;

    /**
     *
     */
    private Double stringcurrent2;

    /**
     *
     */
    private Double stringvoltage3;

    /**
     *
     */
    private Double stringcurrent3;

    /**
     *
     */
    private Double stringvoltage4;

    /**
     *
     */
    private Double stringcurrent4;

    /**
     *
     */
    private Double stringvoltage5;

    /**
     *
     */
    private Double stringcurrent5;

    /**
     *
     */
    private Double stringvoltage6;

    /**
     *
     */
    private Double stringcurrent6;

    /**
     *
     */
    private Double stringvoltage7;

    /**
     *
     */
    private Double stringcurrent7;

    /**
     *
     */
    private Double stringvoltage8;

    /**
     *
     */
    private Double stringcurrent8;

    /**
     *
     */
    private Double stringvoltage9;

    /**
     *
     */
    private Double stringcurrent9;

    /**
     *
     */
    private Double stringvoltage10;

    /**
     *
     */
    private Double stringcurrent10;

    /**
     *
     */
    private Double stringvoltage11;

    /**
     *
     */
    private Double stringcurrent11;

    /**
     *
     */
    private Double stringvoltage12;

    /**
     *
     */
    private Double stringcurrent12;

    /**
     *
     */
    private Double stringvoltage13;

    /**
     *
     */
    private Double stringcurrent13;

    /**
     *
     */
    private Double stringvoltage14;

    /**
     *
     */
    private Double stringcurrent14;

    /**
     *
     */
    private Double stringvoltage15;

    /**
     *
     */
    private Double stringcurrent15;

    /**
     *
     */
    private Double stringvoltage16;

    /**
     *
     */
    private Double stringcurrent16;

    /**
     *
     */
    private Double stringvoltage17;

    /**
     *
     */
    private Double stringcurrent17;

    /**
     *
     */
    private Double stringvoltage18;

    /**
     *
     */
    private Double stringcurrent18;

    /**
     *
     */
    private Double stringvoltage19;

    /**
     *
     */
    private Double stringcurrent19;

    /**
     *
     */
    private Double stringvoltage20;

    /**
     *
     */
    private Double stringcurrent20;

    /**
     *
     */
    private Double stringvoltage21;

    /**
     *
     */
    private Double stringcurrent21;

    /**
     *
     */
    private Double stringvoltage22;

    /**
     *
     */
    private Double stringcurrent22;

    /**
     *
     */
    private Double stringvoltage23;

    /**
     *
     */
    private Double stringcurrent23;

    /**
     *
     */
    private Double stringvoltage24;

    /**
     *
     */
    private Double stringcurrent24;

    /**
     *
     */
    private Double motherlinevoltage;

    /**
     *
     */
    private Double groudvoltage;

    /**
     *
     */
    private Double ngroudvoltage;

    /**
     *
     */
    private Double designpower;

    /**
     *
     */
    private Double voltageab;

    /**
     *
     */
    private Double voltagebc;

    /**
     *
     */
    private Double voltageca;

    /**
     *
     */
    private Integer systemtime;

    /**
     *
     */
    private Integer efficiency;

    /**
     *
     */
    private Integer poweronsecond;

    /**
     *
     */
    private Integer poweroffsecond;

    /**
     *
     */
    private Double mppt1;

    /**
     *
     */
    private Double mppt2;

    /**
     *
     */
    private Double mppt3;

    /**
     *
     */
    private Double mppt4;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", iid=").append(iid);
        sb.append(", clltidday=").append(clltidday);
        sb.append(", clltdaytime=").append(clltdaytime);
        sb.append(", deviceindex=").append(deviceindex);
        sb.append(", daytimes=").append(daytimes);
        sb.append(", regionid=").append(regionid);
        sb.append(", projectid=").append(projectid);
        sb.append(", sectionid=").append(sectionid);
        sb.append(", collectid=").append(collectid);
        sb.append(", comid=").append(comid);
        sb.append(", address=").append(address);
        sb.append(", sn=").append(sn);
        sb.append(", statusint=").append(statusint);
        sb.append(", statusdesc=").append(statusdesc);
        sb.append(", pvvoltage=").append(pvvoltage);
        sb.append(", pvcurrent=").append(pvcurrent);
        sb.append(", pvoutputpower=").append(pvoutputpower);
        sb.append(", pvvoltageb=").append(pvvoltageb);
        sb.append(", pvcurrentb=").append(pvcurrentb);
        sb.append(", pvoutputpowerb=").append(pvoutputpowerb);
        sb.append(", pvvoltagec=").append(pvvoltagec);
        sb.append(", pvcurrentc=").append(pvcurrentc);
        sb.append(", pvoutputpowerc=").append(pvoutputpowerc);
        sb.append(", pvtotaloutputpower=").append(pvtotaloutputpower);
        sb.append(", voltagea=").append(voltagea);
        sb.append(", currenta=").append(currenta);
        sb.append(", frequencya=").append(frequencya);
        sb.append(", outputpowera=").append(outputpowera);
        sb.append(", routputpowera=").append(routputpowera);
        sb.append(", powerfactora=").append(powerfactora);
        sb.append(", voltageb=").append(voltageb);
        sb.append(", currentb=").append(currentb);
        sb.append(", frequencyb=").append(frequencyb);
        sb.append(", outputpowerb=").append(outputpowerb);
        sb.append(", routputpowerb=").append(routputpowerb);
        sb.append(", powerfactorb=").append(powerfactorb);
        sb.append(", voltagec=").append(voltagec);
        sb.append(", currentc=").append(currentc);
        sb.append(", frequencyc=").append(frequencyc);
        sb.append(", outputpowerc=").append(outputpowerc);
        sb.append(", routputpowerc=").append(routputpowerc);
        sb.append(", powerfactorc=").append(powerfactorc);
        sb.append(", totaloutputpower=").append(totaloutputpower);
        sb.append(", rtotaloutputpower=").append(rtotaloutputpower);
        sb.append(", totalpowerfactor=").append(totalpowerfactor);
        sb.append(", todayenergy=").append(todayenergy);
        sb.append(", totalenergy=").append(totalenergy);
        sb.append(", ntodayenergy=").append(ntodayenergy);
        sb.append(", ntotalenergy=").append(ntotalenergy);
        sb.append(", rtodayenergy=").append(rtodayenergy);
        sb.append(", rtotalenergy=").append(rtotalenergy);
        sb.append(", nrtodayenergy=").append(nrtodayenergy);
        sb.append(", nrtotalenergy=").append(nrtotalenergy);
        sb.append(", btryvoltage=").append(btryvoltage);
        sb.append(", btrycurrent=").append(btrycurrent);
        sb.append(", btryoutputpower=").append(btryoutputpower);
        sb.append(", temperatureinverter=").append(temperatureinverter);
        sb.append(", temperaturebooster=").append(temperaturebooster);
        sb.append(", temperaturesink=").append(temperaturesink);
        sb.append(", todayhour=").append(todayhour);
        sb.append(", totalhour=").append(totalhour);
        sb.append(", agcsetting=").append(agcsetting);
        sb.append(", avcsetting=").append(avcsetting);
        sb.append(", groundresistance=").append(groundresistance);
        sb.append(", leakcurrent=").append(leakcurrent);
        sb.append(", stringvoltage1=").append(stringvoltage1);
        sb.append(", stringcurrent1=").append(stringcurrent1);
        sb.append(", stringvoltage2=").append(stringvoltage2);
        sb.append(", stringcurrent2=").append(stringcurrent2);
        sb.append(", stringvoltage3=").append(stringvoltage3);
        sb.append(", stringcurrent3=").append(stringcurrent3);
        sb.append(", stringvoltage4=").append(stringvoltage4);
        sb.append(", stringcurrent4=").append(stringcurrent4);
        sb.append(", stringvoltage5=").append(stringvoltage5);
        sb.append(", stringcurrent5=").append(stringcurrent5);
        sb.append(", stringvoltage6=").append(stringvoltage6);
        sb.append(", stringcurrent6=").append(stringcurrent6);
        sb.append(", stringvoltage7=").append(stringvoltage7);
        sb.append(", stringcurrent7=").append(stringcurrent7);
        sb.append(", stringvoltage8=").append(stringvoltage8);
        sb.append(", stringcurrent8=").append(stringcurrent8);
        sb.append(", stringvoltage9=").append(stringvoltage9);
        sb.append(", stringcurrent9=").append(stringcurrent9);
        sb.append(", stringvoltage10=").append(stringvoltage10);
        sb.append(", stringcurrent10=").append(stringcurrent10);
        sb.append(", stringvoltage11=").append(stringvoltage11);
        sb.append(", stringcurrent11=").append(stringcurrent11);
        sb.append(", stringvoltage12=").append(stringvoltage12);
        sb.append(", stringcurrent12=").append(stringcurrent12);
        sb.append(", stringvoltage13=").append(stringvoltage13);
        sb.append(", stringcurrent13=").append(stringcurrent13);
        sb.append(", stringvoltage14=").append(stringvoltage14);
        sb.append(", stringcurrent14=").append(stringcurrent14);
        sb.append(", stringvoltage15=").append(stringvoltage15);
        sb.append(", stringcurrent15=").append(stringcurrent15);
        sb.append(", stringvoltage16=").append(stringvoltage16);
        sb.append(", stringcurrent16=").append(stringcurrent16);
        sb.append(", stringvoltage17=").append(stringvoltage17);
        sb.append(", stringcurrent17=").append(stringcurrent17);
        sb.append(", stringvoltage18=").append(stringvoltage18);
        sb.append(", stringcurrent18=").append(stringcurrent18);
        sb.append(", stringvoltage19=").append(stringvoltage19);
        sb.append(", stringcurrent19=").append(stringcurrent19);
        sb.append(", stringvoltage20=").append(stringvoltage20);
        sb.append(", stringcurrent20=").append(stringcurrent20);
        sb.append(", stringvoltage21=").append(stringvoltage21);
        sb.append(", stringcurrent21=").append(stringcurrent21);
        sb.append(", stringvoltage22=").append(stringvoltage22);
        sb.append(", stringcurrent22=").append(stringcurrent22);
        sb.append(", stringvoltage23=").append(stringvoltage23);
        sb.append(", stringcurrent23=").append(stringcurrent23);
        sb.append(", stringvoltage24=").append(stringvoltage24);
        sb.append(", stringcurrent24=").append(stringcurrent24);
        sb.append(", motherlinevoltage=").append(motherlinevoltage);
        sb.append(", groudvoltage=").append(groudvoltage);
        sb.append(", ngroudvoltage=").append(ngroudvoltage);
        sb.append(", designpower=").append(designpower);
        sb.append(", voltageab=").append(voltageab);
        sb.append(", voltagebc=").append(voltagebc);
        sb.append(", voltageca=").append(voltageca);
        sb.append(", systemtime=").append(systemtime);
        sb.append(", efficiency=").append(efficiency);
        sb.append(", poweronsecond=").append(poweronsecond);
        sb.append(", poweroffsecond=").append(poweroffsecond);
        sb.append(", mppt1=").append(mppt1);
        sb.append(", mppt2=").append(mppt2);
        sb.append(", mppt3=").append(mppt3);
        sb.append(", mppt4=").append(mppt4);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}
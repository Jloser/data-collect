package com.aelcn.datacollect.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 * 
 * @TableName InverterRpt_CurrYear
 */
@TableName(value ="InverterRpt_CurrYear")
@Data
public class InverterrptCurryear implements Serializable {
    /**
     * 
     */
    @TableId
    private Long iid;

    /**
     * 
     */
    private Integer clltidday;

    /**
     * 
     */
    private Integer deviceindex;

    /**
     * 
     */
    private Integer clltidyear;

    /**
     * 
     */
    private Integer clltidmonth;

    /**
     * 
     */
    private Integer clltiddayindex;

    /**
     * 
     */
    private Integer regionid;

    /**
     * 
     */
    private Integer projectid;

    /**
     * 
     */
    private Integer sectionid;

    /**
     * 
     */
    private Integer collectid;

    /**
     * 
     */
    private Integer comid;

    /**
     * 
     */
    private Integer address;

    /**
     * 
     */
    private Double todayenergy;

    /**
     * 
     */
    private Double totalenergy;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        InverterrptCurryear other = (InverterrptCurryear) that;
        return (this.getIid() == null ? other.getIid() == null : this.getIid().equals(other.getIid()))
            && (this.getClltidday() == null ? other.getClltidday() == null : this.getClltidday().equals(other.getClltidday()))
            && (this.getDeviceindex() == null ? other.getDeviceindex() == null : this.getDeviceindex().equals(other.getDeviceindex()))
            && (this.getClltidyear() == null ? other.getClltidyear() == null : this.getClltidyear().equals(other.getClltidyear()))
            && (this.getClltidmonth() == null ? other.getClltidmonth() == null : this.getClltidmonth().equals(other.getClltidmonth()))
            && (this.getClltiddayindex() == null ? other.getClltiddayindex() == null : this.getClltiddayindex().equals(other.getClltiddayindex()))
            && (this.getRegionid() == null ? other.getRegionid() == null : this.getRegionid().equals(other.getRegionid()))
            && (this.getProjectid() == null ? other.getProjectid() == null : this.getProjectid().equals(other.getProjectid()))
            && (this.getSectionid() == null ? other.getSectionid() == null : this.getSectionid().equals(other.getSectionid()))
            && (this.getCollectid() == null ? other.getCollectid() == null : this.getCollectid().equals(other.getCollectid()))
            && (this.getComid() == null ? other.getComid() == null : this.getComid().equals(other.getComid()))
            && (this.getAddress() == null ? other.getAddress() == null : this.getAddress().equals(other.getAddress()))
            && (this.getTodayenergy() == null ? other.getTodayenergy() == null : this.getTodayenergy().equals(other.getTodayenergy()))
            && (this.getTotalenergy() == null ? other.getTotalenergy() == null : this.getTotalenergy().equals(other.getTotalenergy()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getIid() == null) ? 0 : getIid().hashCode());
        result = prime * result + ((getClltidday() == null) ? 0 : getClltidday().hashCode());
        result = prime * result + ((getDeviceindex() == null) ? 0 : getDeviceindex().hashCode());
        result = prime * result + ((getClltidyear() == null) ? 0 : getClltidyear().hashCode());
        result = prime * result + ((getClltidmonth() == null) ? 0 : getClltidmonth().hashCode());
        result = prime * result + ((getClltiddayindex() == null) ? 0 : getClltiddayindex().hashCode());
        result = prime * result + ((getRegionid() == null) ? 0 : getRegionid().hashCode());
        result = prime * result + ((getProjectid() == null) ? 0 : getProjectid().hashCode());
        result = prime * result + ((getSectionid() == null) ? 0 : getSectionid().hashCode());
        result = prime * result + ((getCollectid() == null) ? 0 : getCollectid().hashCode());
        result = prime * result + ((getComid() == null) ? 0 : getComid().hashCode());
        result = prime * result + ((getAddress() == null) ? 0 : getAddress().hashCode());
        result = prime * result + ((getTodayenergy() == null) ? 0 : getTodayenergy().hashCode());
        result = prime * result + ((getTotalenergy() == null) ? 0 : getTotalenergy().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", iid=").append(iid);
        sb.append(", clltidday=").append(clltidday);
        sb.append(", deviceindex=").append(deviceindex);
        sb.append(", clltidyear=").append(clltidyear);
        sb.append(", clltidmonth=").append(clltidmonth);
        sb.append(", clltiddayindex=").append(clltiddayindex);
        sb.append(", regionid=").append(regionid);
        sb.append(", projectid=").append(projectid);
        sb.append(", sectionid=").append(sectionid);
        sb.append(", collectid=").append(collectid);
        sb.append(", comid=").append(comid);
        sb.append(", address=").append(address);
        sb.append(", todayenergy=").append(todayenergy);
        sb.append(", totalenergy=").append(totalenergy);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}
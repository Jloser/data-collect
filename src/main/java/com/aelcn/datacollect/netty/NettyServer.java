package com.aelcn.datacollect.netty;

import io.netty.channel.EventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.xml.ws.Response;
import java.util.HashMap;
import java.util.Map;

/**
 * Auther: JiaYanzhao
 * Date: 2021/12/20 0:34
 * Describe:
 */
@Component
@Slf4j
public class NettyServer {
    // 保存 response 的map
    public static Map<String, Response> map = new HashMap<>();

    // 保存客户端连接的通道引用
    public static SocketChannel sc = null;

    public static EventLoopGroup acceptor;
    public static EventLoopGroup worker;

    @PostConstruct
    public void init() throws InterruptedException {
        new NettyServerThread().start();
        log.info("nettyServer启动");
    }

    @PreDestroy
    public void exit() {
        acceptor.shutdownGracefully();
        worker.shutdownGracefully();
    }

}

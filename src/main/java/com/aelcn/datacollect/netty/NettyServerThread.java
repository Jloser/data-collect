package com.aelcn.datacollect.netty;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.timeout.ReadTimeoutHandler;
import lombok.extern.slf4j.Slf4j;



/**
 * Auther: JiaYanzhao
 * Date: 2021/12/20 0:42
 * Describe:
 */
@Slf4j
public class NettyServerThread extends Thread{

    @Override
    public void run() {
        EventLoopGroup acceptor = new NioEventLoopGroup();
        EventLoopGroup worker = new NioEventLoopGroup();
        NettyServer.acceptor = acceptor;
        NettyServer.worker = worker;
        ServerBootstrap bootstrap = new ServerBootstrap();

        // 添加boss和worker组
        bootstrap.group(acceptor, worker);
        // 指定允许等待accept的最大连接数，默认50个
        // bootstrap.option(ChannelOption.SO_BACKLOG, 1024);
        bootstrap.option(ChannelOption.TCP_NODELAY, true);
        // 用于构造 sockerchannel 工厂
        bootstrap.channel(NioServerSocketChannel.class);

        /**
         * 传入自定义客户端Hadle（处理消息）
         */
        bootstrap.childHandler(new ChannelInitializer<SocketChannel>() {
            @Override
            protected void initChannel(SocketChannel socketChannel) throws Exception {
                if (NettyServer.sc == null){
                    log.info("来自" + socketChannel.remoteAddress() + "的新连接接入");
                    NettyServer.sc = socketChannel;
                    // 注册 handler
                    socketChannel.pipeline().addLast(new ReadTimeoutHandler(10));
                    socketChannel.pipeline().addLast(new MessageHandler());
                } else {
                    socketChannel.close();
                }
            }
        });

        // 绑定端口，开始接收进来的连接
        ChannelFuture f;
        try {
            f = bootstrap.bind(8888).sync();
            // 等待服务器 socket 关闭 。
            f.channel().closeFuture().sync();
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


    }


}

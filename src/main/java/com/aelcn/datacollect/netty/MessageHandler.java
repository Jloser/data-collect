package com.aelcn.datacollect.netty;

import com.aelcn.datacollect.domain.Request;
import com.aelcn.datacollect.service.RequestService;
import com.aelcn.datacollect.util.ByteUtil;
import com.aelcn.datacollect.util.SpringUtil;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

/**
 * Auther: JiaYanzhao
 * Date: 2021/12/20 0:58
 * Describe:
 */
@Slf4j
public class MessageHandler extends ChannelInboundHandlerAdapter {

    private static RequestService requestService;

    static {
        requestService = SpringUtil.getBean(RequestService.class);
    }

    /**
     * 本方法用于读取客户端发送的信息
     *
     * @param ctx
     * @param msg
     * @throws Exception
     */
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {

        ByteBuf msgByteBuf = (ByteBuf) msg;
//        log.info("读取客户端信息：" + msgByteBuf.toString());
        byte[] msgBytes = new byte[msgByteBuf.readableBytes()];
        // msg中存储的是ByteBuf类型的数据，把数据读取到byte[]中
        msgByteBuf.readBytes(msgBytes);
        // 释放资源
        msgByteBuf.release();

        // 处理信息
        handler(msgBytes, ctx);

        // 可能返回到的msgByteBuf是多条信息拼起来的,把他们拆开分别处理
//        List<byte[]> list = getMsgList(msgBytes);

        // 真正处理信息的方法
//        list.forEach(v -> handler(v, ctx));

    }

    /**
     * 切分信息的方法
     *
     * @param msgBytes
     * @return
     */
    private List<byte[]> getMsgList(byte[] msgBytes) {
        List<byte[]> list = new ArrayList<byte[]>();
        //具体业务代码略
        return list;
    }

    /**
     * 本方法用作处理异常
     *
     * @param ctx
     * @param cause
     * @throws Exception
     */
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        if (cause.getClass() == io.netty.handler.timeout.ReadTimeoutException.class) {
            log.info("来自" + NettyServer.sc.remoteAddress() + "的连接超时断开");
        } else {
            cause.printStackTrace();
            log.info("来自" + NettyServer.sc.remoteAddress() + "的连接异常断开");
            ctx.close();
        }
        NettyServer.sc= null;
    }

    /**
     * 信息获取完毕后操作
     *
     * @param ctx
     * @throws Exception
     */
    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
        ctx.flush();
    }

    /**
     * 断开连接时操作
     *
     * @param ctx
     * @throws Exception
     */
    @Override
    public void channelUnregistered(ChannelHandlerContext ctx) throws Exception {

        if (NettyServer.sc!= null) {
            log.info("来自" + NettyServer.sc.remoteAddress() + "的连接主动断开");
            NettyServer.sc= null;
        }

        ctx.fireChannelUnregistered();
    }

    /**
     * 根据信息具体操作的业务方法
     *
     * @param msgBytes
     * @param ctx
     */
    private void handler(byte[] msgBytes, ChannelHandlerContext ctx) {
        Request request = new Request();
        request.setRecDataLen(msgBytes.length);
        if (msgBytes.length < 674) {
            request.setDeal(false);
            request.setIntverDataId(0L);
        } else {
            request.setDeal(true);
            Long inverDataId = requestService.saveInver(msgBytes);
            request.setIntverDataId(inverDataId);
        }
        String recData = ByteUtil.byteArrayToHexString(msgBytes);
        log.info("接收到数据 ==> " + recData);
        request.setRecData(recData.trim());
        requestService.save(request);
        log.info("收包数据已保存");
        ctx.flush();
    }


}

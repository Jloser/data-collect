package com.aelcn.datacollect.util;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.IdUtil;
import com.aelcn.datacollect.domain.InverterdataCurr;
import com.aelcn.datacollect.domain.InverterdataCurryear;
import com.aelcn.datacollect.domain.InverterrptCurryear;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Auther: JiaYanzhao
 * Date: 2021/12/19 20:04
 * Describe:
 */
public class ConvertByteUtil {

    private static int byte2int(byte[] byteArray, int start, int len) {
        if (len > 4) {
            throw new RuntimeException("转为一个int最多需要4个byte，数量异常");
        }
        int res = 0;
        for (int i = 0; i < len; i++) {
            res |= Byte.toUnsignedInt(byteArray[i + start]) << (8 * i);
        }
        return res;
    }

    private static float byte2float(byte[] byteArray, int start, int len){
        int i = byte2int(byteArray, start, len);
        return Float.intBitsToFloat(i);
    }

    private static String byte2String(byte[] byteArray, int start, int len) {
        String str = new String(byteArray, start, len);
        return str.trim();
    }

    public static InverterdataCurr convertInver(byte[] byteArray) {
        // 校验数据长度
        if (byteArray.length < 674){
            throw new RuntimeException("convertInver 包长度不符 退出");
        }

        // 当前 byteArray数组 下标
        int currPchArrIndex = 7;

        int iDayTimes = byte2int(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        String strDay = byte2String(byteArray, currPchArrIndex, 10);
        currPchArrIndex += 10;

        String strTime = byte2String(byteArray, currPchArrIndex, 8);
        currPchArrIndex += 8;

        int iRegionID = byte2int(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        int iProjectID = byte2int(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        int iSectionID = byte2int(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        int iCollectID = byte2int(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        int iCom = byte2int(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        int iAddress = byte2int(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        String strSN = byte2String(byteArray, currPchArrIndex, 32);
        currPchArrIndex += 32;

        int iStatus = byte2int(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        String strStatus = byte2String(byteArray, currPchArrIndex, 128);
        currPchArrIndex += 128;

        float fltPVVoltage = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltPVCurrent = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltPVOutputPower = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltPVVoltageB = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltPVCurrentB = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltPVOutputPowerB = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltPVVoltageC = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltPVCurrentC = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltPVOutputPowerC = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltPVTotalOutputPower = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltVoltage = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltCurrent = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltFrequency = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltOutputPower = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltROutputPower = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltPowerFactor = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltVoltageB = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltCurrentB = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltFrequencyB = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltOutputPowerB = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltROutputPowerB = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltPowerFactorB = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltVoltageC = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltCurrentC = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltFrequencyC = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltOutputPowerC = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltROutputPowerC = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltPowerFactorC = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltTotalOutputPower = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltRTotalOutputPower = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltTotalPowerFactor = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltTodayEnergy = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltTotalEnergy = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltNTodayEnergy = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltNTotalEnergy = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltRTodayEnergy = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltRTotalEnergy = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltNRTodayEnergy = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltNRTotalEnergy = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltBtryVoltage = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltBtryCurrent = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltBtryOutputPower = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;


        float fltTemperatureInverter = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltTemperatureBooster = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltTemperatureSink = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltTodayHour = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltTotalHour = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltAGCSetting = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltAVCSetting = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltGroundResistance = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltLeakCurrent = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringVoltage1 = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringCurrent1 = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringVoltage2 = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringCurrent2 = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringVoltage3 = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringCurrent3 = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringVoltage4 = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringCurrent4 = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringVoltage5 = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringCurrent5 = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringVoltage6 = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringCurrent6 = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringVoltage7 = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringCurrent7 = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringVoltage8 = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringCurrent8 = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringVoltage9 = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringCurrent9 = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringVoltage10 = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringCurrent10 = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringVoltage11 = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringCurrent11 = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringVoltage12 = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringCurrent12 = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringVoltage13 = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringCurrent13 = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringVoltage14 = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringCurrent14 = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringVoltage15 = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringCurrent15 = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringVoltage16 = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringCurrent16 = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringVoltage17 = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringCurrent17 = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringVoltage18 = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringCurrent18 = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringVoltage19 = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringCurrent19 = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringVoltage20 = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringCurrent20 = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringVoltage21 = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringCurrent21 = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringVoltage22 = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringCurrent22 = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringVoltage23 = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringCurrent23 = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringVoltage24 = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringCurrent24 = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltMotherLineVoltage = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltGroudVoltage = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltNGroudVoltage = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltDesignPower = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltVoltageAB = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltVoltageBC = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltVoltageCA = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        int iSystemTime = byte2int(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        int iEfficiency = byte2int(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        int iPowerOnSecond = byte2int(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        int iPowerOffSecond = byte2int(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltMPPT1 = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltMPPT2 = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltMPPT3 = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltMPPT4 = byte2float(byteArray, currPchArrIndex, 4);
        currPchArrIndex += 4;

        // 处理时间
        //天2009-01-01
        // 12:13:20
        String strTemp = strDay.replace("-", "");
        int iTempDay = Integer.parseInt(strTemp);

        Snowflake snowflake = IdUtil.getSnowflake(1, 1);
        long qwMaxID = snowflake.nextId();


        strTemp = iCom + String.format("%02d",iAddress);
        int iDeviceIndex = Integer.parseInt(strTemp);


        InverterdataCurr inverterdataCurr = new InverterdataCurr();
        inverterdataCurr.setIid(qwMaxID);
        inverterdataCurr.setClltidday(iTempDay);
        inverterdataCurr.setClltdaytime(str2Date(strDay + " " + strTime));
        inverterdataCurr.setDeviceindex(iDeviceIndex);
        inverterdataCurr.setDaytimes(iDayTimes);
        inverterdataCurr.setRegionid(iRegionID);
        inverterdataCurr.setProjectid(iProjectID);
        inverterdataCurr.setSectionid(iSectionID);
        inverterdataCurr.setCollectid(iCollectID);
        inverterdataCurr.setComid(iCom);
        inverterdataCurr.setAddress(iAddress);
        inverterdataCurr.setSn(strSN);
        inverterdataCurr.setStatusint(iStatus);
        inverterdataCurr.setStatusdesc(strStatus);

        inverterdataCurr.setPvvoltage((double) fltPVVoltage);
        inverterdataCurr.setPvcurrent((double) fltPVCurrent);
        inverterdataCurr.setPvoutputpower((double) fltPVOutputPower/1000);
        inverterdataCurr.setPvvoltageb((double) fltPVVoltageB);
        inverterdataCurr.setPvcurrentb((double) fltPVCurrentB);
        inverterdataCurr.setPvoutputpowerb((double) fltPVOutputPowerB/1000);
        inverterdataCurr.setPvvoltagec((double) fltPVVoltageC);
        inverterdataCurr.setPvcurrentc((double) fltPVCurrentC);
        inverterdataCurr.setPvoutputpowerc((double) fltPVOutputPowerC/1000);
        inverterdataCurr.setPvtotaloutputpower((double) fltPVTotalOutputPower/1000);

        inverterdataCurr.setVoltagea((double) fltVoltage);
        inverterdataCurr.setCurrenta((double) fltCurrent);
        inverterdataCurr.setFrequencya((double) fltFrequency);
        inverterdataCurr.setOutputpowera((double) fltOutputPower/1000);
        inverterdataCurr.setRoutputpowera((double) fltROutputPower/1000);
        inverterdataCurr.setPowerfactora((double) fltPowerFactor);
        inverterdataCurr.setVoltageb((double) fltVoltageB);
        inverterdataCurr.setCurrentb((double) fltCurrentB);
        inverterdataCurr.setFrequencyb((double) fltFrequencyB);
        inverterdataCurr.setOutputpowerb((double) fltOutputPowerB/1000);
        inverterdataCurr.setRoutputpowerb((double) fltROutputPowerB/1000);
        inverterdataCurr.setPowerfactorb((double) fltPowerFactorB);
        inverterdataCurr.setVoltagec((double) fltVoltageC);
        inverterdataCurr.setCurrentc((double) fltCurrentC);
        inverterdataCurr.setFrequencyc((double) fltFrequencyC);
        inverterdataCurr.setOutputpowerc((double) fltOutputPowerC/1000);
        inverterdataCurr.setRoutputpowerc((double) fltROutputPowerC/1000);
        inverterdataCurr.setPowerfactorc((double) fltPowerFactorC);

        inverterdataCurr.setTotaloutputpower((double) fltTotalOutputPower/1000);
        inverterdataCurr.setRtotaloutputpower((double) fltRTotalOutputPower/1000);
        inverterdataCurr.setTotalpowerfactor((double) fltTotalPowerFactor);

        inverterdataCurr.setTodayenergy((double) fltTodayEnergy/1000);
        inverterdataCurr.setTotalenergy((double) fltTotalEnergy/1000);

        inverterdataCurr.setNtodayenergy((double) fltNTodayEnergy/1000);
        inverterdataCurr.setNtotalenergy((double) fltNTotalEnergy/1000);

        inverterdataCurr.setRtodayenergy((double) fltRTodayEnergy/1000);
        inverterdataCurr.setRtotalenergy((double) fltRTotalEnergy/1000);

        inverterdataCurr.setNrtodayenergy((double) fltNRTodayEnergy/1000);
        inverterdataCurr.setNrtotalenergy((double) fltNRTotalEnergy/1000);

        inverterdataCurr.setBtryvoltage((double) fltBtryVoltage);
        inverterdataCurr.setBtrycurrent((double) fltBtryCurrent);
        inverterdataCurr.setBtryoutputpower((double) fltBtryOutputPower/1000);

        inverterdataCurr.setTemperatureinverter((double) fltTemperatureInverter);
        inverterdataCurr.setTemperaturebooster((double) fltTemperatureBooster);
        inverterdataCurr.setTemperaturesink((double) fltTemperatureSink);

        inverterdataCurr.setTodayhour((double) fltTodayHour);
        inverterdataCurr.setTotalhour((double) fltTotalHour);

        inverterdataCurr.setAgcsetting((double) fltAGCSetting);
        inverterdataCurr.setAvcsetting((double) fltAVCSetting);

        inverterdataCurr.setGroundresistance((double) fltGroundResistance);
        inverterdataCurr.setLeakcurrent((double) fltLeakCurrent);

        inverterdataCurr.setStringvoltage1((double) fltStringVoltage1);
        inverterdataCurr.setStringcurrent1((double) fltStringCurrent1);
        inverterdataCurr.setStringvoltage2((double) fltStringVoltage2);
        inverterdataCurr.setStringcurrent2((double) fltStringCurrent2);
        inverterdataCurr.setStringvoltage3((double) fltStringVoltage3);
        inverterdataCurr.setStringcurrent3((double) fltStringCurrent3);
        inverterdataCurr.setStringvoltage4((double) fltStringVoltage4);
        inverterdataCurr.setStringcurrent4((double) fltStringCurrent4);
        inverterdataCurr.setStringvoltage5((double) fltStringVoltage5);
        inverterdataCurr.setStringcurrent5((double) fltStringCurrent5);
        inverterdataCurr.setStringvoltage6((double) fltStringVoltage6);
        inverterdataCurr.setStringcurrent6((double) fltStringCurrent6);
        inverterdataCurr.setStringvoltage7((double) fltStringVoltage7);
        inverterdataCurr.setStringcurrent7((double) fltStringCurrent7);
        inverterdataCurr.setStringvoltage8((double) fltStringVoltage8);
        inverterdataCurr.setStringcurrent8((double) fltStringCurrent8);

        inverterdataCurr.setStringvoltage9((double) fltStringVoltage9);
        inverterdataCurr.setStringcurrent9((double) fltStringCurrent9);
        inverterdataCurr.setStringvoltage10((double) fltStringVoltage10);
        inverterdataCurr.setStringcurrent10((double) fltStringCurrent10);
        inverterdataCurr.setStringvoltage11((double) fltStringVoltage11);
        inverterdataCurr.setStringcurrent11((double) fltStringCurrent11);
        inverterdataCurr.setStringvoltage12((double) fltStringVoltage12);
        inverterdataCurr.setStringcurrent12((double) fltStringCurrent12);
        inverterdataCurr.setStringvoltage13((double) fltStringVoltage13);
        inverterdataCurr.setStringcurrent13((double) fltStringCurrent13);
        inverterdataCurr.setStringvoltage14((double) fltStringVoltage14);
        inverterdataCurr.setStringcurrent14((double) fltStringCurrent14);
        inverterdataCurr.setStringvoltage15((double) fltStringVoltage15);
        inverterdataCurr.setStringcurrent15((double) fltStringCurrent15);
        inverterdataCurr.setStringvoltage16((double) fltStringVoltage16);
        inverterdataCurr.setStringcurrent16((double) fltStringCurrent16);
        inverterdataCurr.setStringvoltage17((double) fltStringVoltage17);
        inverterdataCurr.setStringcurrent17((double) fltStringCurrent17);
        inverterdataCurr.setStringvoltage18((double) fltStringVoltage18);
        inverterdataCurr.setStringcurrent18((double) fltStringCurrent18);
        inverterdataCurr.setStringvoltage19((double) fltStringVoltage19);
        inverterdataCurr.setStringcurrent19((double) fltStringCurrent19);
        inverterdataCurr.setStringvoltage20((double) fltStringVoltage20);
        inverterdataCurr.setStringcurrent20((double) fltStringCurrent20);
        inverterdataCurr.setStringvoltage21((double) fltStringVoltage21);
        inverterdataCurr.setStringcurrent21((double) fltStringCurrent21);
        inverterdataCurr.setStringvoltage22((double) fltStringVoltage22);
        inverterdataCurr.setStringcurrent22((double) fltStringCurrent22);
        inverterdataCurr.setStringvoltage23((double) fltStringVoltage23);
        inverterdataCurr.setStringcurrent23((double) fltStringCurrent23);
        inverterdataCurr.setStringvoltage24((double) fltStringVoltage24);
        inverterdataCurr.setStringcurrent24((double) fltStringCurrent24);

        inverterdataCurr.setMotherlinevoltage((double) fltMotherLineVoltage);

        inverterdataCurr.setGroudvoltage((double) fltGroudVoltage);
        inverterdataCurr.setNgroudvoltage((double) fltNGroudVoltage);

        inverterdataCurr.setDesignpower((double) fltDesignPower/1000);

        inverterdataCurr.setVoltageab((double) fltVoltageAB);
        inverterdataCurr.setVoltagebc((double) fltVoltageBC);
        inverterdataCurr.setVoltageca((double) fltVoltageCA);

        inverterdataCurr.setSystemtime(iSystemTime);
        inverterdataCurr.setEfficiency(iEfficiency);
        inverterdataCurr.setPoweronsecond(iPowerOnSecond);
        inverterdataCurr.setPoweroffsecond(iPowerOffSecond);
        inverterdataCurr.setMppt1((double) fltMPPT1/1000);
        inverterdataCurr.setMppt2((double) fltMPPT2/1000);
        inverterdataCurr.setMppt3((double) fltMPPT3/1000);
        inverterdataCurr.setMppt4((double) fltMPPT4/1000);

        return inverterdataCurr;
    }

    public static char[] bytes2char(byte[] bytes) {
        char[] chars = new char[bytes.length];
        for (int i = 0; i < bytes.length; i++) {
            chars[i] = (char) bytes[i];
        }
        return chars;
    }

    private static Date str2Date(String str) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = simpleDateFormat.parse(str);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static InverterdataCurryear inverterDataCurr2Year(InverterdataCurr inverterdataCurr) {
        InverterdataCurryear inverterdataCurryear = new InverterdataCurryear();
        BeanUtil.copyProperties(inverterdataCurr, inverterdataCurryear);
        return inverterdataCurryear;
    }

    public static InverterrptCurryear inverterDataCurr2RptYear(InverterdataCurr inverterdataCurr) {
        InverterrptCurryear inverterrptCurryear = new InverterrptCurryear();
        BeanUtil.copyProperties(inverterdataCurr, inverterrptCurryear);

        String strDay = inverterdataCurr.getClltidday().toString();
        String strYear = strDay.substring(0, 4);
        int iYear  = Integer.parseInt(strYear);

        String strMonth = strDay.substring(4, 6);
        int iMonth = Integer.parseInt(strMonth);

        String strTemp = strDay.substring(6);
        int iDay = Integer.parseInt(strTemp);

        inverterrptCurryear.setClltidyear(iYear);
        inverterrptCurryear.setClltidmonth(iMonth);
        inverterrptCurryear.setClltiddayindex(iDay);
        return inverterrptCurryear;

    }

    public static void main(String[] args) {
        // 674
        // 11 12 0B 9A 02 00
        String hexString = "11120EDA010000C4260000323032312D31322D323231343A30363A3539000000000100000001000000010000000500000001000000303031363132313839343830000000000000000000000000000000000000000003000000D4CBD0D000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000871669404C378140C9760E400000484200004842000048420038A9460020B24600C0464600C028C50050024600806845C9767E3FD7A3703F2B87763F000000000000000000985F470040124608AC7C3FDDF7354E00000000EC9C144C000000000020BC450070BC450030BB4500000000000000000000000000000000000000000000000000642B4900000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000003DC31918111268F2010000CA260000323032312D31322D323231343A30373A3333000000000100000001000000010000000A00000001000000534830433930303630303035323031303134303030310000000000000000000004000000CFB5CDB3B9CAD5CF20204143C4A3BFE9D7E93A20D6B1C1F7CAE4C8EBC7B7D1B9D2ECB3A3204143C4A3BFE9D7E93A20424D53D2ECB3A328BBF2BFAAB9D8B6CFBFAA29204143C4A3BFE9D7E93A20424D53B5E7D1B9D2ECB3A320BBFABCDCB9A6C4DCB0E5B9CAD5CF000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000003333D4430080D4436626D54366660EC19A99C9400000084100007A4400409C4500409C45EC51383E000048420000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000007ED2154C00000000000000006E0B064C00000000000000000000B041000000000000000000000000000000000000000000000000732719181112693A010000CA260000323032312D31322D323231343A30373A333300";
        System.out.println(hexString.length());
        byte[] bytes = ByteUtil.hexStringToBytes(hexString);
        System.out.println(bytes.length);
//        char[] chars = hexString.toCharArray();

//        System.out.println(new String(chars));
        InverterdataCurr inverterdataCurr = convertInver(bytes);
        System.out.println(inverterdataCurr);
    }

}

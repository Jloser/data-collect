package com.aelcn.datacollect.util;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.IdUtil;
import com.aelcn.datacollect.domain.InverterdataCurr;
import com.aelcn.datacollect.domain.InverterdataCurryear;
import com.aelcn.datacollect.domain.InverterrptCurryear;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Auther: JiaYanzhao
 * Date: 2021/12/19 20:04
 * Describe:
 */
public class ConvertUtil {

    private static int char2int(char[] charArray, int start, int len) {
        if (len > 4) {
            throw new RuntimeException("转为一个int最多需要4个char字符，字符数量异常");
        }
        int res = 0;
        for (int i = 0; i < len; i++) {
            res |= Byte.toUnsignedInt((byte) charArray[i + start]) << (8 * i);
        }
        return res;
    }

    private static float char2float(char[] charArray, int start, int len){
        int i = char2int(charArray, start, len);
        return Float.intBitsToFloat(i);
    }

    private static String char2String(char[] charArray, int start, int len) {
        String str = String.copyValueOf(charArray, start, len);
        return str.trim();
    }

    public static InverterdataCurr convertInver(char[] pchData) {
        // 校验数据长度
        if (pchData.length < 674){
            throw new RuntimeException("convertInver 包长度不符 退出");
        }

        // 当前 pchData数组 下标
        int currPchArrIndex = 7;

        int iDayTimes = char2int(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        String strDay = char2String(pchData, currPchArrIndex, 10);
        currPchArrIndex += 10;

        String strTime = char2String(pchData, currPchArrIndex, 8);
        currPchArrIndex += 8;

        int iRegionID = char2int(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        int iProjectID = char2int(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        int iSectionID = char2int(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        int iCollectID = char2int(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        int iCom = char2int(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        int iAddress = char2int(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        String strSN = char2String(pchData, currPchArrIndex, 32);
        currPchArrIndex += 32;

        int iStatus = char2int(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        String strStatus = char2String(pchData, currPchArrIndex, 128);
        currPchArrIndex += 128;

        float fltPVVoltage = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltPVCurrent = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltPVOutputPower = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltPVVoltageB = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltPVCurrentB = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltPVOutputPowerB = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltPVVoltageC = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltPVCurrentC = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltPVOutputPowerC = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltPVTotalOutputPower = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltVoltage = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltCurrent = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltFrequency = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltOutputPower = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltROutputPower = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltPowerFactor = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltVoltageB = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltCurrentB = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltFrequencyB = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltOutputPowerB = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltROutputPowerB = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltPowerFactorB = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltVoltageC = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltCurrentC = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltFrequencyC = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltOutputPowerC = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltROutputPowerC = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltPowerFactorC = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltTotalOutputPower = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltRTotalOutputPower = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltTotalPowerFactor = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltTodayEnergy = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltTotalEnergy = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltNTodayEnergy = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltNTotalEnergy = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltRTodayEnergy = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltRTotalEnergy = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltNRTodayEnergy = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltNRTotalEnergy = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltBtryVoltage = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltBtryCurrent = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltBtryOutputPower = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;


        float fltTemperatureInverter = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltTemperatureBooster = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltTemperatureSink = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltTodayHour = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltTotalHour = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltAGCSetting = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltAVCSetting = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltGroundResistance = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltLeakCurrent = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringVoltage1 = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringCurrent1 = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringVoltage2 = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringCurrent2 = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringVoltage3 = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringCurrent3 = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringVoltage4 = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringCurrent4 = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringVoltage5 = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringCurrent5 = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringVoltage6 = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringCurrent6 = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringVoltage7 = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringCurrent7 = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringVoltage8 = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringCurrent8 = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringVoltage9 = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringCurrent9 = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringVoltage10 = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringCurrent10 = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringVoltage11 = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringCurrent11 = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringVoltage12 = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringCurrent12 = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringVoltage13 = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringCurrent13 = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringVoltage14 = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringCurrent14 = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringVoltage15 = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringCurrent15 = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringVoltage16 = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringCurrent16 = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringVoltage17 = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringCurrent17 = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringVoltage18 = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringCurrent18 = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringVoltage19 = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringCurrent19 = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringVoltage20 = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringCurrent20 = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringVoltage21 = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringCurrent21 = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringVoltage22 = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringCurrent22 = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringVoltage23 = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringCurrent23 = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringVoltage24 = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltStringCurrent24 = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltMotherLineVoltage = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltGroudVoltage = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltNGroudVoltage = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltDesignPower = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltVoltageAB = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltVoltageBC = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltVoltageCA = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        int iSystemTime = char2int(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        int iEfficiency = char2int(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        int iPowerOnSecond = char2int(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        int iPowerOffSecond = char2int(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltMPPT1 = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltMPPT2 = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltMPPT3 = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        float fltMPPT4 = char2float(pchData, currPchArrIndex, 4);
        currPchArrIndex += 4;

        // 处理时间
        //天2009-01-01
        // 12:13:20
        String strTemp = strDay.replace("-", "");
        int iTempDay = Integer.parseInt(strTemp);

        Snowflake snowflake = IdUtil.getSnowflake(1, 1);
        long qwMaxID = snowflake.nextId();


        strTemp = iCom + String.format("%02d",iAddress);
        int iDeviceIndex = Integer.parseInt(strTemp);


        InverterdataCurr inverterdataCurr = new InverterdataCurr();
        inverterdataCurr.setIid(qwMaxID);
        inverterdataCurr.setClltidday(iTempDay);
        inverterdataCurr.setClltdaytime(str2Date(strDay + " " + strTime));
        inverterdataCurr.setDeviceindex(iDeviceIndex);
        inverterdataCurr.setDaytimes(iDayTimes);
        inverterdataCurr.setRegionid(iRegionID);
        inverterdataCurr.setProjectid(iProjectID);
        inverterdataCurr.setSectionid(iSectionID);
        inverterdataCurr.setCollectid(iCollectID);
        inverterdataCurr.setComid(iCom);
        inverterdataCurr.setAddress(iAddress);
        inverterdataCurr.setSn(strSN);
        inverterdataCurr.setStatusint(iStatus);
        inverterdataCurr.setStatusdesc(strStatus);

        inverterdataCurr.setPvvoltage((double) fltPVVoltage);
        inverterdataCurr.setPvcurrent((double) fltPVCurrent);
        inverterdataCurr.setPvoutputpower((double) fltPVOutputPower/1000);
        inverterdataCurr.setPvvoltageb((double) fltPVVoltageB);
        inverterdataCurr.setPvcurrentb((double) fltPVCurrentB);
        inverterdataCurr.setPvoutputpowerb((double) fltPVOutputPowerB/1000);
        inverterdataCurr.setPvvoltagec((double) fltPVVoltageC);
        inverterdataCurr.setPvcurrentc((double) fltPVCurrentC);
        inverterdataCurr.setPvoutputpowerc((double) fltPVOutputPowerC/1000);
        inverterdataCurr.setPvtotaloutputpower((double) fltPVTotalOutputPower/1000);

        inverterdataCurr.setVoltagea((double) fltVoltage);
        inverterdataCurr.setCurrenta((double) fltCurrent);
        inverterdataCurr.setFrequencya((double) fltFrequency);
        inverterdataCurr.setOutputpowera((double) fltOutputPower/1000);
        inverterdataCurr.setRoutputpowera((double) fltROutputPower/1000);
        inverterdataCurr.setPowerfactora((double) fltPowerFactor);
        inverterdataCurr.setVoltageb((double) fltVoltageB);
        inverterdataCurr.setCurrentb((double) fltCurrentB);
        inverterdataCurr.setFrequencyb((double) fltFrequencyB);
        inverterdataCurr.setOutputpowerb((double) fltOutputPowerB/1000);
        inverterdataCurr.setRoutputpowerb((double) fltROutputPowerB/1000);
        inverterdataCurr.setPowerfactorb((double) fltPowerFactorB);
        inverterdataCurr.setVoltagec((double) fltVoltageC);
        inverterdataCurr.setCurrentc((double) fltCurrentC);
        inverterdataCurr.setFrequencyc((double) fltFrequencyC);
        inverterdataCurr.setOutputpowerc((double) fltOutputPowerC/1000);
        inverterdataCurr.setRoutputpowerc((double) fltROutputPowerC/1000);
        inverterdataCurr.setPowerfactorc((double) fltPowerFactorC);

        inverterdataCurr.setTotaloutputpower((double) fltTotalOutputPower/1000);
        inverterdataCurr.setRtotaloutputpower((double) fltRTotalOutputPower/1000);
        inverterdataCurr.setTotalpowerfactor((double) fltTotalPowerFactor);

        inverterdataCurr.setTodayenergy((double) fltTodayEnergy/1000);
        inverterdataCurr.setTotalenergy((double) fltTotalEnergy/1000);

        inverterdataCurr.setNtodayenergy((double) fltNTodayEnergy/1000);
        inverterdataCurr.setNtotalenergy((double) fltNTotalEnergy/1000);

        inverterdataCurr.setRtodayenergy((double) fltRTodayEnergy/1000);
        inverterdataCurr.setRtotalenergy((double) fltRTotalEnergy/1000);

        inverterdataCurr.setNrtodayenergy((double) fltNRTodayEnergy/1000);
        inverterdataCurr.setNrtotalenergy((double) fltNRTotalEnergy/1000);

        inverterdataCurr.setBtryvoltage((double) fltBtryVoltage);
        inverterdataCurr.setBtrycurrent((double) fltBtryCurrent);
        inverterdataCurr.setBtryoutputpower((double) fltBtryOutputPower/1000);

        inverterdataCurr.setTemperatureinverter((double) fltTemperatureInverter);
        inverterdataCurr.setTemperaturebooster((double) fltTemperatureBooster);
        inverterdataCurr.setTemperaturesink((double) fltTemperatureSink);

        inverterdataCurr.setTodayhour((double) fltTodayHour);
        inverterdataCurr.setTotalhour((double) fltTotalHour);

        inverterdataCurr.setAgcsetting((double) fltAGCSetting);
        inverterdataCurr.setAvcsetting((double) fltAVCSetting);

        inverterdataCurr.setGroundresistance((double) fltGroundResistance);
        inverterdataCurr.setLeakcurrent((double) fltLeakCurrent);

        inverterdataCurr.setStringvoltage1((double) fltStringVoltage1);
        inverterdataCurr.setStringcurrent1((double) fltStringCurrent1);
        inverterdataCurr.setStringvoltage2((double) fltStringVoltage2);
        inverterdataCurr.setStringcurrent2((double) fltStringCurrent2);
        inverterdataCurr.setStringvoltage3((double) fltStringVoltage3);
        inverterdataCurr.setStringcurrent3((double) fltStringCurrent3);
        inverterdataCurr.setStringvoltage4((double) fltStringVoltage4);
        inverterdataCurr.setStringcurrent4((double) fltStringCurrent4);
        inverterdataCurr.setStringvoltage5((double) fltStringVoltage5);
        inverterdataCurr.setStringcurrent5((double) fltStringCurrent5);
        inverterdataCurr.setStringvoltage6((double) fltStringVoltage6);
        inverterdataCurr.setStringcurrent6((double) fltStringCurrent6);
        inverterdataCurr.setStringvoltage7((double) fltStringVoltage7);
        inverterdataCurr.setStringcurrent7((double) fltStringCurrent7);
        inverterdataCurr.setStringvoltage8((double) fltStringVoltage8);
        inverterdataCurr.setStringcurrent8((double) fltStringCurrent8);

        inverterdataCurr.setStringvoltage9((double) fltStringVoltage9);
        inverterdataCurr.setStringcurrent9((double) fltStringCurrent9);
        inverterdataCurr.setStringvoltage10((double) fltStringVoltage10);
        inverterdataCurr.setStringcurrent10((double) fltStringCurrent10);
        inverterdataCurr.setStringvoltage11((double) fltStringVoltage11);
        inverterdataCurr.setStringcurrent11((double) fltStringCurrent11);
        inverterdataCurr.setStringvoltage12((double) fltStringVoltage12);
        inverterdataCurr.setStringcurrent12((double) fltStringCurrent12);
        inverterdataCurr.setStringvoltage13((double) fltStringVoltage13);
        inverterdataCurr.setStringcurrent13((double) fltStringCurrent13);
        inverterdataCurr.setStringvoltage14((double) fltStringVoltage14);
        inverterdataCurr.setStringcurrent14((double) fltStringCurrent14);
        inverterdataCurr.setStringvoltage15((double) fltStringVoltage15);
        inverterdataCurr.setStringcurrent15((double) fltStringCurrent15);
        inverterdataCurr.setStringvoltage16((double) fltStringVoltage16);
        inverterdataCurr.setStringcurrent16((double) fltStringCurrent16);
        inverterdataCurr.setStringvoltage17((double) fltStringVoltage17);
        inverterdataCurr.setStringcurrent17((double) fltStringCurrent17);
        inverterdataCurr.setStringvoltage18((double) fltStringVoltage18);
        inverterdataCurr.setStringcurrent18((double) fltStringCurrent18);
        inverterdataCurr.setStringvoltage19((double) fltStringVoltage19);
        inverterdataCurr.setStringcurrent19((double) fltStringCurrent19);
        inverterdataCurr.setStringvoltage20((double) fltStringVoltage20);
        inverterdataCurr.setStringcurrent20((double) fltStringCurrent20);
        inverterdataCurr.setStringvoltage21((double) fltStringVoltage21);
        inverterdataCurr.setStringcurrent21((double) fltStringCurrent21);
        inverterdataCurr.setStringvoltage22((double) fltStringVoltage22);
        inverterdataCurr.setStringcurrent22((double) fltStringCurrent22);
        inverterdataCurr.setStringvoltage23((double) fltStringVoltage23);
        inverterdataCurr.setStringcurrent23((double) fltStringCurrent23);
        inverterdataCurr.setStringvoltage24((double) fltStringVoltage24);
        inverterdataCurr.setStringcurrent24((double) fltStringCurrent24);

        inverterdataCurr.setMotherlinevoltage((double) fltMotherLineVoltage);

        inverterdataCurr.setGroudvoltage((double) fltGroudVoltage);
        inverterdataCurr.setNgroudvoltage((double) fltNGroudVoltage);

        inverterdataCurr.setDesignpower((double) fltDesignPower/1000);

        inverterdataCurr.setVoltageab((double) fltVoltageAB);
        inverterdataCurr.setVoltagebc((double) fltVoltageBC);
        inverterdataCurr.setVoltageca((double) fltVoltageCA);

        inverterdataCurr.setSystemtime(iSystemTime);
        inverterdataCurr.setEfficiency(iEfficiency);
        inverterdataCurr.setPoweronsecond(iPowerOnSecond);
        inverterdataCurr.setPoweroffsecond(iPowerOffSecond);
        inverterdataCurr.setMppt1((double) fltMPPT1/1000);
        inverterdataCurr.setMppt2((double) fltMPPT2/1000);
        inverterdataCurr.setMppt3((double) fltMPPT3/1000);
        inverterdataCurr.setMppt4((double) fltMPPT4/1000);

        return inverterdataCurr;
    }

    public static char[] bytes2char(byte[] bytes) {
        char[] chars = new char[bytes.length];
        for (int i = 0; i < bytes.length; i++) {
            chars[i] = (char) bytes[i];
        }
        return chars;
    }

    private static Date str2Date(String str) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = simpleDateFormat.parse(str);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static InverterdataCurryear inverterDataCurr2Year(InverterdataCurr inverterdataCurr) {
        InverterdataCurryear inverterdataCurryear = new InverterdataCurryear();
        BeanUtil.copyProperties(inverterdataCurr, inverterdataCurryear);
        return inverterdataCurryear;
    }

    public static InverterrptCurryear inverterDataCurr2RptYear(InverterdataCurr inverterdataCurr) {
        InverterrptCurryear inverterrptCurryear = new InverterrptCurryear();
        BeanUtil.copyProperties(inverterdataCurr, inverterrptCurryear);

        String strDay = inverterdataCurr.getClltidday().toString();
        String strYear = strDay.substring(0, 4);
        int iYear  = Integer.parseInt(strYear);

        String strMonth = strDay.substring(4, 6);
        int iMonth = Integer.parseInt(strMonth);

        String strTemp = strDay.substring(6);
        int iDay = Integer.parseInt(strTemp);

        inverterrptCurryear.setClltidyear(iYear);
        inverterrptCurryear.setClltidmonth(iMonth);
        inverterrptCurryear.setClltiddayindex(iDay);
        return inverterrptCurryear;

    }

    public static void main(String[] args) {
        // 674
        // 11 12 0B 9A 02 00
        String hexString = "11120EDA010000C4260000323032312D31322D323231343A30363A3539000000000100000001000000010000000500000001000000303031363132313839343830000000000000000000000000000000000000000003000000D4CBD0D000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000871669404C378140C9760E400000484200004842000048420038A9460020B24600C0464600C028C50050024600806845C9767E3FD7A3703F2B87763F000000000000000000985F470040124608AC7C3FDDF7354E00000000EC9C144C000000000020BC450070BC450030BB4500000000000000000000000000000000000000000000000000642B4900000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000003DC31918111268F2010000CA260000323032312D31322D323231343A30373A3333000000000100000001000000010000000A00000001000000534830433930303630303035323031303134303030310000000000000000000004000000CFB5CDB3B9CAD5CF20204143C4A3BFE9D7E93A20D6B1C1F7CAE4C8EBC7B7D1B9D2ECB3A3204143C4A3BFE9D7E93A20424D53D2ECB3A328BBF2BFAAB9D8B6CFBFAA29204143C4A3BFE9D7E93A20424D53B5E7D1B9D2ECB3A320BBFABCDCB9A6C4DCB0E5B9CAD5CF000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000003333D4430080D4436626D54366660EC19A99C9400000084100007A4400409C4500409C45EC51383E000048420000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000007ED2154C00000000000000006E0B064C00000000000000000000B041000000000000000000000000000000000000000000000000732719181112693A010000CA260000323032312D31322D323231343A30373A333300";
        System.out.println(hexString.length());
        byte[] bytes = ByteUtil.hexStringToBytes(hexString);
        System.out.println(bytes.length);
//        char[] chars = hexString.toCharArray();

        char[] chars = new char[bytes.length];
        for (int i = 0; i < bytes.length; i++) {
            chars[i] = (char) bytes[i];
        }

//        System.out.println(new String(chars));
        InverterdataCurr inverterdataCurr = convertInver(chars);
        System.out.println(inverterdataCurr);
    }

}

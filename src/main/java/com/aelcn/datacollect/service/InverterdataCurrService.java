package com.aelcn.datacollect.service;

import com.aelcn.datacollect.domain.InverterdataCurr;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 */
public interface InverterdataCurrService {

    Boolean mySaveOrUpdate(InverterdataCurr inverterdataCurr);
}

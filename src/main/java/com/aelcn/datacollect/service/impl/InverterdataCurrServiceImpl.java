package com.aelcn.datacollect.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.aelcn.datacollect.domain.InverterdataCurr;
import com.aelcn.datacollect.service.InverterdataCurrService;
import com.aelcn.datacollect.mapper.InverterdataCurrMapper;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class InverterdataCurrServiceImpl extends ServiceImpl<InverterdataCurrMapper, InverterdataCurr>
    implements InverterdataCurrService{

    @Override
    public Boolean mySaveOrUpdate(InverterdataCurr inverterdataCurr) {
        UpdateWrapper<InverterdataCurr> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("ProjectID", inverterdataCurr.getProjectid());
        updateWrapper.eq("ComID", inverterdataCurr.getComid());
        updateWrapper.eq("Address", inverterdataCurr.getAddress());
        return super.saveOrUpdate(inverterdataCurr, updateWrapper);
    }
}





package com.aelcn.datacollect.service.impl;

import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.IdUtil;
import com.aelcn.datacollect.domain.InverterdataCurr;
import com.aelcn.datacollect.domain.InverterdataCurryear;
import com.aelcn.datacollect.domain.InverterrptCurryear;
import com.aelcn.datacollect.service.InverterdataCurrService;
import com.aelcn.datacollect.service.InverterdataCurryearService;
import com.aelcn.datacollect.service.InverterrptCurryearService;
import com.aelcn.datacollect.util.ConvertByteUtil;
import com.aelcn.datacollect.util.ConvertUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.aelcn.datacollect.domain.Request;
import com.aelcn.datacollect.service.RequestService;
import com.aelcn.datacollect.mapper.RequestMapper;
import lombok.AllArgsConstructor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 */
@Service
@AllArgsConstructor
public class RequestServiceImpl extends ServiceImpl<RequestMapper, Request>
    implements RequestService{

    private final InverterdataCurrService inverterdataCurrService;
    private final InverterdataCurryearService inverterdataCurryearService;
    private final InverterrptCurryearService inverterrptCurryearService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Long saveInver(byte[] bytes) {
        if (bytes.length < 674) {
            return -1L;
        }
//        char[] chars = ConvertUtil.bytes2char(bytes);
//        InverterdataCurr inverterdataCurr = ConvertUtil.convertInver(chars);
        Long id = -1L;
        try {
            InverterdataCurr inverterdataCurr = ConvertByteUtil.convertInver(bytes);
            if (inverterdataCurr.getIid() == null) {
                Snowflake snowflake = IdUtil.getSnowflake(1, 1);
                inverterdataCurr.setIid(snowflake.nextId());
            }
            id = inverterdataCurr.getIid();
            inverterdataCurrService.mySaveOrUpdate(inverterdataCurr);

            InverterdataCurryear inverterdataCurryear = ConvertUtil.inverterDataCurr2Year(inverterdataCurr);
            inverterdataCurryearService.mySaveOrUpdate(inverterdataCurryear);

            InverterrptCurryear inverterrptCurryear = ConvertUtil.inverterDataCurr2RptYear(inverterdataCurr);
            inverterrptCurryearService.mySaveOrUpdate(inverterrptCurryear);
        } catch (Exception e) {
            id = -1L;
            log.error("存储Inverterdata出错", e);
        }
        return id;
    }
}





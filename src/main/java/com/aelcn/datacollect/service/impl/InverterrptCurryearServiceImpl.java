package com.aelcn.datacollect.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.aelcn.datacollect.domain.InverterrptCurryear;
import com.aelcn.datacollect.service.InverterrptCurryearService;
import com.aelcn.datacollect.mapper.InverterrptCurryearMapper;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class InverterrptCurryearServiceImpl extends ServiceImpl<InverterrptCurryearMapper, InverterrptCurryear>
    implements InverterrptCurryearService{

    @Override
    public Boolean mySaveOrUpdate(InverterrptCurryear inverterrptCurryear) {

        UpdateWrapper<InverterrptCurryear> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("ClltIDDay", inverterrptCurryear.getClltidday());
        updateWrapper.eq("ProjectID", inverterrptCurryear.getProjectid());
        updateWrapper.eq("ComID", inverterrptCurryear.getComid());
        updateWrapper.eq("Address", inverterrptCurryear.getAddress());
        return super.saveOrUpdate(inverterrptCurryear, updateWrapper);
    }
}





package com.aelcn.datacollect.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.aelcn.datacollect.domain.InverterdataCurryear;
import com.aelcn.datacollect.service.InverterdataCurryearService;
import com.aelcn.datacollect.mapper.InverterdataCurryearMapper;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class InverterdataCurryearServiceImpl extends ServiceImpl<InverterdataCurryearMapper, InverterdataCurryear>
    implements InverterdataCurryearService{

    @Override
    public Boolean mySaveOrUpdate(InverterdataCurryear inverterdataCurryear) {
        UpdateWrapper<InverterdataCurryear> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("ClltIDDay", inverterdataCurryear.getClltidday());
        updateWrapper.eq("ProjectID", inverterdataCurryear.getProjectid());
        updateWrapper.eq("ComID", inverterdataCurryear.getComid());
        updateWrapper.eq("Address", inverterdataCurryear.getAddress());
        updateWrapper.eq("DayTimes", inverterdataCurryear.getDaytimes());
        return super.saveOrUpdate(inverterdataCurryear, updateWrapper);
    }
}





package com.aelcn.datacollect.service;

import com.aelcn.datacollect.domain.InverterdataCurryear;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 */
public interface InverterdataCurryearService extends IService<InverterdataCurryear> {

    Boolean mySaveOrUpdate(InverterdataCurryear inverterdataCurryear);
}

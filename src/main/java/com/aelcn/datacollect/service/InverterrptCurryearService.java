package com.aelcn.datacollect.service;

import com.aelcn.datacollect.domain.InverterrptCurryear;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 */
public interface InverterrptCurryearService extends IService<InverterrptCurryear> {

    Boolean mySaveOrUpdate(InverterrptCurryear inverterrptCurryear);
}

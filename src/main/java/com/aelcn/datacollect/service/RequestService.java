package com.aelcn.datacollect.service;

import com.aelcn.datacollect.domain.Request;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 */
public interface RequestService extends IService<Request> {

    Long saveInver(byte[] bytes);
}

package com.aelcn.datacollect.mapper;

import com.aelcn.datacollect.domain.Request;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Entity com.aelcn.datacollect.domain.Request
 */
@Mapper
public interface RequestMapper extends BaseMapper<Request> {

}





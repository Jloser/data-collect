package com.aelcn.datacollect.mapper;

import com.aelcn.datacollect.domain.InverterdataCurryear;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Entity com.aelcn.datacollect.domain.InverterdataCurryear
 */
@Mapper
public interface InverterdataCurryearMapper extends BaseMapper<InverterdataCurryear> {

}





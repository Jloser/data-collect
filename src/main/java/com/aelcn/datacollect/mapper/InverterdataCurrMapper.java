package com.aelcn.datacollect.mapper;

import com.aelcn.datacollect.domain.InverterdataCurr;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Entity com.aelcn.datacollect.domain.InverterdataCurr
 */
@Mapper
public interface InverterdataCurrMapper extends BaseMapper<InverterdataCurr> {

}





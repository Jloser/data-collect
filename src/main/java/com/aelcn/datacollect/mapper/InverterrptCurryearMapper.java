package com.aelcn.datacollect.mapper;

import com.aelcn.datacollect.domain.InverterrptCurryear;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Entity com.aelcn.datacollect.domain.InverterrptCurryear
 */
@Mapper
public interface InverterrptCurryearMapper extends BaseMapper<InverterrptCurryear> {

}





package com.aelcn.datacollect;

import com.aelcn.datacollect.util.ByteUtil;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.net.Socket;

/**
 * Auther: JiaYanzhao
 * Date: 2021/12/27 0:28
 * Describe:
 */
public class SocketTest {

    @Test
    public void socketClient() throws IOException {
        String serverName = "127.0.0.1";
        int port = 8888;
        try
        {
            System.out.println("连接到主机：" + serverName + " ，端口号：" + port);
            Socket client = new Socket(serverName, port);
            System.out.println("远程主机地址：" + client.getRemoteSocketAddress());
            OutputStream outToServer = client.getOutputStream();
            DataOutputStream out = new DataOutputStream(outToServer);

            String hexString = "11 12 0B 9A 02 00 00 95 23 00 00 32 30 32 31 2D 31 32 2D 32 32 31 32 3A 35 38 3A 30 39 00 00 00 00 01 00 00 00 01 00 00 00 01 00 00 00 01 00 00 00 01 00 00 00 38 31 30 30 4B 48 54 55 32 30 39 47 30 30 35 38 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 02 00 00 00 B5 C8 B4 FD 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 66 E6 53 44 00 00 00 00 00 00 00 00 66 E6 53 44 00 00 80 3E 00 00 00 00 9A B9 54 44 00 00 00 00 00 00 00 00 00 00 00 00 9A 19 71 43 00 00 00 00 7B 14 48 42 00 00 00 00 00 00 00 00 00 00 00 00 CD CC 76 43 00 00 00 00 7B 14 48 42 00 00 00 00 00 00 00 00 00 00 00 00 CD 4C 77 43 00 00 00 00 7B 14 48 42 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 4D 5D 8C 4B 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 33 33 1B 41 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 C0 DA 45 00 00 00 00 00 00 00 00 00 00 00 00 66 E6 53 44 00 00 00 00 66 E6 53 44 00 00 80 3E 9A B9 54 44 00 00 00 00 9A B9 54 44 8F C2 F5 3D 33 73 54 44 00 00 00 00 33 73 54 44 CD CC 4C 3E 9A D9 54 44 00 00 00 00 9A D9 54 44 33 33 B3 3E 00 E0 54 44 00 00 00 00 00 E0 54 44 00 00 80 3E 9A 99 54 44 00 00 00 00 9A 99 54 44 1F 85 6B 3E 9A D9 54 44 00 00 00 00 9A D9 54 44 E1 7A 94 3E 33 73 B7 43 00 00 00 00 33 73 B7 43 52 B8 9E 3E 9A 59 B7 43 00 00 00 00 9A 59 B7 43 29 5C 0F 3E CD 0C B7 43 00 00 00 00 CD 0C B7 43 1F 85 6B 3E 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 66 E6 D3 43 66 66 D4 43 66 66 D4 43 00 00 00 00 8F 02 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 B5 1A 19 18";
            byte[] bytes = ByteUtil.hexStringToBytes(hexString);

            /**
             * 9A
             * 1001 1010
             */
            out.write(bytes);
//            out.writeUTF(new String(bytes));
            InputStream inFromServer = client.getInputStream();
            DataInputStream in = new DataInputStream(inFromServer);
            System.out.println("服务器响应： " + in.readUTF());
            client.close();
        }catch(IOException e)
        {
            e.printStackTrace();
        }
    }
}

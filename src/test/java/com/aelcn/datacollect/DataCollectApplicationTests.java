package com.aelcn.datacollect;

import com.aelcn.datacollect.domain.InverterdataCurr;
import com.aelcn.datacollect.util.ByteUtil;
import com.aelcn.datacollect.util.ConvertUtil;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

class DataCollectApplicationTests {

    @Test
    void contextLoads() {
        System.out.println(Byte.MAX_VALUE);
        String hexString = "11 12 0B 9A 02 00 00 95 23 00 00 32 30 32 31 2D 31 32 2D 32 32 31 32 3A 35 38 3A 30 39 00 00 00 00 01 00 00 00 01 00 00 00 01 00 00 00 01 00 00 00 01 00 00 00 38 31 30 30 4B 48 54 55 32 30 39 47 30 30 35 38 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 02 00 00 00 B5 C8 B4 FD 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 66 E6 53 44 00 00 00 00 00 00 00 00 66 E6 53 44 00 00 80 3E 00 00 00 00 9A B9 54 44 00 00 00 00 00 00 00 00 00 00 00 00 9A 19 71 43 00 00 00 00 7B 14 48 42 00 00 00 00 00 00 00 00 00 00 00 00 CD CC 76 43 00 00 00 00 7B 14 48 42 00 00 00 00 00 00 00 00 00 00 00 00 CD 4C 77 43 00 00 00 00 7B 14 48 42 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 4D 5D 8C 4B 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 33 33 1B 41 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 C0 DA 45 00 00 00 00 00 00 00 00 00 00 00 00 66 E6 53 44 00 00 00 00 66 E6 53 44 00 00 80 3E 9A B9 54 44 00 00 00 00 9A B9 54 44 8F C2 F5 3D 33 73 54 44 00 00 00 00 33 73 54 44 CD CC 4C 3E 9A D9 54 44 00 00 00 00 9A D9 54 44 33 33 B3 3E 00 E0 54 44 00 00 00 00 00 E0 54 44 00 00 80 3E 9A 99 54 44 00 00 00 00 9A 99 54 44 1F 85 6B 3E 9A D9 54 44 00 00 00 00 9A D9 54 44 E1 7A 94 3E 33 73 B7 43 00 00 00 00 33 73 B7 43 52 B8 9E 3E 9A 59 B7 43 00 00 00 00 9A 59 B7 43 29 5C 0F 3E CD 0C B7 43 00 00 00 00 CD 0C B7 43 1F 85 6B 3E 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 66 E6 D3 43 66 66 D4 43 66 66 D4 43 00 00 00 00 8F 02 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 B5 1A 19 18  " +
                           "11 12 0B 9A 02 00 00 95 23 00 00 32 30 32 31 2D 31 32 2D 32 32 31 32 3A 35 38 3A 30 39 00 00 00 00 01 00 00 00 01 00 00 00 01 00 00 00 01 00 00 00 01 00 00 00 38 31 30 30 4B 48 54 55 32 30 39 47 30 30 35 38 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 02 00 00 00 B5 C8 B4 FD 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 66 E6 53 44 00 00 00 00 00 00 00 00 66 E6 53 44 00 00 80 3E 00 00 00 00 9A B9 54 44 00 00 00 00 00 00 00 00 00 00 00 00 9A 19 71 43 00 00 00 00 7B 14 48 42 00 00 00 00 00 00 00 00 00 00 00 00 CD CC 76 43 00 00 00 00 7B 14 48 42 00 00 00 00 00 00 00 00 00 00 00 00 CD 4C 77 43 00 00 00 00 7B 14 48 42 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 4D 5D 8C 4B 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 33 33 1B 41 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 C0 DA 45 00 00 00 00 00 00 00 00 00 00 00 00 66 E6 53 44 00 00 00 00 66 E6 53 44 00 00 80 3E 9A B9 54 44 00 00 00 00 9A B9 54 44 8F C2 F5 3D 33 73 54 44 00 00 00 00 33 73 54 44 CD CC 4C 3E 9A D9 54 44 00 00 00 00 9A D9 54 44 33 33 B3 3E 00 E0 54 44 00 00 00 00 00 E0 54 44 00 00 80 3E 9A 99 54 44 00 00 00 00 9A 99 54 44 1F 85 6B 3E 9A D9 54 44 00 00 00 00 9A D9 54 44 E1 7A 94 3E 33 73 B7 43 00 00 00 00 33 73 B7 43 52 B8 9E 3E 9A 59 B7 43 00 00 00 00 9A 59 B7 43 29 5C 0F 3E CD 0C B7 43 00 00 00 00 CD 0C B7 43 1F 85 6B 3E 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 66 E6 D3 43 66 66 D4 43 66 66 D4 43 00 00 00 00 8F 02 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 B5 1A 19 18 ";
        System.out.println("hexstring length ==> " + hexString.length());
        String hexstr = hexString.replace(" ", "");
        System.out.println("hexstr lenght ==> " + hexstr.length());

        byte[] bytes = ByteUtil.hexStringToBytes(hexString);
        System.out.println("bytes length ==> " + bytes.length);
        String str = new String(bytes);
        System.out.println("string length ==> " + str.length());
        System.out.println(str.indexOf("2021-12-22"));
        System.out.println(str.lastIndexOf("2021-12-22"));
        System.out.println(str);
        char[] chars = new char[bytes.length];
        for (int i = 0; i < bytes.length; i++) {
            chars[i] = (char) bytes[i];
        }
        InverterdataCurr inverterdataCurr = ConvertUtil.convertInver(chars);
        System.out.println(inverterdataCurr);
    }

    @Test
    void floatConverTest() {
        String hexStr = "85EB3442";
//        String hexStr = "B81EF742";

        byte[] bytes = ByteUtil.hexStringToBytes(hexStr);

        int res = 0;
        for (int i = 0; i < 4; i++) {
            res |= Byte.toUnsignedInt(bytes[i]) << (8 * i);
        }
        System.out.println(Integer.toBinaryString(res));

        float v1 = Float.intBitsToFloat(res);
        System.out.println(v1);

        /**
         * 0100 0010
         * 0011 0100
         * 1110 1011
         * 1000 0101
         *
         * 42 34 EB 85
         */

        System.out.println(1/3.0f);
        int i = Float.floatToIntBits(1/3.0f);
        String s = Integer.toBinaryString(i);
//        System.out.println(s);
//        String s = "1000010001101001110101110000101";

        int i1 = Integer.parseInt(s, 2);
        System.out.println("parse bin to int ==> " + i1);

        float v = Float.intBitsToFloat(i1);
        System.out.println("int to Float ==> " + v);
    }

    @Test
    void test() {
        byte bb = (byte) (9 << 4 | 10);
        System.out.println(bb);
        char c = (char) bb;
        System.out.println((byte) c);
//        System.out.println(Integer.toBinaryString(bb));
//        int i = Integer.parseInt("10011010", 2);
//        System.out.println(i);
//        System.out.println((byte) i);
    }

}

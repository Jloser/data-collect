-- show databases;

-- drop table if exists M2WDB.student;
-- drop database if exists M2WDB;

-- create database M2WDB default charset utf8mb4 COLLATE utf8_general_ci;
-- use M2WDB;

create table MaxIDInfo
(
	IID bigint not null primary key,		 		-- ID 
	
	MaxIDType  int not null,						-- 类型
	MaxIDValue int not null							-- 值  1=User 2=Region 3=Project 4=Section 5=Collector 6=Com 7=Device
	
);


create table UserInfo 
(
	IID bigint not null primary key,		 		-- ID 
	UserID int not null,		 					-- 用户ID 
	
    UserName varchar(32)  not null,           		-- 姓名  
    Password varchar(32)  not null,           		-- 密码 
    Mobile varchar(32)  not null,            		-- 手机
    Tel varchar(32)  not null,            			-- 电话 
	QQ varchar(32)  not null,            			-- QQ
	Wechat varchar(32)  not null,            		-- 微信
	Address varchar(128)  not null,            		-- 地址  
	Email varchar(32)  not null            			-- Email 
	
);

-- 索引
create index index_UserInfo on UserInfo(UserID asc);

-- 删除索引
-- drop index index_UserInfo on UserInfo ;

-- 查看索引
-- EXEC Sp_helpindex UserInfo;
-- show index from UserInfo;








create table UserProject 
(
	IID bigint not null primary key,		 		-- ID 
	UserProjectID int not null,		 				-- 用户项目ID 
	
    UserID int not null,		 					-- 用户ID 
    ProjectID int not null		 					-- 项目ID
	
);

-- 索引
create index index_UserProject on UserProject(ProjectID asc);

-- 删除索引
-- drop index index_UserProject on UserProject ;

-- 查看索引
-- EXEC Sp_helpindex UserProject;
-- show index from UserProject;


create table RegionInfo
(
	IID bigint not null primary key,		 		-- ID 
	RegionID int not null,		 					-- ID   
	                                        
    RegionNo varchar(128)  not null,               	-- 县市ID
    RegionName varchar(128)  not null,             	-- 县市名称  	   
    RegionDescription varchar(128)  not null,      	-- 描述	    
	    
	Caption varchar(128)  not null,              	-- 标题
	    
	RegionUser varchar(128)  not null              	-- 区域用户
	
);

-- 索引
create index index_RegionInfo on RegionInfo(RegionID asc);

-- 删除索引
-- drop index index_RegionInfo on RegionInfo ;

-- 查看索引
-- EXEC Sp_helpindex RegionInfo;
-- show index from RegionInfo;


create table ProjectInfo 
(
	IID bigint not null primary key,		 		-- ID 
	ProjectID int not null,		 					-- ID   
	
	RegionID int not null, 							-- 区 县市ID
	    
    ProjectNo varchar(128)  not null,               -- 项目ID
    ProjectName varchar(128)  not null,           	-- 项目名称   
	ProjectDesignPower int not null,        		-- 设计功率
    ProjectAddress varchar(128)  not null,         	-- 地址
    ProjectDescription varchar(128)  not null,    	-- 描述	    
	    
	Caption varchar(128)  not null,               	-- 标题    
	
	ProjectFupinCollectID varchar(64)  not null, 	-- 扶贫 采集器ID
    ProjectFupinStationID int not null,	            -- 扶贫 电站ID 场站ID
        
	StationType int not null, 						-- 电站类型
	    
	ProjectCustomerCount int not null, 				-- 分布式用户接入数
	    
	ProjectUser varchar(128)  not null,          	-- 用户 user123,123456|user222,123456|
	ControlUser varchar(128)  not null,          	-- 控制用户 user123,123456|user222,123456| 网页 APP 远程控制逆变器 视频
    ProjectEnergyEPM int not null, 					-- 项目发电量，电表为准	1=TRUE 0=FALSE
        	    
	ProjectMainLine varchar(128)  not null,   		-- 主接线图
	    
	ProjectLongitude float not null, 				-- 经度
	ProjectLatitude float not null, 				-- 维度
	ProjectMapGrade int not null, 					-- 地图等级 百度地图	
	    
	ProjectPerson varchar(32)  not null,          	-- 联系人
	ProjectTel varchar(32)  not null,            	-- 联系电话
	ProjectBankName varchar(32)  not null,        	-- 银行
	ProjectBankCard varchar(32)  not null,        	-- 银行卡
	ProjectInstallDay varchar(16)  not null,       	-- 安装日期
	ProjectGridDay varchar(16)  not null         	-- 并网日期
		
);

-- 索引
create index index_ProjectInfo on ProjectInfo(ProjectID asc);

-- 删除索引
-- drop index index_ProjectInfo on ProjectInfo ;

-- 查看索引
-- EXEC Sp_helpindex ProjectInfo;
-- show index from ProjectInfo;


create table SectionInfo 
(
	IID bigint not null primary key,		 		-- ID 
	SectionID int not null,		 					-- ID  
	
	RegionID int not null, 							-- 区 县市ID 
	ProjectID int not null, 	        			-- 项目ID
	    
	SectionNo varchar(128)  not null, 
    SectionName varchar(128)  not null,        
   	SectionAddress varchar(128)  not null, 
    SectionDescription varchar(128)  not null,      
        
   	Caption varchar(128)  not null   				-- 标题    	
	
);

-- 索引
create index index_SectionInfo on SectionInfo(ProjectID asc);

-- 删除索引
-- drop index index_SectionInfo on SectionInfo ;

-- 查看索引
-- EXEC Sp_helpindex SectionInfo;
-- show index from SectionInfo;



create table CollectorInfo 
(
	IID bigint not null primary key,		 		-- ID 
	CollectID int not null,		 					-- ID  
	
	RegionID int not null, 							-- 区 县市ID 
	ProjectID int not null,            				-- 项目ID
	SectionID int not null,             			-- 区域ID
		    
	CollectNo varchar(128)  not null, 
    CollectName varchar(128)  not null,        
    CollectAddress varchar(128)  not null, 
    CollectDescription varchar(128)  not null,     
        
    Caption varchar(128)  not null                	-- 标题  
	
);
 
-- 索引
create index index_CollectorInfo on CollectorInfo(ProjectID asc);

-- 删除索引
-- drop index index_CollectorInfo on CollectorInfo ;

-- 查看索引
-- EXEC Sp_helpindex CollectorInfo;
-- show index from CollectorInfo;



create table ComInfo 
(
	IID bigint not null primary key,		 		-- ID 
	ComID int not null,		 						-- ID   
	
	RegionID int not null, 							-- 区 县市ID
	ProjectID int not null,                     	-- 项目ID	
	SectionID int not null,                     	-- 区域ID
	CollectID int not null,                     	-- 采集器ID
				
	DevCom int not null, 							-- 设备连接真实串口
		
	ComName varchar(128)  not null,               	-- Com Name
		
	COM int not null, 		                     	-- COM
	Baudrate int not null, 	                      	-- Baudrate
	Databit int not null, 			              	-- Databit
	Stopbit varchar(8)  not null, 			     	-- Stopbit
	Parity varchar(8)  not null, 			      	-- Parity	
				
    Description varchar(128)  not null, 		  	-- description
    Caption varchar(128)  not null,               	-- 标题
        
    IP varchar(32)  not null, 	                	-- IP
	Port int not null,                         		-- Port
	Timeout int not null,                         	-- Timeout
		
	ServerComtoGPRSCom int not null, 				-- 设备连接转发串口   
    ServerComtoGPRSMin varchar(128)  not null,    	-- 转发时间  分钟 58|59|00|01|02|03|13|14|15|16|17|18|28|29|30|31|32|33|43|44|45|46|47|48|
        
    CollectMinuteThirdCollect varchar(128)  not null, 	-- 串口采集58|59|00|01|02| Monitor 支持多个采集器，分时段采集 1-6串口接第三方采集器 串口服务器接设备 配置
    
    ComtoComTrans int not null, 					-- 串口 到串口 转发
    
    IsGPRSMode int not null,                     	-- 是否是是否是GPRS模式模式 1=TRUE 0=FALSE
       
    IsCANMode int not null,                     	-- 是否是CAN模式 1=TRUE 0=FALSE
        
    IsSMSMode int not null,                      	-- 是否是SMS模式         
        
    Is104Mode int not null,                       	-- 是否是104 Client模式       
        
	Is101Mode int not null,                       	-- 是否是101 Client模式 
	
	CollectSecond int not null						-- 采集时间 秒	
		
);

-- 索引
create index index_ComInfo on ComInfo(ProjectID asc);

-- 删除索引
-- drop index index_ComInfo on ComInfo ;

-- 查看索引
-- EXEC Sp_helpindex ComInfo;
-- show index from ComInfo;



create table DeviceInfo 
(
	IID bigint not null primary key,		 		-- ID 
	DeviceIndex int not null,		 				-- ID   所有的设备的唯一ID,从1开始
	
	RegionID int not null, 							-- 区 县市ID
	ProjectID int not null,                     	-- 项目ID	
	SectionID int not null,                     	-- 区域ID	
	CollectID int not null,                     	-- 采集器ID
	ComID int not null,		 						-- ID   
	
	ComLinkType int not null,	                   	-- 设备类型
	
	DeviceName varchar(128)  not null,		    	-- 设备名称
		
	Address int not null,			            	-- address 	
	DeviceID varchar(128)  not null,		   			-- device type			
	Description varchar(128)  not null,		     	-- description
	Caption varchar(128)  not null,              	-- 标题
	PN varchar(64)  not null,						-- PN
	SN varchar(64)  not null,						-- SN
	
	DeviceKey varchar(128)  not null,				-- 设备Key 给清华大学传数据 key：项目ID_串口ID_逆变器地址ID。ID都是递增的。 例如：project1_com1_address1
	
	DesignPower int not null,						-- 逆变器设计功率，额定功率 W
	EnergyOffset float not null, 					-- 偏移量, 电表累积发电量需要增加偏移值
	
	Factor float not null,                     		-- 因子 电流因子
	FactorV float not null,                     	-- 因子 电压因子
	FactorP float not null,                      	-- 因子 功率因子
	FactorQ float not null,                    		-- 因子 无功功率因子
	FactorPF float not null,                   		-- 因子 功率因素因子
		
	ChargeOffset float not null,					-- 储能 充电量 偏移量
	DisChargeOffset float not null,	             	-- 储能 放电量 偏移
		
	BMSGroupCount int not null, 					-- 储能 电池组数量
	BMSGroupDescription varchar(128)  not null,		-- 电池组 description
	BMSGroupUnitCount int not null, 				-- 储能 电池组单体电池数量
	BMSGroupUnitDescription varchar(128)  not null,	-- 电池 description        
    BMSGroupTemperatureCount int not null, 			-- 储能 电池组单体温度数量
	BMSGroupTemperatureDescription varchar(128)  not null,	-- 电池 description
    BMSMaxSOC float not null, 						-- 最高 SOC > 不能充电
    BMSMinSOC float not null, 							-- 最低 SOC < 不能放电 只能在最高 最低之间充放电
            
    MaxChannel int not null,                     	-- 汇流箱最大通道数  
    
    YCID int not null,								-- 104 遥测ID
	YCType int not null,							-- 104 遥测Type
	YXID int not null,								-- 104 遥信ID
	YTID int not null,								-- 104 遥调ID
	YTType int not null,							-- 104 遥调Type 1=AGC 2=AVC
	YTDataID int not null,                       	-- 104 遥调返回点遥信点ID
	YKID int not null,								-- 104 遥控ID
			    
    MaxOutputPower float not null,					-- 最大输出功率
    MaxTodayEnergy float not null,					-- 最大当天发电量
    MaxTotalEnergy float not null,					-- 最大累计发电量
    MaxVoltage float not null,						-- 最大电压
    MaxCurrent float not null,						-- 最大电流
    MaxTemperature float not null,					-- 最大温度
    MaxIrradiance float not null,					-- 最大光照        
    MinTotalEnergy float not null,					-- 最小累计发电量
    
    -- 实时数据
	ClltDaytime datetime  not null,         		-- 天 时间
	StatusInt  int not null,	          			-- 状态	  
	StatusDesc varchar(1024)  not null,				-- 状态	
	Power float not null,							-- 实时功率
	PositiveValue float not null,					-- 保留参数1 正向值 充电, 正向电能, 正向流量
	NegativeValue float not null,					-- 保留参数2 反向值 放电, 反向电能, 反向流量
	RemainPara3 float not null,						-- 保留参数3
	RemainPara4 float not null,						-- 保留参数4
	RemainPara5 float not null						-- 保留参数5
	
);
 
-- 索引
create index index_DeviceInfo on DeviceInfo(ProjectID asc);

-- 删除索引
-- drop index index_DeviceInfo on DeviceInfo ;

-- 查看索引
-- EXEC Sp_helpindex DeviceInfo;
-- show index from DeviceInfo;



create table CollectorStatus_Curr 
(
	IID bigint not null primary key,		 		-- ID 
	ClltIDDay int not null,             			-- 创建时日期 int
	ClltDaytime datetime  not null,         		-- 天 时间	
	
	CollectID int not null,                     	-- 采集器ID		
	
	RegionID int not null,         					-- 区 县市ID
	ProjectID int not null,         				-- 项目ID
	SectionID int not null,         				-- 区域ID
	
	StatusInt  int not null,	            		-- 状态 0=离线 1=在线	  
	StatusDesc varchar(1024)  not null,				-- 状态	
	
	ClientIP varchar(32)  not null,					-- 客户端ip
	ClientPort  int not null						-- 客户端Port
			
);
 
-- 索引
create index index_CollectorStatus_Curr on CollectorStatus_Curr(ProjectID asc);

-- 删除索引
-- drop index index_CollectorStatus_Curr on CollectorStatus_Curr ;

-- 查看索引
-- EXEC Sp_helpindex CollectorStatus_Curr
-- show index from CollectorStatus_Curr;


create table CollectorStatus_CurrYear 
(
	IID bigint not null primary key,		 		-- ID 
	ClltIDDay int not null,             			-- 创建时日期 int
	ClltDaytime datetime  not null,         		-- 天 时间	
	
	CollectID int not null,                     	-- 采集器ID
		
	RegionID int not null,         					-- 区 县市ID
	ProjectID int not null,         				-- 项目ID
	SectionID int not null,         				-- 区域ID
	
	StatusInt  int not null,	            		-- 状态 -1=未注册 0=离线 1=在线	  
	StatusDesc varchar(1024)  not null,				-- 状态	
	
	ClientIP varchar(32)  not null,					-- 客户端ip
	ClientPort  int not null						-- 客户端Port
			
);
 
-- 索引
create index index_CollectorStatus_CurrYear on CollectorStatus_CurrYear(ClltIDDay asc, ProjectID asc);

-- 删除索引
-- drop index index_CollectorStatus_CurrYear on CollectorStatus_CurrYear ;

-- 查看索引
-- EXEC Sp_helpindex CollectorStatus_CurrYear
-- show index from CollectorStatus_CurrYear;


create table DeviceStatus_Curr 
(
	IID bigint not null primary key,		-- ID 
	ClltIDDay int not null,             	-- 创建时日期 int
	ClltDaytime datetime  not null,         -- 天 时间
	
	DeviceIndex int not null,         		-- 设备ID
	
	DayTimes int not null, 					-- 采集ID(本次采集的N个点都有相同的ID)
		
	RegionID int not null,         			-- 区 县市ID
	ProjectID int not null,         		-- 项目ID
	SectionID int not null,         		-- 区域ID
	CollectID int not null,         		-- 采集器ID
	ComID int not null,         			-- Com
	Address int not null,         			-- Address	
		
	StatusInt  int not null,	          	-- 状态	  
	StatusDesc varchar(1024)  not null,		-- 状态	
	
	Power float not null,					-- 实时功率
	PositiveValue float not null,			-- 保留参数1 正向值 充电, 正向电能, 正向流量
	NegativeValue float not null,			-- 保留参数2 反向值 放电, 反向电能, 反向流量
	RemainPara3 float not null,				-- 保留参数3
	RemainPara4 float not null,				-- 保留参数4
	RemainPara5 float not null				-- 保留参数5
	
);

-- 索引
create index index_DeviceStatus_Curr on DeviceStatus_Curr(ProjectID asc);

-- 删除索引
-- drop index index_DeviceStatus_Curr on DeviceStatus_Curr ;

-- 查看索引
-- EXEC Sp_helpindex DeviceStatus_Curr
-- show index from DeviceStatus_Curr;



create table DeviceStatus_CurrYear
(
	IID bigint not null primary key,		-- ID 
	ClltIDDay int not null,             	-- 创建时日期 int
	ClltDaytime datetime  not null,         -- 天 时间
	
	DeviceIndex int not null,         		-- 设备ID			
	
	DayTimes int not null, 					-- 采集ID(本次采集的N个点都有相同的ID)
	
	RegionID int not null,         			-- 区 县市ID
	ProjectID int not null,         		-- 项目ID
	SectionID int not null,         		-- 区域ID
	CollectID int not null,         		-- 采集器ID
	ComID int not null,         			-- Com
	Address int not null,         			-- Address	
		
	StatusInt  int not null,	          	-- 状态	  
	StatusDesc varchar(1024)  not null,		-- 状态	
	
	Power float not null,					-- 实时功率
	PositiveValue float not null,			-- 保留参数1 正向值 充电, 正向电能, 正向流量
	NegativeValue float not null,			-- 保留参数2 反向值 放电, 反向电能, 反向流量
	RemainPara3 float not null,				-- 保留参数3
	RemainPara4 float not null,				-- 保留参数4
	RemainPara5 float not null				-- 保留参数5
);

-- 索引
create index index_DeviceStatus_CurrYear on DeviceStatus_CurrYear(ClltIDDay asc, ProjectID asc);

-- 删除索引
-- drop index index_DeviceStatus_CurrYear on DeviceStatus_CurrYear ;

-- 查看索引
-- EXEC Sp_helpindex DeviceStatus_CurrYear
-- show index from DeviceStatus_CurrYear;


create table DeviceStatusRpt_CurrYear
(
	IID bigint not null primary key,		 	-- ID 
	ClltIDDay int not null,             		-- 创建时日期 int
	
	DeviceIndex int not null,         				-- 设备ID
	
	ClltIDYear int not null,             		-- 创建时年 int	
	ClltIDMonth int not null,             		-- 创建时月 int
	ClltIDDayIndex int not null,             	-- 创建时日 int
	
	RegionID int not null,         				-- 区 县市ID
	ProjectID int not null,         			-- 项目ID
	SectionID int not null,         			-- 区域ID
	CollectID int not null,         			-- 采集器ID
	ComID int not null,         				-- Com
	Address int not null,         				-- Address	
	
	StatusInt  int not null,	          		-- 状态	  
	StatusDesc varchar(1024)  not null			-- 状态	
);
 
-- 索引
create index index_DeviceStatusRpt_CurrYear on DeviceStatusRpt_CurrYear(ClltIDDay asc, ProjectID asc);

-- 删除索引
-- drop index index_DeviceStatusRpt_CurrYear on DeviceStatusRpt_CurrYear ;

-- 查看索引
-- EXEC Sp_helpindex DeviceStatusRpt_CurrYear
-- show index from DeviceStatusRpt_CurrYear;




create table AlertData_Curr 
(
	IID bigint not null primary key,		-- ID 
	ClltIDDay int not null,             	-- 创建时日期 int
	ClltDaytime datetime  not null,         -- 天 时间
	
	DeviceIndex int not null,         		-- 设备ID		
	
	DayTimes int not null, 					-- 采集ID(本次采集的N个点都有相同的ID)
	
	ClltIDYear int not null,             		-- 创建时年 int	
	ClltIDMonth int not null,             		-- 创建时月 int
	ClltIDDayIndex int not null, 
	
	RegionID int not null,         			-- 区 县市ID
	ProjectID int not null,         		-- 项目ID
	SectionID int not null,         		-- 区域ID
	CollectID int not null,         		-- 采集器ID
	ComID int not null,         			-- Com
	Address int not null,         			-- Address	
		
	StatusInt  int not null,	          	-- 状态	  
	StatusDesc varchar(1024)  not null		-- 状态	
);

-- 索引
create index index_AlertData_Curr on AlertData_Curr(ProjectID asc);

-- 删除索引
-- drop index index_AlertData_Curr on AlertData_Curr ;

-- 查看索引
-- EXEC Sp_helpindex AlertData_Curr
-- show index from AlertData_Curr;


create table AlertData_CurrYear 
(
	IID bigint not null primary key,		-- ID 
	ClltIDDay int not null,             	-- 创建时日期 int
	ClltDaytime datetime  not null,         -- 天 时间
	
	DeviceIndex int not null,         		-- 设备ID	
	
	DayTimes int not null, 					-- 采集ID(本次采集的N个点都有相同的ID)
	
	ClltIDYear int not null,             		-- 创建时年 int	
	ClltIDMonth int not null,             		-- 创建时月 int
	ClltIDDayIndex int not null,             	-- 创建时日 int
	
	RegionID int not null,         			-- 区 县市ID
	ProjectID int not null,         		-- 项目ID
	SectionID int not null,         		-- 区域ID
	CollectID int not null,         		-- 采集器ID
	ComID int not null,         			-- Com
	Address int not null,         			-- Address	
		
	StatusInt  int not null,	          	-- 状态	  
	StatusDesc varchar(1024)  not null		-- 状态	
);

-- 索引
create index index_AlertData_CurrYear on AlertData_CurrYear(ClltIDDay asc, ProjectID asc);

-- 删除索引
-- drop index index_AlertData_CurrYear on AlertData_CurrYear ;

-- 查看索引
-- EXEC Sp_helpindex AlertData_CurrYear
-- show index from AlertData_CurrYear;



create table InverterData_Curr 
(
	IID bigint not null primary key,		-- ID 
	ClltIDDay int not null,             	-- 创建时日期 int
	ClltDaytime datetime  not null,         -- 天 时间
	
	DeviceIndex int not null,         		-- 设备ID	
	
	DayTimes int not null, 					-- 采集ID(本次采集的N个点都有相同的ID)
	
	RegionID int not null,         			-- 区 县市ID
	ProjectID int not null,         		-- 项目ID
	SectionID int not null,         		-- 区域ID
	CollectID int not null,         		-- 采集器ID
	ComID int not null,         			-- Com
	Address int not null,         			-- Address	
	
	SN varchar(64)  not null,	            -- SN 序列号	
	
	StatusInt  int not null,	          	-- 状态	  
	StatusDesc varchar(1024)  not null,		-- 状态		
	
	PVVoltage float not null,		    	-- PV电压
    PVCurrent float not null,		    	-- PV电流
    PVOutputPower float not null,			-- PV A输出功率
    PVVoltageB float not null,		    	-- PV电压B
    PVCurrentB float not null,		    	-- PV电流B
    PVOutputPowerB float not null,			-- PV B输出功率
	PVVoltageC float not null,		    	-- PV电压C
   	PVCurrentC float not null,		    	-- PV电流C
    PVOutputPowerC float not null,			-- PV C输出功率        
    PVTotalOutputPower float not null,		-- PV总输出功率  
               
    VoltageA float not null,		    	-- 交流电压A 相电压
    CurrentA float not null,		    	-- 交流电流 A      
    FrequencyA float not null,		    	-- 交流频率 A  
    OutputPowerA float not null,			-- 交流功率A   
    ROutputPowerA float not null,			-- 交流无功功率A
    PowerFactorA float not null,			-- 功率因素A
    VoltageB float not null,		    	-- 交流电压B  相电压
    CurrentB float not null,		    	-- 交流电流B       
    FrequencyB float not null,		    	-- 交流频率B
    OutputPowerB float not null,			-- 交流功率B 
    ROutputPowerB float not null,			-- 交流无功功率B 
    PowerFactorB float not null,			-- 功率因素B   
    VoltageC float not null,		    	-- 交流电压C  相电压
    CurrentC float not null,		    	-- 交流电流C        
    FrequencyC float not null,		    	-- 交流频率C 
    OutputPowerC float not null,			-- 交流功率C
    ROutputPowerC float not null,			-- 交流无功功率C        
    PowerFactorC float not null,			-- 功率因素C
        
    TotalOutputPower float not null,		-- 交流总功率
    RTotalOutputPower float not null,		-- 交流总无功功率
    TotalPowerFactor float not null,		-- 交流总功率因素           
        
    TodayEnergy float not null,		    	-- 当天能量
    TotalEnergy float not null,		    	-- 总能量
        
    NTodayEnergy float not null,			-- 反向当天能量
    NTotalEnergy float not null,			-- 反向总能量
        
    RTodayEnergy float not null,			-- 当天无功能量
    RTotalEnergy float not null,			-- 总无功能量
        
    NRTodayEnergy float not null,			-- 反向当天无功能量
    NRTotalEnergy float not null,			-- 反向无功总能量        
        
    BtryVoltage float not null,		    	-- 蓄电池电压
    BtryCurrent float not null,		    	-- 蓄电池电流
    BtryOutputPower float not null,			-- 蓄电池输出功率  
        
    TemperatureInverter float not null,		-- Inverter温度    
    TemperatureBooster float not null,		-- Booster温度
    TemperatureSink float not null,			-- Sink 散热器       
        
    TodayHour float not null,		    	-- 当天发电小时
    TotalHour float not null,		    	-- 总发电小时
        
    AGCSetting float not null,		    	-- AGC 设置值
    AVCSetting float not null,		    	-- AVC 设置值
        
    GroundResistance float not null,		-- 接地电阻 MΩ
    LeakCurrent float not null,		    	-- 漏电流 mA
       
    StringVoltage1 float not null,		    -- 组串 电压
    StringCurrent1 float not null,		    -- 组串 电流        
    StringVoltage2 float not null,		    -- 组串 电压
    StringCurrent2 float not null,		    -- 组串 电流
    StringVoltage3 float not null,		    -- 组串 电压
    StringCurrent3 float not null,		    -- 组串 电流
    StringVoltage4 float not null,		    -- 组串 电压
    StringCurrent4 float not null,		    -- 组串 电流
    StringVoltage5 float not null,		    -- 组串 电压
    StringCurrent5 float not null,		    -- 组串 电流
    StringVoltage6 float not null,		    -- 组串 电压
    StringCurrent6 float not null,		    -- 组串 电流
    StringVoltage7 float not null,		    -- 组串 电压
    StringCurrent7 float not null,		    -- 组串 电流
    StringVoltage8 float not null,		    -- 组串 电压
    StringCurrent8 float not null,		    -- 组串 电流
        
    StringVoltage9 float not null,		    -- 组串 电压 2018-03-13  华旭 东莞 宝胜 阳光组串逆变器 未保存文件
    StringCurrent9 float not null,		    -- 组串 电流
    StringVoltage10 float not null,		    -- 组串 电压
    StringCurrent10 float not null,		    -- 组串 电流
    StringVoltage11 float not null,		    -- 组串 电压
    StringCurrent11 float not null,		    -- 组串 电流
    StringVoltage12 float not null,		    -- 组串 电压
    StringCurrent12 float not null,		    -- 组串 电流
    StringVoltage13 float not null,		    -- 组串 电压
    StringCurrent13 float not null,		    -- 组串 电流
    StringVoltage14 float not null,		    -- 组串 电压
    StringCurrent14 float not null,		    -- 组串 电流
    StringVoltage15 float not null,		    -- 组串 电压
    StringCurrent15 float not null,		    -- 组串 电流
    StringVoltage16 float not null,		    -- 组串 电压
    StringCurrent16 float not null,		    -- 组串 电流
    
    StringVoltage17 float not null,		    -- 组串 电压
    StringCurrent17 float not null,		    -- 组串 电流
    StringVoltage18 float not null,		    -- 组串 电压
    StringCurrent18 float not null,		    -- 组串 电流
    StringVoltage19 float not null,		    -- 组串 电压
    StringCurrent19 float not null,		    -- 组串 电流
    StringVoltage20 float not null,		    -- 组串 电压
    StringCurrent20 float not null,		    -- 组串 电流
    StringVoltage21 float not null,		    -- 组串 电压
    StringCurrent21 float not null,		    -- 组串 电流
    StringVoltage22 float not null,		    -- 组串 电压
    StringCurrent22 float not null,		    -- 组串 电流
    StringVoltage23 float not null,		    -- 组串 电压
    StringCurrent23 float not null,		    -- 组串 电流
    StringVoltage24 float not null,		    -- 组串 电压
    StringCurrent24 float not null,		    -- 组串 电流
            
    MotherLineVoltage float not null,		-- 母线电压  
        
    GroudVoltage float not null,		    -- 正极对地电 压 
    NGroudVoltage float not null,		    -- 负极对地电 压 
        
    DesignPower float not null,		    	-- 额定输出功 率 
        
    VoltageAB float not null,		    	-- 交流电压AB 线电压
    VoltageBC float not null,		    	-- 交流电压BC 线电压
    VoltageCA float not null,		    	-- 交流电压CA 线电压        
        
    SystemTime  int not null,	        	-- 系统时间
	Efficiency  int not null,	       		-- 逆变器效率		
	PowerOnSecond  int not null,	  		-- 开机时间		
	PowerOffSecond  int not null,	  		-- 关机时间 
	MPPT1 float not null,		    		-- 输入总功率
	MPPT2 float not null,		    		-- 输入总功率
	MPPT3 float not null,		    		-- 输入总功率
	MPPT4 float not null		    		-- 输入总功率
		
);
 
-- 索引
create index index_InverterData_Curr on InverterData_Curr(ProjectID asc);

-- 删除索引
-- drop index index_InverterData_Curr on InverterData_Curr ;

-- 查看索引
-- EXEC Sp_helpindex InverterData_Curr
-- show index from InverterData_Curr;


create table InverterData_CurrYear 
(
	IID bigint not null primary key,		-- ID 
	ClltIDDay int not null,             	-- 创建时日期 int
	ClltDaytime datetime  not null,         -- 天 时间
	
	DeviceIndex int not null,         		-- 设备ID
	
	DayTimes int not null, 					-- 采集ID(本次采集的N个点都有相同的ID)
	
	RegionID int not null,         			-- 区 县市ID
	ProjectID int not null,         		-- 项目ID
	SectionID int not null,         		-- 区域ID
	CollectID int not null,         		-- 采集器ID
	ComID int not null,         			-- Com
	Address int not null,         			-- Address	
	
	SN varchar(64)  not null,	            -- SN 序列号
	
	StatusInt  int not null,	          	-- 状态	  
	StatusDesc varchar(1024)  not null,		-- 状态	
			
	PVVoltage float not null,		    -- PV电压
    PVCurrent float not null,		    -- PV电流
    PVOutputPower float not null,		    -- PV A输出功率
    PVVoltageB float not null,		    -- PV电压B
    PVCurrentB float not null,		    -- PV电流B
    PVOutputPowerB float not null,		    -- PV B输出功率
	PVVoltageC float not null,		    -- PV电压C
   	PVCurrentC float not null,		    -- PV电流C
    PVOutputPowerC float not null,		    -- PV C输出功率        
    PVTotalOutputPower float not null,		    -- PV总输出功率  
               
    VoltageA float not null,		    -- 交流电压A 相电压
    CurrentA float not null,		    -- 交流电流 A      
    FrequencyA float not null,		    -- 交流频率 A  
    OutputPowerA float not null,		    -- 交流功率A   
    ROutputPowerA float not null,		    -- 交流无功功率A
    PowerFactorA float not null,		    -- 功率因素A
    VoltageB float not null,		    -- 交流电压B  相电压
    CurrentB float not null,		    -- 交流电流B       
    FrequencyB float not null,		    -- 交流频率B
    OutputPowerB float not null,		    -- 交流功率B 
    ROutputPowerB float not null,		    -- 交流无功功率B 
    PowerFactorB float not null,		    -- 功率因素B   
    VoltageC float not null,		    -- 交流电压C  相电压
    CurrentC float not null,		    -- 交流电流C        
    FrequencyC float not null,		    -- 交流频率C 
    OutputPowerC float not null,		    -- 交流功率C
    ROutputPowerC float not null,		    -- 交流无功功率C        
    PowerFactorC float not null,		    -- 功率因素C
        
    TotalOutputPower float not null,		    -- 交流总功率
    RTotalOutputPower float not null,		    -- 交流总无功功率
    TotalPowerFactor float not null,		    -- 交流总功率因素           
        
    TodayEnergy float not null,		    -- 当天能量
    TotalEnergy float not null,		    -- 总能量
        
    NTodayEnergy float not null,		    -- 反向当天能量
    NTotalEnergy float not null,		    -- 反向总能量
        
    RTodayEnergy float not null,		    -- 当天无功能量
    RTotalEnergy float not null,		    -- 总无功能量
        
    NRTodayEnergy float not null,		    -- 反向当天无功能量
    NRTotalEnergy float not null,		    -- 反向无功总能量        
        
    BtryVoltage float not null,		    -- 蓄电池电压
    BtryCurrent float not null,		    -- 蓄电池电流
    BtryOutputPower float not null,		    -- 蓄电池输出功率  
        
    TemperatureInverter float not null,		    -- Inverter温度    
    TemperatureBooster float not null,		    -- Booster温度
    TemperatureSink float not null,		    -- Sink 散热器       
        
    TodayHour float not null,		    -- 当天发电小时
    TotalHour float not null,		    -- 总发电小时
        
    AGCSetting float not null,		    -- AGC 设置值
    AVCSetting float not null,		    -- AVC 设置值
        
    GroundResistance float not null,		    -- 接地电阻 MΩ
    LeakCurrent float not null,		    -- 漏电流 mA
       
    StringVoltage1 float not null,		    -- 组串 电压
    StringCurrent1 float not null,		    -- 组串 电流        
    StringVoltage2 float not null,		    -- 组串 电压
    StringCurrent2 float not null,		    -- 组串 电流
    StringVoltage3 float not null,		    -- 组串 电压
    StringCurrent3 float not null,		    -- 组串 电流
    StringVoltage4 float not null,		    -- 组串 电压
    StringCurrent4 float not null,		    -- 组串 电流
    StringVoltage5 float not null,		    -- 组串 电压
    StringCurrent5 float not null,		    -- 组串 电流
    StringVoltage6 float not null,		    -- 组串 电压
    StringCurrent6 float not null,		    -- 组串 电流
    StringVoltage7 float not null,		    -- 组串 电压
    StringCurrent7 float not null,		    -- 组串 电流
    StringVoltage8 float not null,		    -- 组串 电压
    StringCurrent8 float not null,		    -- 组串 电流
        
    StringVoltage9 float not null,		    -- 组串 电压 2018-03-13  华旭 东莞 宝胜 阳光组串逆变器 未保存文件
    StringCurrent9 float not null,		    -- 组串 电流
    StringVoltage10 float not null,		    -- 组串 电压
    StringCurrent10 float not null,		    -- 组串 电流
    StringVoltage11 float not null,		    -- 组串 电压
    StringCurrent11 float not null,		    -- 组串 电流
    StringVoltage12 float not null,		    -- 组串 电压
    StringCurrent12 float not null,		    -- 组串 电流
    StringVoltage13 float not null,		    -- 组串 电压
    StringCurrent13 float not null,		    -- 组串 电流
    StringVoltage14 float not null,		    -- 组串 电压
    StringCurrent14 float not null,		    -- 组串 电流
    StringVoltage15 float not null,		    -- 组串 电压
    StringCurrent15 float not null,		    -- 组串 电流
    StringVoltage16 float not null,		    -- 组串 电压
    StringCurrent16 float not null,		    -- 组串 电流
    
    StringVoltage17 float not null,		    -- 组串 电压
    StringCurrent17 float not null,		    -- 组串 电流
    StringVoltage18 float not null,		    -- 组串 电压
    StringCurrent18 float not null,		    -- 组串 电流
    StringVoltage19 float not null,		    -- 组串 电压
    StringCurrent19 float not null,		    -- 组串 电流
    StringVoltage20 float not null,		    -- 组串 电压
    StringCurrent20 float not null,		    -- 组串 电流
    StringVoltage21 float not null,		    -- 组串 电压
    StringCurrent21 float not null,		    -- 组串 电流
    StringVoltage22 float not null,		    -- 组串 电压
    StringCurrent22 float not null,		    -- 组串 电流
    StringVoltage23 float not null,		    -- 组串 电压
    StringCurrent23 float not null,		    -- 组串 电流
    StringVoltage24 float not null,		    -- 组串 电压
    StringCurrent24 float not null,		    -- 组串 电流
                
    MotherLineVoltage float not null,		    -- 母线电压  
        
    GroudVoltage float not null,		    -- 正极对地电 压 
    NGroudVoltage float not null,		    -- 负极对地电 压 
        
    DesignPower float not null,		    -- 额定输出功 率 
        
    VoltageAB float not null,		    -- 交流电压AB 线电压
    VoltageBC float not null,		    -- 交流电压BC 线电压
    VoltageCA float not null,		    -- 交流电压CA 线电压        
        
    SystemTime  int not null,	            				-- 系统时间
	Efficiency  int not null,	            				-- 逆变器效率		
	PowerOnSecond  int not null,	            				-- 开机时间		
	PowerOffSecond  int not null,	            				-- 关机时间 
	MPPT1 float not null,		    -- 输入总功率
	MPPT2 float not null,		    -- 输入总功率
	MPPT3 float not null,		    -- 输入总功率
	MPPT4 float not null		    -- 输入总功率
		
);
 
-- 索引
create index index_InverterData_CurrYear on InverterData_CurrYear(ClltIDDay asc, ProjectID asc);

-- 删除索引
-- drop index index_InverterData_CurrYear on InverterData_CurrYear ;

-- 查看索引
-- EXEC Sp_helpindex InverterData_CurrYear
-- show index from InverterData_CurrYear;



create table InverterRpt_CurrYear
(
	IID bigint not null primary key,		 	-- ID 
	ClltIDDay int not null,             		-- 创建时日期 int
	
	DeviceIndex int not null,         				-- 设备ID
	
	ClltIDYear int not null,             		-- 创建时年 int	
	ClltIDMonth int not null,             		-- 创建时月 int
	ClltIDDayIndex int not null,             	-- 创建时日 int
	
	RegionID int not null,         				-- 区 县市ID
	ProjectID int not null,         			-- 项目ID
	SectionID int not null,         			-- 区域ID
	CollectID int not null,         			-- 采集器ID
	ComID int not null,         				-- Com
	Address int not null,         				-- Address	
	
	TodayEnergy float not null,		    		-- 逆变器部分 当天能量			                    
    TotalEnergy float not null	    			-- 总能量  
);
 
-- 索引
create index index_InverterRpt_CurrYear on InverterRpt_CurrYear(ClltIDDay asc, ProjectID asc);

-- 删除索引
-- drop index index_InverterRpt_CurrYear on InverterRpt_CurrYear ;

-- 查看索引
-- EXEC Sp_helpindex InverterRpt_CurrYear
-- show index from InverterRpt_CurrYear;



create table ProtectData_Curr 
(
	IID bigint not null primary key,		-- ID 
	ClltIDDay int not null,             	-- 创建时日期 int
	ClltDaytime datetime  not null,         -- 天 时间
	
	DeviceIndex int not null,         		-- 设备ID
	
	DayTimes int not null, 					-- 采集ID(本次采集的N个点都有相同的ID)
	
	RegionID int not null,         			-- 区 县市ID
	ProjectID int not null,         		-- 项目ID
	SectionID int not null,         		-- 区域ID
	CollectID int not null,         		-- 采集器ID
	ComID int not null,         			-- Com
	Address int not null,         			-- Address	
	
	SN varchar(64)  not null,	            -- SN 序列号
		
	StatusInt  int not null,	          	-- 状态	  
	StatusDesc varchar(1024)  not null,		-- 状态	
	
	IA float not null,              -- 电流A
    IB float not null,              -- 电流B    
    IC float not null,              -- 电流C 
    Ua float not null,              -- 相电压A   
    Ub float not null,              -- 相电压B
    Uc float not null,		        -- 相电压C
    Uab float not null,             -- 线电压Uab
    Ubc float not null,             -- 线电压Ubc       
    Uca float not null,             -- 线电压Uca
    P float not null,               -- 有功功率
    Q float not null,               -- 无功功率 
    S float not null,		        -- 视在功率
    PF float not null,              -- 功率因素
    F float not null,               -- 频率
        
    -- 遥信 
    YXBreakerCloseStatus int not null,      -- 断路器的合闸
    YXBreakerOpenStatus int not null,       -- 断路器的分闸
    YXManualCarWorkStatus int not null,     -- 手车的工作位置
    YXManualCarTestStatus int not null,	    -- 手车的试验位置
    YXEarthStatus int not null,	            -- 接地刀闸位置 		
	YXSpringSaveEnergyStatus int not null,	-- 弹簧储能位置 
	YXRemoteLocalStatus int not null,       -- 远方就地   		
	YXRepeatCloseStatus int not null,       -- 闭合重合闸   	
	YXHighVoltageCloseStatus int not null,  -- 高压负载开关合闸  	
	YXHighVoltageOpenStatus int not null,   -- 高压负载开关分闸
		
    IA2 float not null,             -- 电流A
    IB2 float not null,             -- 电流B    
    IC2 float not null,             -- 电流C 
    Ua2 float not null,             -- 相电压A   
    Ub2 float not null,             -- 相电压B
    Uc2 float not null,		        -- 相电压C
    Uab2 float not null,            -- 线电压Uab
    Ubc2 float not null,            -- 线电压Ubc       
    Uca2 float not null,            -- 线电压Uca
    P2 float not null,              -- 有功功率
    Q2 float not null,              -- 无功功率 
    S2 float not null,		        -- 视在功率
    PF2 float not null,             -- 功率因素
    F2 float not null,              -- 频率
        
    -- 遥信             
    YXBreakerCloseStatus2 int not null,         -- 断路器的合闸
    YXBreakerOpenStatus2 int not null,          -- 断路器的分闸
    YXManualCarWorkStatus2 int not null,        -- 手车的工作位置
    YXManualCarTestStatus2 int not null,	    -- 手车的试验位置
    YXEarthStatus2 int not null,	            -- 接地刀闸位置 		
	YXSpringSaveEnergyStatus2 int not null,	    -- 弹簧储能位置 
	YXRemoteLocalStatus2 int not null,          -- 远方就地   
	YXRepeatCloseStatus2 int not null,          -- 闭合重合闸   	
	YXHighVoltageCloseStatus2 int not null,     -- 高压负载开关 
	YXHighVoltageOpenStatus2 int not null       -- 高压负载开关分闸
	
);
 
-- 索引
create index index_ProtectData_Curr on ProtectData_Curr(ProjectID asc);

-- 删除索引
-- drop index index_ProtectData_Curr on ProtectData_Curr ;

-- 查看索引
-- EXEC Sp_helpindex ProtectData_Curr
-- show index from ProtectData_Curr;



create table ProtectData_CurrYear 
(
	IID bigint not null primary key,		-- ID 
	ClltIDDay int not null,             	-- 创建时日期 int
	ClltDaytime datetime  not null,         -- 天 时间
	
	DeviceIndex int not null,         		-- 设备ID
	
	DayTimes int not null, 					-- 采集ID(本次采集的N个点都有相同的ID)
	
	RegionID int not null,         			-- 区 县市ID
	ProjectID int not null,         		-- 项目ID
	SectionID int not null,         		-- 区域ID
	CollectID int not null,         		-- 采集器ID
	ComID int not null,         			-- Com
	Address int not null,         			-- Address
	
	SN varchar(64)  not null,	            -- SN 序列号	
	
	StatusInt  int not null,	          	-- 状态	  
	StatusDesc varchar(1024)  not null,		-- 状态	
	
	IA float not null,              -- 电流A
    IB float not null,              -- 电流B    
    IC float not null,              -- 电流C 
    Ua float not null,              -- 相电压A   
    Ub float not null,              -- 相电压B
    Uc float not null,		        -- 相电压C
    Uab float not null,             -- 线电压Uab
    Ubc float not null,             -- 线电压Ubc       
    Uca float not null,             -- 线电压Uca
    P float not null,               -- 有功功率
    Q float not null,               -- 无功功率 
    S float not null,		        -- 视在功率
    PF float not null,              -- 功率因素
    F float not null,               -- 频率
        
    -- 遥信 
    YXBreakerCloseStatus int not null,      -- 断路器的合闸
    YXBreakerOpenStatus int not null,       -- 断路器的分闸
    YXManualCarWorkStatus int not null,     -- 手车的工作位置
    YXManualCarTestStatus int not null,	    -- 手车的试验位置
    YXEarthStatus int not null,	            -- 接地刀闸位置 		
	YXSpringSaveEnergyStatus int not null,	-- 弹簧储能位置 
	YXRemoteLocalStatus int not null,       -- 远方就地   		
	YXRepeatCloseStatus int not null,       -- 闭合重合闸   	
	YXHighVoltageCloseStatus int not null,  -- 高压负载开关合闸  	
	YXHighVoltageOpenStatus int not null,   -- 高压负载开关分闸
		
    IA2 float not null,             -- 电流A
    IB2 float not null,             -- 电流B    
    IC2 float not null,             -- 电流C 
    Ua2 float not null,             -- 相电压A   
    Ub2 float not null,             -- 相电压B
    Uc2 float not null,		        -- 相电压C
    Uab2 float not null,            -- 线电压Uab
    Ubc2 float not null,            -- 线电压Ubc       
    Uca2 float not null,            -- 线电压Uca
    P2 float not null,              -- 有功功率
    Q2 float not null,              -- 无功功率 
    S2 float not null,		        -- 视在功率
    PF2 float not null,             -- 功率因素
    F2 float not null,              -- 频率
        
    -- 遥信             
    YXBreakerCloseStatus2 int not null,         -- 断路器的合闸
    YXBreakerOpenStatus2 int not null,          -- 断路器的分闸
    YXManualCarWorkStatus2 int not null,        -- 手车的工作位置
    YXManualCarTestStatus2 int not null,	    -- 手车的试验位置
    YXEarthStatus2 int not null,	            -- 接地刀闸位置 		
	YXSpringSaveEnergyStatus2 int not null,	    -- 弹簧储能位置 
	YXRemoteLocalStatus2 int not null,          -- 远方就地   
	YXRepeatCloseStatus2 int not null,          -- 闭合重合闸   	
	YXHighVoltageCloseStatus2 int not null,     -- 高压负载开关 
	YXHighVoltageOpenStatus2 int not null      -- 高压负载开关分闸
	
);

-- 索引
create index index_ProtectData_CurrYear on ProtectData_CurrYear(ClltIDDay asc, ProjectID asc);

-- 删除索引
-- drop index index_ProtectData_CurrYear on ProtectData_CurrYear ;

-- 查看索引
-- EXEC Sp_helpindex ProtectData_CurrYear
-- show index from ProtectData_CurrYear;



create table EMSData_Curr 
(
	IID bigint not null primary key,		-- ID 
	ClltIDDay int not null,             	-- 创建时日期 int
	ClltDaytime datetime  not null,         -- 天 时间
	
	DeviceIndex int not null,         		-- 设备ID	
	
	DayTimes int not null, 					-- 采集ID(本次采集的N个点都有相同的ID)
	
	RegionID int not null,         			-- 区 县市ID
	ProjectID int not null,         		-- 项目ID
	SectionID int not null,         		-- 区域ID
	CollectID int not null,         		-- 采集器ID
	ComID int not null,         			-- Com
	Address int not null,         			-- Address	
	
	SN varchar(64)  not null,	            -- SN 序列号	
	
	StatusInt  int not null,	          	-- 状态	  
	StatusDesc varchar(1024)  not null,		-- 状态	
	
	Irradiance float not null,      -- flaot 值 光照瞬时值 w/m2
    Temperature float not null,     -- flaot 值
    PVTemperature float not null,   -- flaot 值
    WindSpeed float not null,       -- flaot 值
    WindDirect float not null,      -- flaot 值
        
    Humidity float not null,        -- 湿度
        
    Radiation float not null       -- 辐射值 光照能量 mj/m2
	
);
 
-- 索引
create index index_EMSData_Curr on EMSData_Curr(ProjectID asc);

-- 删除索引
-- drop index index_EMSData_Curr on EMSData_Curr ;

-- 查看索引
-- EXEC Sp_helpindex EMSData_Curr
-- show index from EMSData_Curr;


create table EMSData_CurrYear 
(
	IID bigint not null primary key,		-- ID 
	ClltIDDay int not null,             	-- 创建时日期 int
	ClltDaytime datetime  not null,         -- 天 时间
	
	DeviceIndex int not null,         		-- 设备ID
	
	DayTimes int not null, 					-- 采集ID(本次采集的N个点都有相同的ID)
	
	RegionID int not null,         			-- 区 县市ID
	ProjectID int not null,         		-- 项目ID
	SectionID int not null,         		-- 区域ID
	CollectID int not null,         		-- 采集器ID
	ComID int not null,         			-- Com
	Address int not null,         			-- Address
	
	SN varchar(64)  not null,	            -- SN 序列号			
	
	StatusInt  int not null,	          	-- 状态	  
	StatusDesc varchar(1024)  not null,		-- 状态	
	
	Irradiance float not null,      -- flaot 值 光照瞬时值 w/m2
    Temperature float not null,     -- flaot 值
    PVTemperature float not null,   -- flaot 值
    WindSpeed float not null,       -- flaot 值
    WindDirect float not null,      -- flaot 值
        
    Humidity float not null,        -- 湿度
        
    Radiation float not null       -- 辐射值 光照能量 mj/m2
	
);

-- 索引
create index index_EMSData_CurrYear on EMSData_CurrYear(ClltIDDay asc, ProjectID asc);

-- 删除索引
-- drop index index_EMSData_CurrYear on EMSData_CurrYear ;

-- 查看索引
-- EXEC Sp_helpindex EMSData_CurrYear
-- show index from EMSData_CurrYear;



create table EMSRpt_CurrYear
(
	IID bigint not null primary key,		 	-- ID 
	ClltIDDay int not null,             		-- 创建时日期 int
	
	DeviceIndex int not null,         				-- 设备ID
	
	ClltIDYear int not null,             		-- 创建时年 int	
	ClltIDMonth int not null,             		-- 创建时月 int
	ClltIDDayIndex int not null,             	-- 创建时日 int
	
	RegionID int not null,         				-- 区 县市ID
	ProjectID int not null,         			-- 项目ID
	SectionID int not null,         			-- 区域ID
	CollectID int not null,         			-- 采集器ID
	ComID int not null,         				-- Com
	Address int not null,         				-- Address	
			                    
    Radiation float not null	    			-- 辐射值  
);
 
-- 索引
create index index_EMSRpt_CurrYear on EMSRpt_CurrYear(ClltIDDay asc, ProjectID asc);

-- 删除索引
-- drop index index_EMSRpt_CurrYear on EMSRpt_CurrYear ;

-- 查看索引
-- EXEC Sp_helpindex EMSRpt_CurrYear
-- show index from EMSRpt_CurrYear;


create table IOData_Curr 
(
	IID bigint not null primary key,		-- ID 
	ClltIDDay int not null,             	-- 创建时日期 int
	ClltDaytime datetime  not null,         -- 天 时间
	
	DeviceIndex int not null,         		-- 设备ID
	
	DayTimes int not null, 					-- 采集ID(本次采集的N个点都有相同的ID)
	
	RegionID int not null,         			-- 区 县市ID
	ProjectID int not null,         		-- 项目ID
	SectionID int not null,         		-- 区域ID
	CollectID int not null,         		-- 采集器ID
	ComID int not null,         			-- Com
	Address int not null,         			-- Address	
	
	SN varchar(64)  not null,	            -- SN 序列号	
	
	StatusInt  int not null,	          	-- 状态	  
	StatusDesc varchar(1024)  not null,		-- 状态	
	
	InputIO0 int not null,          -- 输入IO 0 1= 闭合 0=开
    InputIO1 int not null,          -- 输入IO 1
    InputIO2 int not null,          -- 输入IO 2
    InputIO3 int not null,          -- 输入IO 3
    InputIO4 int not null,          -- 输入IO 4
    InputIO5 int not null,          -- 输入IO 5
    InputIO6 int not null,          -- 输入IO 6
    InputIO7 int not null,          -- 输入IO 7
    InputIO8 int not null,          -- 输入IO 0 1= 闭合 0=开
    InputIO9 int not null,          -- 输入IO 1
    InputIO10 int not null,         -- 输入IO 2
    InputIO11 int not null,         -- 输入IO 3
    InputIO12 int not null,         -- 输入IO 4
    InputIO13 int not null,         -- 输入IO 5
    InputIO14 int not null,         -- 输入IO 6
    InputIO15 int not null,         -- 输入IO 7
        
    OutputIO0 int not null,         -- 输出IO 0  1= 闭合 0=开
    OutputIO1 int not null,         -- 输出IO 1
    OutputIO2 int not null,         -- 输出IO 2
    OutputIO3 int not null,         -- 输出IO 3
    OutputIO4 int not null,         -- 输出IO 4
    OutputIO5 int not null,         -- 输出IO 5
    OutputIO6 int not null,         -- 输出IO 6
    OutputIO7 int not null,         -- 输出IO 7
    OutputIO8 int not null,         -- 输出IO 0  1= 闭合 0=开
    OutputIO9 int not null,         -- 输出IO 1
    OutputIO10 int not null,        -- 输出IO 2
    OutputIO11 int not null,        -- 输出IO 3
    OutputIO12 int not null,        -- 输出IO 4
    OutputIO13 int not null,        -- 输出IO 5
    OutputIO14 int not null,        -- 输出IO 6
    OutputIO15 int not null         -- 输出IO 7
	
);
 
-- 索引
create index index_IOData_Curr on IOData_Curr(ProjectID asc);

-- 删除索引
-- drop index index_IOData_Curr on IOData_Curr ;

-- 查看索引
-- EXEC Sp_helpindex IOData_Curr
-- show index from IOData_Curr;



create table IOData_CurrYear 
(
	IID bigint not null primary key,		-- ID 
	ClltIDDay int not null,             	-- 创建时日期 int
	ClltDaytime datetime  not null,         -- 天 时间
	
	DeviceIndex int not null,         		-- 设备ID
	
	DayTimes int not null, 					-- 采集ID(本次采集的N个点都有相同的ID)
	
	RegionID int not null,         			-- 区 县市ID
	ProjectID int not null,         		-- 项目ID
	SectionID int not null,         		-- 区域ID
	CollectID int not null,         		-- 采集器ID
	ComID int not null,         			-- Com
	Address int not null,         			-- Address	
	
	SN varchar(64)  not null,	            -- SN 序列号	
	
	StatusInt  int not null,	          	-- 状态	  
	StatusDesc varchar(1024)  not null,		-- 状态	
	
	InputIO0 int not null,          -- 输入IO 0 1= 闭合 0=开
    InputIO1 int not null,          -- 输入IO 1
    InputIO2 int not null,          -- 输入IO 2
    InputIO3 int not null,          -- 输入IO 3
    InputIO4 int not null,          -- 输入IO 4
    InputIO5 int not null,          -- 输入IO 5
    InputIO6 int not null,          -- 输入IO 6
    InputIO7 int not null,          -- 输入IO 7
    InputIO8 int not null,          -- 输入IO 0 1= 闭合 0=开
    InputIO9 int not null,          -- 输入IO 1
    InputIO10 int not null,         -- 输入IO 2
    InputIO11 int not null,         -- 输入IO 3
    InputIO12 int not null,         -- 输入IO 4
    InputIO13 int not null,         -- 输入IO 5
    InputIO14 int not null,         -- 输入IO 6
    InputIO15 int not null,         -- 输入IO 7
        
    OutputIO0 int not null,         -- 输出IO 0  1= 闭合 0=开
    OutputIO1 int not null,         -- 输出IO 1
    OutputIO2 int not null,         -- 输出IO 2
    OutputIO3 int not null,         -- 输出IO 3
    OutputIO4 int not null,         -- 输出IO 4
    OutputIO5 int not null,         -- 输出IO 5
    OutputIO6 int not null,         -- 输出IO 6
    OutputIO7 int not null,         -- 输出IO 7
    OutputIO8 int not null,         -- 输出IO 0  1= 闭合 0=开
    OutputIO9 int not null,         -- 输出IO 1
    OutputIO10 int not null,        -- 输出IO 2
    OutputIO11 int not null,        -- 输出IO 3
    OutputIO12 int not null,        -- 输出IO 4
    OutputIO13 int not null,        -- 输出IO 5
    OutputIO14 int not null,        -- 输出IO 6
    OutputIO15 int not null        	-- 输出IO 7
	
);
 
-- 索引
create index index_IOData_CurrYear on IOData_CurrYear(ClltIDDay asc, ProjectID asc);

-- 删除索引
-- drop index index_IOData_CurrYear on IOData_CurrYear ;

-- 查看索引
-- EXEC Sp_helpindex IOData_CurrYear
-- show index from IOData_CurrYear;



create table EPMData_Curr 
(
	IID bigint not null primary key,		-- ID 
	ClltIDDay int not null,             	-- 创建时日期 int
	ClltDaytime datetime  not null,         -- 天 时间
	
	DeviceIndex int not null,         		-- 设备ID
	
	DayTimes int not null, 					-- 采集ID(本次采集的N个点都有相同的ID)
	
	RegionID int not null,         			-- 区 县市ID
	ProjectID int not null,         		-- 项目ID
	SectionID int not null,         		-- 区域ID
	CollectID int not null,         		-- 采集器ID
	ComID int not null,         			-- Com
	Address int not null,         			-- Address
	
	SN varchar(64)  not null,	            -- SN 序列号		
	
	StatusInt  int not null,	          	-- 状态	  
	StatusDesc varchar(1024)  not null,		-- 状态	
	
	Voltage1 float not null,		-- 线1电压
	Voltage2 float not null,		-- 线2电压
	Voltage3 float not null,		-- 线3电压		
	Current1 float not null,		-- 线1电流
	Current2 float not null,		-- 线2电流
	Current3 float not null,		-- 线3电流	
	Frequency1 float not null,		-- 线1频率
	Frequency2 float not null,		-- 线2频率
	Frequency3 float not null,		-- 线3频率			
		
	ActivePower1 float not null,	-- 线1有功功率 w
	ActivePower2 float not null,	-- 线2有功功率
	ActivePower3 float not null,	-- 线3有功功率	
		
	Var1 float not null,			-- 线1无功功率 var
	Var2 float not null,			-- 线2无功功率
	Var3 float not null,			-- 线3无功功率
				
	PowerFactor1 float not null,	-- 线1功率因数 PF
	PowerFactor2 float not null,	-- 线2功率因数
	PowerFactor3 float not null,	-- 线3功率因数			
		
	VSum float not null,			-- 总电压
	ASum float not null,			-- 总电流
	WSum float not null,			-- 总功率
	VarSum float not null,			-- 总无功功率
	PFSum float not null,			-- 总功率因数		
	WhSum float not null,           -- 总有功发电量       WhSum		
	VarhSum float not null,         -- 总无功发电量       VarhSum	
	WhNSum float not null,          -- 总有功发电量-      WhSum -	
	VarhNSum float not null,        -- 总无功发电量-      VarhSum -	
		
	Voltage1N float not null,		-- 相1电压
	Voltage2N float not null,		-- 相2电压
	Voltage3N float not null,		-- 相3电压	
		
	THDV1 float not null,			-- 总谐波 V1
	THDV2 float not null,			-- 总谐波 V2
	THDV3 float not null,			-- 总谐波 V3
    THDA1 float not null,			-- 总谐波 A1
	THDA2 float not null,			-- 总谐波 A2
	THDA3 float not null,			-- 总谐波 A3
		
	WhToday float not null,         -- 当天有功发电量       WhSum		(一般电表不会当天统计，需要自己计算得到)
	VarhToday float not null,       -- 当天无功发电量       VarhSum	
	WhNToday float not null,        -- 当天有功发电量-      WhSum -	
	VarhNToday float not null,      -- 当天无功发电量-      VarhSum -
		
	WhJian float not null,          -- 总有功发电量       当前 尖	
	WhFeng float not null,          -- 总有功发电量       当前 峰	
	WhPing float not null,          -- 总有功发电量       当前 平	
	WhGu float not null,            -- 总有功发电量         当前  谷	
		
	WhNJian float not null,         -- 总有功发电量-       当前 尖	
	WhNFeng float not null,         -- 总有功发电量-       当前 峰	
	WhNPing float not null,         -- 总有功发电量-       当前 平	
	WhNGu float not null,           -- 总有功发电量 -        当前  谷	
		
	WhLastMonth float not null,           	-- 总有功发电量       上月 尖
	WhLastMonthJian float not null,         -- 总有功发电量       上月 尖	
	WhLastMonthFeng float not null,         -- 总有功发电量       上月 峰	
	WhLastMonthPing float not null,         -- 总有功发电量       上月 平	
	WhLastMonthGu float not null,           -- 总有功发电量         上月  谷	
		
	WhNLastMonth float not null,            -- 总有功发电量       上月 尖
	WhNLastMonthJian float not null,        -- 总有功发电量-       上月 尖	
	WhNLastMonthFeng float not null,        -- 总有功发电量-       上月 峰	
	WhNLastMonthPing float not null,        -- 总有功发电量-       上月 平	
	WhNLastMonthGu float not null,          -- 总有功发电量 -        上月  谷		
		
	DI0 int not null,                       -- DI
	DI1 int not null,                       -- DI
	DI2 int not null,                       -- DI
	DI3 int not null,                       -- DI
		
	DO0 int not null,                       -- DO
	DO1 int not null,                       -- DO
	DO2 int not null,                       -- DO
	DO3 int not null                        -- DO 
	
);
 
-- 索引
create index index_EPMData_Curr on EPMData_Curr(ProjectID asc);

-- 删除索引
-- drop index index_EPMData_Curr on EPMData_Curr ;

-- 查看索引
-- EXEC Sp_helpindex EPMData_Curr
-- show index from EPMData_Curr;


create table EPMData_CurrYear 
(
	IID bigint not null primary key,		-- ID 
	ClltIDDay int not null,             	-- 创建时日期 int
	ClltDaytime datetime  not null,         -- 天 时间
	
	DeviceIndex int not null,         		-- 设备ID
	
	DayTimes int not null, 					-- 采集ID(本次采集的N个点都有相同的ID)
	
	RegionID int not null,         			-- 区 县市ID
	ProjectID int not null,         		-- 项目ID
	SectionID int not null,         		-- 区域ID
	CollectID int not null,         		-- 采集器ID
	ComID int not null,         			-- Com
	Address int not null,         			-- Address	
	
	SN varchar(64)  not null,	            -- SN 序列号
		
	StatusInt  int not null,	          	-- 状态	  
	StatusDesc varchar(1024)  not null,		-- 状态	
	
	Voltage1 float not null,		-- 线1电压
	Voltage2 float not null,		-- 线2电压
	Voltage3 float not null,		-- 线3电压		
	Current1 float not null,		-- 线1电流
	Current2 float not null,		-- 线2电流
	Current3 float not null,		-- 线3电流	
	Frequency1 float not null,		-- 线1频率
	Frequency2 float not null,		-- 线2频率
	Frequency3 float not null,		-- 线3频率			
		
	ActivePower1 float not null,	-- 线1有功功率 w
	ActivePower2 float not null,	-- 线2有功功率
	ActivePower3 float not null,	-- 线3有功功率	
		
	Var1 float not null,			-- 线1无功功率 var
	Var2 float not null,			-- 线2无功功率
	Var3 float not null,			-- 线3无功功率
				
	PowerFactor1 float not null,	-- 线1功率因数 PF
	PowerFactor2 float not null,	-- 线2功率因数
	PowerFactor3 float not null,	-- 线3功率因数			
		
	VSum float not null,			-- 总电压
	ASum float not null,			-- 总电流
	WSum float not null,			-- 总功率
	VarSum float not null,			-- 总无功功率
	PFSum float not null,			-- 总功率因数		
	WhSum float not null,           -- 总有功发电量       WhSum		
	VarhSum float not null,         -- 总无功发电量       VarhSum	
	WhNSum float not null,          -- 总有功发电量-      WhSum -	
	VarhNSum float not null,        -- 总无功发电量-      VarhSum -	
		
	Voltage1N float not null,		-- 相1电压
	Voltage2N float not null,		-- 相2电压
	Voltage3N float not null,		-- 相3电压	
		
	THDV1 float not null,			-- 总谐波 V1
	THDV2 float not null,			-- 总谐波 V2
	THDV3 float not null,			-- 总谐波 V3
    THDA1 float not null,			-- 总谐波 A1
	THDA2 float not null,			-- 总谐波 A2
	THDA3 float not null,			-- 总谐波 A3
		
	WhToday float not null,         -- 当天有功发电量       WhSum		(一般电表不会当天统计，需要自己计算得到)
	VarhToday float not null,       -- 当天无功发电量       VarhSum	
	WhNToday float not null,        -- 当天有功发电量-      WhSum -	
	VarhNToday float not null,      -- 当天无功发电量-      VarhSum -
		
	WhJian float not null,          -- 总有功发电量       当前 尖	
	WhFeng float not null,          -- 总有功发电量       当前 峰	
	WhPing float not null,          -- 总有功发电量       当前 平	
	WhGu float not null,            -- 总有功发电量         当前  谷	
		
	WhNJian float not null,         -- 总有功发电量-       当前 尖	
	WhNFeng float not null,         -- 总有功发电量-       当前 峰	
	WhNPing float not null,         -- 总有功发电量-       当前 平	
	WhNGu float not null,           -- 总有功发电量 -        当前  谷	
		
	WhLastMonth float not null,           	-- 总有功发电量       上月 尖
	WhLastMonthJian float not null,         -- 总有功发电量       上月 尖	
	WhLastMonthFeng float not null,         -- 总有功发电量       上月 峰	
	WhLastMonthPing float not null,         -- 总有功发电量       上月 平	
	WhLastMonthGu float not null,           -- 总有功发电量         上月  谷	
		
	WhNLastMonth float not null,            -- 总有功发电量       上月 尖
	WhNLastMonthJian float not null,        -- 总有功发电量-       上月 尖	
	WhNLastMonthFeng float not null,        -- 总有功发电量-       上月 峰	
	WhNLastMonthPing float not null,        -- 总有功发电量-       上月 平	
	WhNLastMonthGu float not null,          -- 总有功发电量 -        上月  谷		
		
	DI0 int not null,                       -- DI
	DI1 int not null,                       -- DI
	DI2 int not null,                       -- DI
	DI3 int not null,                       -- DI
		
	DO0 int not null,                       -- DO
	DO1 int not null,                       -- DO
	DO2 int not null,                       -- DO
	DO3 int not null                        -- DO 
	
);
 
-- 索引
create index index_EPMData_CurrYear on EPMData_CurrYear(ClltIDDay asc, ProjectID asc);

-- 删除索引
-- drop index index_EPMData_CurrYear on EPMData_CurrYear ;

-- 查看索引
-- EXEC Sp_helpindex EPMData_CurrYear
-- show index from EPMData_CurrYear;



create table EPMRpt_CurrYear 
(
	IID bigint not null primary key,		 	-- ID 
	ClltIDDay int not null,             		-- 创建时日期 int
	
	DeviceIndex int not null,         			-- 设备ID
	
	ClltIDYear int not null,             		-- 创建时年 int	
	ClltIDMonth int not null,             		-- 创建时月 int
	ClltIDDayIndex int not null,             	-- 创建时日 int
	
	RegionID int not null,         				-- 区 县市ID
	ProjectID int not null,         			-- 项目ID
	SectionID int not null,         			-- 区域ID
	CollectID int not null,         			-- 采集器ID
	ComID int not null,         				-- Com
	Address int not null,         				-- Address	
	
	WhToday float not null,         -- 当天有功发电量       WhSum		(一般电表不会当天统计，需要自己计算得到)
	VarhToday float not null,       -- 当天无功发电量       VarhSum	
	WhNToday float not null,        -- 当天有功发电量-      WhSum -	
	VarhNToday float not null,      -- 当天无功发电量-      VarhSum -
		
	WhSum float not null,           -- 总有功发电量       WhSum		
	VarhSum float not null,         -- 总无功发电量       VarhSum	
	WhNSum float not null,          -- 总有功发电量-      WhSum -	
	VarhNSum float not null        	-- 总无功发电量-      VarhSum -
	
);
 
-- 索引
create index index_EPMRpt_CurrYear on EPMRpt_CurrYear(ClltIDDay asc, ProjectID asc);

-- 删除索引
-- drop index index_EPMRpt_CurrYear on EPMRpt_CurrYear ;

-- 查看索引
-- EXEC Sp_helpindex EPMRpt_CurrYear
-- show index from EPMRpt_CurrYear;



create table PVSData_Curr 
(
	IID bigint not null primary key,		-- ID 
	ClltIDDay int not null,             	-- 创建时日期 int
	ClltDaytime datetime  not null,         -- 天 时间
	
	DeviceIndex int not null,         		-- 设备ID
	
	DayTimes int not null, 					-- 采集ID(本次采集的N个点都有相同的ID)
	
	RegionID int not null,         			-- 区 县市ID
	ProjectID int not null,         		-- 项目ID
	SectionID int not null,         		-- 区域ID
	CollectID int not null,         		-- 采集器ID
	ComID int not null,         			-- Com
	Address int not null,         			-- Address
	
	SN varchar(64)  not null,	            -- SN 序列号		
	
	StatusInt  int not null,	          	-- 状态	  
	StatusDesc varchar(1024)  not null,		-- 状态	
	
	MaxChannel int not null,				-- 通道数
	DCMotherVoltage float not null, 		-- 直流母线电压
	Temperature float not null,				-- 温度
	MaxCurrent float not null,				-- 最大电流
	MinCurrent float not null,				-- 最小电流
	AvgCurrent float not null,				-- 平均电流
	TotalCurrent float not null,			-- 总电流	
	
	Current1 float not null,		-- 第1路电流
	Current2 float not null,		-- 第2路电流
	Current3 float not null,		-- 第3路电流
	Current4 float not null,		-- 第4路电流
	Current5 float not null,		-- 第5路电流
	Current6 float not null,		-- 第6路电流
	Current7 float not null,		-- 第7路电流
	Current8 float not null,		-- 第8路电流
	Current9 float not null,		-- 第9路电流
	Current10 float not null,		-- 第10路电流
	Current11 float not null,		-- 第11路电流
	Current12 float not null,		-- 第12路电流
	Current13 float not null,		-- 第13路电流
	Current14 float not null,		-- 第14路电流
	Current15 float not null,		-- 第15路电流
	Current16 float not null,		-- 第16路电流
	
	Current17 float not null,		-- 第17路电流
	Current18 float not null,		-- 第18路电流
	Current19 float not null,		-- 第19路电流
	Current20 float not null,		-- 第20路电流
	Current21 float not null,		-- 第21路电流
	Current22 float not null,		-- 第22路电流
	Current23 float not null,		-- 第23路电流
	Current24 float not null		-- 第24路电流	
);
 
-- 索引
create index index_PVSData_Curr on PVSData_Curr(ProjectID asc);

-- 删除索引
-- drop index index_PVSData_Curr on PVSData_Curr ;

-- 查看索引
-- EXEC Sp_helpindex PVSData_Curr
-- show index from PVSData_Curr;


create table PVSData_CurrYear 
(
	IID bigint not null primary key,		-- ID 
	ClltIDDay int not null,             	-- 创建时日期 int
	ClltDaytime datetime  not null,         -- 天 时间
	
	DeviceIndex int not null,         		-- 设备ID
	
	DayTimes int not null, 					-- 采集ID(本次采集的N个点都有相同的ID)
	
	RegionID int not null,         			-- 区 县市ID
	ProjectID int not null,         		-- 项目ID
	SectionID int not null,         		-- 区域ID
	CollectID int not null,         		-- 采集器ID
	ComID int not null,         			-- Com
	Address int not null,         			-- Address	
	
	SN varchar(64)  not null,	            -- SN 序列号
		
	StatusInt  int not null,	          	-- 状态	  
	StatusDesc varchar(1024)  not null,		-- 状态		
	
	MaxChannel int not null,				-- 通道数
	DCMotherVoltage float not null, 		-- 直流母线电压
	Temperature float not null,				-- 温度
	MaxCurrent float not null,				-- 最大电流
	MinCurrent float not null,				-- 最小电流
	AvgCurrent float not null,				-- 平均电流
	TotalCurrent float not null,			-- 总电流
	
	Current1 float not null,		-- 第1路电流
	Current2 float not null,		-- 第2路电流
	Current3 float not null,		-- 第3路电流
	Current4 float not null,		-- 第4路电流
	Current5 float not null,		-- 第5路电流
	Current6 float not null,		-- 第6路电流
	Current7 float not null,		-- 第7路电流
	Current8 float not null,		-- 第8路电流
	Current9 float not null,		-- 第9路电流
	Current10 float not null,		-- 第10路电流
	Current11 float not null,		-- 第11路电流
	Current12 float not null,		-- 第12路电流
	Current13 float not null,		-- 第13路电流
	Current14 float not null,		-- 第14路电流
	Current15 float not null,		-- 第15路电流
	Current16 float not null,		-- 第16路电流
	
	Current17 float not null,		-- 第17路电流
	Current18 float not null,		-- 第18路电流
	Current19 float not null,		-- 第19路电流
	Current20 float not null,		-- 第20路电流
	Current21 float not null,		-- 第21路电流
	Current22 float not null,		-- 第22路电流
	Current23 float not null,		-- 第23路电流
	Current24 float not null		-- 第24路电流
);
 
-- 索引
create index index_PVSData_CurrYear on PVSData_CurrYear(ClltIDDay asc, ProjectID asc);

-- 删除索引
-- drop index index_PVSData_CurrYear on PVSData_CurrYear ;

-- 查看索引
-- EXEC Sp_helpindex PVSData_CurrYear
-- show index from PVSData_CurrYear;


create table ACPVSData_Curr   
(
	IID bigint not null primary key,		-- ID 
	ClltIDDay int not null,             	-- 创建时日期 int
	ClltDaytime datetime  not null,         -- 天 时间
	
	DeviceIndex int not null,         		-- 设备ID	
	
	DayTimes int not null, 					-- 采集ID(本次采集的N个点都有相同的ID)
	
	RegionID int not null,         			-- 区 县市ID
	ProjectID int not null,         		-- 项目ID
	SectionID int not null,         		-- 区域ID
	CollectID int not null,         		-- 采集器ID
	ComID int not null,         			-- Com
	Address int not null,         			-- Address	
	
	SN varchar(64)  not null,	            -- SN 序列号	
	
	StatusInt  int not null,	          	-- 状态	  
	StatusDesc varchar(1024)  not null,		-- 状态	
		
	Temperature float not null,		-- 温度
		
	VoltageL1 float not null,		-- 线1电压
	VoltageL2 float not null,		-- 线2电压
	VoltageL3 float not null,		-- 线3电压	
		
	Voltage1 float not null,		-- 相1电压
	Voltage2 float not null,		-- 相2电压
	Voltage3 float not null,		-- 相3电压	
			
	Current1 float not null,		-- 线1电流
	Current2 float not null,		-- 线2电流
	Current3 float not null,		-- 线3电流	
		
	Frequency1 float not null,		-- 线1频率
	Frequency2 float not null,		-- 线2频率
	Frequency3 float not null,		-- 线3频率			
		
	ActivePower1 float not null,	-- 线1有功功率 w
	ActivePower2 float not null,	-- 线2有功功率
	ActivePower3 float not null,	-- 线3有功功率	
		
	Var1 float not null,			-- 线1无功功率 var
	Var2 float not null,			-- 线2无功功率
	Var3 float not null,			-- 线3无功功率
				
	PowerFactor1 float not null,	-- 线1功率因数 PF
	PowerFactor2 float not null,	-- 线2功率因数
	PowerFactor3 float not null,	-- 线3功率因数		
		
	WSum float not null,			-- 总功率
	VarSum float not null,			-- 总无功功率
	PFSum float not null,			-- 总功率因数        
        
	Currentl1 float not null,		-- 第1路电流
	Currentl2 float not null,		-- 第2路电流
	Currentl3 float not null,		-- 第3路电流
	Currentl4 float not null,		-- 第4路电流
	Currentl5 float not null,		-- 第5路电流
	Currentl6 float not null,		-- 第6路电流
	Currentl7 float not null,		-- 第7路电流
	Currentl8 float not null,		-- 第8路电流
	Currentl9 float not null,		-- 第9路电流
	Currentl10 float not null,		-- 第10路电流
	Currentl11 float not null,		-- 第11路电流
	Currentl12 float not null,		-- 第12路电流
	Currentl13 float not null,		-- 第13路电流
	Currentl14 float not null,		-- 第14路电流
	Currentl15 float not null,		-- 第15路电流
	Currentl16 float not null,		-- 第16路电流
	Currentl17 float not null,		-- 第17路电流
	Currentl18 float not null,		-- 第18路电流
	Currentl19 float not null,		-- 第19路电流
	Currentl20 float not null,		-- 第20路电流
	Currentl21 float not null,		-- 第21路电流
	Currentl22 float not null,		-- 第22路电流
	Currentl23 float not null,		-- 第23路电流
	Currentl24 float not null,		-- 第24路电流
		
	InputIO1 int not null,          -- 输入IO 1  0 1= 闭合 0=开
    InputIO2 int not null,          -- 输入IO 2
    InputIO3 int not null,          -- 输入IO 3
    InputIO4 int not null,          -- 输入IO 4
    InputIO5 int not null,          -- 输入IO 5
    InputIO6 int not null,          -- 输入IO 6
    InputIO7 int not null,          -- 输入IO 7
    InputIO8 int not null,          -- 输入IO 8
    InputIO9 int not null,          -- 输入IO 9
    InputIO10 int not null,         -- 输入IO 10
    InputIO11 int not null,         -- 输入IO 11
    InputIO12 int not null,         -- 输入IO 12
    InputIO13 int not null,         -- 输入IO 13
    InputIO14 int not null,         -- 输入IO 14
    InputIO15 int not null,         -- 输入IO 15
    InputIO16 int not null,         -- 输入IO 16
    InputIO17 int not null,         -- 输入IO 17
    InputIO18 int not null,         -- 输入IO 18
    InputIO19 int not null,         -- 输入IO 19
    InputIO20 int not null,         -- 输入IO 20
    InputIO21 int not null,         -- 输入IO 21
    InputIO22 int not null,         -- 输入IO 22
    InputIO23 int not null,         -- 输入IO 23
    InputIO24 int not null         	-- 输入IO 24
    
);
 
-- 索引
create index index_ACPVSData_Curr on ACPVSData_Curr(ProjectID asc);

-- 删除索引
-- drop index index_ACPVSData_Curr on ACPVSData_Curr ;

-- 查看索引
-- EXEC Sp_helpindex ACPVSData_Curr
-- show index from ACPVSData_Curr;


create table ACPVSData_CurrYear   
(
	IID bigint not null primary key,		-- ID 
	ClltIDDay int not null,             	-- 创建时日期 int
	ClltDaytime datetime  not null,         -- 天 时间
	
	DeviceIndex int not null,         		-- 设备ID	
	
	DayTimes int not null, 					-- 采集ID(本次采集的N个点都有相同的ID)
	
	RegionID int not null,         			-- 区 县市ID
	ProjectID int not null,         		-- 项目ID
	SectionID int not null,         		-- 区域ID
	CollectID int not null,         		-- 采集器ID
	ComID int not null,         			-- Com
	Address int not null,         			-- Address	
	
	SN varchar(64)  not null,	            -- SN 序列号	
	
	StatusInt  int not null,	          	-- 状态	  
	StatusDesc varchar(1024)  not null,		-- 状态	
		
	Temperature float not null,		-- 温度
		
	VoltageL1 float not null,		-- 线1电压
	VoltageL2 float not null,		-- 线2电压
	VoltageL3 float not null,		-- 线3电压	
		
	Voltage1 float not null,		-- 相1电压
	Voltage2 float not null,		-- 相2电压
	Voltage3 float not null,		-- 相3电压	
			
	Current1 float not null,		-- 线1电流
	Current2 float not null,		-- 线2电流
	Current3 float not null,		-- 线3电流	
		
	Frequency1 float not null,		-- 线1频率
	Frequency2 float not null,		-- 线2频率
	Frequency3 float not null,		-- 线3频率			
		
	ActivePower1 float not null,	-- 线1有功功率 w
	ActivePower2 float not null,	-- 线2有功功率
	ActivePower3 float not null,	-- 线3有功功率	
		
	Var1 float not null,			-- 线1无功功率 var
	Var2 float not null,			-- 线2无功功率
	Var3 float not null,			-- 线3无功功率
				
	PowerFactor1 float not null,	-- 线1功率因数 PF
	PowerFactor2 float not null,	-- 线2功率因数
	PowerFactor3 float not null,	-- 线3功率因数		
		
	WSum float not null,			-- 总功率
	VarSum float not null,			-- 总无功功率
	PFSum float not null,			-- 总功率因数        
        
	Currentl1 float not null,		-- 第1路电流
	Currentl2 float not null,		-- 第2路电流
	Currentl3 float not null,		-- 第3路电流
	Currentl4 float not null,		-- 第4路电流
	Currentl5 float not null,		-- 第5路电流
	Currentl6 float not null,		-- 第6路电流
	Currentl7 float not null,		-- 第7路电流
	Currentl8 float not null,		-- 第8路电流
	Currentl9 float not null,		-- 第9路电流
	Currentl10 float not null,		-- 第10路电流
	Currentl11 float not null,		-- 第11路电流
	Currentl12 float not null,		-- 第12路电流
	Currentl13 float not null,		-- 第13路电流
	Currentl14 float not null,		-- 第14路电流
	Currentl15 float not null,		-- 第15路电流
	Currentl16 float not null,		-- 第16路电流
	Currentl17 float not null,		-- 第17路电流
	Currentl18 float not null,		-- 第18路电流
	Currentl19 float not null,		-- 第19路电流
	Currentl20 float not null,		-- 第20路电流
	Currentl21 float not null,		-- 第21路电流
	Currentl22 float not null,		-- 第22路电流
	Currentl23 float not null,		-- 第23路电流
	Currentl24 float not null,		-- 第24路电流
		
	InputIO1 int not null,          -- 输入IO 1  0 1= 闭合 0=开
    InputIO2 int not null,          -- 输入IO 2
    InputIO3 int not null,          -- 输入IO 3
    InputIO4 int not null,          -- 输入IO 4
    InputIO5 int not null,          -- 输入IO 5
    InputIO6 int not null,          -- 输入IO 6
    InputIO7 int not null,          -- 输入IO 7
    InputIO8 int not null,          -- 输入IO 8
    InputIO9 int not null,          -- 输入IO 9
    InputIO10 int not null,         -- 输入IO 10
    InputIO11 int not null,         -- 输入IO 11
    InputIO12 int not null,         -- 输入IO 12
    InputIO13 int not null,         -- 输入IO 13
    InputIO14 int not null,         -- 输入IO 14
    InputIO15 int not null,         -- 输入IO 15
    InputIO16 int not null,         -- 输入IO 16
    InputIO17 int not null,         -- 输入IO 17
    InputIO18 int not null,         -- 输入IO 18
    InputIO19 int not null,         -- 输入IO 19
    InputIO20 int not null,         -- 输入IO 20
    InputIO21 int not null,         -- 输入IO 21
    InputIO22 int not null,         -- 输入IO 22
    InputIO23 int not null,         -- 输入IO 23
    InputIO24 int not null         	-- 输入IO 24
    
);
 
-- 索引
create index index_ACPVSData_CurrYear on ACPVSData_CurrYear(ClltIDDay asc, ProjectID asc);

-- 删除索引
-- drop index index_ACPVSData_CurrYear on ACPVSData_CurrYear ;

-- 查看索引
-- EXEC Sp_helpindex ACPVSData_CurrYear
-- show index from ACPVSData_CurrYear;


create table DCCabinetData_Curr   
(
	IID bigint not null primary key,		-- ID 
	ClltIDDay int not null,             	-- 创建时日期 int
	ClltDaytime datetime  not null,         -- 天 时间
	
	DeviceIndex int not null,         		-- 设备ID	
	
	DayTimes int not null, 					-- 采集ID(本次采集的N个点都有相同的ID)
	
	RegionID int not null,         			-- 区 县市ID
	ProjectID int not null,         		-- 项目ID
	SectionID int not null,         		-- 区域ID
	CollectID int not null,         		-- 采集器ID
	ComID int not null,         			-- Com
	Address int not null,         			-- Address	
	
	SN varchar(64)  not null,	            -- SN 序列号	
	
	StatusInt  int not null,	          	-- 状态	  
	StatusDesc varchar(1024)  not null,		-- 状态					-- 状态
	
	Voltage float not null,         -- 电压		
	Current float not null,		    -- 电流
	Power float not null,		    -- 功率
	Temperature float not null,		-- 温度
	WhSum float not null,           -- 总有功发电量       WhSum		
	VarhSum float not null,         -- 总无功发电量       VarhSum	
	WhNSum float not null,          -- 总有功发电量-      WhSum -	
	VarhNSum float not null         -- 总无功发电量- 	
);
 
-- 索引
create index index_DCCabinetData_Curr on DCCabinetData_Curr(ProjectID asc);

-- 删除索引
-- drop index index_DCCabinetData_Curr on DCCabinetData_Curr ;

-- 查看索引
-- EXEC Sp_helpindex DCCabinetData_Curr
-- show index from DCCabinetData_Curr;


create table DCCabinetData_CurrYear   
(
	IID bigint not null primary key,		-- ID 
	ClltIDDay int not null,             	-- 创建时日期 int
	ClltDaytime datetime  not null,         -- 天 时间
	
	DeviceIndex int not null,         		-- 设备ID	
	
	DayTimes int not null, 					-- 采集ID(本次采集的N个点都有相同的ID)
	
	RegionID int not null,         			-- 区 县市ID
	ProjectID int not null,         		-- 项目ID
	SectionID int not null,         		-- 区域ID
	CollectID int not null,         		-- 采集器ID
	ComID int not null,         			-- Com
	Address int not null,         			-- Address	
	
	SN varchar(64)  not null,	            -- SN 序列号	
	
	StatusInt  int not null,	          	-- 状态	  
	StatusDesc varchar(1024)  not null,		-- 状态					-- 状态
	
	Voltage float not null,         -- 电压		
	Current float not null,		    -- 电流
	Power float not null,		    -- 功率
	Temperature float not null,		-- 温度
	WhSum float not null,           -- 总有功发电量       WhSum		
	VarhSum float not null,         -- 总无功发电量       VarhSum	
	WhNSum float not null,          -- 总有功发电量-      WhSum -	
	VarhNSum float not null         -- 总无功发电量- 	
);
 
-- 索引
create index index_DCCabinetData_CurrYear on DCCabinetData_CurrYear(ClltIDDay asc, ProjectID asc);

-- 删除索引
-- drop index index_DCCabinetData_CurrYear on DCCabinetData_CurrYear ;

-- 查看索引
-- EXEC Sp_helpindex DCCabinetData_CurrYear
-- show index from DCCabinetData_CurrYear;




create table ContraryData_Curr   
(
	IID bigint not null primary key,		-- ID 
	ClltIDDay int not null,             	-- 创建时日期 int
	ClltDaytime datetime  not null,         -- 天 时间
	
	DeviceIndex int not null,         		-- 设备ID	
	
	DayTimes int not null, 					-- 采集ID(本次采集的N个点都有相同的ID)
	
	RegionID int not null,         			-- 区 县市ID
	ProjectID int not null,         		-- 项目ID
	SectionID int not null,         		-- 区域ID
	CollectID int not null,         		-- 采集器ID
	ComID int not null,         			-- Com
	Address int not null,         			-- Address	
	
	SN varchar(64)  not null,	            -- SN 序列号	
	
	StatusInt  int not null,	          	-- 状态	  
	StatusDesc varchar(1024)  not null,		-- 状态	
	
	Voltage float not null,             -- 交流电压A
    Current float not null,             -- 交流电流 A      
    Frequency float not null,           -- 交流频率 A  
    OutputPower float not null,         -- 交流功率A   
    ROutputPower float not null,        -- 交流无功功率A
    PowerFactor float not null,		    -- 功率因素A
        
    VoltageB float not null,            -- 交流电压B
    CurrentB float not null,            -- 交流电流B       
    FrequencyB float not null,          -- 交流频率B
    OutputPowerB float not null,        -- 交流功率B 
    ROutputPowerB float not null,       -- 交流无功功率B 
    PowerFactorB float not null,		-- 功率因素B   
        
    VoltageC float not null,            -- 交流电压C
    CurrentC float not null,            -- 交流电流C        
    FrequencyC float not null,          -- 交流频率C 
    OutputPowerC float not null,        -- 交流功率C
    ROutputPowerC float not null,       -- 交流无功功率C        
    PowerFactorC float not null,		-- 功率因素C
        
    NVoltage float not null,            -- 逆向交流电压A
    NCurrent float not null,            -- 逆向交流电流 A 
        
    NVoltageB float not null,           -- 逆向交流电压A
    NCurrentB float not null,           -- 逆向交流电流 A 
        
    NVoltageC float not null,           -- 逆向交流电压A
    NCurrentC float not null,           -- 逆向交流电流 A  
		
	NTotalVoltage float not null,       -- 逆向交流电压总
    NTotalCurrent float not null,       -- 逆向交流电流总
        
    TotalOutputPower float not null,    -- 交流总功率
    RTotalOutputPower float not null,   -- 交流总无功功率
    TotalPowerFactor float not null,    -- 交流总功率因素           
        
    WhSum float not null,               -- 总有功发电量       WhSum		
	VarhSum float not null,             -- 总无功发电量       VarhSum	
	WhNSum float not null,              -- 总有功发电量-      WhSum -	
	VarhNSum float not null            	-- 总无功发电量-      VarhSum -		
);
 
-- 索引
create index index_ContraryData_Curr on ContraryData_Curr(ProjectID asc);

-- 删除索引
-- drop index index_ContraryData_Curr on ContraryData_Curr ;

-- 查看索引
-- EXEC Sp_helpindex ContraryData_Curr
-- show index from ContraryData_Curr;


create table ContraryData_CurrYear   
(
	IID bigint not null primary key,		-- ID 
	ClltIDDay int not null,             	-- 创建时日期 int
	ClltDaytime datetime  not null,         -- 天 时间
	
	DeviceIndex int not null,         		-- 设备ID	
	
	DayTimes int not null, 					-- 采集ID(本次采集的N个点都有相同的ID)
	
	RegionID int not null,         			-- 区 县市ID
	ProjectID int not null,         		-- 项目ID
	SectionID int not null,         		-- 区域ID
	CollectID int not null,         		-- 采集器ID
	ComID int not null,         			-- Com
	Address int not null,         			-- Address	
	
	SN varchar(64)  not null,	            -- SN 序列号	
	
	StatusInt  int not null,	          	-- 状态	  
	StatusDesc varchar(1024)  not null,		-- 状态	
	
	Voltage float not null,             -- 交流电压A
    Current float not null,             -- 交流电流 A      
    Frequency float not null,           -- 交流频率 A  
    OutputPower float not null,         -- 交流功率A   
    ROutputPower float not null,        -- 交流无功功率A
    PowerFactor float not null,		    -- 功率因素A
        
    VoltageB float not null,            -- 交流电压B
    CurrentB float not null,            -- 交流电流B       
    FrequencyB float not null,          -- 交流频率B
    OutputPowerB float not null,        -- 交流功率B 
    ROutputPowerB float not null,       -- 交流无功功率B 
    PowerFactorB float not null,		-- 功率因素B   
        
    VoltageC float not null,            -- 交流电压C
    CurrentC float not null,            -- 交流电流C        
    FrequencyC float not null,          -- 交流频率C 
    OutputPowerC float not null,        -- 交流功率C
    ROutputPowerC float not null,       -- 交流无功功率C        
    PowerFactorC float not null,		-- 功率因素C
        
    NVoltage float not null,            -- 逆向交流电压A
    NCurrent float not null,            -- 逆向交流电流 A 
        
    NVoltageB float not null,           -- 逆向交流电压A
    NCurrentB float not null,           -- 逆向交流电流 A 
        
    NVoltageC float not null,           -- 逆向交流电压A
    NCurrentC float not null,           -- 逆向交流电流 A  
		
	NTotalVoltage float not null,       -- 逆向交流电压总
    NTotalCurrent float not null,       -- 逆向交流电流总
        
    TotalOutputPower float not null,    -- 交流总功率
    RTotalOutputPower float not null,   -- 交流总无功功率
    TotalPowerFactor float not null,    -- 交流总功率因素           
        
    WhSum float not null,               -- 总有功发电量       WhSum		
	VarhSum float not null,             -- 总无功发电量       VarhSum	
	WhNSum float not null,              -- 总有功发电量-      WhSum -	
	VarhNSum float not null            	-- 总无功发电量-      VarhSum -		
);
 
-- 索引
create index index_ContraryData_CurrYear on ContraryData_CurrYear(ClltIDDay asc, ProjectID asc);

-- 删除索引
-- drop index index_ContraryData_CurrYear on ContraryData_CurrYear ;

-- 查看索引
-- EXEC Sp_helpindex ContraryData_CurrYear
-- show index from ContraryData_CurrYear;


create table PCSData_Curr   
(
	IID bigint not null primary key,		-- ID 
	ClltIDDay int not null,             	-- 创建时日期 int
	ClltDaytime datetime  not null,         -- 天 时间
	
	DeviceIndex int not null,         		-- 设备ID	
	
	DayTimes int not null, 					-- 采集ID(本次采集的N个点都有相同的ID)
	
	RegionID int not null,         			-- 区 县市ID
	ProjectID int not null,         		-- 项目ID
	SectionID int not null,         		-- 区域ID
	CollectID int not null,         		-- 采集器ID
	ComID int not null,         			-- Com
	Address int not null,         			-- Address	
	
	SN varchar(64)  not null,	            -- SN 序列号	
	
	StatusInt  int not null,	          	-- 状态	  
	StatusDesc varchar(1024)  not null,		-- 状态	
	
	Udc float not null,             -- 直流电压   主 
	Idc float not null,             -- 直流电流 
	Pdc float not null,             -- 直流有功功率	
    Ua float not null,              -- 相电压A   
    Ub float not null,              -- 相电压B
    Uc float not null,		        -- 相电压C
    Uab float not null,             -- 线电压Uab
    Ubc float not null,             -- 线电压Ubc       
    Uca float not null,             -- 线电压Uca
    Ia float not null,              -- 电流A 
    Ib float not null,              -- 电流B    
    Ic float not null,              -- 电流C 
    P float not null,               -- 有功功率
    Q float not null,               -- 无功功率 
    S float not null,		        -- 视在功率
    PF float not null,              -- 功率因素
    F float not null,               -- 频率        
        
    Udc1 float not null,            -- 直流电压    支路1
	Idc1 float not null,            -- 直流电流 
	Pdc1 float not null,            -- 直流有功功率	
    Ua1 float not null,             -- 相电压A   
    Ub1 float not null,             -- 相电压B
    Uc1 float not null,		        -- 相电压C
    Uab1 float not null,            -- 线电压Uab
    Ubc1 float not null,            -- 线电压Ubc       
    Uca1 float not null,            -- 线电压Uca
    Ia1 float not null,             -- 电流A
    Ib1 float not null,             -- 电流B    
    Ic1 float not null,             -- 电流C 
    P1 float not null,              -- 有功功率
    Q1 float not null,              -- 无功功率 
    S1 float not null,		        -- 视在功率
    PF1 float not null,             -- 功率因素
    F1 float not null,              -- 频率
        
    Udc2 float not null,            -- 直流电压   支路2
	Idc2 float not null,            -- 直流电流 
	Pdc2 float not null,            -- 直流有功功率	
    Ua2 float not null,             -- 相电压A   
    Ub2 float not null,             -- 相电压B
    Uc2 float not null,		        -- 相电压C
    Uab2 float not null,            -- 线电压Uab
    Ubc2 float not null,            -- 线电压Ubc       
    Uca2 float not null,            -- 线电压Uca
    Ia2 float not null,             -- 电流A
    Ib2 float not null,             -- 电流B    
    Ic2 float not null,             -- 电流C 
    P2 float not null,              -- 有功功率
    Q2 float not null,              -- 无功功率 
    S2 float not null,		        -- 视在功率
    PF2 float not null,             -- 功率因素
    F2 float not null,              -- 频率
        
    TodayHourCharge float not null, 		-- 当天充电小时
    YearyHourCharge float not null,        	-- 年充电小时
    TotalHourCharge float not null,        	-- 总充电小时
        
    TodayHourDisCharge float not null,    	-- 当天放电小时
    YearyHourDisCharge float not null,     	-- 年放电小时
    TotalHourDisCharge float not null,    	-- 总放电小时
        
    TodayEnergyCharge float not null,  		-- 当天充电量
    YearEnergyCharge float not null,        -- 年充电量
    TotalEnergyCharge float not null,       -- 总充电量
        
    TodayEnergyDisCharge float not null,    -- 当天放电量
    YearEnergyDisCharge float not null,     -- 年放电量
    TotalEnergyDisCharge float not null,    -- 总放电量
        
    ChargeCount int not null,				-- 充电次数
    DisChargeCount int not null,			-- 放电次数
        
	Temperature float not null,			-- 温度
		
	GroundResistance float not null,        -- 接地电阻 MΩ
	GroundResistanceN float not null,       -- 接地电阻- MΩ
		
    LeakCurrent float not null,             -- 漏电流 mA
        
    Mode int not null,						-- 模式 充电 恒流 恒压 娟流 还是 放电 恒功率
        
    ChargePower int not null,				-- 充电功率 按SOC决定充电功率
    DisChargePower int not null				-- 放电功率	
);
 
-- 索引
create index index_PCSData_Curr on PCSData_Curr(ProjectID asc);

-- 删除索引
-- drop index index_PCSData_Curr on PCSData_Curr ;

-- 查看索引
-- EXEC Sp_helpindex PCSData_Curr
-- show index from PCSData_Curr;


create table PCSData_CurrYear   
(
	IID bigint not null primary key,		-- ID 
	ClltIDDay int not null,             	-- 创建时日期 int
	ClltDaytime datetime  not null,         -- 天 时间
	
	DeviceIndex int not null,         		-- 设备ID	
	
	DayTimes int not null, 					-- 采集ID(本次采集的N个点都有相同的ID)
	
	RegionID int not null,         			-- 区 县市ID
	ProjectID int not null,         		-- 项目ID
	SectionID int not null,         		-- 区域ID
	CollectID int not null,         		-- 采集器ID
	ComID int not null,         			-- Com
	Address int not null,         			-- Address	
	
	SN varchar(64)  not null,	            -- SN 序列号	
	
	StatusInt  int not null,	          	-- 状态	  
	StatusDesc varchar(1024)  not null,		-- 状态	
	
	Udc float not null,             -- 直流电压   主 
	Idc float not null,             -- 直流电流 
	Pdc float not null,             -- 直流有功功率	
    Ua float not null,              -- 相电压A   
    Ub float not null,              -- 相电压B
    Uc float not null,		        -- 相电压C
    Uab float not null,             -- 线电压Uab
    Ubc float not null,             -- 线电压Ubc       
    Uca float not null,             -- 线电压Uca
    Ia float not null,              -- 电流A 
    Ib float not null,              -- 电流B    
    Ic float not null,              -- 电流C 
    P float not null,               -- 有功功率
    Q float not null,               -- 无功功率 
    S float not null,		        -- 视在功率
    PF float not null,              -- 功率因素
    F float not null,               -- 频率        
        
    Udc1 float not null,            -- 直流电压    支路1
	Idc1 float not null,            -- 直流电流 
	Pdc1 float not null,            -- 直流有功功率	
    Ua1 float not null,             -- 相电压A   
    Ub1 float not null,             -- 相电压B
    Uc1 float not null,		        -- 相电压C
    Uab1 float not null,            -- 线电压Uab
    Ubc1 float not null,            -- 线电压Ubc       
    Uca1 float not null,            -- 线电压Uca
    Ia1 float not null,             -- 电流A
    Ib1 float not null,             -- 电流B    
    Ic1 float not null,             -- 电流C 
    P1 float not null,              -- 有功功率
    Q1 float not null,              -- 无功功率 
    S1 float not null,		        -- 视在功率
    PF1 float not null,             -- 功率因素
    F1 float not null,              -- 频率
        
    Udc2 float not null,            -- 直流电压   支路2
	Idc2 float not null,            -- 直流电流 
	Pdc2 float not null,            -- 直流有功功率	
    Ua2 float not null,             -- 相电压A   
    Ub2 float not null,             -- 相电压B
    Uc2 float not null,		        -- 相电压C
    Uab2 float not null,            -- 线电压Uab
    Ubc2 float not null,            -- 线电压Ubc       
    Uca2 float not null,            -- 线电压Uca
    Ia2 float not null,             -- 电流A
    Ib2 float not null,             -- 电流B    
    Ic2 float not null,             -- 电流C 
    P2 float not null,              -- 有功功率
    Q2 float not null,              -- 无功功率 
    S2 float not null,		        -- 视在功率
    PF2 float not null,             -- 功率因素
    F2 float not null,              -- 频率
        
    TodayHourCharge float not null, 		-- 当天充电小时
    YearyHourCharge float not null,        	-- 年充电小时
    TotalHourCharge float not null,        	-- 总充电小时
        
    TodayHourDisCharge float not null,    	-- 当天放电小时
    YearyHourDisCharge float not null,     	-- 年放电小时
    TotalHourDisCharge float not null,    	-- 总放电小时
        
    TodayEnergyCharge float not null,  		-- 当天充电量
    YearEnergyCharge float not null,        -- 年充电量
    TotalEnergyCharge float not null,       -- 总充电量
        
    TodayEnergyDisCharge float not null,    -- 当天放电量
    YearEnergyDisCharge float not null,     -- 年放电量
    TotalEnergyDisCharge float not null,    -- 总放电量
        
    ChargeCount int not null,				-- 充电次数
    DisChargeCount int not null,			-- 放电次数
        
	Temperature float not null,				-- 温度
		
	GroundResistance float not null,        -- 接地电阻 MΩ
	GroundResistanceN float not null,       -- 接地电阻- MΩ
		
    LeakCurrent float not null,             -- 漏电流 mA
        
    Mode int not null,						-- 模式 充电 恒流 恒压 娟流 还是 放电 恒功率
        
    ChargePower int not null,				-- 充电功率 按SOC决定充电功率
    DisChargePower int not null				-- 放电功率	
);
 
-- 索引
create index index_PCSData_CurrYear on PCSData_CurrYear(ClltIDDay asc, ProjectID asc);

-- 删除索引
-- drop index index_PCSData_CurrYear on PCSData_CurrYear ;

-- 查看索引
-- EXEC Sp_helpindex PCSData_CurrYear
-- show index from PCSData_CurrYear;



create table PCSRpt_CurrYear 
(
	IID bigint not null primary key,		 	-- ID 
	ClltIDDay int not null,             		-- 创建时日期 int
	
	DeviceIndex int not null,         			-- 设备ID
	
	ClltIDYear int not null,             		-- 创建时年 int	
	ClltIDMonth int not null,             		-- 创建时月 int
	ClltIDDayIndex int not null,             	-- 创建时日 int
	
	RegionID int not null,         				-- 区 县市ID
	ProjectID int not null,         			-- 项目ID
	SectionID int not null,         			-- 区域ID
	CollectID int not null,         			-- 采集器ID
	ComID int not null,         				-- Com
	Address int not null,         				-- Address	
	
	TodayEnergyCharge float not null,  		-- 当天充电量
    TotalEnergyCharge float not null,       -- 总充电量
        
    TodayEnergyDisCharge float not null,    -- 当天放电量
    TotalEnergyDisCharge float not null    -- 总放电量
	
);
 
-- 索引
create index index_PCSRpt_CurrYear on PCSRpt_CurrYear(ClltIDDay asc, ProjectID asc);

-- 删除索引
-- drop index index_PCSRpt_CurrYear on PCSRpt_CurrYear ;

-- 查看索引
-- EXEC Sp_helpindex PCSRpt_CurrYear
-- show index from PCSRpt_CurrYear;




create table PCSGroupData_Curr   
(
	IID bigint not null primary key,		-- ID 
	ClltIDDay int not null,             	-- 创建时日期 int
	ClltDaytime datetime  not null,         -- 天 时间
	
	DeviceIndex int not null,         		-- 设备ID	
	
	DayTimes int not null, 					-- 采集ID(本次采集的N个点都有相同的ID)
	
	RegionID int not null,         			-- 区 县市ID
	ProjectID int not null,         		-- 项目ID
	SectionID int not null,         		-- 区域ID
	CollectID int not null,         		-- 采集器ID
	ComID int not null,         			-- Com
	Address int not null,         			-- Address	
	
	SN varchar(64)  not null,	            -- SN 序列号	
	
	StatusInt  int not null,	          	-- 状态	  
	StatusDesc varchar(1024)  not null,		-- 状态					-- 状态
	
	GroupID int not null,			-- PCS 支路ID
		
	Udc float not null,             -- 直流电压   主 
	Idc float not null,             -- 直流电流 
	Pdc float not null,             -- 直流有功功率	
    MotherLineVoltage float not null,              -- 母线电压   
                
    TodayHourCharge float not null, 		-- 当天充电小时
    YearyHourCharge float not null,        	-- 年充电小时
    TotalHourCharge float not null,        	-- 总充电小时
        
    TodayHourDisCharge float not null,    	-- 当天放电小时
    YearyHourDisCharge float not null,     	-- 年放电小时
    TotalHourDisCharge float not null,    	-- 总放电小时
        
    TodayEnergyCharge float not null,  		-- 当天充电量
    YearEnergyCharge float not null,        -- 年充电量
    TotalEnergyCharge float not null,       -- 总充电量
        
    TodayEnergyDisCharge float not null,    -- 当天放电量
    YearEnergyDisCharge float not null,     -- 年放电量
    TotalEnergyDisCharge float not null,    -- 总放电量
        
    ChargeCount int not null,				-- 充电次数
    DisChargeCount int not null,			-- 放电次数
        
	Temperature float not null,				-- 温度
		
	GroundResistance float not null,        -- 接地电阻 MΩ
	GroundResistanceN float not null,       -- 接地电阻- MΩ
		
    LeakCurrent float not null,             -- 漏电流 mA
        
    Mode int not null,						-- 模式 充电 恒流 恒压 娟流 还是 放电 恒功率
        
    ChargePower int not null,				-- 充电功率 按SOC决定充电功率
    DisChargePower int not null				-- 放电功率
);
 
-- 索引
create index index_PCSGroupData_Curr on PCSGroupData_Curr(ProjectID asc);

-- 删除索引
-- drop index index_PCSGroupData_Curr on PCSGroupData_Curr ;

-- 查看索引
-- EXEC Sp_helpindex PCSGroupData_Curr
-- show index from PCSGroupData_Curr;


create table PCSGroupData_CurrYear   
(
	IID bigint not null primary key,		-- ID 
	ClltIDDay int not null,             	-- 创建时日期 int
	ClltDaytime datetime  not null,         -- 天 时间
	
	DeviceIndex int not null,         		-- 设备ID	
	
	DayTimes int not null, 					-- 采集ID(本次采集的N个点都有相同的ID)
	
	RegionID int not null,         			-- 区 县市ID
	ProjectID int not null,         		-- 项目ID
	SectionID int not null,         		-- 区域ID
	CollectID int not null,         		-- 采集器ID
	ComID int not null,         			-- Com
	Address int not null,         			-- Address	
	
	SN varchar(64)  not null,	            -- SN 序列号	
	
	StatusInt  int not null,	          	-- 状态	  
	StatusDesc varchar(1024)  not null,		-- 状态					-- 状态
	
	GroupID int not null,			-- PCS 支路ID
		
	Udc float not null,             -- 直流电压   主 
	Idc float not null,             -- 直流电流 
	Pdc float not null,             -- 直流有功功率	
    MotherLineVoltage float not null,              -- 母线电压   
                
    TodayHourCharge float not null, 		-- 当天充电小时
    YearyHourCharge float not null,        	-- 年充电小时
    TotalHourCharge float not null,        	-- 总充电小时
        
    TodayHourDisCharge float not null,    	-- 当天放电小时
    YearyHourDisCharge float not null,     	-- 年放电小时
    TotalHourDisCharge float not null,    	-- 总放电小时
        
    TodayEnergyCharge float not null,  		-- 当天充电量
    YearEnergyCharge float not null,        -- 年充电量
    TotalEnergyCharge float not null,       -- 总充电量
        
    TodayEnergyDisCharge float not null,    -- 当天放电量
    YearEnergyDisCharge float not null,     -- 年放电量
    TotalEnergyDisCharge float not null,    -- 总放电量
        
    ChargeCount int not null,				-- 充电次数
    DisChargeCount int not null,			-- 放电次数
        
	Temperature float not null,				-- 温度
		
	GroundResistance float not null,        -- 接地电阻 MΩ
	GroundResistanceN float not null,       -- 接地电阻- MΩ
		
    LeakCurrent float not null,             -- 漏电流 mA
        
    Mode int not null,						-- 模式 充电 恒流 恒压 娟流 还是 放电 恒功率
        
    ChargePower int not null,				-- 充电功率 按SOC决定充电功率
    DisChargePower int not null				-- 放电功率
);
 
-- 索引
create index index_PCSGroupData_CurrYear on PCSGroupData_CurrYear(ClltIDDay asc, ProjectID asc);

-- 删除索引
-- drop index index_PCSGroupData_CurrYear on PCSGroupData_CurrYear ;

-- 查看索引
-- EXEC Sp_helpindex PCSGroupData_CurrYear
-- show index from PCSGroupData_CurrYear;


create table BMSData_Curr   
(
	IID bigint not null primary key,		-- ID 
	ClltIDDay int not null,             	-- 创建时日期 int
	ClltDaytime datetime  not null,         -- 天 时间
	
	DeviceIndex int not null,         		-- 设备ID	
	
	DayTimes int not null, 					-- 采集ID(本次采集的N个点都有相同的ID)
	
	RegionID int not null,         			-- 区 县市ID
	ProjectID int not null,         		-- 项目ID
	SectionID int not null,         		-- 区域ID
	CollectID int not null,         		-- 采集器ID
	ComID int not null,         			-- Com
	Address int not null,         			-- Address	
	
	SN varchar(64)  not null,	            -- SN 序列号	
	
	StatusInt  int not null,	          	-- 状态	  
	StatusDesc varchar(1024)  not null,		-- 状态	
	
	Voltage float not null,					-- 电压		电池堆		
	Current float not null,					-- 电流
	SOC int not null,						-- SOC
	SOH int not null,						-- SOH	
		
	TodayCharge float not null,       		-- 当日充电量
    TodayDisCharge float not null,    		-- 当日放电量
        
	TotalCharge float not null,       		-- 累计充电量
    TotalDisCharge float not null,    		-- 累计放电量
        
    ChargeCount int not null,				-- 充电次数
    DisChargeCount int not null,			-- 放电次数
        
    MaxVoltage float not null,			-- 最高电池电压
	MaxVoltageGroup int not null,			-- 最高电压电池组号
	MaxVoltageUnit int not null,			-- 最高电压电池所在Unit ID			
		
	MinVoltage float not null,				-- 最低电池电压
	MinVoltageGroup int not null,			-- 最低电压电池组号
	MinVoltageUnit int not null,			-- 最低电压电池所在Unit ID		
		
	MaxTemperature float not null,			-- 最高电池温度
	MaxTemperatureGroup int not null,		-- 最高温度电池组号
	MaxTemperatureUnit int not null,		-- 最高温度电池所在Unit ID			
		
	MinTemperature float not null,			-- 最低电池温度
	MinTemperatureGroup int not null,		-- 最低温度电池组号
	MinTemperatureUnit int not null,		-- 最低温度电池所在Unit ID	
		
	GroupCount int not null,				-- 电池堆下辖电池组数
	GroupUnitCount int not null,			-- 电池组下辖 Unit 数
		
	ChargeMode int not null,				-- BMS决定充电模式  三段式充电  恒压 读电压 写电压 改模式 ，恒流 读电流 改电流，改模式， 涓流  
	ConstantVoltage float not null,			-- 恒压设定值
	ConstantCurrent float not null,			-- 恒流设定值
	PurlingCurrent float not null			-- 涓流设定值
	
);
 
-- 索引
create index index_BMSData_Curr on BMSData_Curr(ProjectID asc);

-- 删除索引
-- drop index index_BMSData_Curr on BMSData_Curr ;

-- 查看索引
-- EXEC Sp_helpindex BMSData_Curr
-- show index from BMSData_Curr;


create table BMSData_CurrYear   
(
	IID bigint not null primary key,		-- ID 
	ClltIDDay int not null,             	-- 创建时日期 int
	ClltDaytime datetime  not null,         -- 天 时间
	
	DeviceIndex int not null,         		-- 设备ID	
	
	DayTimes int not null, 					-- 采集ID(本次采集的N个点都有相同的ID)
	
	RegionID int not null,         			-- 区 县市ID
	ProjectID int not null,         		-- 项目ID
	SectionID int not null,         		-- 区域ID
	CollectID int not null,         		-- 采集器ID
	ComID int not null,         			-- Com
	Address int not null,         			-- Address	
	
	SN varchar(64)  not null,	            -- SN 序列号	
	
	StatusInt  int not null,	          	-- 状态	  
	StatusDesc varchar(1024)  not null,		-- 状态	
	
	Voltage float not null,					-- 电压		电池堆		
	Current float not null,					-- 电流
	SOC int not null,						-- SOC
	SOH int not null,						-- SOH	
		
	TodayCharge float not null,       		-- 当日充电量
    TodayDisCharge float not null,    		-- 当日放电量
        
	TotalCharge float not null,       		-- 累计充电量
    TotalDisCharge float not null,    		-- 累计放电量
        
    ChargeCount int not null,				-- 充电次数
    DisChargeCount int not null,			-- 放电次数
        
    MaxVoltage float not null,				-- 最高电池电压
	MaxVoltageGroup int not null,			-- 最高电压电池组号
	MaxVoltageUnit int not null,			-- 最高电压电池所在Unit ID			
		
	MinVoltage float not null,				-- 最低电池电压
	MinVoltageGroup int not null,			-- 最低电压电池组号
	MinVoltageUnit int not null,			-- 最低电压电池所在Unit ID		
		
	MaxTemperature float not null,			-- 最高电池温度
	MaxTemperatureGroup int not null,		-- 最高温度电池组号
	MaxTemperatureUnit int not null,		-- 最高温度电池所在Unit ID			
		
	MinTemperature float not null,			-- 最低电池温度
	MinTemperatureGroup int not null,		-- 最低温度电池组号
	MinTemperatureUnit int not null,		-- 最低温度电池所在Unit ID	
		
	GroupCount int not null,				-- 电池堆下辖电池组数
	GroupUnitCount int not null,			-- 电池组下辖 Unit 数
		
	ChargeMode int not null,				-- BMS决定充电模式  三段式充电  恒压 读电压 写电压 改模式 ，恒流 读电流 改电流，改模式， 涓流  
	ConstantVoltage float not null,			-- 恒压设定值
	ConstantCurrent float not null,			-- 恒流设定值
	PurlingCurrent float not null			-- 涓流设定值
	
);
 
-- 索引
create index index_BMSData_CurrYear on BMSData_CurrYear(ClltIDDay asc, ProjectID asc);

-- 删除索引
-- drop index index_BMSData_CurrYear on BMSData_CurrYear ;

-- 查看索引
-- EXEC Sp_helpindex BMSData_CurrYear
-- show index from BMSData_CurrYear;


create table BMSRpt_CurrYear 
(
	IID bigint not null primary key,		 	-- ID 
	ClltIDDay int not null,             		-- 创建时日期 int
	
	DeviceIndex int not null,         			-- 设备ID
	
	ClltIDYear int not null,             		-- 创建时年 int	
	ClltIDMonth int not null,             		-- 创建时月 int
	ClltIDDayIndex int not null,             	-- 创建时日 int
	
	RegionID int not null,         				-- 区 县市ID
	ProjectID int not null,         			-- 项目ID
	SectionID int not null,         			-- 区域ID
	CollectID int not null,         			-- 采集器ID
	ComID int not null,         				-- Com
	Address int not null,         				-- Address	
	
	TodayCharge float not null,       		-- 当日充电量
    TodayDisCharge float not null,    		-- 当日放电量
            
	TotalCharge float not null,       		-- 累计充电量
    TotalDisCharge float not null    		-- 累计放电量
    
);
 
-- 索引
create index index_BMSRpt_CurrYear on BMSRpt_CurrYear(ClltIDDay asc, ProjectID asc);

-- 删除索引
-- drop index index_BMSRpt_CurrYear on BMSRpt_CurrYear ;

-- 查看索引
-- EXEC Sp_helpindex BMSRpt_CurrYear
-- show index from BMSRpt_CurrYear;



create table BMSGroupData_Curr   
(
	IID bigint not null primary key,		-- ID 
	ClltIDDay int not null,             	-- 创建时日期 int
	ClltDaytime datetime  not null,         -- 天 时间
	
	DeviceIndex int not null,         		-- 设备ID	
	
	DayTimes int not null, 					-- 采集ID(本次采集的N个点都有相同的ID)
	
	RegionID int not null,         			-- 区 县市ID
	ProjectID int not null,         		-- 项目ID
	SectionID int not null,         		-- 区域ID
	CollectID int not null,         		-- 采集器ID
	ComID int not null,         			-- Com
	Address int not null,         			-- Address	
	
	SN varchar(64)  not null,	            -- SN 序列号	
	
	StatusInt  int not null,	          	-- 状态	  
	StatusDesc varchar(1024)  not null,		-- 状态	
	
	GroupID int not null,					-- 电池组ID 电池组
		
	Voltage float not null,					-- 电池组电压				
	Current float not null,					-- 电池组电流
		
	SOC int not null,						-- SOC
	SOH int not null,						-- SOH	
		
    TotalCharge float not null,       		-- 累计充电量
    TotalDisCharge float not null,    		-- 累计放电量
        
    ChargeCount int not null,				-- 充电次数
    DisChargeCount int not null,			-- 放电次数
        
	MaxVoltage float not null,				-- 最高电池电压
	MaxVoltageUnit int not null,			-- 最高电压电池所在Unit ID			
		
	MinVoltage float not null,				-- 最低电池电压
	MinVoltageUnit int not null,			-- 最低电压电池所在Unit ID		
		
	AvgVoltage float not null,				-- 平均电池电压
		
	MaxTemperature float not null,			-- 最高电池温度
	MaxTemperatureUnit  int not null,		-- 最高温度电池所在Unit ID			
		
	MinTemperature float not null,			-- 最低电池温度
	MinTemperatureUnit int not null,		-- 最低温度电池所在Unit ID		
		
	AvgTemperature float not null,			-- 平均电池温度
        
    MaxSOC float not null,					-- 最高SOC
	MaxSOCUnit int not null,				-- 最高SOC所在Unit ID			
		
	MinSOC float not null,					-- 最低SOC
	MinSOCUnit int not null,				-- 最低SOC所在Unit ID	
		
	MaxSOH float not null,					-- 最高SOH
	MaxSOHUnit int not null,				-- 最高SOH所在Unit ID			
		
	MinSOH float not null,					-- 最低SOH
	MinSOHUnit int not null,				-- 最低SOH所在Unit ID	
		
	UnitCount int not null,					-- 电池节数
	DesignPower int not null				-- 额定容量
	
);
 
-- 索引
create index index_BMSGroupData_Curr on BMSGroupData_Curr(ProjectID asc);

-- 删除索引
-- drop index index_BMSGroupData_Curr on BMSGroupData_Curr ;

-- 查看索引
-- EXEC Sp_helpindex BMSGroupData_Curr
-- show index from BMSGroupData_Curr;


create table BMSGroupData_CurrYear   
(
	IID bigint not null primary key,		-- ID 
	ClltIDDay int not null,             	-- 创建时日期 int
	ClltDaytime datetime  not null,         -- 天 时间
	
	DeviceIndex int not null,         		-- 设备ID	
	
	DayTimes int not null, 					-- 采集ID(本次采集的N个点都有相同的ID)
	
	RegionID int not null,         			-- 区 县市ID
	ProjectID int not null,         		-- 项目ID
	SectionID int not null,         		-- 区域ID
	CollectID int not null,         		-- 采集器ID
	ComID int not null,         			-- Com
	Address int not null,         			-- Address	
	
	SN varchar(64)  not null,	            -- SN 序列号	
	
	StatusInt  int not null,	          	-- 状态	  
	StatusDesc varchar(1024)  not null,		-- 状态	
	
	GroupID int not null,					-- 电池组ID 电池组
		
	Voltage float not null,					-- 电池组电压				
	Current float not null,					-- 电池组电流
		
	SOC int not null,						-- SOC
	SOH int not null,						-- SOH	
		
    TotalCharge float not null,       		-- 累计充电量
    TotalDisCharge float not null,    		-- 累计放电量
        
    ChargeCount int not null,				-- 充电次数
    DisChargeCount int not null,			-- 放电次数
        
	MaxVoltage float not null,				-- 最高电池电压
	MaxVoltageUnit int not null,			-- 最高电压电池所在Unit ID			
		
	MinVoltage float not null,				-- 最低电池电压
	MinVoltageUnit int not null,			-- 最低电压电池所在Unit ID		
		
	AvgVoltage float not null,				-- 平均电池电压
		
	MaxTemperature float not null,			-- 最高电池温度
	MaxTemperatureUnit  int not null,		-- 最高温度电池所在Unit ID			
		
	MinTemperature float not null,			-- 最低电池温度
	MinTemperatureUnit int not null,		-- 最低温度电池所在Unit ID		
		
	AvgTemperature float not null,			-- 平均电池温度
        
    MaxSOC float not null,					-- 最高SOC
	MaxSOCUnit int not null,				-- 最高SOC所在Unit ID			
		
	MinSOC float not null,					-- 最低SOC
	MinSOCUnit int not null,				-- 最低SOC所在Unit ID	
		
	MaxSOH float not null,					-- 最高SOH
	MaxSOHUnit int not null,				-- 最高SOH所在Unit ID			
		
	MinSOH float not null,					-- 最低SOH
	MinSOHUnit int not null,				-- 最低SOH所在Unit ID	
		
	UnitCount int not null,					-- 电池节数
	DesignPower int not null				-- 额定容量
	
);
 
-- 索引
create index index_BMSGroupData_CurrYear on BMSGroupData_CurrYear(ClltIDDay asc, ProjectID asc);

-- 删除索引
-- drop index index_BMSGroupData_CurrYear on BMSGroupData_CurrYear ;

-- 查看索引
-- EXEC Sp_helpindex BMSGroupData_CurrYear
-- show index from BMSGroupData_CurrYear;


create table BMSUnitData_Curr   
(
	IID bigint not null primary key,		-- ID 
	ClltIDDay int not null,             	-- 创建时日期 int
	ClltDaytime datetime  not null,         -- 天 时间
	
	DeviceIndex int not null,         		-- 设备ID	
	
	DayTimes int not null, 					-- 采集ID(本次采集的N个点都有相同的ID)
	
	RegionID int not null,         			-- 区 县市ID
	ProjectID int not null,         		-- 项目ID
	SectionID int not null,         		-- 区域ID
	CollectID int not null,         		-- 采集器ID
	ComID int not null,         			-- Com
	Address int not null,         			-- Address	
	
	SN varchar(64)  not null,	            -- SN 序列号	
	
	StatusInt  int not null,	          	-- 状态	  
	StatusDesc varchar(1024)  not null,		-- 状态	
	
	GroupID int not null,			-- 电池组ID
	UnitID int not null,			-- 单体电池ID
		
	Voltage float not null,			-- 单体电压
	Temperature float not null,		-- 单体温度	
	SOC int not null,				-- SOC
	SOH int not null				-- SOH	
);
 
-- 索引
create index index_BMSUnitData_Curr on BMSUnitData_Curr(ProjectID asc);

-- 删除索引
-- drop index index_BMSUnitData_Curr on BMSUnitData_Curr ;

-- 查看索引
-- EXEC Sp_helpindex BMSUnitData_Curr
-- show index from BMSUnitData_Curr;


create table BMSUnitData_CurrYear   
(
	IID bigint not null primary key,		-- ID 
	ClltIDDay int not null,             	-- 创建时日期 int
	ClltDaytime datetime  not null,         -- 天 时间
	
	DeviceIndex int not null,         		-- 设备ID	
	
	DayTimes int not null, 					-- 采集ID(本次采集的N个点都有相同的ID)
	
	RegionID int not null,         			-- 区 县市ID
	ProjectID int not null,         		-- 项目ID
	SectionID int not null,         		-- 区域ID
	CollectID int not null,         		-- 采集器ID
	ComID int not null,         			-- Com
	Address int not null,         			-- Address	
	
	SN varchar(64)  not null,	            -- SN 序列号	
	
	StatusInt  int not null,	          	-- 状态	  
	StatusDesc varchar(1024)  not null,		-- 状态	
	
	GroupID int not null,			-- 电池组ID
	UnitID int not null,			-- 单体电池ID
		
	Voltage float not null,			-- 单体电压
	Temperature float not null,		-- 单体温度	
	SOC int not null,				-- SOC
	SOH int not null				-- SOH	
);
 
-- 索引
create index index_BMSUnitData_CurrYear on BMSUnitData_CurrYear(ClltIDDay asc, ProjectID asc);

-- 删除索引
-- drop index index_BMSUnitData_CurrYear on BMSUnitData_CurrYear ;

-- 查看索引
-- EXEC Sp_helpindex BMSUnitData_CurrYear
-- show index from BMSUnitData_CurrYear;


create table BMSTemperatureData_Curr   
(
	IID bigint not null primary key,		-- ID 
	ClltIDDay int not null,             	-- 创建时日期 int
	ClltDaytime datetime  not null,         -- 天 时间
	
	DeviceIndex int not null,         		-- 设备ID	
	
	DayTimes int not null, 					-- 采集ID(本次采集的N个点都有相同的ID)
	
	RegionID int not null,         			-- 区 县市ID
	ProjectID int not null,         		-- 项目ID
	SectionID int not null,         		-- 区域ID
	CollectID int not null,         		-- 采集器ID
	ComID int not null,         			-- Com
	Address int not null,         			-- Address	
	
	SN varchar(64)  not null,	            -- SN 序列号	
	
	StatusInt  int not null,	          	-- 状态	  
	StatusDesc varchar(1024)  not null,		-- 状态	
	
	GroupID int not null,					-- 电池组ID
	TemperatureID int not null,				-- 单体温度ID
		
	Voltage float not null,					-- 单体电压
	Temperature float not null,				-- 单体温度	
	SOC int not null,						-- SOC
	SOH int not null						-- SOH		
);
 
-- 索引
create index index_BMSTemperatureData_Curr on BMSTemperatureData_Curr(ProjectID asc);

-- 删除索引
-- drop index index_BMSTemperatureData_Curr on BMSTemperatureData_Curr ;

-- 查看索引
-- EXEC Sp_helpindex BMSTemperatureData_Curr
-- show index from BMSTemperatureData_Curr;


create table BMSTemperatureData_CurrYear   
(
	IID bigint not null primary key,		-- ID 
	ClltIDDay int not null,             	-- 创建时日期 int
	ClltDaytime datetime  not null,         -- 天 时间
	
	DeviceIndex int not null,         		-- 设备ID	
	
	DayTimes int not null, 					-- 采集ID(本次采集的N个点都有相同的ID)
	
	RegionID int not null,         			-- 区 县市ID
	ProjectID int not null,         		-- 项目ID
	SectionID int not null,         		-- 区域ID
	CollectID int not null,         		-- 采集器ID
	ComID int not null,         			-- Com
	Address int not null,         			-- Address	
	
	SN varchar(64)  not null,	            -- SN 序列号	
	
	StatusInt  int not null,	          	-- 状态	  
	StatusDesc varchar(1024)  not null,		-- 状态	
	
	GroupID int not null,					-- 电池组ID
	TemperatureID int not null,				-- 单体温度ID
		
	Voltage float not null,					-- 单体电压
	Temperature float not null,				-- 单体温度	
	SOC int not null,						-- SOC
	SOH int not null						-- SOH		
);
 
-- 索引
create index index_BMSTemperatureData_CurrYear on BMSTemperatureData_CurrYear(ClltIDDay asc, ProjectID asc);

-- 删除索引
-- drop index index_BMSTemperatureData_CurrYear on BMSTemperatureData_CurrYear ;

-- 查看索引
-- EXEC Sp_helpindex BMSTemperatureData_CurrYear
-- show index from BMSTemperatureData_CurrYear;


create table AirConditionData_Curr   
(
	IID bigint not null primary key,		-- ID 
	ClltIDDay int not null,             	-- 创建时日期 int
	ClltDaytime datetime  not null,         -- 天 时间
	
	DeviceIndex int not null,         		-- 设备ID	
	
	DayTimes int not null, 					-- 采集ID(本次采集的N个点都有相同的ID)
	
	RegionID int not null,         			-- 区 县市ID
	ProjectID int not null,         		-- 项目ID
	SectionID int not null,         		-- 区域ID
	CollectID int not null,         		-- 采集器ID
	ComID int not null,         			-- Com
	Address int not null,         			-- Address	
	
	SN varchar(64)  not null,	            -- SN 序列号	
	
	StatusInt  int not null,	          	-- 状态	  
	StatusDesc varchar(1024)  not null,		-- 状态	
	
	FanIn int not null,				    	-- 内风机
	FanOut int not null,					-- 外风机
		
	Compressor int not null,				-- 压缩机		
	Heater int not null,					-- 电加热器
		
	Fanemergency int not null,		    	-- 应急风机
		
	TemperatureCoil float not null,			-- 盘管温度
	TemperatureIn float not null,			-- 室内温度
	TemperatureOut float not null,			-- 室外温度
	TemperatureGas float not null,			-- 排气温度
	TemperatureCondensation float not null,	-- 冷凝温度
		
	Humidity float not null,        		-- 湿度
		
	Udc float not null,            			-- 直流电压   
	Ua float not null,             			-- 电压 
	Ia float not null,             			-- 电流	
	
	SettingTemperature float not null		-- 设置温度
);
 
-- 索引
create index index_AirConditionData_Curr on AirConditionData_Curr(ProjectID asc);

-- 删除索引
-- drop index index_AirConditionData_Curr on AirConditionData_Curr ;

-- 查看索引
-- EXEC Sp_helpindex AirConditionData_Curr
-- show index from AirConditionData_Curr;


create table AirConditionData_CurrYear   
(
	IID bigint not null primary key,		-- ID 
	ClltIDDay int not null,             	-- 创建时日期 int
	ClltDaytime datetime  not null,         -- 天 时间
	
	DeviceIndex int not null,         		-- 设备ID	
	
	DayTimes int not null, 					-- 采集ID(本次采集的N个点都有相同的ID)
	
	RegionID int not null,         			-- 区 县市ID
	ProjectID int not null,         		-- 项目ID
	SectionID int not null,         		-- 区域ID
	CollectID int not null,         		-- 采集器ID
	ComID int not null,         			-- Com
	Address int not null,         			-- Address	
	
	SN varchar(64)  not null,	            -- SN 序列号	
	
	StatusInt  int not null,	          	-- 状态	  
	StatusDesc varchar(1024)  not null,		-- 状态	
	
	FanIn int not null,				    	-- 内风机
	FanOut int not null,					-- 外风机
		
	Compressor int not null,				-- 压缩机		
	Heater int not null,					-- 电加热器
		
	Fanemergency int not null,		    	-- 应急风机
		
	TemperatureCoil float not null,			-- 盘管温度
	TemperatureIn float not null,			-- 室内温度
	TemperatureOut float not null,			-- 室外温度
	TemperatureGas float not null,			-- 排气温度
	TemperatureCondensation float not null,	-- 冷凝温度
		
	Humidity float not null,        		-- 湿度
		
	Udc float not null,            			-- 直流电压   
	Ua float not null,             			-- 电压 
	Ia float not null,             			-- 电流	
	
	SettingTemperature float not null		-- 设置温度
);
 
-- 索引
create index index_AirConditionData_CurrYear on AirConditionData_CurrYear(ClltIDDay asc, ProjectID asc);

-- 删除索引
-- drop index index_AirConditionData_CurrYear on AirConditionData_CurrYear ;

-- 查看索引
-- EXEC Sp_helpindex AirConditionData_CurrYear
-- show index from AirConditionData_CurrYear;


create table FireAlertData_Curr   
(
	IID bigint not null primary key,		-- ID 
	ClltIDDay int not null,             	-- 创建时日期 int
	ClltDaytime datetime  not null,         -- 天 时间
	
	DeviceIndex int not null,         		-- 设备ID	
	
	DayTimes int not null, 					-- 采集ID(本次采集的N个点都有相同的ID)
	
	RegionID int not null,         			-- 区 县市ID
	ProjectID int not null,         		-- 项目ID
	SectionID int not null,         		-- 区域ID
	CollectID int not null,         		-- 采集器ID
	ComID int not null,         			-- Com
	Address int not null,         			-- Address	
	
	SN varchar(64)  not null,	            -- SN 序列号	
	
	StatusInt  int not null,	          	-- 状态	  
	StatusDesc varchar(1024)  not null,		-- 状态	
	
	WarnSignal int not null,				-- 预警信号
        
    Spray int not null,						-- 喷洒
    Alert int not null,						-- 故障
    Fire int not null						-- 火警	
);
 
-- 索引
create index index_FireAlertData_Curr on FireAlertData_Curr(ProjectID asc);

-- 删除索引
-- drop index index_FireAlertData_Curr on FireAlertData_Curr ;

-- 查看索引
-- EXEC Sp_helpindex FireAlertData_Curr
-- show index from FireAlertData_Curr;


create table FireAlertData_CurrYear   
(
	IID bigint not null primary key,		-- ID 
	ClltIDDay int not null,             	-- 创建时日期 int
	ClltDaytime datetime  not null,         -- 天 时间
	
	DeviceIndex int not null,         		-- 设备ID	
	
	DayTimes int not null, 					-- 采集ID(本次采集的N个点都有相同的ID)
	
	RegionID int not null,         			-- 区 县市ID
	ProjectID int not null,         		-- 项目ID
	SectionID int not null,         		-- 区域ID
	CollectID int not null,         		-- 采集器ID
	ComID int not null,         			-- Com
	Address int not null,         			-- Address	
	
	SN varchar(64)  not null,	            -- SN 序列号	
	
	StatusInt  int not null,	          	-- 状态	  
	StatusDesc varchar(1024)  not null,		-- 状态	
	
	WarnSignal int not null,				-- 预警信号
        
    Spray int not null,						-- 喷洒
    Alert int not null,						-- 故障
    Fire int not null						-- 火警	
);
 
-- 索引
create index index_FireAlertData_CurrYear on FireAlertData_CurrYear(ClltIDDay asc, ProjectID asc);

-- 删除索引
-- drop index index_FireAlertData_CurrYear on FireAlertData_CurrYear ;

-- 查看索引
-- EXEC Sp_helpindex FireAlertData_CurrYear
-- show index from FireAlertData_CurrYear;


create table AGCData_Curr   
(
	IID bigint not null primary key,		-- ID 
	ClltIDDay int not null,             	-- 创建时日期 int
	ClltDaytime datetime  not null,         -- 天 时间
	
	DeviceIndex int not null,         		-- 设备ID	
	
	DayTimes int not null, 					-- 采集ID(本次采集的N个点都有相同的ID)
	
	RegionID int not null,         			-- 区 县市ID
	ProjectID int not null,         		-- 项目ID
	SectionID int not null,         		-- 区域ID
	CollectID int not null,         		-- 采集器ID
	ComID int not null,         			-- Com
	Address int not null,         			-- Address	
	
	SN varchar(64)  not null,	            -- SN 序列号	
	
	StatusInt  int not null,	          	-- 状态	  
	StatusDesc varchar(1024)  not null,		-- 状态	
	
	AgcSetting float not null,				-- AGC
	SolarPower float not null,				-- 光伏实时功率
	SolarMaxPower float not null,			-- 光伏最大功率			
        
    PowerLine float not null,				-- 集电线功率
    RemoteLocal int not null				-- 远方/就地 1=远方
	
);
 
-- 索引
create index index_AGCData_Curr on AGCData_Curr(ProjectID asc);

-- 删除索引
-- drop index index_AGCData_Curr on AGCData_Curr ;

-- 查看索引
-- EXEC Sp_helpindex AGCData_Curr
-- show index from AGCData_Curr;


create table AGCData_CurrYear   
(
	IID bigint not null primary key,		-- ID 
	ClltIDDay int not null,             	-- 创建时日期 int
	ClltDaytime datetime  not null,         -- 天 时间
	
	DeviceIndex int not null,         		-- 设备ID	
	
	DayTimes int not null, 					-- 采集ID(本次采集的N个点都有相同的ID)
	
	RegionID int not null,         			-- 区 县市ID
	ProjectID int not null,         		-- 项目ID
	SectionID int not null,         		-- 区域ID
	CollectID int not null,         		-- 采集器ID
	ComID int not null,         			-- Com
	Address int not null,         			-- Address	
	
	SN varchar(64)  not null,	            -- SN 序列号	
	
	StatusInt  int not null,	          	-- 状态	  
	StatusDesc varchar(1024)  not null,		-- 状态	
	
	AgcSetting float not null,				-- AGC
	SolarPower float not null,				-- 光伏实时功率
	SolarMaxPower float not null,			-- 光伏最大功率			
        
    PowerLine float not null,				-- 集电线功率
    RemoteLocal int not null				-- 远方/就地 1=远方
	
);
 
-- 索引
create index index_AGCData_CurrYear on AGCData_CurrYear(ClltIDDay asc, ProjectID asc);

-- 删除索引
-- drop index index_AGCData_CurrYear on AGCData_CurrYear ;

-- 查看索引
-- EXEC Sp_helpindex AGCData_CurrYear
-- show index from AGCData_CurrYear;


create table InverterOffLineData_Curr   
(
	IID bigint not null primary key,		-- ID 
	ClltIDDay int not null,             	-- 创建时日期 int
	ClltDaytime datetime  not null,         -- 天 时间
	
	DeviceIndex int not null,         		-- 设备ID	
	
	DayTimes int not null, 					-- 采集ID(本次采集的N个点都有相同的ID)
	
	RegionID int not null,         			-- 区 县市ID
	ProjectID int not null,         		-- 项目ID
	SectionID int not null,         		-- 区域ID
	CollectID int not null,         		-- 采集器ID
	ComID int not null,         			-- Com
	Address int not null,         			-- Address	
	
	SN varchar(64)  not null,	            -- SN 序列号	
	
	StatusInt  int not null,	          	-- 状态	  
	StatusDesc varchar(1024)  not null,		-- 状态	
	
	DCVoltage float not null,		-- 直流电压
    DCCurrent float not null,	    -- 直流电流
    DCVoltageB float not null,	    -- 直流电压B
    DCCurrentB float not null,	    -- 直流电流B
    DCVoltageC float not null,	    -- 直流电压C
    DCCurrentC float not null,	    -- 直流电流C   
        
    StandbyYearEnergy float not null,       -- Standby本年能量
    StandbyTotalEnergy float not null,      -- Standby总能量 
    StandbyTemperature float not null,      -- Standby温度  
            
    StandbyVoltage float not null,		    -- 旁路电压
    StandbyCurrent float not null,	    -- 旁路电流
    StandbyFrequency float not null,        -- 旁路 A输出频率
    StandbyVoltageB float not null,		    -- 旁路电压B
    StandbyCurrentB float not null,		    -- 旁路电流B
    StandbyFrequencyB float not null,       -- 旁路 B输出频率
    StandbyVoltageC float not null,		    -- 旁路电压C
    StandbyCurrentC float not null,		    -- 旁路电流C
    StandbyFrequencyC float not null,       -- 旁路 C输出频率       
    StandbyTotalOutputPower float not null,	-- 旁路总输出功率  
               
    Voltage float not null,             -- 交流电压A
    Current float not null,             -- 交流电流 A      
    Frequency float not null,           -- 交流频率 A  
    OutputPower float not null,         -- 交流功率A   
    ROutputPower float not null,        -- 交流无功功率A
    PowerFactor float not null,		    -- 功率因素A
    VoltageB float not null,            -- 交流电压B
    CurrentB float not null,            -- 交流电流B       
    FrequencyB float not null,          -- 交流频率B
    OutputPowerB float not null,        -- 交流功率B 
    ROutputPowerB float not null,       -- 交流无功功率B 
    PowerFactorB float not null,		-- 功率因素B   
    VoltageC float not null,            -- 交流电压C
    CurrentC float not null,            -- 交流电流C        
    FrequencyC float not null,          -- 交流频率C 
    OutputPowerC float not null,        -- 交流功率C
    ROutputPowerC float not null,       -- 交流无功功率C        
    PowerFactorC float not null,		-- 功率因素C
        
    TotalOutputPower float not null,        -- 交流总功率
    RTotalOutputPower float not null,       -- 交流总无功功率
        
    TotalPowerFactor float not null,        -- 交流总功率因素           
        
    TodayEnergy float not null,             -- 当天能量
    TotalEnergy float not null,             -- 总能量
        
    NTodayEnergy float not null,            -- 反向当天能量
    NTotalEnergy float not null,            -- 反向总能量
        
    DCUnderVoltage float not null,          -- 直流欠压
    DCOverVoltage float not null,           -- 直流过压
        
    StandbyUnderVoltage float not null,     -- 旁路欠压
    StandbyOverVoltage float not null,      -- 旁路过压        
        
    BtryVoltage float not null,		        -- 蓄电池电压
    BtryCurrent float not null,		        -- 蓄电池电流
    BtryOutputPower float not null,	        -- 蓄电池输出功率  
        
    TemperatureInverter float not null,		-- Inverter温度    
    TemperatureBooster float not null,		-- Booster温度
    TemperatureSink float not null			-- Sink 散热器 
	
);
 
-- 索引
create index index_InverterOffLineData_Curr on InverterOffLineData_Curr(ProjectID asc);

-- 删除索引
-- drop index index_InverterOffLineData_Curr on InverterOffLineData_Curr ;

-- 查看索引
-- EXEC Sp_helpindex InverterOffLineData_Curr
-- show index from InverterOffLineData_Curr;


create table InverterOffLineData_CurrYear   
(
	IID bigint not null primary key,		-- ID 
	ClltIDDay int not null,             	-- 创建时日期 int
	ClltDaytime datetime  not null,         -- 天 时间
	
	DeviceIndex int not null,         		-- 设备ID	
	
	DayTimes int not null, 					-- 采集ID(本次采集的N个点都有相同的ID)
	
	RegionID int not null,         			-- 区 县市ID
	ProjectID int not null,         		-- 项目ID
	SectionID int not null,         		-- 区域ID
	CollectID int not null,         		-- 采集器ID
	ComID int not null,         			-- Com
	Address int not null,         			-- Address	
	
	SN varchar(64)  not null,	            -- SN 序列号	
	
	StatusInt  int not null,	          	-- 状态	  
	StatusDesc varchar(1024)  not null,		-- 状态	
	
	DCVoltage float not null,		-- 直流电压
    DCCurrent float not null,	    -- 直流电流
    DCVoltageB float not null,	    -- 直流电压B
    DCCurrentB float not null,	    -- 直流电流B
    DCVoltageC float not null,	    -- 直流电压C
    DCCurrentC float not null,	    -- 直流电流C   
        
    StandbyYearEnergy float not null,       -- Standby本年能量
    StandbyTotalEnergy float not null,      -- Standby总能量 
    StandbyTemperature float not null,      -- Standby温度  
            
    StandbyVoltage float not null,		    -- 旁路电压
    StandbyCurrent float not null,	    -- 旁路电流
    StandbyFrequency float not null,        -- 旁路 A输出频率
    StandbyVoltageB float not null,		    -- 旁路电压B
    StandbyCurrentB float not null,		    -- 旁路电流B
    StandbyFrequencyB float not null,       -- 旁路 B输出频率
    StandbyVoltageC float not null,		    -- 旁路电压C
    StandbyCurrentC float not null,		    -- 旁路电流C
    StandbyFrequencyC float not null,       -- 旁路 C输出频率       
    StandbyTotalOutputPower float not null,	-- 旁路总输出功率  
               
    Voltage float not null,             -- 交流电压A
    Current float not null,             -- 交流电流 A      
    Frequency float not null,           -- 交流频率 A  
    OutputPower float not null,         -- 交流功率A   
    ROutputPower float not null,        -- 交流无功功率A
    PowerFactor float not null,		    -- 功率因素A
    VoltageB float not null,            -- 交流电压B
    CurrentB float not null,            -- 交流电流B       
    FrequencyB float not null,          -- 交流频率B
    OutputPowerB float not null,        -- 交流功率B 
    ROutputPowerB float not null,       -- 交流无功功率B 
    PowerFactorB float not null,		-- 功率因素B   
    VoltageC float not null,            -- 交流电压C
    CurrentC float not null,            -- 交流电流C        
    FrequencyC float not null,          -- 交流频率C 
    OutputPowerC float not null,        -- 交流功率C
    ROutputPowerC float not null,       -- 交流无功功率C        
    PowerFactorC float not null,		-- 功率因素C
        
    TotalOutputPower float not null,        -- 交流总功率
    RTotalOutputPower float not null,       -- 交流总无功功率
        
    TotalPowerFactor float not null,        -- 交流总功率因素           
        
    TodayEnergy float not null,             -- 当天能量
    TotalEnergy float not null,             -- 总能量
        
    NTodayEnergy float not null,            -- 反向当天能量
    NTotalEnergy float not null,            -- 反向总能量
        
    DCUnderVoltage float not null,          -- 直流欠压
    DCOverVoltage float not null,           -- 直流过压
        
    StandbyUnderVoltage float not null,     -- 旁路欠压
    StandbyOverVoltage float not null,      -- 旁路过压        
        
    BtryVoltage float not null,		        -- 蓄电池电压
    BtryCurrent float not null,		        -- 蓄电池电流
    BtryOutputPower float not null,	        -- 蓄电池输出功率  
        
    TemperatureInverter float not null,		-- Inverter温度    
    TemperatureBooster float not null,		-- Booster温度
    TemperatureSink float not null			-- Sink 散热器 
	
);
 
-- 索引
create index index_InverterOffLineData_CurrYear on InverterOffLineData_CurrYear(ClltIDDay asc, ProjectID asc);

-- 删除索引
-- drop index index_InverterOffLineData_CurrYear on InverterOffLineData_CurrYear ;

-- 查看索引
-- EXEC Sp_helpindex InverterOffLineData_CurrYear
-- show index from InverterOffLineData_CurrYear;


create table ChargeData_Curr   
(
	IID bigint not null primary key,		-- ID 
	ClltIDDay int not null,             	-- 创建时日期 int
	ClltDaytime datetime  not null,         -- 天 时间
	
	DeviceIndex int not null,         		-- 设备ID	
	
	DayTimes int not null, 					-- 采集ID(本次采集的N个点都有相同的ID)
	
	RegionID int not null,         			-- 区 县市ID
	ProjectID int not null,         		-- 项目ID
	SectionID int not null,         		-- 区域ID
	CollectID int not null,         		-- 采集器ID
	ComID int not null,         			-- Com
	Address int not null,         			-- Address	
	
	SN varchar(64)  not null,	            -- SN 序列号	
	
	StatusInt  int not null,	          	-- 状态	  
	StatusDesc varchar(1024)  not null,		-- 状态	
	
	GridVoltage float not null,		-- 电网电压
    GridCurrent float not null,	    -- 电网电流
    GridVoltageB float not null,	-- 电网电压B
    GridCurrentB float not null,	-- 电网电流B
    GridVoltageC float not null,	-- 电网电压C
    GridCurrentC float not null,	-- 电网电流C   
    PVYearEnergy float not null,    -- PV本年能量
    PVTotalEnergy float not null,   -- PV总能量 
    PVTemperature float not null,   -- PV温度  
            
    PVVoltage float not null,		    -- PV电压
    PVCurrent float not null,		    -- PV电流
    PVInputPower float not null,       	-- PV A输入功率
    PVVoltageB float not null,		    -- PV电压B
    PVCurrentB float not null,		    -- PV电流B
    PVInputPowerB float not null,      	-- PV B输入功率
    PVVoltageC float not null,		    -- PV电压C
    PVCurrentC float not null,		    -- PV电流C
    PVInputPowerC float not null,      	-- PV C输入功率        
    PVTotalInputPower float not null,	-- PV总输入功率  
    AhPVTotal float not null,			-- PV总输入功率  
              
    Voltage float not null,             -- 交流电压A
    Current float not null,             -- 交流电流 A      
    Frequency float not null,           -- 交流频率 A  
    OutputPower float not null,         -- 交流功率A   
    ROutputPower float not null,        -- 交流无功功率A
    PowerFactor float not null,		    -- 功率因素A
    VoltageB float not null,            -- 交流电压B
    CurrentB float not null,            -- 交流电流B       
    FrequencyB float not null,          -- 交流频率B
    OutputPowerB float not null,        -- 交流功率B 
    ROutputPowerB float not null,       -- 交流无功功率B 
    PowerFactorB float not null,		-- 功率因素B   
    VoltageC float not null,            -- 交流电压C
    CurrentC float not null,            -- 交流电流C        
    FrequencyC float not null,          -- 交流频率C 
    OutputPowerC float not null,        -- 交流功率C
    ROutputPowerC float not null,       -- 交流无功功率C        
    PowerFactorC float not null,		-- 功率因素C
        
    TotalOutputPower float not null,    -- 交流总功率
    RTotalOutputPower float not null,   -- 交流总无功功率
    TotalPowerFactor float not null,    -- 交流总功率因素           
        
    TodayEnergy float not null,         -- 当天能量
    TotalEnergy float not null,         -- 总能量
        
    NTodayEnergy float not null,        -- 反向当天能量
    NTotalEnergy float not null,        -- 反向总能量
        
    AhChargeReset float not null,        -- Ah Resetable
    AhChargeTotal float not null,        -- Ah Total
        
    KwhChargeReset float not null,       -- kWh Resetable
    KwhChargeTotal float not null,       -- kWh Total        
        
    BtryVoltage float not null,		    -- 蓄电池电压
    BtryCurrent float not null,	    	-- 蓄电池电流
    BtryOutputPower float not null,	    -- 蓄电池输出功率  
    AhBtryTotal float not null,	    	-- 蓄电池输出功率  
        
    TemperatureBattery float not null,	-- 蓄电池温度    
    TemperatureRTS float not null,		-- RTS温度
    TemperatureSink float not null	    -- Sink 散热器 
	
);
 
-- 索引
create index index_ChargeData_Curr on ChargeData_Curr(ProjectID asc);

-- 删除索引
-- drop index index_ChargeData_Curr on ChargeData_Curr ;

-- 查看索引
-- EXEC Sp_helpindex ChargeData_Curr
-- show index from ChargeData_Curr;


create table ChargeData_CurrYear   
(
	IID bigint not null primary key,		-- ID 
	ClltIDDay int not null,             	-- 创建时日期 int
	ClltDaytime datetime  not null,         -- 天 时间
	
	DeviceIndex int not null,         		-- 设备ID	
	
	DayTimes int not null, 					-- 采集ID(本次采集的N个点都有相同的ID)
	
	RegionID int not null,         			-- 区 县市ID
	ProjectID int not null,         		-- 项目ID
	SectionID int not null,         		-- 区域ID
	CollectID int not null,         		-- 采集器ID
	ComID int not null,         			-- Com
	Address int not null,         			-- Address	
	
	SN varchar(64)  not null,	            -- SN 序列号	
	
	StatusInt  int not null,	          	-- 状态	  
	StatusDesc varchar(1024)  not null,		-- 状态	
	
	GridVoltage float not null,		-- 电网电压
    GridCurrent float not null,	    -- 电网电流
    GridVoltageB float not null,	-- 电网电压B
    GridCurrentB float not null,	-- 电网电流B
    GridVoltageC float not null,	-- 电网电压C
    GridCurrentC float not null,	-- 电网电流C   
    PVYearEnergy float not null,    -- PV本年能量
    PVTotalEnergy float not null,   -- PV总能量 
    PVTemperature float not null,   -- PV温度  
            
    PVVoltage float not null,		    -- PV电压
    PVCurrent float not null,		    -- PV电流
    PVInputPower float not null,       	-- PV A输入功率
    PVVoltageB float not null,		    -- PV电压B
    PVCurrentB float not null,		    -- PV电流B
    PVInputPowerB float not null,      	-- PV B输入功率
    PVVoltageC float not null,		    -- PV电压C
    PVCurrentC float not null,		    -- PV电流C
    PVInputPowerC float not null,      	-- PV C输入功率        
    PVTotalInputPower float not null,	-- PV总输入功率  
    AhPVTotal float not null,			-- PV总输入功率  
              
    Voltage float not null,             -- 交流电压A
    Current float not null,             -- 交流电流 A      
    Frequency float not null,           -- 交流频率 A  
    OutputPower float not null,         -- 交流功率A   
    ROutputPower float not null,        -- 交流无功功率A
    PowerFactor float not null,		    -- 功率因素A
    VoltageB float not null,            -- 交流电压B
    CurrentB float not null,            -- 交流电流B       
    FrequencyB float not null,          -- 交流频率B
    OutputPowerB float not null,        -- 交流功率B 
    ROutputPowerB float not null,       -- 交流无功功率B 
    PowerFactorB float not null,		-- 功率因素B   
    VoltageC float not null,            -- 交流电压C
    CurrentC float not null,            -- 交流电流C        
    FrequencyC float not null,          -- 交流频率C 
    OutputPowerC float not null,        -- 交流功率C
    ROutputPowerC float not null,       -- 交流无功功率C        
    PowerFactorC float not null,		-- 功率因素C
        
    TotalOutputPower float not null,    -- 交流总功率
    RTotalOutputPower float not null,   -- 交流总无功功率
    TotalPowerFactor float not null,    -- 交流总功率因素           
        
    TodayEnergy float not null,         -- 当天能量
    TotalEnergy float not null,         -- 总能量
        
    NTodayEnergy float not null,        -- 反向当天能量
    NTotalEnergy float not null,        -- 反向总能量
        
    AhChargeReset float not null,        -- Ah Resetable
    AhChargeTotal float not null,        -- Ah Total
        
    KwhChargeReset float not null,       -- kWh Resetable
    KwhChargeTotal float not null,       -- kWh Total        
        
    BtryVoltage float not null,		    -- 蓄电池电压
    BtryCurrent float not null,	    	-- 蓄电池电流
    BtryOutputPower float not null,	    -- 蓄电池输出功率  
    AhBtryTotal float not null,	    	-- 蓄电池输出功率  
        
    TemperatureBattery float not null,	-- 蓄电池温度    
    TemperatureRTS float not null,		-- RTS温度
    TemperatureSink float not null	    -- Sink 散热器 
	
);
 
-- 索引
create index index_ChargeData_CurrYear on ChargeData_CurrYear(ClltIDDay asc, ProjectID asc);

-- 删除索引
-- drop index index_ChargeData_CurrYear on ChargeData_CurrYear ;

-- 查看索引
-- EXEC Sp_helpindex ChargeData_CurrYear
-- show index from ChargeData_CurrYear;



create table ChargeStationData_Curr   
(
	IID bigint not null primary key,		-- ID 
	ClltIDDay int not null,             	-- 创建时日期 int
	ClltDaytime datetime  not null,         -- 天 时间
	
	DeviceIndex int not null,         		-- 设备ID	
	
	DayTimes int not null, 					-- 采集ID(本次采集的N个点都有相同的ID)
	
	RegionID int not null,         			-- 区 县市ID
	ProjectID int not null,         		-- 项目ID
	SectionID int not null,         		-- 区域ID
	CollectID int not null,         		-- 采集器ID
	ComID int not null,         			-- Com
	Address int not null,         			-- Address	
	
	SN varchar(64)  not null,	            -- SN 序列号	
	
	StatusInt  int not null,	          	-- 状态	  
	StatusDesc varchar(1024)  not null,		-- 状态	
	
	StationType int not null,				-- 桩类型，直流 交流
        
    GunID int not null,						-- 枪ID        
	SOC int not null,						-- 当前 soc
	
	BMSNeedVoltage float not null,		  	-- BMS 需求电压
    BMSNeedCurrent float not null,		 	-- BMS 需求电流
    
    Voltage float not null,		    		-- 当前充电电压
    Current float not null,		    		-- 当前充电电流  
    
    BMSMeasureVoltage float not null,		-- BMS 测量充电电压
    BMSMeasureCurrent float not null,		-- BMS 测量充电电流
    
    WhSum float not null,           		-- 本次充电电表当前读数
    ThisTimeCharge float not null, 			-- 本次充电电量
    ThisTimeMoney float not null, 			-- 本次充电金额
    
    MaxVoltage float not null,				-- 单体最高电压 
    MaxTemperature float not null,			-- 单体最高温度
    
    ThisTimeRemainderHour float not null,	-- 预计剩余充电时间
    
    GunID1 int not null,				    -- 枪ID        
	SOC1 int not null,						-- 当前 soc
	
	BMSNeedVoltage1 float not null,		  	-- BMS 需求电压
    BMSNeedCurrent1 float not null,		 	-- BMS 需求电流
    
    Voltage1 float not null,		    	-- 当前充电电压
    Current1 float not null,		    	-- 当前充电电流  
    
    BMSMeasureVoltage1 float not null,		-- BMS 测量充电电压
    BMSMeasureCurrent1 float not null,		-- BMS 测量充电电流
    
    WhSum1 float not null,           		-- 本次充电电表当前读数
    ThisTimeCharge1 float not null, 			-- 本次充电电量
    ThisTimeMoney1 float not null, 			-- 本次充电金额
    
    MaxVoltage1 float not null,				-- 单体最高电压 
    MaxTemperature1 float not null,			-- 单体最高温度
    
    ThisTimeRemainderHour1 float not null	-- 预计剩余充电时间
        
);
 
-- 索引
create index index_ChargeStationData_Curr on ChargeStationData_Curr(ProjectID asc);

-- 删除索引
-- drop index index_ChargeStationData_Curr on ChargeStationData_Curr ;

-- 查看索引
-- EXEC Sp_helpindex ChargeStationData_Curr
-- show index from ChargeStationData_Curr;


create table ChargeStationData_CurrYear   
(
	IID bigint not null primary key,		-- ID 
	ClltIDDay int not null,             	-- 创建时日期 int
	ClltDaytime datetime  not null,         -- 天 时间
	
	DeviceIndex int not null,         		-- 设备ID	
	
	DayTimes int not null, 					-- 采集ID(本次采集的N个点都有相同的ID)
	
	RegionID int not null,         			-- 区 县市ID
	ProjectID int not null,         		-- 项目ID
	SectionID int not null,         		-- 区域ID
	CollectID int not null,         		-- 采集器ID
	ComID int not null,         			-- Com
	Address int not null,         			-- Address	
	
	SN varchar(64)  not null,	            -- SN 序列号	
	
	StatusInt  int not null,	          	-- 状态	  
	StatusDesc varchar(1024)  not null,		-- 状态	
	
	StationType int not null,				-- 桩类型，直流 交流
        
    GunID int not null,						-- 枪ID        
	SOC int not null,						-- 当前 soc
	
	BMSNeedVoltage float not null,		  	-- BMS 需求电压
    BMSNeedCurrent float not null,		 	-- BMS 需求电流
    
    Voltage float not null,		    		-- 当前充电电压
    Current float not null,		    		-- 当前充电电流  
    
    BMSMeasureVoltage float not null,		-- BMS 测量充电电压
    BMSMeasureCurrent float not null,		-- BMS 测量充电电流
    
    WhSum float not null,           		-- 本次充电电表当前读数
    ThisTimeCharge float not null, 			-- 本次充电电量
    ThisTimeMoney float not null, 			-- 本次充电金额
    
    MaxVoltage float not null,				-- 单体最高电压 
    MaxTemperature float not null,			-- 单体最高温度
    
    ThisTimeRemainderHour float not null,	-- 预计剩余充电时间
    
    GunID1 int not null,				    -- 枪ID        
	SOC1 int not null,						-- 当前 soc
	
	BMSNeedVoltage1 float not null,		  	-- BMS 需求电压
    BMSNeedCurrent1 float not null,		 	-- BMS 需求电流
    
    Voltage1 float not null,		    	-- 当前充电电压
    Current1 float not null,		    	-- 当前充电电流  
    
    BMSMeasureVoltage1 float not null,		-- BMS 测量充电电压
    BMSMeasureCurrent1 float not null,		-- BMS 测量充电电流
    
    WhSum1 float not null,           		-- 本次充电电表当前读数
    ThisTimeCharge1 float not null, 			-- 本次充电电量
    ThisTimeMoney1 float not null, 			-- 本次充电金额
    
    MaxVoltage1 float not null,				-- 单体最高电压 
    MaxTemperature1 float not null,			-- 单体最高温度
    
    ThisTimeRemainderHour1 float not null	-- 预计剩余充电时间
);
 
-- 索引
create index index_ChargeStationData_CurrYear on ChargeStationData_CurrYear(ClltIDDay asc, ProjectID asc);

-- 删除索引
-- drop index index_ChargeStationData_CurrYear on ChargeStationData_CurrYear ;

-- 查看索引
-- EXEC Sp_helpindex ChargeStationData_CurrYear
-- show index from ChargeStationData_CurrYear;


create table ChargeStationRpt_CurrYear 
(
	IID bigint not null primary key,		 	-- ID 
	ClltIDDay int not null,             		-- 创建时日期 int
	
	DeviceIndex int not null,         			-- 设备ID
	
	ClltIDYear int not null,             		-- 创建时年 int	
	ClltIDMonth int not null,             		-- 创建时月 int
	ClltIDDayIndex int not null,             	-- 创建时日 int
	
	RegionID int not null,         				-- 区 县市ID
	ProjectID int not null,         			-- 项目ID
	SectionID int not null,         			-- 区域ID
	CollectID int not null,         			-- 采集器ID
	ComID int not null,         				-- Com
	Address int not null,         				-- Address	
	
	WhSum float not null,           			-- 本次充电电表当前读数
    ThisTimeCharge float not null, 				-- 本次充电电量
    ThisTimeMoney float not null 				-- 本次充电金额
    	
);
 
-- 索引
create index index_ChargeStationRpt_CurrYear on ChargeStationRpt_CurrYear(ClltIDDay asc, ProjectID asc);

-- 删除索引
-- drop index index_ChargeStationRpt_CurrYear on ChargeStationRpt_CurrYear ;

-- 查看索引
-- EXEC Sp_helpindex ChargeStationRpt_CurrYear
-- show index from ChargeStationRpt_CurrYear;


create table TransformData_Curr   
(
	IID bigint not null primary key,		-- ID 
	ClltIDDay int not null,             	-- 创建时日期 int
	ClltDaytime datetime  not null,         -- 天 时间
	
	DeviceIndex int not null,         		-- 设备ID	
	
	DayTimes int not null, 					-- 采集ID(本次采集的N个点都有相同的ID)
	
	RegionID int not null,         			-- 区 县市ID
	ProjectID int not null,         		-- 项目ID
	SectionID int not null,         		-- 区域ID
	CollectID int not null,         		-- 采集器ID
	ComID int not null,         			-- Com
	Address int not null,         			-- Address	
	
	SN varchar(64)  not null,	            -- SN 序列号	
	
	StatusInt  int not null,	          	-- 状态	  
	StatusDesc varchar(1024)  not null,		-- 状态	
	
	TemperatureA float not null,    		-- flaot 值 温度
	TemperatureB float not null,    		-- flaot 值 温度
    TemperatureC float not null,    		-- flaot 值 温度
    TemperatureD float not null,    		-- flaot 值 温度
        
    AlertBit int not null,                  -- 故障报警状态 1=闭合 0=开
    OverTemperatureBit int not null,        -- 超温报警状态 1=闭合 0=开
    OverTemperatureTripBit int not null,    -- 超温跳闸状态 1=闭合 0=开
    FanControlBit int not null,             -- 风机控制状态 1=闭合 0=开
    IronCoreOverTemperatureBit int not null -- 铁心超温报警状态 1=闭合 0=开
	
);
 
-- 索引
create index index_TransformData_Curr on TransformData_Curr(ProjectID asc);

-- 删除索引
-- drop index index_TransformData_Curr on TransformData_Curr ;

-- 查看索引
-- EXEC Sp_helpindex TransformData_Curr
-- show index from TransformData_Curr;


create table TransformData_CurrYear   
(
	IID bigint not null primary key,		-- ID 
	ClltIDDay int not null,             	-- 创建时日期 int
	ClltDaytime datetime  not null,         -- 天 时间
	
	DeviceIndex int not null,         		-- 设备ID	
	
	DayTimes int not null, 					-- 采集ID(本次采集的N个点都有相同的ID)
	
	RegionID int not null,         			-- 区 县市ID
	ProjectID int not null,         		-- 项目ID
	SectionID int not null,         		-- 区域ID
	CollectID int not null,         		-- 采集器ID
	ComID int not null,         			-- Com
	Address int not null,         			-- Address	
	
	SN varchar(64)  not null,	            -- SN 序列号	
	
	StatusInt  int not null,	          	-- 状态	  
	StatusDesc varchar(1024)  not null,		-- 状态	
	
	TemperatureA float not null,    		-- flaot 值 温度
	TemperatureB float not null,    		-- flaot 值 温度
    TemperatureC float not null,    		-- flaot 值 温度
    TemperatureD float not null,    		-- flaot 值 温度
        
    AlertBit int not null,                  -- 故障报警状态 1=闭合 0=开
    OverTemperatureBit int not null,        -- 超温报警状态 1=闭合 0=开
    OverTemperatureTripBit int not null,    -- 超温跳闸状态 1=闭合 0=开
    FanControlBit int not null,             -- 风机控制状态 1=闭合 0=开
    IronCoreOverTemperatureBit int not null -- 铁心超温报警状态 1=闭合 0=开
	
);
 
-- 索引
create index index_TransformData_CurrYear on TransformData_CurrYear(ClltIDDay asc, ProjectID asc);

-- 删除索引
-- drop index index_TransformData_CurrYear on TransformData_CurrYear ;

-- 查看索引
-- EXEC Sp_helpindex TransformData_CurrYear
-- show index from TransformData_CurrYear;









-- 创建视图
create view view_alertdata_curr AS 
SELECT
alertdata_curr.IID AS IID,
alertdata_curr.ClltIDDay AS ClltIDDay,
alertdata_curr.ClltDaytime AS ClltDaytime,
alertdata_curr.DeviceIndex AS DeviceIndex,
alertdata_curr.DayTimes AS DayTimes,
alertdata_curr.ClltIDYear AS ClltIDYear,
alertdata_curr.ClltIDMonth AS ClltIDMonth,
alertdata_curr.ClltIDDayIndex AS ClltIDDayIndex,
alertdata_curr.RegionID AS RegionID,
alertdata_curr.ProjectID AS ProjectID,
alertdata_curr.SectionID AS SectionID,
alertdata_curr.CollectID AS CollectID,
alertdata_curr.ComID AS ComID,
alertdata_curr.Address AS Address,
alertdata_curr.StatusInt AS StatusInt,
alertdata_curr.StatusDesc AS StatusDesc,
deviceinfo.ComLinkType AS ComLinkType,
deviceinfo.DeviceName AS DeviceName,
deviceinfo.DeviceID AS DeviceID,
deviceinfo.Description AS Description,
deviceinfo.Caption AS Caption
from (alertdata_curr join deviceinfo on(((alertdata_curr.ProjectID = deviceinfo.ProjectID) and (alertdata_curr.ComID = deviceinfo.ComID) and (alertdata_curr.Address = deviceinfo.Address)))) ;



-- 创建视图
create view view_bmsdata_curr AS 
SELECT
bmsdata_curr.IID AS IID,
bmsdata_curr.ClltIDDay AS ClltIDDay,
bmsdata_curr.ClltDaytime AS ClltDaytime,
bmsdata_curr.DeviceIndex AS DeviceIndex,
bmsdata_curr.DayTimes AS DayTimes,
bmsdata_curr.RegionID AS RegionID,
bmsdata_curr.ProjectID AS ProjectID,
bmsdata_curr.SectionID AS SectionID,
bmsdata_curr.CollectID AS CollectID,
bmsdata_curr.ComID AS ComID,
bmsdata_curr.Address AS Address,
bmsdata_curr.SN AS SN,
bmsdata_curr.StatusInt AS StatusInt,
bmsdata_curr.StatusDesc AS StatusDesc,
bmsdata_curr.Voltage AS Voltage,
bmsdata_curr.Current AS Current,
bmsdata_curr.SOC AS SOC,
bmsdata_curr.SOH AS SOH,
bmsdata_curr.TodayCharge AS TodayCharge,
bmsdata_curr.TodayDisCharge AS TodayDisCharge,
bmsdata_curr.TotalCharge AS TotalCharge,
bmsdata_curr.TotalDisCharge AS TotalDisCharge,
bmsdata_curr.ChargeCount AS ChargeCount,
bmsdata_curr.DisChargeCount AS DisChargeCount,
bmsdata_curr.MaxVoltage AS MaxVoltage,
bmsdata_curr.MaxVoltageGroup AS MaxVoltageGroup,
bmsdata_curr.MaxVoltageUnit AS MaxVoltageUnit,
bmsdata_curr.MinVoltage AS MinVoltage,
bmsdata_curr.MinVoltageGroup AS MinVoltageGroup,
bmsdata_curr.MinVoltageUnit AS MinVoltageUnit,
bmsdata_curr.MaxTemperature AS MaxTemperature,
bmsdata_curr.MaxTemperatureGroup AS MaxTemperatureGroup,
bmsdata_curr.MaxTemperatureUnit AS MaxTemperatureUnit,
bmsdata_curr.MinTemperature AS MinTemperature,
bmsdata_curr.MinTemperatureGroup AS MinTemperatureGroup,
bmsdata_curr.MinTemperatureUnit AS MinTemperatureUnit,
bmsdata_curr.GroupCount AS GroupCount,
bmsdata_curr.GroupUnitCount AS GroupUnitCount,
bmsdata_curr.ChargeMode AS ChargeMode,
bmsdata_curr.ConstantVoltage AS ConstantVoltage,
bmsdata_curr.ConstantCurrent AS ConstantCurrent,
bmsdata_curr.PurlingCurrent AS PurlingCurrent,
deviceinfo.ComLinkType AS ComLinkType,
deviceinfo.DeviceName AS DeviceName,
deviceinfo.DeviceID AS DeviceID,
deviceinfo.Description AS Description,
deviceinfo.Caption AS Caption
from (bmsdata_curr join deviceinfo on(((bmsdata_curr.ProjectID = deviceinfo.ProjectID) and (bmsdata_curr.ComID = deviceinfo.ComID) and (bmsdata_curr.Address = deviceinfo.Address)))) ;



-- 创建视图
create view view_chargestationdata_curr AS 
SELECT
chargestationdata_curr.IID AS IID,
chargestationdata_curr.ClltIDDay AS ClltIDDay,
chargestationdata_curr.DeviceIndex AS DeviceIndex,
chargestationdata_curr.DayTimes AS DayTimes,
chargestationdata_curr.ClltDaytime AS ClltDaytime,
chargestationdata_curr.RegionID AS RegionID,
chargestationdata_curr.ProjectID AS ProjectID,
chargestationdata_curr.SectionID AS SectionID,
chargestationdata_curr.CollectID AS CollectID,
chargestationdata_curr.ComID AS ComID,
chargestationdata_curr.Address AS Address,
chargestationdata_curr.SN AS SN,
chargestationdata_curr.StatusInt AS StatusInt,
chargestationdata_curr.StatusDesc AS StatusDesc,
chargestationdata_curr.StationType AS StationType,
chargestationdata_curr.GunID AS GunID,
chargestationdata_curr.SOC AS SOC,
chargestationdata_curr.BMSNeedVoltage AS BMSNeedVoltage,
chargestationdata_curr.BMSNeedCurrent AS BMSNeedCurrent,
chargestationdata_curr.Voltage AS Voltage,
chargestationdata_curr.Current AS Current,
chargestationdata_curr.BMSMeasureVoltage AS BMSMeasureVoltage,
chargestationdata_curr.BMSMeasureCurrent AS BMSMeasureCurrent,
chargestationdata_curr.WhSum AS WhSum,
chargestationdata_curr.ThisTimeCharge AS ThisTimeCharge,
chargestationdata_curr.ThisTimeMoney AS ThisTimeMoney,
chargestationdata_curr.MaxVoltage AS MaxVoltage,
chargestationdata_curr.MaxTemperature AS MaxTemperature,
chargestationdata_curr.ThisTimeRemainderHour AS ThisTimeRemainderHour,
chargestationdata_curr.GunID1 AS GunID1,
chargestationdata_curr.SOC1 AS SOC1,
chargestationdata_curr.BMSNeedVoltage1 AS BMSNeedVoltage1,
chargestationdata_curr.BMSNeedCurrent1 AS BMSNeedCurrent1,
chargestationdata_curr.Voltage1 AS Voltage1,
chargestationdata_curr.Current1 AS Current1,
chargestationdata_curr.BMSMeasureVoltage1 AS BMSMeasureVoltage1,
chargestationdata_curr.BMSMeasureCurrent1 AS BMSMeasureCurrent1,
chargestationdata_curr.WhSum1 AS WhSum1,
chargestationdata_curr.ThisTimeCharge1 AS ThisTimeCharge1,
chargestationdata_curr.ThisTimeMoney1 AS ThisTimeMoney1,
chargestationdata_curr.MaxVoltage1 AS MaxVoltage1,
chargestationdata_curr.MaxTemperature1 AS MaxTemperature1,
chargestationdata_curr.ThisTimeRemainderHour1 AS ThisTimeRemainderHour1,
deviceinfo.ComLinkType AS ComLinkType,
deviceinfo.DeviceName AS DeviceName,
deviceinfo.DeviceID AS DeviceID,
deviceinfo.Description AS Description,
deviceinfo.Caption AS Caption
from (chargestationdata_curr join deviceinfo on(((chargestationdata_curr.ProjectID = deviceinfo.ProjectID) and (chargestationdata_curr.ComID = deviceinfo.ComID) and (chargestationdata_curr.Address = deviceinfo.Address))));



-- 创建视图
create view view_epmdata_curr AS 
SELECT
epmdata_curr.IID AS IID,
epmdata_curr.ClltIDDay AS ClltIDDay,
epmdata_curr.ClltDaytime AS ClltDaytime,
epmdata_curr.DeviceIndex AS DeviceIndex,
epmdata_curr.DayTimes AS DayTimes,
epmdata_curr.RegionID AS RegionID,
epmdata_curr.ProjectID AS ProjectID,
epmdata_curr.SectionID AS SectionID,
epmdata_curr.CollectID AS CollectID,
epmdata_curr.ComID AS ComID,
epmdata_curr.Address AS Address,
epmdata_curr.SN AS SN,
epmdata_curr.StatusInt AS StatusInt,
epmdata_curr.StatusDesc AS StatusDesc,
epmdata_curr.Voltage1 AS Voltage1,
epmdata_curr.Voltage2 AS Voltage2,
epmdata_curr.Voltage3 AS Voltage3,
epmdata_curr.Current1 AS Current1,
epmdata_curr.Current2 AS Current2,
epmdata_curr.Current3 AS Current3,
epmdata_curr.Frequency1 AS Frequency1,
epmdata_curr.Frequency2 AS Frequency2,
epmdata_curr.Frequency3 AS Frequency3,
epmdata_curr.ActivePower1 AS ActivePower1,
epmdata_curr.ActivePower2 AS ActivePower2,
epmdata_curr.ActivePower3 AS ActivePower3,
epmdata_curr.Var1 AS Var1,
epmdata_curr.Var2 AS Var2,
epmdata_curr.Var3 AS Var3,
epmdata_curr.PowerFactor1 AS PowerFactor1,
epmdata_curr.PowerFactor2 AS PowerFactor2,
epmdata_curr.PowerFactor3 AS PowerFactor3,
epmdata_curr.VSum AS VSum,
epmdata_curr.ASum AS ASum,
epmdata_curr.WSum AS WSum,
epmdata_curr.VarSum AS VarSum,
epmdata_curr.PFSum AS PFSum,
epmdata_curr.WhSum AS WhSum,
epmdata_curr.VarhSum AS VarhSum,
epmdata_curr.WhNSum AS WhNSum,
epmdata_curr.VarhNSum AS VarhNSum,
epmdata_curr.Voltage1N AS Voltage1N,
epmdata_curr.Voltage2N AS Voltage2N,
epmdata_curr.Voltage3N AS Voltage3N,
epmdata_curr.THDV1 AS THDV1,
epmdata_curr.THDV2 AS THDV2,
epmdata_curr.THDV3 AS THDV3,
epmdata_curr.THDA1 AS THDA1,
epmdata_curr.THDA2 AS THDA2,
epmdata_curr.THDA3 AS THDA3,
epmdata_curr.WhToday AS WhToday,
epmdata_curr.VarhToday AS VarhToday,
epmdata_curr.WhNToday AS WhNToday,
epmdata_curr.VarhNToday AS VarhNToday,
epmdata_curr.WhJian AS WhJian,
epmdata_curr.WhFeng AS WhFeng,
epmdata_curr.WhPing AS WhPing,
epmdata_curr.WhGu AS WhGu,
epmdata_curr.WhNJian AS WhNJian,
epmdata_curr.WhNFeng AS WhNFeng,
epmdata_curr.WhNPing AS WhNPing,
epmdata_curr.WhNGu AS WhNGu,
epmdata_curr.WhLastMonth AS WhLastMonth,
epmdata_curr.WhLastMonthJian AS WhLastMonthJian,
epmdata_curr.WhLastMonthFeng AS WhLastMonthFeng,
epmdata_curr.WhLastMonthPing AS WhLastMonthPing,
epmdata_curr.WhLastMonthGu AS WhLastMonthGu,
epmdata_curr.WhNLastMonth AS WhNLastMonth,
epmdata_curr.WhNLastMonthJian AS WhNLastMonthJian,
epmdata_curr.WhNLastMonthFeng AS WhNLastMonthFeng,
epmdata_curr.WhNLastMonthPing AS WhNLastMonthPing,
epmdata_curr.WhNLastMonthGu AS WhNLastMonthGu,
epmdata_curr.DI0 AS DI0,
epmdata_curr.DI1 AS DI1,
epmdata_curr.DI2 AS DI2,
epmdata_curr.DI3 AS DI3,
epmdata_curr.DO0 AS DO0,
epmdata_curr.DO1 AS DO1,
epmdata_curr.DO2 AS DO2,
epmdata_curr.DO3 AS DO3,
deviceinfo.ComLinkType AS ComLinkType,
deviceinfo.DeviceName AS DeviceName,
deviceinfo.DeviceID AS DeviceID,
deviceinfo.Description AS Description,
deviceinfo.Caption AS Caption
from (epmdata_curr join deviceinfo on(((epmdata_curr.ProjectID = deviceinfo.ProjectID) and (epmdata_curr.ComID = deviceinfo.ComID) and (epmdata_curr.Address = deviceinfo.Address))));



-- 创建视图
create view view_inverterdata_curr AS 
SELECT
inverterdata_curr.IID AS IID,
inverterdata_curr.ClltIDDay AS ClltIDDay,
inverterdata_curr.ClltDaytime AS ClltDaytime,
inverterdata_curr.DeviceIndex AS DeviceIndex,
inverterdata_curr.DayTimes AS DayTimes,
inverterdata_curr.RegionID AS RegionID,
inverterdata_curr.ProjectID AS ProjectID,
inverterdata_curr.SectionID AS SectionID,
inverterdata_curr.CollectID AS CollectID,
inverterdata_curr.ComID AS ComID,
inverterdata_curr.Address AS Address,
inverterdata_curr.SN AS SN,
inverterdata_curr.StatusInt AS StatusInt,
inverterdata_curr.StatusDesc AS StatusDesc,
inverterdata_curr.PVVoltage AS PVVoltage,
inverterdata_curr.PVCurrent AS PVCurrent,
inverterdata_curr.PVOutputPower AS PVOutputPower,
inverterdata_curr.PVVoltageB AS PVVoltageB,
inverterdata_curr.PVCurrentB AS PVCurrentB,
inverterdata_curr.PVOutputPowerB AS PVOutputPowerB,
inverterdata_curr.PVVoltageC AS PVVoltageC,
inverterdata_curr.PVCurrentC AS PVCurrentC,
inverterdata_curr.PVOutputPowerC AS PVOutputPowerC,
inverterdata_curr.PVTotalOutputPower AS PVTotalOutputPower,
inverterdata_curr.VoltageA AS VoltageA,
inverterdata_curr.CurrentA AS CurrentA,
inverterdata_curr.FrequencyA AS FrequencyA,
inverterdata_curr.OutputPowerA AS OutputPowerA,
inverterdata_curr.ROutputPowerA AS ROutputPowerA,
inverterdata_curr.PowerFactorA AS PowerFactorA,
inverterdata_curr.VoltageB AS VoltageB,
inverterdata_curr.CurrentB AS CurrentB,
inverterdata_curr.FrequencyB AS FrequencyB,
inverterdata_curr.OutputPowerB AS OutputPowerB,
inverterdata_curr.ROutputPowerB AS ROutputPowerB,
inverterdata_curr.PowerFactorB AS PowerFactorB,
inverterdata_curr.VoltageC AS VoltageC,
inverterdata_curr.CurrentC AS CurrentC,
inverterdata_curr.FrequencyC AS FrequencyC,
inverterdata_curr.OutputPowerC AS OutputPowerC,
inverterdata_curr.ROutputPowerC AS ROutputPowerC,
inverterdata_curr.PowerFactorC AS PowerFactorC,
inverterdata_curr.TotalOutputPower AS TotalOutputPower,
inverterdata_curr.RTotalOutputPower AS RTotalOutputPower,
inverterdata_curr.TotalPowerFactor AS TotalPowerFactor,
inverterdata_curr.TodayEnergy AS TodayEnergy,
inverterdata_curr.TotalEnergy AS TotalEnergy,
inverterdata_curr.NTodayEnergy AS NTodayEnergy,
inverterdata_curr.NTotalEnergy AS NTotalEnergy,
inverterdata_curr.RTodayEnergy AS RTodayEnergy,
inverterdata_curr.RTotalEnergy AS RTotalEnergy,
inverterdata_curr.NRTodayEnergy AS NRTodayEnergy,
inverterdata_curr.NRTotalEnergy AS NRTotalEnergy,
inverterdata_curr.BtryVoltage AS BtryVoltage,
inverterdata_curr.BtryCurrent AS BtryCurrent,
inverterdata_curr.BtryOutputPower AS BtryOutputPower,
inverterdata_curr.TemperatureInverter AS TemperatureInverter,
inverterdata_curr.TemperatureBooster AS TemperatureBooster,
inverterdata_curr.TemperatureSink AS TemperatureSink,
inverterdata_curr.TodayHour AS TodayHour,
inverterdata_curr.TotalHour AS TotalHour,
inverterdata_curr.AGCSetting AS AGCSetting,
inverterdata_curr.AVCSetting AS AVCSetting,
inverterdata_curr.GroundResistance AS GroundResistance,
inverterdata_curr.LeakCurrent AS LeakCurrent,
inverterdata_curr.StringVoltage1 AS StringVoltage1,
inverterdata_curr.StringCurrent1 AS StringCurrent1,
inverterdata_curr.StringVoltage2 AS StringVoltage2,
inverterdata_curr.StringCurrent2 AS StringCurrent2,
inverterdata_curr.StringVoltage3 AS StringVoltage3,
inverterdata_curr.StringCurrent3 AS StringCurrent3,
inverterdata_curr.StringVoltage4 AS StringVoltage4,
inverterdata_curr.StringCurrent4 AS StringCurrent4,
inverterdata_curr.StringVoltage5 AS StringVoltage5,
inverterdata_curr.StringCurrent5 AS StringCurrent5,
inverterdata_curr.StringVoltage6 AS StringVoltage6,
inverterdata_curr.StringCurrent6 AS StringCurrent6,
inverterdata_curr.StringVoltage7 AS StringVoltage7,
inverterdata_curr.StringCurrent7 AS StringCurrent7,
inverterdata_curr.StringVoltage8 AS StringVoltage8,
inverterdata_curr.StringCurrent8 AS StringCurrent8,
inverterdata_curr.StringVoltage9 AS StringVoltage9,
inverterdata_curr.StringCurrent9 AS StringCurrent9,
inverterdata_curr.StringVoltage10 AS StringVoltage10,
inverterdata_curr.StringCurrent10 AS StringCurrent10,
inverterdata_curr.StringVoltage11 AS StringVoltage11,
inverterdata_curr.StringCurrent11 AS StringCurrent11,
inverterdata_curr.StringVoltage12 AS StringVoltage12,
inverterdata_curr.StringCurrent12 AS StringCurrent12,
inverterdata_curr.StringVoltage13 AS StringVoltage13,
inverterdata_curr.StringCurrent13 AS StringCurrent13,
inverterdata_curr.StringVoltage14 AS StringVoltage14,
inverterdata_curr.StringCurrent14 AS StringCurrent14,
inverterdata_curr.StringVoltage15 AS StringVoltage15,
inverterdata_curr.StringCurrent15 AS StringCurrent15,
inverterdata_curr.StringVoltage16 AS StringVoltage16,
inverterdata_curr.StringCurrent16 AS StringCurrent16,
inverterdata_curr.StringVoltage17 AS StringVoltage17,
inverterdata_curr.StringCurrent17 AS StringCurrent17,
inverterdata_curr.StringVoltage18 AS StringVoltage18,
inverterdata_curr.StringCurrent18 AS StringCurrent18,
inverterdata_curr.StringVoltage19 AS StringVoltage19,
inverterdata_curr.StringCurrent19 AS StringCurrent19,
inverterdata_curr.StringVoltage20 AS StringVoltage20,
inverterdata_curr.StringCurrent20 AS StringCurrent20,
inverterdata_curr.StringVoltage21 AS StringVoltage21,
inverterdata_curr.StringCurrent21 AS StringCurrent21,
inverterdata_curr.StringVoltage22 AS StringVoltage22,
inverterdata_curr.StringCurrent22 AS StringCurrent22,
inverterdata_curr.StringVoltage23 AS StringVoltage23,
inverterdata_curr.StringCurrent23 AS StringCurrent23,
inverterdata_curr.StringVoltage24 AS StringVoltage24,
inverterdata_curr.StringCurrent24 AS StringCurrent24,
inverterdata_curr.MotherLineVoltage AS MotherLineVoltage,
inverterdata_curr.GroudVoltage AS GroudVoltage,
inverterdata_curr.NGroudVoltage AS NGroudVoltage,
inverterdata_curr.DesignPower AS DesignPower,
inverterdata_curr.VoltageAB AS VoltageAB,
inverterdata_curr.VoltageBC AS VoltageBC,
inverterdata_curr.VoltageCA AS VoltageCA,
inverterdata_curr.SystemTime AS SystemTime,
inverterdata_curr.Efficiency AS Efficiency,
inverterdata_curr.PowerOnSecond AS PowerOnSecond,
inverterdata_curr.PowerOffSecond AS PowerOffSecond,
inverterdata_curr.MPPT1 AS MPPT1,
inverterdata_curr.MPPT2 AS MPPT2,
inverterdata_curr.MPPT3 AS MPPT3,
inverterdata_curr.MPPT4 AS MPPT4,
deviceinfo.ComLinkType AS ComLinkType,
deviceinfo.DeviceName AS DeviceName,
deviceinfo.DeviceID AS DeviceID,
deviceinfo.Description AS Description,
deviceinfo.Caption AS Caption
FROM ( inverterdata_curr JOIN deviceinfo ON ((( inverterdata_curr.ProjectID = deviceinfo.ProjectID ) AND ( inverterdata_curr.ComID = deviceinfo.ComID ) AND ( inverterdata_curr.Address = deviceinfo.Address ))));




-- 创建视图
create view view_pcsdata_curr AS 
SELECT
pcsdata_curr.IID AS IID,
pcsdata_curr.ClltIDDay AS ClltIDDay,
pcsdata_curr.ClltDaytime AS ClltDaytime,
pcsdata_curr.DeviceIndex AS DeviceIndex,
pcsdata_curr.DayTimes AS DayTimes,
pcsdata_curr.RegionID AS RegionID,
pcsdata_curr.ProjectID AS ProjectID,
pcsdata_curr.SectionID AS SectionID,
pcsdata_curr.CollectID AS CollectID,
pcsdata_curr.ComID AS ComID,
pcsdata_curr.Address AS Address,
pcsdata_curr.SN AS SN,
pcsdata_curr.StatusInt AS StatusInt,
pcsdata_curr.StatusDesc AS StatusDesc,
pcsdata_curr.Udc AS Udc,
pcsdata_curr.Idc AS Idc,
pcsdata_curr.Pdc AS Pdc,
pcsdata_curr.Ua AS Ua,
pcsdata_curr.Ub AS Ub,
pcsdata_curr.Uc AS Uc,
pcsdata_curr.Uab AS Uab,
pcsdata_curr.Ubc AS Ubc,
pcsdata_curr.Uca AS Uca,
pcsdata_curr.Ia AS Ia,
pcsdata_curr.Ib AS Ib,
pcsdata_curr.Ic AS Ic,
pcsdata_curr.P AS P,
pcsdata_curr.Q AS Q,
pcsdata_curr.S AS S,
pcsdata_curr.PF AS PF,
pcsdata_curr.F AS F,
pcsdata_curr.Udc1 AS Udc1,
pcsdata_curr.Idc1 AS Idc1,
pcsdata_curr.Pdc1 AS Pdc1,
pcsdata_curr.Ua1 AS Ua1,
pcsdata_curr.Ub1 AS Ub1,
pcsdata_curr.Uc1 AS Uc1,
pcsdata_curr.Uab1 AS Uab1,
pcsdata_curr.Ubc1 AS Ubc1,
pcsdata_curr.Uca1 AS Uca1,
pcsdata_curr.Ia1 AS Ia1,
pcsdata_curr.Ib1 AS Ib1,
pcsdata_curr.Ic1 AS Ic1,
pcsdata_curr.P1 AS P1,
pcsdata_curr.Q1 AS Q1,
pcsdata_curr.S1 AS S1,
pcsdata_curr.PF1 AS PF1,
pcsdata_curr.F1 AS F1,
pcsdata_curr.Udc2 AS Udc2,
pcsdata_curr.Idc2 AS Idc2,
pcsdata_curr.Pdc2 AS Pdc2,
pcsdata_curr.Ua2 AS Ua2,
pcsdata_curr.Ub2 AS Ub2,
pcsdata_curr.Uc2 AS Uc2,
pcsdata_curr.Uab2 AS Uab2,
pcsdata_curr.Ubc2 AS Ubc2,
pcsdata_curr.Uca2 AS Uca2,
pcsdata_curr.Ia2 AS Ia2,
pcsdata_curr.Ib2 AS Ib2,
pcsdata_curr.Ic2 AS Ic2,
pcsdata_curr.P2 AS P2,
pcsdata_curr.Q2 AS Q2,
pcsdata_curr.S2 AS S2,
pcsdata_curr.PF2 AS PF2,
pcsdata_curr.F2 AS F2,
pcsdata_curr.TodayHourCharge AS TodayHourCharge,
pcsdata_curr.YearyHourCharge AS YearyHourCharge,
pcsdata_curr.TotalHourCharge AS TotalHourCharge,
pcsdata_curr.TodayHourDisCharge AS TodayHourDisCharge,
pcsdata_curr.YearyHourDisCharge AS YearyHourDisCharge,
pcsdata_curr.TotalHourDisCharge AS TotalHourDisCharge,
pcsdata_curr.TodayEnergyCharge AS TodayEnergyCharge,
pcsdata_curr.YearEnergyCharge AS YearEnergyCharge,
pcsdata_curr.TotalEnergyCharge AS TotalEnergyCharge,
pcsdata_curr.TodayEnergyDisCharge AS TodayEnergyDisCharge,
pcsdata_curr.YearEnergyDisCharge AS YearEnergyDisCharge,
pcsdata_curr.TotalEnergyDisCharge AS TotalEnergyDisCharge,
pcsdata_curr.ChargeCount AS ChargeCount,
pcsdata_curr.DisChargeCount AS DisChargeCount,
pcsdata_curr.Temperature AS Temperature,
pcsdata_curr.GroundResistance AS GroundResistance,
pcsdata_curr.GroundResistanceN AS GroundResistanceN,
pcsdata_curr.LeakCurrent AS LeakCurrent,
pcsdata_curr.Mode AS Mode,
pcsdata_curr.ChargePower AS ChargePower,
pcsdata_curr.DisChargePower AS DisChargePower,
deviceinfo.ComLinkType AS ComLinkType,
deviceinfo.DeviceName AS DeviceName,
deviceinfo.DeviceID AS DeviceID,
deviceinfo.Description AS Description,
deviceinfo.Caption AS Caption
from (pcsdata_curr join deviceinfo on(((pcsdata_curr.ProjectID = deviceinfo.ProjectID) and (pcsdata_curr.ComID = deviceinfo.ComID) and (pcsdata_curr.Address = deviceinfo.Address)))) ;









--  中文乱码测试 
create table UserInfo1 
(
    IID bigint not null primary key,	                -- ID 
    UserName varchar(32)  not null,           		-- 姓名  
    Password varchar(32)  not null,           		-- 密码 
    age int not null	
);

insert into UserInfo1 values(1,'利州','123',18);
insert into UserInfo1 values(2,'allen','密码',28);

select * from UserInfo1;




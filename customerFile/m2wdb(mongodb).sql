// show databases;

// drop table if exists M2WDB.student;
// drop database if exists M2WDB;

// create database M2WDB default charset utf8mb4 COLLATE utf8_general_ci;
// use M2WDB;



db.createCollection("MaxIDInfo");

db.MaxIDInfo.insert(
   {	
        _id:1, 
   		MaxIDType: 0, 
  	    MaxIDValue: 0
   }
);





db.createCollection("UserInfo");

db.UserInfo.insert(
   {	
        _id:1, 
   		UserID: 0,		 					
	
	    UserName: "",           		  
	    Password: "",   
	    Mobile: "",   
	    Tel: "",   
		QQ: "",   
		Wechat: "",   
		Address: "",   
		Email: "" 
   }
);


// 索引
db.UserInfo.createIndex({"UserID":1},{name:"idx_UserID"});

// 删除索引
// db.UserInfo.dropIndex("idx_UserID");

// 查看索引
// db.UserInfo.getIndexes();






db.createCollection("UserProject");

db.UserProject.insert(
	{
		_id:1,
		UserProjectID: 0,		 				 
		
	    UserID: 0,		 					 
	    ProjectID: 0	
	}
);

// 索引
db.UserProject.createIndex({"ProjectID":1},{name:"idx_ProjectID"});

// 删除索引
// db.UserProject.dropIndex("idx_ProjectID");

// 查看索引
// db.UserProject.getIndexes();





db.createCollection("RegionInfo");

db.RegionInfo.insert(
   	{	
		_id:1,
		RegionID: 0,		 					   
		                                        
	    RegionNo: "",               	
	    RegionName: "",             	  	   
	    RegionDescription: "",      	    
		    
		Caption: "",              	
		    
		RegionUser: ""   
	}
);

// 索引
db.RegionInfo.createIndex({"RegionID":1},{name:"idx_RegionID"});

// 删除索引
// db.RegionInfo.dropIndex("idx_RegionID");

// 查看索引
// db.RegionInfo.getIndexes();





db.createCollection("ProjectInfo");

db.ProjectInfo.insert(
   	{	
		_id:1,
		ProjectID: 0,		 					   
		
		RegionID: 0, 						
		    
	    ProjectNo: "",               
	    ProjectName: "",           	  
		ProjectDesignPower: 0,        		
	    ProjectAddress: "",         	
	    ProjectDescription: "",    	 
		    
		Caption: "",               	 
		
		ProjectFupinCollectID: "", 	
	    ProjectFupinStationID: 0,	            
	        
		StationType: 0, 						
		    
		ProjectCustomerCount: 0, 				
		    
		ProjectUser: "",          	
		ControlUser: "",          	
	    ProjectEnergyEPM: 0, 					
	        	    
		ProjectMainLine: "",   		
		    
		ProjectLongitude: 0.0, 				
		ProjectLatitude: 0.0, 				
		ProjectMapGrade: 0, 					
		    
		ProjectPerson: "",          	
		ProjectTel: "",            	
		ProjectBankName: "",        	
		ProjectBankCard: "",        	
		ProjectInstallDay: "",       	
		ProjectGridDay: ""      
	}
);

// 索引
db.ProjectInfo.createIndex({"ProjectID":1},{name:"idx_ProjectID"});

// 删除索引
// db.ProjectInfo.dropIndex("idx_ProjectID");

// 查看索引
// db.ProjectInfo.getIndexes();





db.createCollection("SectionInfo");

db.SectionInfo.insert(
   	{	
		_id:1,
		SectionID: 0,		 					  
		
		RegionID: 0, 							 
		ProjectID: 0, 	        			
		    
		SectionNo: "", 
	    SectionName: "",        
	   	SectionAddress: "", 
	    SectionDescription: "",      
	        
	   	Caption: ""   	
	}
);

// 索引
db.SectionInfo.createIndex({"ProjectID":1},{name:"idx_ProjectID"});

// 删除索引
// db.SectionInfo.dropIndex("idx_ProjectID");

// 查看索引
// db.SectionInfo.getIndexes();





db.createCollection("CollectorInfo");

db.CollectorInfo.insert(
   	{	   
		_id:1,
		CollectID: 0,		 					  
		
		RegionID: 0, 							 
		ProjectID: 0,            				
		SectionID: 0,             			
			    
		CollectNo: "", 
	    CollectName: "",        
	    CollectAddress: "", 
	    CollectDescription: "",     
	        
	    Caption: ""   
	}
);
 
// 索引
db.CollectorInfo.createIndex({"ProjectID":1},{name:"idx_ProjectID"});

// 删除索引
// db.CollectorInfo.dropIndex("idx_ProjectID");

// 查看索引
// db.CollectorInfo.getIndexes();






db.createCollection("ComInfo");

db.ComInfo.insert(
   	{	
		_id:1,
		ComID: 0,		 						   
		
		RegionID: 0, 							
		ProjectID: 0,                     		
		SectionID: 0,                     	
		CollectID: 0,                     	
					
		DevCom: 0, 							
			
		ComName: "",               	
			
		COM: 0, 		                     	
		Baudrate: 0, 	                      	
		Databit: 0, 			              
		Stopbit: "", 			     	
		Parity: "", 			      		
					
	    Description: "", 		  	
	    Caption: "",               	
	        
	    IP: "", 	                	
		Port: 0,                         		
		Timeout: 0,                         	
			
		ServerComtoGPRSCom: 0, 				 
	    ServerComtoGPRSMin: "",    	
	        
	    CollectMinuteThirdCollect: "", 	
	    
	    ComtoComTrans: 0, 					
	    
	    IsGPRSMode: 0,                     	
	       
	    IsCANMode: 0,                     	
	        
	    IsSMSMode: 0,                      	 
	        
	    Is104Mode: 0,                       	     
	        
		Is101Mode: 0,                       	
		
		CollectSecond: 0		
	}
);

// 索引
db.ComInfo.createIndex({"ProjectID":1},{name:"idx_ProjectID"});

// 删除索引
// db.ComInfo.dropIndex("idx_ProjectID");

// 查看索引
// db.ComInfo.getIndexes();






db.createCollection("DeviceInfo");

db.DeviceInfo.insert(
   	{	
		_id:1,
		DeviceIndex: 0,		 				
		
		RegionID: 0, 							
		ProjectID: 0,                     		
		SectionID: 0,                     	
		CollectID: 0,                     	
		ComID: 0,		 						  
		
		ComLinkType: 0,	                   	
		
		DeviceName: "",		    	
			
		Address: 0,			            		
		DeviceID: "",		   						
		Description: "",		     	
		Caption: "",              	
		PN: "",						
		SN: "",					
		
		DeviceKey: "",				
		
		DesignPower: 0,						
		EnergyOffset: 0.0, 					
		
		Factor: 0.0,                     		
		FactorV: 0.0,                     	
		FactorP: 0.0,                      	
		FactorQ: 0.0,                    		
		FactorPF: 0.0,                   		
			
		ChargeOffset: 0.0,					
		DisChargeOffset: 0.0,	             	
			
		BMSGroupCount: 0, 					
		BMSGroupDescription: "",		
		BMSGroupUnitCount: 0, 				
		BMSGroupUnitDescription: "",	       
	    BMSGroupTemperatureCount: 0, 			
		BMSGroupTemperatureDescription: "",	
	    BMSMaxSOC: 0.0, 						
	    BMSMinSOC: 0.0, 							
	            
	    MaxChannel: 0,                     	
	    
	    YCID: 0,								
		YCType: 0,							
		YXID: 0,								
		YTID: 0,								
		YTType: 0,							
		YTDataID: 0,                       	
		YKID: 0,								
				    
	    MaxOutputPower: 0.0,					
	    MaxTodayEnergy: 0.0,					
	    MaxTotalEnergy: 0.0,					
	    MaxVoltage: 0.0,						
	    MaxCurrent: 0.0,						
	    MaxTemperature: 0.0,				
	    MaxIrradiance: 0.0,					     
	    MinTotalEnergy: 0.0,					
	    
	    
		ClltDaytime: 1528183743111,         		
		StatusInt : 0,	          				  
		StatusDesc: "",				
		Power: 0.0,							
		PositiveValue: 0.0,					
		NegativeValue: 0.0,					
		RemainPara3: 0.0,						
		RemainPara4: 0.0,						
		RemainPara5: 0.0		
	}
);
 
// 索引
db.DeviceInfo.createIndex({"ProjectID":1},{name:"idx_ProjectID"});

// 删除索引
// db.DeviceInfo.dropIndex("idx_ProjectID");

// 查看索引
// db.DeviceInfo.getIndexes();





db.createCollection("CollectorStatus_Curr");

db.CollectorStatus_Curr.insert(
   	{	
		_id:1,
		ClltIDDay: 0,             			
		ClltDaytime: 1528183743111,         	
		
		CollectID: 0,                     		
		
		RegionID: 0,         					
		ProjectID: 0,         				
		SectionID: 0,         				
		
		StatusInt : 0,	            		  
		StatusDesc: "",				
		
		ClientIP: "",					
		ClientPort : 0	
	}
);
 
// 索引
db.CollectorStatus_Curr.createIndex({"ProjectID":1},{name:"idx_ProjectID"});

// 删除索引
// db.CollectorStatus_Curr.dropIndex("idx_ProjectID");

// 查看索引
// db.CollectorStatus_Curr.getIndexes();





db.createCollection("CollectorStatus_CurrYear");

db.CollectorStatus_CurrYear.insert(
   	{	
		_id:1,
		ClltIDDay: 0,             			
		ClltDaytime: 1528183743111,         		
		
		CollectID: 0,                     	
			
		RegionID: 0,         					
		ProjectID: 0,         				
		SectionID: 0,         				
		
		StatusInt : 0,	            		  
		StatusDesc: "",				
		
		ClientIP: "",					
		ClientPort : 0	
	}
);
 
// 索引
db.CollectorStatus_CurrYear.createIndex({"ClltIDDay":1, "ProjectID":1},{name:"idx_ClltIDDay_ProjectID"});

// 删除索引
// db.CollectorStatus_CurrYear.dropIndex("idx_ClltIDDay_ProjectID");

// 查看索引
// db.CollectorStatus_CurrYear.getIndexes();





db.createCollection("DeviceStatus_Curr");

db.DeviceStatus_Curr.insert(
   	{	   
		_id:1, 
		ClltIDDay: 0,             	
		ClltDaytime: 1528183743111,         
		
		DeviceIndex: 0,         		
		
		DayTimes: 0, 					
			
		RegionID: 0,         			
		ProjectID: 0,         		
		SectionID: 0,         		
		CollectID: 0,         		
		ComID: 0,         			
		Address: 0,         			
			
		StatusInt : 0,	          	  
		StatusDesc: "",		
		
		Power: 0.0,					
		PositiveValue: 0.0,			
		NegativeValue: 0.0,			
		RemainPara3: 0.0,				
		RemainPara4: 0.0,				
		RemainPara5: 0.0	
	}
);

// 索引
db.DeviceStatus_Curr.createIndex({"ProjectID":1},{name:"idx_ProjectID"});

// 删除索引
// db.DeviceStatus_Curr.dropIndex("idx_ProjectID");

// 查看索引
// db.DeviceStatus_Curr.getIndexes();






db.createCollection("DeviceStatus_CurrYear");

db.DeviceStatus_CurrYear.insert(
   	{	
		_id:1, 
		ClltIDDay: 0,             	
		ClltDaytime: 1528183743111,         
		
		DeviceIndex: 0,         					
		
		DayTimes: 0, 					
		
		RegionID: 0,         			
		ProjectID: 0,         		
		SectionID: 0,         		
		CollectID: 0,         		
		ComID: 0,         			
		Address: 0,         			
			
		StatusInt : 0,	          		  
		StatusDesc: "",			
		
		Power: 0.0,					
		PositiveValue: 0.0,			
		NegativeValue: 0.0,			
		RemainPara3: 0.0,				
		RemainPara4: 0.0,				
		RemainPara5: 0.0				
	}
);

// 索引
db.DeviceStatus_CurrYear.createIndex({"ClltIDDay":1, "ProjectID":1},{name:"idx_ClltIDDay_ProjectID"});

// 删除索引
// db.DeviceStatus_CurrYear.dropIndex("idx_ClltIDDay_ProjectID");

// 查看索引
// db.DeviceStatus_CurrYear.getIndexes();






db.createCollection("DeviceStatusRpt_CurrYear");

db.DeviceStatusRpt_CurrYear.insert(
   	{	
		_id:1, 
		ClltIDDay: 0,             		
		
		DeviceIndex: 0,         				
		
		ClltIDYear: 0,             		
		ClltIDMonth: 0,             		
		ClltIDDayIndex: 0,             
		
		RegionID: 0,         				
		ProjectID: 0,         			
		SectionID: 0,         			
		CollectID: 0,         			
		ComID: 0,         				
		Address: 0,         				
		
		StatusInt : 0,	          		 
		StatusDesc: ""			
	}
);
 
// 索引
db.DeviceStatusRpt_CurrYear.createIndex({"ClltIDDay":1, "ProjectID":1},{name:"idx_ClltIDDay_ProjectID"});

// 删除索引
// db.DeviceStatusRpt_CurrYear.dropIndex("idx_ClltIDDay_ProjectID");

// 查看索引
// db.DeviceStatusRpt_CurrYear.getIndexes();





db.createCollection("AlertData_Curr");

db.AlertData_Curr.insert(
   	{	
		_id:1, 
		ClltIDDay: 0,             	
		ClltDaytime: 1528183743111,         
		
		DeviceIndex: 0,         				
		
		DayTimes: 0, 					
		
		ClltIDYear: 0,             		
		ClltIDMonth: 0,             		
		ClltIDDayIndex: 0, 
		
		RegionID: 0,         			
		ProjectID: 0,         		
		SectionID: 0,         		
		CollectID: 0,         		
		ComID: 0,         			
		Address: 0,         				
			
		StatusInt : 0,	          		  
		StatusDesc: ""			
	}
);

// 索引
db.AlertData_Curr.createIndex({"ProjectID":1},{name:"idx_ProjectID"});

// 删除索引
// db.AlertData_Curr.dropIndex("idx_ProjectID");

// 查看索引
// db.AlertData_Curr.getIndexes();






db.createCollection("AlertData_CurrYear");

db.AlertData_CurrYear.insert(
   	{	
		_id:1, 
		ClltIDDay: 0,             	
		ClltDaytime: 1528183743111,         
		
		DeviceIndex: 0,         			
		
		DayTimes: 0, 					
		
		ClltIDYear: 0,             			
		ClltIDMonth: 0,             		
		ClltIDDayIndex: 0,             	
		
		RegionID: 0,         			
		ProjectID: 0,         		
		SectionID: 0,         		
		CollectID: 0,         		
		ComID: 0,         			
		Address: 0,         				
			
		StatusInt : 0,	          		  
		StatusDesc: ""			
	}
);

// 索引
db.AlertData_CurrYear.createIndex({"ClltIDDay":1, "ProjectID":1},{name:"idx_ClltIDDay_ProjectID"});

// 删除索引
// db.AlertData_CurrYear.dropIndex("idx_ClltIDDay_ProjectID");

// 查看索引
// db.AlertData_CurrYear.getIndexes();






db.createCollection("InverterData_Curr");

db.InverterData_Curr.insert(
   	{	
		_id:1, 
		ClltIDDay: 0,             	
		ClltDaytime: 1528183743111,         
		
		DeviceIndex: 0,         			
		
		DayTimes: 0, 					
		
		RegionID: 0,         			
		ProjectID: 0,         		
		SectionID: 0,         		
		CollectID: 0,         		
		ComID: 0,         			
		Address: 0,         				
		
		SN: "",	            	
		
		StatusInt : 0,	          		  
		StatusDesc: "",				
		
		PVVoltage: 0.0,		    	
	    PVCurrent: 0.0,		    	
	    PVOutputPower: 0.0,			
	    PVVoltageB: 0.0,		    
	    PVCurrentB: 0.0,		    	
	    PVOutputPowerB: 0.0,			
		PVVoltageC: 0.0,		    	
	   	PVCurrentC: 0.0,		    	
	    PVOutputPowerC: 0.0,			        
	    PVTotalOutputPower: 0.0,		  
	               
	    VoltageA: 0.0,		    	
	    CurrentA: 0.0,		    	      
	    FrequencyA: 0.0,		    	
	    OutputPowerA: 0.0,			  
	    ROutputPowerA: 0.0,			
	    PowerFactorA: 0.0,			
	    VoltageB: 0.0,		    	
	    CurrentB: 0.0,		    	       
	    FrequencyB: 0.0,		    	
	    OutputPowerB: 0.0,			
	    ROutputPowerB: 0.0,			
	    PowerFactorB: 0.0,			 
	    VoltageC: 0.0,		    	
	    CurrentC: 0.0,		    	     
	    FrequencyC: 0.0,		    	
	    OutputPowerC: 0.0,			
	    ROutputPowerC: 0.0,			        
	    PowerFactorC: 0.0,			
	        
	    TotalOutputPower: 0.0,		
	    RTotalOutputPower: 0.0,		
	    TotalPowerFactor: 0.0,		           
	        
	    TodayEnergy: 0.0,		    	
	    TotalEnergy: 0.0,		    	
	        
	    NTodayEnergy: 0.0,			
	    NTotalEnergy: 0.0,			
	        
	    RTodayEnergy: 0.0,			
	    RTotalEnergy: 0.0,			
	        
	    NRTodayEnergy: 0.0,			
	    NRTotalEnergy: 0.0,			      
	        
	    BtryVoltage: 0.0,		    	
	    BtryCurrent: 0.0,		    	
	    BtryOutputPower: 0.0,			
	        
	    TemperatureInverter: 0.0,		    
	    TemperatureBooster: 0.0,		
	    TemperatureSink: 0.0,			      
	        
	    TodayHour: 0.0,		    	
	    TotalHour: 0.0,		    	
	        
	    AGCSetting: 0.0,		    	
	    AVCSetting: 0.0,		    	
	        
	    GroundResistance: 0.0,		
	    LeakCurrent: 0.0,		    	
	       
	    StringVoltage1: 0.0,		    
	    StringCurrent1: 0.0,		            
	    StringVoltage2: 0.0,		    
	    StringCurrent2: 0.0,		    
	    StringVoltage3: 0.0,		    
	    StringCurrent3: 0.0,		    
	    StringVoltage4: 0.0,		    
	    StringCurrent4: 0.0,		    
	    StringVoltage5: 0.0,		    
	    StringCurrent5: 0.0,		    
	    StringVoltage6: 0.0,		    
	    StringCurrent6: 0.0,		    
	    StringVoltage7: 0.0,		    
	    StringCurrent7: 0.0,		    
	    StringVoltage8: 0.0,		    
	    StringCurrent8: 0.0,		    
	        
	    StringVoltage9: 0.0,		    
	    StringCurrent9: 0.0,		    
	    StringVoltage10: 0.0,		    
	    StringCurrent10: 0.0,		    
	    StringVoltage11: 0.0,		    
	    StringCurrent11: 0.0,		    
	    StringVoltage12: 0.0,		    
	    StringCurrent12: 0.0,		    
	    StringVoltage13: 0.0,		    
	    StringCurrent13: 0.0,		    
	    StringVoltage14: 0.0,		    
	    StringCurrent14: 0.0,		    
	    StringVoltage15: 0.0,		    
	    StringCurrent15: 0.0,		    
	    StringVoltage16: 0.0,		    
	    StringCurrent16: 0.0,		    
	    
	    StringVoltage17: 0.0,		    
	    StringCurrent17: 0.0,		    
	    StringVoltage18: 0.0,		    
	    StringCurrent18: 0.0,		    
	    StringVoltage19: 0.0,		    
	    StringCurrent19: 0.0,		    
	    StringVoltage20: 0.0,		    
	    StringCurrent20: 0.0,		    
	    StringVoltage21: 0.0,		    
	    StringCurrent21: 0.0,		    
	    StringVoltage22: 0.0,		    
	    StringCurrent22: 0.0,		    
	    StringVoltage23: 0.0,		    
	    StringCurrent23: 0.0,		    
	    StringVoltage24: 0.0,		    
	    StringCurrent24: 0.0,		    
	            
	    MotherLineVoltage: 0.0,		  
	        
	    GroudVoltage: 0.0,		     
	    NGroudVoltage: 0.0,		    
	        
	    DesignPower: 0.0,		    	
	        
	    VoltageAB: 0.0,		    	
	    VoltageBC: 0.0,		    	
	    VoltageCA: 0.0,		    	       
	        
	    SystemTime : 0,	        	
		Efficiency : 0,	       				
		PowerOnSecond : 0,	  		
		PowerOffSecond : 0,	  		
		MPPT1: 0.0,		    		
		MPPT2: 0.0,		    		
		MPPT3: 0.0,		    		
		MPPT4: 0.0		
	}
);
 
// 索引
db.InverterData_Curr.createIndex({"ProjectID":1},{name:"idx_ProjectID"});

// 删除索引
// db.InverterData_Curr.dropIndex("idx_ProjectID");

// 查看索引
// db.InverterData_Curr.getIndexes();





db.createCollection("InverterData_CurrYear");

db.InverterData_CurrYear.insert(
   	{	
		_id:1, 
		ClltIDDay: 0,             	
		ClltDaytime: 1528183743111,         
		
		DeviceIndex: 0,         		
		
		DayTimes: 0, 					
		
		RegionID: 0,         			
		ProjectID: 0,         		
		SectionID: 0,         		
		CollectID: 0,         		
		ComID: 0,         			
		Address: 0,         				
		
		SN: "",	            
		
		StatusInt : 0,	          		  
		StatusDesc: "",			
				
		PVVoltage: 0.0,		    
	    PVCurrent: 0.0,		    
	    PVOutputPower: 0.0,		    
	    PVVoltageB: 0.0,		    
	    PVCurrentB: 0.0,		    
	    PVOutputPowerB: 0.0,		    
		PVVoltageC: 0.0,		    
	   	PVCurrentC: 0.0,		    
	    PVOutputPowerC: 0.0,		        
	    PVTotalOutputPower: 0.0,		    
	               
	    VoltageA: 0.0,		    
	    CurrentA: 0.0,		          
	    FrequencyA: 0.0,		     
	    OutputPowerA: 0.0,		    
	    ROutputPowerA: 0.0,		    
	    PowerFactorA: 0.0,		    
	    VoltageB: 0.0,		    
	    CurrentB: 0.0,		          
	    FrequencyB: 0.0,		    
	    OutputPowerB: 0.0,		    
	    ROutputPowerB: 0.0,		    
	    PowerFactorB: 0.0,		       
	    VoltageC: 0.0,		    
	    CurrentC: 0.0,		           
	    FrequencyC: 0.0,		    
	    OutputPowerC: 0.0,		    
	    ROutputPowerC: 0.0,		           
	    PowerFactorC: 0.0,		    
	        
	    TotalOutputPower: 0.0,		    
	    RTotalOutputPower: 0.0,		    
	    TotalPowerFactor: 0.0,		              
	        
	    TodayEnergy: 0.0,		    
	    TotalEnergy: 0.0,		    
	        
	    NTodayEnergy: 0.0,		    
	    NTotalEnergy: 0.0,		    
	        
	    RTodayEnergy: 0.0,		    
	    RTotalEnergy: 0.0,		    
	        
	    NRTodayEnergy: 0.0,		    
	    NRTotalEnergy: 0.0,		           
	        
	    BtryVoltage: 0.0,		   
	    BtryCurrent: 0.0,		    
	    BtryOutputPower: 0.0,		      
	        
	    TemperatureInverter: 0.0,		       
	    TemperatureBooster: 0.0,		    
	    TemperatureSink: 0.0,		          
	        
	    TodayHour: 0.0,		    
	    TotalHour: 0.0,		    
	        
	    AGCSetting: 0.0,		    
	    AVCSetting: 0.0,		    
	        
	    GroundResistance: 0.0,		    
	    LeakCurrent: 0.0,		    
	       
	    StringVoltage1: 0.0,		    
	    StringCurrent1: 0.0,		           
	    StringVoltage2: 0.0,		    
	    StringCurrent2: 0.0,		    
	    StringVoltage3: 0.0,		    
	    StringCurrent3: 0.0,		    
	    StringVoltage4: 0.0,		    
	    StringCurrent4: 0.0,		    
	    StringVoltage5: 0.0,		    
	    StringCurrent5: 0.0,		    
	    StringVoltage6: 0.0,		    
	    StringCurrent6: 0.0,		    
	    StringVoltage7: 0.0,		    
	    StringCurrent7: 0.0,		    
	    StringVoltage8: 0.0,		    
	    StringCurrent8: 0.0,		    
	        
	    StringVoltage9: 0.0,		    
	    StringCurrent9: 0.0,		    
	    StringVoltage10: 0.0,		    
	    StringCurrent10: 0.0,		    
	    StringVoltage11: 0.0,		   
	    StringCurrent11: 0.0,		    
	    StringVoltage12: 0.0,		    
	    StringCurrent12: 0.0,		    
	    StringVoltage13: 0.0,		    
	    StringCurrent13: 0.0,		    
	    StringVoltage14: 0.0,		    
	    StringCurrent14: 0.0,		    
	    StringVoltage15: 0.0,		    
	    StringCurrent15: 0.0,		    
	    StringVoltage16: 0.0,		    
	    StringCurrent16: 0.0,		    
	    
	    StringVoltage17: 0.0,		    
	    StringCurrent17: 0.0,		    
	    StringVoltage18: 0.0,		    
	    StringCurrent18: 0.0,		    
	    StringVoltage19: 0.0,		    
	    StringCurrent19: 0.0,		    
	    StringVoltage20: 0.0,		    
	    StringCurrent20: 0.0,		    
	    StringVoltage21: 0.0,		    
	    StringCurrent21: 0.0,		    
	    StringVoltage22: 0.0,		    
	    StringCurrent22: 0.0,		    
	    StringVoltage23: 0.0,		    
	    StringCurrent23: 0.0,		    
	    StringVoltage24: 0.0,		    
	    StringCurrent24: 0.0,		    
	                
	    MotherLineVoltage: 0.0,		      
	        
	    GroudVoltage: 0.0,		    
	    NGroudVoltage: 0.0,		    
	        
	    DesignPower: 0.0,		    
	        
	    VoltageAB: 0.0,		    
	    VoltageBC: 0.0,		    
	    VoltageCA: 0.0,		        
	        
	    SystemTime : 0,	            				
		Efficiency : 0,	            						
		PowerOnSecond : 0,	            					
		PowerOffSecond : 0,	            				
		MPPT1: 0.0,		    
		MPPT2: 0.0,		    
		MPPT3: 0.0,		    
		MPPT4: 0.0		
	}
);
 
// 索引
db.InverterData_CurrYear.createIndex({"ClltIDDay":1, "ProjectID":1},{name:"idx_ClltIDDay_ProjectID"});

// 删除索引
// db.InverterData_CurrYear.dropIndex("idx_ClltIDDay_ProjectID");

// 查看索引
// db.InverterData_CurrYear.getIndexes();






db.createCollection("InverterRpt_CurrYear");

db.InverterRpt_CurrYear.insert(
   	{	
		_id:1, 
		ClltIDDay: 0,             		
		
		DeviceIndex: 0,         				
		
		ClltIDYear: 0,             			
		ClltIDMonth: 0,             		
		ClltIDDayIndex: 0,             	
		
		RegionID: 0,         				
		ProjectID: 0,         			
		SectionID: 0,         			
		CollectID: 0,         			
		ComID: 0,         				
		Address: 0,         					
		
		TodayEnergy: 0.0,		    					                    
	    TotalEnergy: 0.0	    			  
	}
);
 
// 索引
db.InverterRpt_CurrYear.createIndex({"ClltIDDay":1, "ProjectID":1},{name:"idx_ClltIDDay_ProjectID"});

// 删除索引
// db.InverterRpt_CurrYear.dropIndex("idx_ClltIDDay_ProjectID");

// 查看索引
// db.InverterRpt_CurrYear.getIndexes();






db.createCollection("ProtectData_Curr");

db.ProtectData_Curr.insert(
   	{	
		_id:1, 
		ClltIDDay: 0,             	
		ClltDaytime: 1528183743111,         
		
		DeviceIndex: 0,         		
		
		DayTimes: 0, 					
		
		RegionID: 0,         			
		ProjectID: 0,         		
		SectionID: 0,         		
		CollectID: 0,         		
		ComID: 0,         			
		Address: 0,         				
		
		SN: "",	            
			
		StatusInt : 0,	          		  
		StatusDesc: "",			
		
		IA: 0.0,              
	    IB: 0.0,                  
	    IC: 0.0,               
	    Ua: 0.0,                 
	    Ub: 0.0,              
	    Uc: 0.0,		        
	    Uab: 0.0,             
	    Ubc: 0.0,                    
	    Uca: 0.0,             
	    P: 0.0,               
	    Q: 0.0,                
	    S: 0.0,		        
	    PF: 0.0,              
	    F: 0.0,  
	     
	    YXBreakerCloseStatus: 0,      
	    YXBreakerOpenStatus: 0,       
	    YXManualCarWorkStatus: 0,     
	    YXManualCarTestStatus: 0,	    
	    YXEarthStatus: 0,	             		
		YXSpringSaveEnergyStatus: 0,	 
		YXRemoteLocalStatus: 0,          		
		YXRepeatCloseStatus: 0,          	
		YXHighVoltageCloseStatus: 0,    	
		YXHighVoltageOpenStatus: 0,   
			
	    IA2: 0.0,             
	    IB2: 0.0,                 
	    IC2: 0.0,              
	    Ua2: 0.0,                
	    Ub2: 0.0,             
	    Uc2: 0.0,		        
	    Uab2: 0.0,            
	    Ubc2: 0.0,                   
	    Uca2: 0.0,            
	    P2: 0.0,              
	    Q2: 0.0,               
	    S2: 0.0,		        
	    PF2: 0.0,             
	    F2: 0.0,  
	                 
	    YXBreakerCloseStatus2: 0,         
	    YXBreakerOpenStatus2: 0,          
	    YXManualCarWorkStatus2: 0,        
	    YXManualCarTestStatus2: 0,	    
	    YXEarthStatus2: 0,	             		
		YXSpringSaveEnergyStatus2: 0,	     
		YXRemoteLocalStatus2: 0,             
		YXRepeatCloseStatus2: 0,             	
		YXHighVoltageCloseStatus2: 0,      
		YXHighVoltageOpenStatus2: 0  
	}
);
 
// 索引
db.ProtectData_Curr.createIndex({"ProjectID":1},{name:"idx_ProjectID"});

// 删除索引
// db.ProtectData_Curr.dropIndex("idx_ProjectID");

// 查看索引
// db.ProtectData_Curr.getIndexes();






db.createCollection("ProtectData_CurrYear");

db.ProtectData_CurrYear.insert(
   	{	
		_id:1, 
		ClltIDDay: 0,             	
		ClltDaytime: 1528183743111,         
		
		DeviceIndex: 0,         		
		
		DayTimes: 0, 					
		
		RegionID: 0,         			
		ProjectID: 0,         		
		SectionID: 0,         		
		CollectID: 0,         		
		ComID: 0,         			
		Address: 0,         			
		
		SN: "",	            	
		
		StatusInt : 0,	          		  
		StatusDesc: "",			
		
		IA: 0.0,              
	    IB: 0.0,                  
	    IC: 0.0,               
	    Ua: 0.0,                 
	    Ub: 0.0,              
	    Uc: 0.0,		        
	    Uab: 0.0,             
	    Ubc: 0.0,                    
	    Uca: 0.0,             
	    P: 0.0,               
	    Q: 0.0,                
	    S: 0.0,		        
	    PF: 0.0,              
	    F: 0.0,  
	     
	    YXBreakerCloseStatus: 0,      
	    YXBreakerOpenStatus: 0,       
	    YXManualCarWorkStatus: 0,     
	    YXManualCarTestStatus: 0,	    
	    YXEarthStatus: 0,	             		
		YXSpringSaveEnergyStatus: 0,	 
		YXRemoteLocalStatus: 0,          		
		YXRepeatCloseStatus: 0,          	
		YXHighVoltageCloseStatus: 0,    	
		YXHighVoltageOpenStatus: 0,   
			
	    IA2: 0.0,             
	    IB2: 0.0,                 
	    IC2: 0.0,              
	    Ua2: 0.0,                
	    Ub2: 0.0,             
	    Uc2: 0.0,		        
	    Uab2: 0.0,            
	    Ubc2: 0.0,                   
	    Uca2: 0.0,            
	    P2: 0.0,              
	    Q2: 0.0,               
	    S2: 0.0,		        
	    PF2: 0.0,             
	    F2: 0.0, 
	                 
	    YXBreakerCloseStatus2: 0,         
	    YXBreakerOpenStatus2: 0,          
	    YXManualCarWorkStatus2: 0,        
	    YXManualCarTestStatus2: 0,	    
	    YXEarthStatus2: 0,	             		
		YXSpringSaveEnergyStatus2: 0,	     
		YXRemoteLocalStatus2: 0,             
		YXRepeatCloseStatus2: 0,             	
		YXHighVoltageCloseStatus2: 0,      
		YXHighVoltageOpenStatus2: 0  
	}
);

// 索引
db.ProtectData_CurrYear.createIndex({"ClltIDDay":1, "ProjectID":1},{name:"idx_ClltIDDay_ProjectID"});

// 删除索引
// db.ProtectData_CurrYear.dropIndex("idx_ClltIDDay_ProjectID");

// 查看索引
// db.ProtectData_CurrYear.getIndexes();






db.createCollection("EMSData_Curr");

db.EMSData_Curr.insert(
   	{	
		_id:1, 
		ClltIDDay: 0,             	
		ClltDaytime: 1528183743111,         
		
		DeviceIndex: 0,         			
		
		DayTimes: 0, 					
		
		RegionID: 0,         			
		ProjectID: 0,         		
		SectionID: 0,         		
		CollectID: 0,         		
		ComID: 0,         			
		Address: 0,         				
		
		SN: "",	            	
		
		StatusInt : 0,	          		  
		StatusDesc: "",			
		
		Irradiance: 0.0,      
	    Temperature: 0.0,     
	    PVTemperature: 0.0,   
	    WindSpeed: 0.0,       
	    WindDirect: 0.0,      
	        
	    Humidity: 0.0,        
	        
	    Radiation: 0.0    
	}
);
 
// 索引
db.EMSData_Curr.createIndex({"ProjectID":1},{name:"idx_ProjectID"});

// 删除索引
// db.EMSData_Curr.dropIndex("idx_ProjectID");

// 查看索引
// db.EMSData_Curr.getIndexes();






db.createCollection("EMSData_CurrYear");

db.EMSData_CurrYear.insert(
   	{	
		_id:1, 
		ClltIDDay: 0,             	
		ClltDaytime: 1528183743111,         
		
		DeviceIndex: 0,         		
		
		DayTimes: 0, 					
		
		RegionID: 0,         			
		ProjectID: 0,         		
		SectionID: 0,         		
		CollectID: 0,         		
		ComID: 0,         			
		Address: 0,         			
		
		SN: "",	            			
		
		StatusInt : 0,	          		  
		StatusDesc: "",			
		
		Irradiance: 0.0,      
	    Temperature: 0.0,     
	    PVTemperature: 0.0,   
	    WindSpeed: 0.0,       
	    WindDirect: 0.0,      
	        
	    Humidity: 0.0,        
	        
	    Radiation: 0.0 
	}
);

// 索引
db.EMSData_CurrYear.createIndex({"ClltIDDay":1, "ProjectID":1},{name:"idx_ClltIDDay_ProjectID"});

// 删除索引
// db.EMSData_CurrYear.dropIndex("idx_ClltIDDay_ProjectID");

// 查看索引
// db.EMSData_CurrYear.getIndexes();







db.createCollection("EMSRpt_CurrYear");

db.EMSRpt_CurrYear.insert(
   	{	
		_id:1, 
		ClltIDDay: 0,             		
		
		DeviceIndex: 0,         				
		
		ClltIDYear: 0,             			
		ClltIDMonth: 0,             		
		ClltIDDayIndex: 0,             	
		
		RegionID: 0,         				
		ProjectID: 0,         			
		SectionID: 0,         			
		CollectID: 0,         			
		ComID: 0,         				
		Address: 0,         					
				                    
	    Radiation: 0.0	    			  
	}
);
 
// 索引
db.EMSRpt_CurrYear.createIndex({"ClltIDDay":1, "ProjectID":1},{name:"idx_ClltIDDay_ProjectID"});

// 删除索引
// db.EMSRpt_CurrYear.dropIndex("idx_ClltIDDay_ProjectID");

// 查看索引
// db.EMSRpt_CurrYear.getIndexes();







db.createCollection("IOData_Curr");

db.IOData_Curr.insert(
   	{	
		_id:1, 
		ClltIDDay: 0,             	
		ClltDaytime: 1528183743111,         
		
		DeviceIndex: 0,         		
		
		DayTimes: 0, 					
		
		RegionID: 0,         			
		ProjectID: 0,         		
		SectionID: 0,         		
		CollectID: 0,         		
		ComID: 0,         			
		Address: 0,         				
		
		SN: "",	            	
		
		StatusInt : 0,	          		  
		StatusDesc: "",			
		
		InputIO0: 0,          
	    InputIO1: 0,          
	    InputIO2: 0,          
	    InputIO3: 0,          
	    InputIO4: 0,          
	    InputIO5: 0,          
	    InputIO6: 0,          
	    InputIO7: 0,          
	    InputIO8: 0,          
	    InputIO9: 0,          
	    InputIO10: 0,         
	    InputIO11: 0,         
	    InputIO12: 0,         
	    InputIO13: 0,         
	    InputIO14: 0,         
	    InputIO15: 0,         
	        
	    OutputIO0: 0,         
	    OutputIO1: 0,         
	    OutputIO2: 0,         
	    OutputIO3: 0,         
	    OutputIO4: 0,         
	    OutputIO5: 0,         
	    OutputIO6: 0,         
	    OutputIO7: 0,         
	    OutputIO8: 0,         
	    OutputIO9: 0,         
	    OutputIO10: 0,        
	    OutputIO11: 0,        
	    OutputIO12: 0,        
	    OutputIO13: 0,        
	    OutputIO14: 0,        
	    OutputIO15: 0   
	}
);
 
// 索引
db.IOData_Curr.createIndex({"ProjectID":1},{name:"idx_ProjectID"});

// 删除索引
// db.IOData_Curr.dropIndex("idx_ProjectID");

// 查看索引
// db.IOData_Curr.getIndexes();







db.createCollection("IOData_CurrYear");

db.IOData_CurrYear.insert(
   	{	
		_id:1, 
		ClltIDDay: 0,             	
		ClltDaytime: 1528183743111,         
		
		DeviceIndex: 0,         		
		
		DayTimes: 0, 					
		
		RegionID: 0,         			
		ProjectID: 0,         		
		SectionID: 0,         		
		CollectID: 0,         		
		ComID: 0,         			
		Address: 0,         				
		
		SN: "",	            	
		
		StatusInt : 0,	          		  
		StatusDesc: "",			
		
		InputIO0: 0,          
	    InputIO1: 0,          
	    InputIO2: 0,          
	    InputIO3: 0,          
	    InputIO4: 0,          
	    InputIO5: 0,          
	    InputIO6: 0,          
	    InputIO7: 0,          
	    InputIO8: 0,          
	    InputIO9: 0,          
	    InputIO10: 0,         
	    InputIO11: 0,         
	    InputIO12: 0,         
	    InputIO13: 0,         
	    InputIO14: 0,         
	    InputIO15: 0,         
	        
	    OutputIO0: 0,         
	    OutputIO1: 0,         
	    OutputIO2: 0,         
	    OutputIO3: 0,         
	    OutputIO4: 0,         
	    OutputIO5: 0,         
	    OutputIO6: 0,         
	    OutputIO7: 0,         
	    OutputIO8: 0,         
	    OutputIO9: 0,         
	    OutputIO10: 0,        
	    OutputIO11: 0,        
	    OutputIO12: 0,        
	    OutputIO13: 0,        
	    OutputIO14: 0,        
	    OutputIO15: 0 
	}
);
 
// 索引
db.IOData_CurrYear.createIndex({"ClltIDDay":1, "ProjectID":1},{name:"idx_ClltIDDay_ProjectID"});

// 删除索引
// db.IOData_CurrYear.dropIndex("idx_ClltIDDay_ProjectID");

// 查看索引
// db.IOData_CurrYear.getIndexes();







db.createCollection("EPMData_Curr");

db.EPMData_Curr.insert(
   	{	
		_id:1, 
		ClltIDDay: 0,             	
		ClltDaytime: 1528183743111,         
		
		DeviceIndex: 0,         		
		
		DayTimes: 0, 					
		
		RegionID: 0,         			
		ProjectID: 0,         		
		SectionID: 0,         		
		CollectID: 0,         		
		ComID: 0,         			
		Address: 0,         			
		
		SN: "",	            		
		
		StatusInt : 0,	          		  
		StatusDesc: "",			
		
		Voltage1: 0.0,		
		Voltage2: 0.0,		
		Voltage3: 0.0,				
		Current1: 0.0,		
		Current2: 0.0,		
		Current3: 0.0,			
		Frequency1: 0.0,		
		Frequency2: 0.0,		
		Frequency3: 0.0,					
			
		ActivePower1: 0.0,	
		ActivePower2: 0.0,	
		ActivePower3: 0.0,		
			
		Var1: 0.0,			
		Var2: 0.0,			
		Var3: 0.0,			
					
		PowerFactor1: 0.0,	
		PowerFactor2: 0.0,	
		PowerFactor3: 0.0,				
			
		VSum: 0.0,			
		ASum: 0.0,			
		WSum: 0.0,			
		VarSum: 0.0,			
		PFSum: 0.0,					
		WhSum: 0.0,           		
		VarhSum: 0.0,         	
		WhNSum: 0.0,          	
		VarhNSum: 0.0,        	
			
		Voltage1N: 0.0,		
		Voltage2N: 0.0,		
		Voltage3N: 0.0,			
			
		THDV1: 0.0,			
		THDV2: 0.0,			
		THDV3: 0.0,			
	    THDA1: 0.0,			
		THDA2: 0.0,			
		THDA3: 0.0,			
			
		WhToday: 0.0,         		
		VarhToday: 0.0,       	
		WhNToday: 0.0,        	
		VarhNToday: 0.0,      
			
		WhJian: 0.0,          	
		WhFeng: 0.0,          	
		WhPing: 0.0,          	
		WhGu: 0.0,            	
			
		WhNJian: 0.0,         	
		WhNFeng: 0.0,         	
		WhNPing: 0.0,         	
		WhNGu: 0.0,           	
			
		WhLastMonth: 0.0,           	
		WhLastMonthJian: 0.0,         	
		WhLastMonthFeng: 0.0,         	
		WhLastMonthPing: 0.0,         	
		WhLastMonthGu: 0.0,           	
			
		WhNLastMonth: 0.0,            
		WhNLastMonthJian: 0.0,         	
		WhNLastMonthFeng: 0.0,         	
		WhNLastMonthPing: 0.0,         	
		WhNLastMonthGu: 0.0,          		
			
		DI0: 0,                       
		DI1: 0,                       
		DI2: 0,                       
		DI3: 0,                       
			
		DO0: 0,                       
		DO1: 0,                       
		DO2: 0,                       
		DO3: 0   
	}
);
 
// 索引
db.EPMData_Curr.createIndex({"ProjectID":1},{name:"idx_ProjectID"});

// 删除索引
// db.EPMData_Curr.dropIndex("idx_ProjectID");

// 查看索引
// db.EPMData_Curr.getIndexes();






db.createCollection("EPMData_CurrYear");

db.EPMData_CurrYear.insert(
   	{	
		_id:1, 
		ClltIDDay: 0,             	
		ClltDaytime: 1528183743111,         
		
		DeviceIndex: 0,         		
		
		DayTimes: 0, 					
		
		RegionID: 0,         			
		ProjectID: 0,         		
		SectionID: 0,         		
		CollectID: 0,         		
		ComID: 0,         			
		Address: 0,         				
		
		SN: "",	            
			
		StatusInt : 0,	          		  
		StatusDesc: "",			
		
		Voltage1: 0.0,		
		Voltage2: 0.0,		
		Voltage3: 0.0,				
		Current1: 0.0,		
		Current2: 0.0,		
		Current3: 0.0,			
		Frequency1: 0.0,		
		Frequency2: 0.0,		
		Frequency3: 0.0,					
			
		ActivePower1: 0.0,	
		ActivePower2: 0.0,	
		ActivePower3: 0.0,		
			
		Var1: 0.0,			
		Var2: 0.0,			
		Var3: 0.0,			
					
		PowerFactor1: 0.0,	
		PowerFactor2: 0.0,	
		PowerFactor3: 0.0,				
			
		VSum: 0.0,			
		ASum: 0.0,			
		WSum: 0.0,			
		VarSum: 0.0,			
		PFSum: 0.0,					
		WhSum: 0.0,           		
		VarhSum: 0.0,         	
		WhNSum: 0.0,          	
		VarhNSum: 0.0,        	
			
		Voltage1N: 0.0,		
		Voltage2N: 0.0,		
		Voltage3N: 0.0,			
			
		THDV1: 0.0,			
		THDV2: 0.0,			
		THDV3: 0.0,			
	    THDA1: 0.0,			
		THDA2: 0.0,			
		THDA3: 0.0,			
			
		WhToday: 0.0,         		
		VarhToday: 0.0,       	
		WhNToday: 0.0,        	
		VarhNToday: 0.0,      
			
		WhJian: 0.0,          	
		WhFeng: 0.0,          	
		WhPing: 0.0,          	
		WhGu: 0.0,            	
			
		WhNJian: 0.0,         	
		WhNFeng: 0.0,         	
		WhNPing: 0.0,         	
		WhNGu: 0.0,           	
			
		WhLastMonth: 0.0,           	
		WhLastMonthJian: 0.0,         	
		WhLastMonthFeng: 0.0,         	
		WhLastMonthPing: 0.0,         	
		WhLastMonthGu: 0.0,           	
			
		WhNLastMonth: 0.0,            
		WhNLastMonthJian: 0.0,         	
		WhNLastMonthFeng: 0.0,         	
		WhNLastMonthPing: 0.0,         	
		WhNLastMonthGu: 0.0,          		
			
		DI0: 0,                       
		DI1: 0,                       
		DI2: 0,                       
		DI3: 0,                       
			
		DO0: 0,                       
		DO1: 0,                       
		DO2: 0,                       
		DO3: 0  
	}
);
 
// 索引
db.EPMData_CurrYear.createIndex({"ClltIDDay":1, "ProjectID":1},{name:"idx_ClltIDDay_ProjectID"});

// 删除索引
// db.EPMData_CurrYear.dropIndex("idx_ClltIDDay_ProjectID");

// 查看索引
// db.EPMData_CurrYear.getIndexes();







db.createCollection("EPMRpt_CurrYear");

db.EPMRpt_CurrYear.insert(
   	{	
		_id:1, 
		ClltIDDay: 0,             		
		
		DeviceIndex: 0,         			
		
		ClltIDYear: 0,             			
		ClltIDMonth: 0,             		
		ClltIDDayIndex: 0,             	
		
		RegionID: 0,         				
		ProjectID: 0,         			
		SectionID: 0,         			
		CollectID: 0,         			
		ComID: 0,         				
		Address: 0,         					
		
		WhToday: 0.0,         		
		VarhToday: 0.0,       	
		WhNToday: 0.0,        	
		VarhNToday: 0.0,      
			
		WhSum: 0.0,           		
		VarhSum: 0.0,         	
		WhNSum: 0.0,          	
		VarhNSum: 0.0 
	}
);
 
// 索引
db.EPMRpt_CurrYear.createIndex({"ClltIDDay":1, "ProjectID":1},{name:"idx_ClltIDDay_ProjectID"});

// 删除索引
// db.EPMRpt_CurrYear.dropIndex("idx_ClltIDDay_ProjectID");

// 查看索引
// db.EPMRpt_CurrYear.getIndexes();








db.createCollection("PVSData_Curr");

db.PVSData_Curr.insert(
   	{	
		_id:1, 
		ClltIDDay: 0,             	
		ClltDaytime: 1528183743111,         
		
		DeviceIndex: 0,         		
		
		DayTimes: 0, 					
		
		RegionID: 0,         			
		ProjectID: 0,         		
		SectionID: 0,         		
		CollectID: 0,         		
		ComID: 0,         			
		Address: 0,         			
		
		SN: "",	            		
		
		StatusInt : 0,	          		  
		StatusDesc: "",			
		
		MaxChannel: 0,				
		DCMotherVoltage: 0.0, 		
		Temperature: 0.0,				
		MaxCurrent: 0.0,				
		MinCurrent: 0.0,				
		AvgCurrent: 0.0,				
		TotalCurrent: 0.0,				
		
		Current1: 0.0,		
		Current2: 0.0,		
		Current3: 0.0,		
		Current4: 0.0,		
		Current5: 0.0,		
		Current6: 0.0,		
		Current7: 0.0,		
		Current8: 0.0,		
		Current9: 0.0,		
		Current10: 0.0,		
		Current11: 0.0,		
		Current12: 0.0,		
		Current13: 0.0,		
		Current14: 0.0,		
		Current15: 0.0,		
		Current16: 0.0,		
		
		Current17: 0.0,		
		Current18: 0.0,		
		Current19: 0.0,		
		Current20: 0.0,		
		Current21: 0.0,		
		Current22: 0.0,		
		Current23: 0.0,		
		Current24: 0.0			
	}
);
 
// 索引
db.PVSData_Curr.createIndex({"ProjectID":1},{name:"idx_ProjectID"});

// 删除索引
// db.PVSData_Curr.dropIndex("idx_ProjectID");

// 查看索引
// db.PVSData_Curr.getIndexes();






db.createCollection("PVSData_CurrYear");

db.PVSData_CurrYear.insert(
   	{	
		_id:1, 
		ClltIDDay: 0,             	
		ClltDaytime: 1528183743111,         
		
		DeviceIndex: 0,         		
		
		DayTimes: 0, 					
		
		RegionID: 0,         			
		ProjectID: 0,         		
		SectionID: 0,         		
		CollectID: 0,         		
		ComID: 0,         			
		Address: 0,         				
		
		SN: "",	            
			
		StatusInt : 0,	          		  
		StatusDesc: "",				
		
		MaxChannel: 0,				
		DCMotherVoltage: 0.0, 		
		Temperature: 0.0,				
		MaxCurrent: 0.0,				
		MinCurrent: 0.0,				
		AvgCurrent: 0.0,				
		TotalCurrent: 0.0,			
		
		Current1: 0.0,		
		Current2: 0.0,		
		Current3: 0.0,		
		Current4: 0.0,		
		Current5: 0.0,		
		Current6: 0.0,		
		Current7: 0.0,		
		Current8: 0.0,		
		Current9: 0.0,		
		Current10: 0.0,		
		Current11: 0.0,		
		Current12: 0.0,		
		Current13: 0.0,		
		Current14: 0.0,		
		Current15: 0.0,		
		Current16: 0.0,		
		
		Current17: 0.0,		
		Current18: 0.0,		
		Current19: 0.0,		
		Current20: 0.0,		
		Current21: 0.0,		
		Current22: 0.0,		
		Current23: 0.0,		
		Current24: 0.0		
	}
);
 
// 索引
db.PVSData_CurrYear.createIndex({"ClltIDDay":1, "ProjectID":1},{name:"idx_ClltIDDay_ProjectID"});

// 删除索引
// db.PVSData_CurrYear.dropIndex("idx_ClltIDDay_ProjectID");

// 查看索引
// db.PVSData_CurrYear.getIndexes();






db.createCollection("ACPVSData_Curr");

db.ACPVSData_Curr.insert(
   	{	
		_id:1, 
		ClltIDDay: 0,             	
		ClltDaytime: 1528183743111,         
		
		DeviceIndex: 0,         			
		
		DayTimes: 0, 					
		
		RegionID: 0,         			
		ProjectID: 0,         		
		SectionID: 0,         		
		CollectID: 0,         		
		ComID: 0,         			
		Address: 0,         				
		
		SN: "",	            	
		
		StatusInt : 0,	          		  
		StatusDesc: "",			
			
		Temperature: 0.0,		
			
		VoltageL1: 0.0,		
		VoltageL2: 0.0,		
		VoltageL3: 0.0,			
			
		Voltage1: 0.0,		
		Voltage2: 0.0,		
		Voltage3: 0.0,			
				
		Current1: 0.0,		
		Current2: 0.0,		
		Current3: 0.0,			
			
		Frequency1: 0.0,		
		Frequency2: 0.0,		
		Frequency3: 0.0,					
			
		ActivePower1: 0.0,	
		ActivePower2: 0.0,	
		ActivePower3: 0.0,		
			
		Var1: 0.0,			
		Var2: 0.0,			
		Var3: 0.0,			
					
		PowerFactor1: 0.0,	
		PowerFactor2: 0.0,	
		PowerFactor3: 0.0,			
			
		WSum: 0.0,			
		VarSum: 0.0,			
		PFSum: 0.0,			        
	        
		Currentl1: 0.0,		
		Currentl2: 0.0,		
		Currentl3: 0.0,		
		Currentl4: 0.0,		
		Currentl5: 0.0,		
		Currentl6: 0.0,		
		Currentl7: 0.0,		
		Currentl8: 0.0,		
		Currentl9: 0.0,		
		Currentl10: 0.0,		
		Currentl11: 0.0,		
		Currentl12: 0.0,		
		Currentl13: 0.0,		
		Currentl14: 0.0,		
		Currentl15: 0.0,		
		Currentl16: 0.0,		
		Currentl17: 0.0,		
		Currentl18: 0.0,		
		Currentl19: 0.0,		
		Currentl20: 0.0,		
		Currentl21: 0.0,		
		Currentl22: 0.0,		
		Currentl23: 0.0,		
		Currentl24: 0.0,		
			
		InputIO1: 0,            
	    InputIO2: 0,          
	    InputIO3: 0,          
	    InputIO4: 0,          
	    InputIO5: 0,          
	    InputIO6: 0,          
	    InputIO7: 0,          
	    InputIO8: 0,          
	    InputIO9: 0,          
	    InputIO10: 0,         
	    InputIO11: 0,         
	    InputIO12: 0,         
	    InputIO13: 0,         
	    InputIO14: 0,         
	    InputIO15: 0,         
	    InputIO16: 0,         
	    InputIO17: 0,         
	    InputIO18: 0,         
	    InputIO19: 0,         
	    InputIO20: 0,         
	    InputIO21: 0,         
	    InputIO22: 0,         
	    InputIO23: 0,         
	    InputIO24: 0   
	}
);
 
// 索引
db.ACPVSData_Curr.createIndex({"ProjectID":1},{name:"idx_ProjectID"});

// 删除索引
// db.ACPVSData_Curr.dropIndex("idx_ProjectID");

// 查看索引
// db.ACPVSData_Curr.getIndexes();







db.createCollection("ACPVSData_CurrYear");

db.ACPVSData_CurrYear.insert(
   	{	
		_id:1, 
		ClltIDDay: 0,             	
		ClltDaytime: 1528183743111,         
		
		DeviceIndex: 0,         			
		
		DayTimes: 0, 					
		
		RegionID: 0,         			
		ProjectID: 0,         		
		SectionID: 0,         		
		CollectID: 0,         		
		ComID: 0,         			
		Address: 0,         				
		
		SN: "",	            	
		
		StatusInt : 0,	          		  
		StatusDesc: "",			
			
		Temperature: 0.0,		
			
		VoltageL1: 0.0,		
		VoltageL2: 0.0,		
		VoltageL3: 0.0,			
			
		Voltage1: 0.0,		
		Voltage2: 0.0,		
		Voltage3: 0.0,			
				
		Current1: 0.0,		
		Current2: 0.0,		
		Current3: 0.0,			
			
		Frequency1: 0.0,		
		Frequency2: 0.0,		
		Frequency3: 0.0,					
			
		ActivePower1: 0.0,	
		ActivePower2: 0.0,	
		ActivePower3: 0.0,		
			
		Var1: 0.0,			
		Var2: 0.0,			
		Var3: 0.0,			
					
		PowerFactor1: 0.0,	
		PowerFactor2: 0.0,	
		PowerFactor3: 0.0,			
			
		WSum: 0.0,			
		VarSum: 0.0,			
		PFSum: 0.0,			        
	        
		Currentl1: 0.0,		
		Currentl2: 0.0,		
		Currentl3: 0.0,		
		Currentl4: 0.0,		
		Currentl5: 0.0,		
		Currentl6: 0.0,		
		Currentl7: 0.0,		
		Currentl8: 0.0,		
		Currentl9: 0.0,		
		Currentl10: 0.0,		
		Currentl11: 0.0,		
		Currentl12: 0.0,		
		Currentl13: 0.0,		
		Currentl14: 0.0,		
		Currentl15: 0.0,		
		Currentl16: 0.0,		
		Currentl17: 0.0,		
		Currentl18: 0.0,		
		Currentl19: 0.0,		
		Currentl20: 0.0,		
		Currentl21: 0.0,		
		Currentl22: 0.0,		
		Currentl23: 0.0,		
		Currentl24: 0.0,		
			
		InputIO1: 0,            
	    InputIO2: 0,          
	    InputIO3: 0,          
	    InputIO4: 0,          
	    InputIO5: 0,          
	    InputIO6: 0,          
	    InputIO7: 0,          
	    InputIO8: 0,          
	    InputIO9: 0,          
	    InputIO10: 0,         
	    InputIO11: 0,         
	    InputIO12: 0,         
	    InputIO13: 0,         
	    InputIO14: 0,         
	    InputIO15: 0,         
	    InputIO16: 0,         
	    InputIO17: 0,         
	    InputIO18: 0,         
	    InputIO19: 0,         
	    InputIO20: 0,         
	    InputIO21: 0,         
	    InputIO22: 0,         
	    InputIO23: 0,         
	    InputIO24: 0      
	}
);
 
// 索引
db.ACPVSData_CurrYear.createIndex({"ClltIDDay":1, "ProjectID":1},{name:"idx_ClltIDDay_ProjectID"});

// 删除索引
// db.ACPVSData_CurrYear.dropIndex("idx_ClltIDDay_ProjectID");

// 查看索引
// db.ACPVSData_CurrYear.getIndexes();







db.createCollection("DCCabinetData_Curr");

db.DCCabinetData_Curr.insert(
   	{	
		_id:1, 
		ClltIDDay: 0,             	
		ClltDaytime: 1528183743111,         
		
		DeviceIndex: 0,         			
		
		DayTimes: 0, 					
		
		RegionID: 0,         			
		ProjectID: 0,         		
		SectionID: 0,         		
		CollectID: 0,         		
		ComID: 0,         			
		Address: 0,         				
		
		SN: "",	            	
		
		StatusInt : 0,	          		  
		StatusDesc: "",							
		
		Voltage: 0.0,         		
		Current: 0.0,		    
		Power: 0.0,		    
		Temperature: 0.0,		
		WhSum: 0.0,           		
		VarhSum: 0.0,         	
		WhNSum: 0.0,          	
		VarhNSum: 0.0          	
	}
);
 
// 索引
db.DCCabinetData_Curr.createIndex({"ProjectID":1},{name:"idx_ProjectID"});

// 删除索引
// db.DCCabinetData_Curr.dropIndex("idx_ProjectID");

// 查看索引
// db.DCCabinetData_Curr.getIndexes();








db.createCollection("DCCabinetData_CurrYear");

db.DCCabinetData_CurrYear.insert(
   	{	
		_id:1, 
		ClltIDDay: 0,             	
		ClltDaytime: 1528183743111,         
		
		DeviceIndex: 0,         			
		
		DayTimes: 0, 					
		
		RegionID: 0,         			
		ProjectID: 0,         		
		SectionID: 0,         		
		CollectID: 0,         		
		ComID: 0,         			
		Address: 0,         				
		
		SN: "",	            	
		
		StatusInt : 0,	          		  
		StatusDesc: "",							
		
		Voltage: 0.0,         		
		Current: 0.0,		    
		Power: 0.0,		    
		Temperature: 0.0,		
		WhSum: 0.0,           		
		VarhSum: 0.0,         	
		WhNSum: 0.0,          	
		VarhNSum: 0.0          	
	}
);
 
// 索引
db.DCCabinetData_CurrYear.createIndex({"ClltIDDay":1, "ProjectID":1},{name:"idx_ClltIDDay_ProjectID"});

// 删除索引
// db.DCCabinetData_CurrYear.dropIndex("idx_ClltIDDay_ProjectID");

// 查看索引
// db.DCCabinetData_CurrYear.getIndexes();








db.createCollection("ContraryData_Curr");

db.ContraryData_Curr.insert(
   	{	
		_id:1, 
		ClltIDDay: 0,             	
		ClltDaytime: 1528183743111,         
		
		DeviceIndex: 0,         			
		
		DayTimes: 0, 					
		
		RegionID: 0,         			
		ProjectID: 0,         		
		SectionID: 0,         		
		CollectID: 0,         		
		ComID: 0,         			
		Address: 0,         				
		
		SN: "",	            	
		
		StatusInt : 0,	          		  
		StatusDesc: "",			
		
		Voltage: 0.0,             
	    Current: 0.0,                   
	    Frequency: 0.0,             
	    OutputPower: 0.0,            
	    ROutputPower: 0.0,        
	    PowerFactor: 0.0,		    
	        
	    VoltageB: 0.0,            
	    CurrentB: 0.0,                  
	    FrequencyB: 0.0,          
	    OutputPowerB: 0.0,         
	    ROutputPowerB: 0.0,        
	    PowerFactorB: 0.0,		   
	        
	    VoltageC: 0.0,            
	    CurrentC: 0.0,                    
	    FrequencyC: 0.0,           
	    OutputPowerC: 0.0,        
	    ROutputPowerC: 0.0,               
	    PowerFactorC: 0.0,		
	        
	    NVoltage: 0.0,            
	    NCurrent: 0.0,            
	        
	    NVoltageB: 0.0,           
	    NCurrentB: 0.0,           
	        
	    NVoltageC: 0.0,           
	    NCurrentC: 0.0,            
			
		NTotalVoltage: 0.0,       
	    NTotalCurrent: 0.0,       
	        
	    TotalOutputPower: 0.0,    
	    RTotalOutputPower: 0.0,   
	    TotalPowerFactor: 0.0,               
	        
	    WhSum: 0.0,               		
		VarhSum: 0.0,             	
		WhNSum: 0.0,              	
		VarhNSum: 0.0            			
	}
);
 
// 索引
db.ContraryData_Curr.createIndex({"ProjectID":1},{name:"idx_ProjectID"});

// 删除索引
// db.ContraryData_Curr.dropIndex("idx_ProjectID");

// 查看索引
// db.ContraryData_Curr.getIndexes();








db.createCollection("ContraryData_CurrYear");

db.ContraryData_CurrYear.insert(
   	{	
		_id:1, 
		ClltIDDay: 0,             	
		ClltDaytime: 1528183743111,         
		
		DeviceIndex: 0,         			
		
		DayTimes: 0, 					
		
		RegionID: 0,         			
		ProjectID: 0,         		
		SectionID: 0,         		
		CollectID: 0,         		
		ComID: 0,         			
		Address: 0,         				
		
		SN: "",	            	
		
		StatusInt : 0,	          		  
		StatusDesc: "",			
		
		Voltage: 0.0,             
	    Current: 0.0,                   
	    Frequency: 0.0,             
	    OutputPower: 0.0,            
	    ROutputPower: 0.0,        
	    PowerFactor: 0.0,		    
	        
	    VoltageB: 0.0,            
	    CurrentB: 0.0,                  
	    FrequencyB: 0.0,          
	    OutputPowerB: 0.0,         
	    ROutputPowerB: 0.0,        
	    PowerFactorB: 0.0,		   
	        
	    VoltageC: 0.0,            
	    CurrentC: 0.0,                    
	    FrequencyC: 0.0,           
	    OutputPowerC: 0.0,        
	    ROutputPowerC: 0.0,               
	    PowerFactorC: 0.0,		
	        
	    NVoltage: 0.0,            
	    NCurrent: 0.0,            
	        
	    NVoltageB: 0.0,           
	    NCurrentB: 0.0,           
	        
	    NVoltageC: 0.0,           
	    NCurrentC: 0.0,            
			
		NTotalVoltage: 0.0,       
	    NTotalCurrent: 0.0,       
	        
	    TotalOutputPower: 0.0,    
	    RTotalOutputPower: 0.0,   
	    TotalPowerFactor: 0.0,               
	        
	    WhSum: 0.0,               		
		VarhSum: 0.0,             	
		WhNSum: 0.0,              	
		VarhNSum: 0.0            			
	}
);
 
// 索引
db.ContraryData_CurrYear.createIndex({"ClltIDDay":1, "ProjectID":1},{name:"idx_ClltIDDay_ProjectID"});

// 删除索引
// db.ContraryData_CurrYear.dropIndex("idx_ClltIDDay_ProjectID");

// 查看索引
// db.ContraryData_CurrYear.getIndexes();







db.createCollection("PCSData_Curr");

db.PCSData_Curr.insert(
   	{	
		_id:1, 
		ClltIDDay: 0,             	
		ClltDaytime: 1528183743111,         
		
		DeviceIndex: 0,         			
		
		DayTimes: 0, 					
		
		RegionID: 0,         			
		ProjectID: 0,         		
		SectionID: 0,         		
		CollectID: 0,         		
		ComID: 0,         			
		Address: 0,         				
		
		SN: "",	            	
		
		StatusInt : 0,	          		  
		StatusDesc: "",			
		
		Udc: 0.0,              
		Idc: 0.0,              
		Pdc: 0.0,             	
	    Ua: 0.0,                 
	    Ub: 0.0,              
	    Uc: 0.0,		        
	    Uab: 0.0,             
	    Ubc: 0.0,                    
	    Uca: 0.0,             
	    Ia: 0.0,               
	    Ib: 0.0,                  
	    Ic: 0.0,               
	    P: 0.0,               
	    Q: 0.0,                
	    S: 0.0,		        
	    PF: 0.0,              
	    F: 0.0,                       
	        
	    Udc1: 0.0,            
		Idc1: 0.0,             
		Pdc1: 0.0,            	
	    Ua1: 0.0,                
	    Ub1: 0.0,             
	    Uc1: 0.0,		        
	    Uab1: 0.0,            
	    Ubc1: 0.0,                   
	    Uca1: 0.0,            
	    Ia1: 0.0,             
	    Ib1: 0.0,                 
	    Ic1: 0.0,              
	    P1: 0.0,              
	    Q1: 0.0,               
	    S1: 0.0,		        
	    PF1: 0.0,             
	    F1: 0.0,              
	        
	    Udc2: 0.0,            
		Idc2: 0.0,             
		Pdc2: 0.0,            	
	    Ua2: 0.0,                
	    Ub2: 0.0,             
	    Uc2: 0.0,		        
	    Uab2: 0.0,            
	    Ubc2: 0.0,                   
	    Uca2: 0.0,            
	    Ia2: 0.0,             
	    Ib2: 0.0,                 
	    Ic2: 0.0,              
	    P2: 0.0,              
	    Q2: 0.0,               
	    S2: 0.0,		        
	    PF2: 0.0,             
	    F2: 0.0,              
	        
	    TodayHourCharge: 0.0, 		
	    YearyHourCharge: 0.0,        	
	    TotalHourCharge: 0.0,        	
	        
	    TodayHourDisCharge: 0.0,    	
	    YearyHourDisCharge: 0.0,     	
	    TotalHourDisCharge: 0.0,    	
	        
	    TodayEnergyCharge: 0.0,  		
	    YearEnergyCharge: 0.0,        
	    TotalEnergyCharge: 0.0,       
	        
	    TodayEnergyDisCharge: 0.0,    
	    YearEnergyDisCharge: 0.0,     
	    TotalEnergyDisCharge: 0.0,    
	        
	    ChargeCount: 0,				
	    DisChargeCount: 0,			
	        
		Temperature: 0.0,			
			
		GroundResistance: 0.0,        
		GroundResistanceN: 0.0,       
			
	    LeakCurrent: 0.0,             
	        
	    Mode: 0,						
	        
	    ChargePower: 0,				
	    DisChargePower: 0					
	}
);
 
// 索引
db.PCSData_Curr.createIndex({"ProjectID":1},{name:"idx_ProjectID"});

// 删除索引
// db.PCSData_Curr.dropIndex("idx_ProjectID");

// 查看索引
// db.PCSData_Curr.getIndexes();








db.createCollection("PCSData_CurrYear");

db.PCSData_CurrYear.insert(
   	{	
		_id:1, 
		ClltIDDay: 0,             	
		ClltDaytime: 1528183743111,         
		
		DeviceIndex: 0,         			
		
		DayTimes: 0, 					
		
		RegionID: 0,         			
		ProjectID: 0,         		
		SectionID: 0,         		
		CollectID: 0,         		
		ComID: 0,         			
		Address: 0,         				
		
		SN: "",	            	
		
		StatusInt : 0,	          		  
		StatusDesc: "",			
		
		Udc: 0.0,              
		Idc: 0.0,              
		Pdc: 0.0,             	
	    Ua: 0.0,                 
	    Ub: 0.0,              
	    Uc: 0.0,		        
	    Uab: 0.0,             
	    Ubc: 0.0,                    
	    Uca: 0.0,             
	    Ia: 0.0,               
	    Ib: 0.0,                  
	    Ic: 0.0,               
	    P: 0.0,               
	    Q: 0.0,                
	    S: 0.0,		        
	    PF: 0.0,              
	    F: 0.0,                       
	        
	    Udc1: 0.0,            
		Idc1: 0.0,             
		Pdc1: 0.0,            	
	    Ua1: 0.0,                
	    Ub1: 0.0,             
	    Uc1: 0.0,		        
	    Uab1: 0.0,            
	    Ubc1: 0.0,                   
	    Uca1: 0.0,            
	    Ia1: 0.0,             
	    Ib1: 0.0,                 
	    Ic1: 0.0,              
	    P1: 0.0,              
	    Q1: 0.0,               
	    S1: 0.0,		        
	    PF1: 0.0,             
	    F1: 0.0,              
	        
	    Udc2: 0.0,            
		Idc2: 0.0,             
		Pdc2: 0.0,            	
	    Ua2: 0.0,                
	    Ub2: 0.0,             
	    Uc2: 0.0,		        
	    Uab2: 0.0,            
	    Ubc2: 0.0,                   
	    Uca2: 0.0,            
	    Ia2: 0.0,             
	    Ib2: 0.0,                 
	    Ic2: 0.0,              
	    P2: 0.0,              
	    Q2: 0.0,               
	    S2: 0.0,		        
	    PF2: 0.0,             
	    F2: 0.0,              
	        
	    TodayHourCharge: 0.0, 		
	    YearyHourCharge: 0.0,        	
	    TotalHourCharge: 0.0,        	
	        
	    TodayHourDisCharge: 0.0,    	
	    YearyHourDisCharge: 0.0,     	
	    TotalHourDisCharge: 0.0,    	
	        
	    TodayEnergyCharge: 0.0,  		
	    YearEnergyCharge: 0.0,        
	    TotalEnergyCharge: 0.0,       
	        
	    TodayEnergyDisCharge: 0.0,    
	    YearEnergyDisCharge: 0.0,     
	    TotalEnergyDisCharge: 0.0,    
	        
	    ChargeCount: 0,				
	    DisChargeCount: 0,			
	        
		Temperature: 0.0,				
			
		GroundResistance: 0.0,        
		GroundResistanceN: 0.0,       
			
	    LeakCurrent: 0.0,             
	        
	    Mode: 0,						
	        
	    ChargePower: 0,				
	    DisChargePower: 0					
	}
);
 
// 索引
db.PCSData_CurrYear.createIndex({"ClltIDDay":1, "ProjectID":1},{name:"idx_ClltIDDay_ProjectID"});

// 删除索引
// db.PCSData_CurrYear.dropIndex("idx_ClltIDDay_ProjectID");

// 查看索引
// db.PCSData_CurrYear.getIndexes();








db.createCollection("PCSRpt_CurrYear");

db.PCSRpt_CurrYear.insert(
   	{	
		_id:1, 
		ClltIDDay: 0,             		
		
		DeviceIndex: 0,         			
		
		ClltIDYear: 0,             			
		ClltIDMonth: 0,             		
		ClltIDDayIndex: 0,             	
		
		RegionID: 0,         				
		ProjectID: 0,         			
		SectionID: 0,         			
		CollectID: 0,         			
		ComID: 0,         				
		Address: 0,         					
		
		TodayEnergyCharge: 0.0,  		
	    TotalEnergyCharge: 0.0,       
	        
	    TodayEnergyDisCharge: 0.0,    
	    TotalEnergyDisCharge: 0.0 
	}
);
 
// 索引
db.PCSRpt_CurrYear.createIndex({"ClltIDDay":1, "ProjectID":1},{name:"idx_ClltIDDay_ProjectID"});

// 删除索引
// db.PCSRpt_CurrYear.dropIndex("idx_ClltIDDay_ProjectID");

// 查看索引
// db.PCSRpt_CurrYear.getIndexes();






db.createCollection("PCSGroupData_Curr");

db.PCSGroupData_Curr.insert(
   	{	
		_id:1, 
		ClltIDDay: 0,             	
		ClltDaytime: 1528183743111,         
		
		DeviceIndex: 0,         			
		
		DayTimes: 0, 					
		
		RegionID: 0,         			
		ProjectID: 0,         		
		SectionID: 0,         		
		CollectID: 0,         		
		ComID: 0,         			
		Address: 0,         				
		
		SN: "",	            	
		
		StatusInt : 0,	          		  
		StatusDesc: "",							
		
		GroupID: 0,			
			
		Udc: 0.0,              
		Idc: 0.0,              
		Pdc: 0.0,             	
	    MotherLineVoltage: 0.0,                 
	                
	    TodayHourCharge: 0.0, 		
	    YearyHourCharge: 0.0,        	
	    TotalHourCharge: 0.0,        	
	        
	    TodayHourDisCharge: 0.0,    	
	    YearyHourDisCharge: 0.0,     	
	    TotalHourDisCharge: 0.0,    	
	        
	    TodayEnergyCharge: 0.0,  		
	    YearEnergyCharge: 0.0,        
	    TotalEnergyCharge: 0.0,       
	        
	    TodayEnergyDisCharge: 0.0,    
	    YearEnergyDisCharge: 0.0,     
	    TotalEnergyDisCharge: 0.0,    
	        
	    ChargeCount: 0,				
	    DisChargeCount: 0,			
	        
		Temperature: 0.0,				
			
		GroundResistance: 0.0,        
		GroundResistanceN: 0.0,       
			
	    LeakCurrent: 0.0,             
	        
	    Mode: 0,						
	        
	    ChargePower: 0,				
	    DisChargePower: 0				
	}
);
 
// 索引
db.PCSGroupData_Curr.createIndex({"ProjectID":1},{name:"idx_ProjectID"});

// 删除索引
// db.PCSGroupData_Curr.dropIndex("idx_ProjectID");

// 查看索引
// db.PCSGroupData_Curr.getIndexes();







db.createCollection("PCSGroupData_CurrYear");

db.PCSGroupData_CurrYear.insert(
   	{	
		_id:1, 
		ClltIDDay: 0,             	
		ClltDaytime: 1528183743111,         
		
		DeviceIndex: 0,         			
		
		DayTimes: 0, 					
		
		RegionID: 0,         			
		ProjectID: 0,         		
		SectionID: 0,         		
		CollectID: 0,         		
		ComID: 0,         			
		Address: 0,         				
		
		SN: "",	            	
		
		StatusInt : 0,	          		  
		StatusDesc: "",							
		
		GroupID: 0,			
			
		Udc: 0.0,              
		Idc: 0.0,              
		Pdc: 0.0,             	
	    MotherLineVoltage: 0.0,                 
	                
	    TodayHourCharge: 0.0, 		
	    YearyHourCharge: 0.0,        	
	    TotalHourCharge: 0.0,        	
	        
	    TodayHourDisCharge: 0.0,    	
	    YearyHourDisCharge: 0.0,     	
	    TotalHourDisCharge: 0.0,    	
	        
	    TodayEnergyCharge: 0.0,  		
	    YearEnergyCharge: 0.0,        
	    TotalEnergyCharge: 0.0,       
	        
	    TodayEnergyDisCharge: 0.0,    
	    YearEnergyDisCharge: 0.0,     
	    TotalEnergyDisCharge: 0.0,    
	        
	    ChargeCount: 0,				
	    DisChargeCount: 0,			
	        
		Temperature: 0.0,				
			
		GroundResistance: 0.0,        
		GroundResistanceN: 0.0,       
			
	    LeakCurrent: 0.0,             
	        
	    Mode: 0,						
	        
	    ChargePower: 0,				
	    DisChargePower: 0				
	}
);
 
// 索引
db.PCSGroupData_CurrYear.createIndex({"ClltIDDay":1, "ProjectID":1},{name:"idx_ClltIDDay_ProjectID"});

// 删除索引
// db.PCSGroupData_CurrYear.dropIndex("idx_ClltIDDay_ProjectID");

// 查看索引
// db.PCSGroupData_CurrYear.getIndexes();







db.createCollection("BMSData_Curr");

db.BMSData_Curr.insert(
   	{	
		_id:1, 
		ClltIDDay: 0,             	
		ClltDaytime: 1528183743111,         
		
		DeviceIndex: 0,         			
		
		DayTimes: 0, 					
		
		RegionID: 0,         			
		ProjectID: 0,         		
		SectionID: 0,         		
		CollectID: 0,         		
		ComID: 0,         			
		Address: 0,         				
		
		SN: "",	            	
		
		StatusInt : 0,	          		  
		StatusDesc: "",			
		
		Voltage: 0.0,									
		Current: 0.0,					
		SOC: 0,						
		SOH: 0,							
			
		TodayCharge: 0.0,       		
	    TodayDisCharge: 0.0,    		
	        
		TotalCharge: 0.0,       		
	    TotalDisCharge: 0.0,    		
	        
	    ChargeCount: 0,				
	    DisChargeCount: 0,			
	        
	    MaxVoltage: 0.0,			
		MaxVoltageGroup: 0,			
		MaxVoltageUnit: 0,						
			
		MinVoltage: 0.0,				
		MinVoltageGroup: 0,			
		MinVoltageUnit: 0,					
			
		MaxTemperature: 0.0,			
		MaxTemperatureGroup: 0,		
		MaxTemperatureUnit: 0,					
			
		MinTemperature: 0.0,			
		MinTemperatureGroup: 0,		
		MinTemperatureUnit: 0,			
			
		GroupCount: 0,				
		GroupUnitCount: 0,			
			
		ChargeMode: 0,				
		ConstantVoltage: 0.0,			
		ConstantCurrent: 0.0,			
		PurlingCurrent: 0.0		
	}
);
 
// 索引
db.BMSData_Curr.createIndex({"ProjectID":1},{name:"idx_ProjectID"});

// 删除索引
// db.BMSData_Curr.dropIndex("idx_ProjectID");

// 查看索引
// db.BMSData_Curr.getIndexes();







db.createCollection("BMSData_CurrYear");

db.BMSData_CurrYear.insert(
   	{	
		_id:1, 
		ClltIDDay: 0,             	
		ClltDaytime: 1528183743111,         
		
		DeviceIndex: 0,         			
		
		DayTimes: 0, 					
		
		RegionID: 0,         			
		ProjectID: 0,         		
		SectionID: 0,         		
		CollectID: 0,         		
		ComID: 0,         			
		Address: 0,         				
		
		SN: "",	            	
		
		StatusInt : 0,	          		  
		StatusDesc: "",			
		
		Voltage: 0.0,									
		Current: 0.0,					
		SOC: 0,						
		SOH: 0,							
			
		TodayCharge: 0.0,       		
	    TodayDisCharge: 0.0,    		
	        
		TotalCharge: 0.0,       		
	    TotalDisCharge: 0.0,    		
	        
	    ChargeCount: 0,				
	    DisChargeCount: 0,			
	        
	    MaxVoltage: 0.0,				
		MaxVoltageGroup: 0,			
		MaxVoltageUnit: 0,						
			
		MinVoltage: 0.0,				
		MinVoltageGroup: 0,			
		MinVoltageUnit: 0,					
			
		MaxTemperature: 0.0,			
		MaxTemperatureGroup: 0,		
		MaxTemperatureUnit: 0,					
			
		MinTemperature: 0.0,			
		MinTemperatureGroup: 0,		
		MinTemperatureUnit: 0,			
			
		GroupCount: 0,				
		GroupUnitCount: 0,			
			
		ChargeMode: 0,				
		ConstantVoltage: 0.0,			
		ConstantCurrent: 0.0,			
		PurlingCurrent: 0.0	
	}
);
 
// 索引
db.BMSData_CurrYear.createIndex({"ClltIDDay":1, "ProjectID":1},{name:"idx_ClltIDDay_ProjectID"});

// 删除索引
// db.BMSData_CurrYear.dropIndex("idx_ClltIDDay_ProjectID");

// 查看索引
// db.BMSData_CurrYear.getIndexes();







db.createCollection("BMSRpt_CurrYear");

db.BMSRpt_CurrYear.insert(
   	{	
		_id:1, 
		ClltIDDay: 0,             		
		
		DeviceIndex: 0,         			
		
		ClltIDYear: 0,             			
		ClltIDMonth: 0,             		
		ClltIDDayIndex: 0,             	
		
		RegionID: 0,         				
		ProjectID: 0,         			
		SectionID: 0,         			
		CollectID: 0,         			
		ComID: 0,         				
		Address: 0,         					
		
		TodayCharge: 0.0,       		
	    TodayDisCharge: 0.0,    		
	            
		TotalCharge: 0.0,       		
	    TotalDisCharge: 0.0  
	}
);
 
// 索引
db.BMSRpt_CurrYear.createIndex({"ClltIDDay":1, "ProjectID":1},{name:"idx_ClltIDDay_ProjectID"});

// 删除索引
// db.BMSRpt_CurrYear.dropIndex("idx_ClltIDDay_ProjectID");

// 查看索引
// db.BMSRpt_CurrYear.getIndexes();








db.createCollection("BMSGroupData_Curr");

db.BMSGroupData_Curr.insert(
   	{	
		_id:1, 
		ClltIDDay: 0,             	
		ClltDaytime: 1528183743111,         
		
		DeviceIndex: 0,         			
		
		DayTimes: 0, 					
		
		RegionID: 0,         			
		ProjectID: 0,         		
		SectionID: 0,         		
		CollectID: 0,         		
		ComID: 0,         			
		Address: 0,         				
		
		SN: "",	            	
		
		StatusInt : 0,	          		  
		StatusDesc: "",			
		
		GroupID: 0,					
			
		Voltage: 0.0,									
		Current: 0.0,					
			
		SOC: 0,						
		SOH: 0,							
			
	    TotalCharge: 0.0,       		
	    TotalDisCharge: 0.0,    		
	        
	    ChargeCount: 0,				
	    DisChargeCount: 0,			
	        
		MaxVoltage: 0.0,				
		MaxVoltageUnit: 0,						
			
		MinVoltage: 0.0,				
		MinVoltageUnit: 0,					
			
		AvgVoltage: 0.0,				
			
		MaxTemperature: 0.0,			
		MaxTemperatureUnit : 0,					
			
		MinTemperature: 0.0,			
		MinTemperatureUnit: 0,				
			
		AvgTemperature: 0.0,			
	        
	    MaxSOC: 0.0,					
		MaxSOCUnit: 0,							
			
		MinSOC: 0.0,					
		MinSOCUnit: 0,					
			
		MaxSOH: 0.0,					
		MaxSOHUnit: 0,							
			
		MinSOH: 0.0,					
		MinSOHUnit: 0,					
			
		UnitCount: 0,					
		DesignPower: 0	
	}
);
 
// 索引
db.BMSGroupData_Curr.createIndex({"ProjectID":1},{name:"idx_ProjectID"});

// 删除索引
// db.BMSGroupData_Curr.dropIndex("idx_ProjectID");

// 查看索引
// db.BMSGroupData_Curr.getIndexes();








db.createCollection("BMSGroupData_CurrYear");

db.BMSGroupData_CurrYear.insert(
   	{	
		_id:1, 
		ClltIDDay: 0,             	
		ClltDaytime: 1528183743111,         
		
		DeviceIndex: 0,         			
		
		DayTimes: 0, 					
		
		RegionID: 0,         			
		ProjectID: 0,         		
		SectionID: 0,         		
		CollectID: 0,         		
		ComID: 0,         			
		Address: 0,         				
		
		SN: "",	            	
		
		StatusInt : 0,	          		  
		StatusDesc: "",			
		
		GroupID: 0,					
			
		Voltage: 0.0,									
		Current: 0.0,					
			
		SOC: 0,						
		SOH: 0,							
			
	    TotalCharge: 0.0,       		
	    TotalDisCharge: 0.0,    		
	        
	    ChargeCount: 0,				
	    DisChargeCount: 0,			
	        
		MaxVoltage: 0.0,				
		MaxVoltageUnit: 0,						
			
		MinVoltage: 0.0,				
		MinVoltageUnit: 0,					
			
		AvgVoltage: 0.0,				
			
		MaxTemperature: 0.0,			
		MaxTemperatureUnit : 0,					
			
		MinTemperature: 0.0,			
		MinTemperatureUnit: 0,				
			
		AvgTemperature: 0.0,			
	        
	    MaxSOC: 0.0,					
		MaxSOCUnit: 0,							
			
		MinSOC: 0.0,					
		MinSOCUnit: 0,					
			
		MaxSOH: 0.0,					
		MaxSOHUnit: 0,							
			
		MinSOH: 0.0,					
		MinSOHUnit: 0,					
			
		UnitCount: 0,					
		DesignPower: 0	
	}
);
 
// 索引
db.BMSGroupData_CurrYear.createIndex({"ClltIDDay":1, "ProjectID":1},{name:"idx_ClltIDDay_ProjectID"});

// 删除索引
// db.BMSGroupData_CurrYear.dropIndex("idx_ClltIDDay_ProjectID");

// 查看索引
// db.BMSGroupData_CurrYear.getIndexes();








db.createCollection("BMSUnitData_Curr");

db.BMSUnitData_Curr.insert(
   	{	
		_id:1, 
		ClltIDDay: 0,             	
		ClltDaytime: 1528183743111,         
		
		DeviceIndex: 0,         			
		
		DayTimes: 0, 					
		
		RegionID: 0,         			
		ProjectID: 0,         		
		SectionID: 0,         		
		CollectID: 0,         		
		ComID: 0,         			
		Address: 0,         				
		
		SN: "",	            	
		
		StatusInt : 0,	          		  
		StatusDesc: "",			
		
		GroupID: 0,			
		UnitID: 0,			
			
		Voltage: 0.0,			
		Temperature: 0.0,			
		SOC: 0,				
		SOH: 0					
	}
);
 
// 索引
db.BMSUnitData_Curr.createIndex({"ProjectID":1},{name:"idx_ProjectID"});

// 删除索引
// db.BMSUnitData_Curr.dropIndex("idx_ProjectID");

// 查看索引
// db.BMSUnitData_Curr.getIndexes();








db.createCollection("BMSUnitData_CurrYear");

db.BMSUnitData_CurrYear.insert(
   	{	
		_id:1, 
		ClltIDDay: 0,             	
		ClltDaytime: 1528183743111,         
		
		DeviceIndex: 0,         			
		
		DayTimes: 0, 					
		
		RegionID: 0,         			
		ProjectID: 0,         		
		SectionID: 0,         		
		CollectID: 0,         		
		ComID: 0,         			
		Address: 0,         				
		
		SN: "",	            	
		
		StatusInt : 0,	          		  
		StatusDesc: "",			
		
		GroupID: 0,			
		UnitID: 0,			
			
		Voltage: 0.0,			
		Temperature: 0.0,			
		SOC: 0,				
		SOH: 0					
	}
);
 
// 索引
db.BMSUnitData_CurrYear.createIndex({"ClltIDDay":1, "ProjectID":1},{name:"idx_ClltIDDay_ProjectID"});

// 删除索引
// db.BMSUnitData_CurrYear.dropIndex("idx_ClltIDDay_ProjectID");

// 查看索引
// db.BMSUnitData_CurrYear.getIndexes();








db.createCollection("BMSTemperatureData_Curr");

db.BMSTemperatureData_Curr.insert(
   	{	
		_id:1, 
		ClltIDDay: 0,             	
		ClltDaytime: 1528183743111,         
		
		DeviceIndex: 0,         			
		
		DayTimes: 0, 					
		
		RegionID: 0,         			
		ProjectID: 0,         		
		SectionID: 0,         		
		CollectID: 0,         		
		ComID: 0,         			
		Address: 0,         				
		
		SN: "",	            	
		
		StatusInt : 0,	          		  
		StatusDesc: "",			
		
		GroupID: 0,					
		TemperatureID: 0,				
			
		Voltage: 0.0,					
		Temperature: 0.0,					
		SOC: 0,						
		SOH: 0								
	}
);
 
// 索引
db.BMSTemperatureData_Curr.createIndex({"ProjectID":1},{name:"idx_ProjectID"});

// 删除索引
// db.BMSTemperatureData_Curr.dropIndex("idx_ProjectID");

// 查看索引
// db.BMSTemperatureData_Curr.getIndexes();









db.createCollection("BMSTemperatureData_CurrYear");

db.BMSTemperatureData_CurrYear.insert(
   	{	
		_id:1, 
		ClltIDDay: 0,             	
		ClltDaytime: 1528183743111,         
		
		DeviceIndex: 0,         			
		
		DayTimes: 0, 					
		
		RegionID: 0,         			
		ProjectID: 0,         		
		SectionID: 0,         		
		CollectID: 0,         		
		ComID: 0,         			
		Address: 0,         				
		
		SN: "",	            	
		
		StatusInt : 0,	          		  
		StatusDesc: "",			
		
		GroupID: 0,					
		TemperatureID: 0,				
			
		Voltage: 0.0,					
		Temperature: 0.0,					
		SOC: 0,						
		SOH: 0								
	}
);
 
// 索引
db.BMSTemperatureData_CurrYear.createIndex({"ClltIDDay":1, "ProjectID":1},{name:"idx_ClltIDDay_ProjectID"});

// 删除索引
// db.BMSTemperatureData_CurrYear.dropIndex("idx_ClltIDDay_ProjectID");

// 查看索引
// db.BMSTemperatureData_CurrYear.getIndexes();








db.createCollection("AirConditionData_Curr");

db.AirConditionData_Curr.insert(
   	{	
		_id:1, 
		ClltIDDay: 0,             	
		ClltDaytime: 1528183743111,         
		
		DeviceIndex: 0,         			
		
		DayTimes: 0, 					
		
		RegionID: 0,         			
		ProjectID: 0,         		
		SectionID: 0,         		
		CollectID: 0,         		
		ComID: 0,         			
		Address: 0,         				
		
		SN: "",	            	
		
		StatusInt : 0,	          		  
		StatusDesc: "",			
		
		FanIn: 0,				    	
		FanOut: 0,					
			
		Compressor: 0,						
		Heater: 0,					
			
		Fanemergency: 0,		    	
			
		TemperatureCoil: 0.0,			
		TemperatureIn: 0.0,			
		TemperatureOut: 0.0,			
		TemperatureGas: 0.0,			
		TemperatureCondensation: 0.0,	
			
		Humidity: 0.0,        		
			
		Udc: 0.0,            			   
		Ua: 0.0,             			 
		Ia: 0.0,             				
		
		SettingTemperature: 0.0		
	}
);
 
// 索引
db.AirConditionData_Curr.createIndex({"ProjectID":1},{name:"idx_ProjectID"});

// 删除索引
// db.AirConditionData_Curr.dropIndex("idx_ProjectID");

// 查看索引
// db.AirConditionData_Curr.getIndexes();








db.createCollection("AirConditionData_CurrYear");

db.AirConditionData_CurrYear.insert(
   	{	
		_id:1, 
		ClltIDDay: 0,             	
		ClltDaytime: 1528183743111,         
		
		DeviceIndex: 0,         			
		
		DayTimes: 0, 					
		
		RegionID: 0,         			
		ProjectID: 0,         		
		SectionID: 0,         		
		CollectID: 0,         		
		ComID: 0,         			
		Address: 0,         				
		
		SN: "",	            	
		
		StatusInt : 0,	          		  
		StatusDesc: "",			
		
		FanIn: 0,				    	
		FanOut: 0,					
			
		Compressor: 0,						
		Heater: 0,					
			
		Fanemergency: 0,		    	
			
		TemperatureCoil: 0.0,			
		TemperatureIn: 0.0,			
		TemperatureOut: 0.0,			
		TemperatureGas: 0.0,			
		TemperatureCondensation: 0.0,	
			
		Humidity: 0.0,        		
			
		Udc: 0.0,            			   
		Ua: 0.0,             			 
		Ia: 0.0,             				
		
		SettingTemperature: 0.0		
	}
);
 
// 索引
db.AirConditionData_CurrYear.createIndex({"ClltIDDay":1, "ProjectID":1},{name:"idx_ClltIDDay_ProjectID"});

// 删除索引
// db.AirConditionData_CurrYear.dropIndex("idx_ClltIDDay_ProjectID");

// 查看索引
// db.AirConditionData_CurrYear.getIndexes();









db.createCollection("FireAlertData_Curr");

db.FireAlertData_Curr.insert(
   	{	
		_id:1, 
		ClltIDDay: 0,             	
		ClltDaytime: 1528183743111,         
		
		DeviceIndex: 0,         			
		
		DayTimes: 0, 					
		
		RegionID: 0,         			
		ProjectID: 0,         		
		SectionID: 0,         		
		CollectID: 0,         		
		ComID: 0,         			
		Address: 0,         				
		
		SN: "",	            	
		
		StatusInt : 0,	          		  
		StatusDesc: "",			
		
		WarnSignal: 0,				
	        
	    Spray: 0,						
	    Alert: 0,						
	    Fire: 0							
	}
);
 
// 索引
db.FireAlertData_Curr.createIndex({"ProjectID":1},{name:"idx_ProjectID"});

// 删除索引
// db.FireAlertData_Curr.dropIndex("idx_ProjectID");

// 查看索引
// db.FireAlertData_Curr.getIndexes();









db.createCollection("FireAlertData_CurrYear");

db.FireAlertData_CurrYear.insert(
   	{	
		_id:1, 
		ClltIDDay: 0,             	
		ClltDaytime: 1528183743111,         
		
		DeviceIndex: 0,         			
		
		DayTimes: 0, 					
		
		RegionID: 0,         			
		ProjectID: 0,         		
		SectionID: 0,         		
		CollectID: 0,         		
		ComID: 0,         			
		Address: 0,         				
		
		SN: "",	            	
		
		StatusInt : 0,	          		  
		StatusDesc: "",			
		
		WarnSignal: 0,				
	        
	    Spray: 0,						
	    Alert: 0,						
	    Fire: 0							
	}
);
 
// 索引
db.FireAlertData_CurrYear.createIndex({"ClltIDDay":1, "ProjectID":1},{name:"idx_ClltIDDay_ProjectID"});

// 删除索引
// db.FireAlertData_CurrYear.dropIndex("idx_ClltIDDay_ProjectID");

// 查看索引
// db.FireAlertData_CurrYear.getIndexes();







db.createCollection("AGCData_Curr");

db.AGCData_Curr.insert(
   	{	
		_id:1, 
		ClltIDDay: 0,             	
		ClltDaytime: 1528183743111,         
		
		DeviceIndex: 0,         			
		
		DayTimes: 0, 					
		
		RegionID: 0,         			
		ProjectID: 0,         		
		SectionID: 0,         		
		CollectID: 0,         		
		ComID: 0,         			
		Address: 0,         				
		
		SN: "",	            	
		
		StatusInt : 0,	          		  
		StatusDesc: "",			
		
		AgcSetting: 0.0,				
		SolarPower: 0.0,				
		SolarMaxPower: 0.0,						
	        
	    PowerLine: 0.0,				
	    RemoteLocal: 0		
	}
);
 
// 索引
db.AGCData_Curr.createIndex({"ProjectID":1},{name:"idx_ProjectID"});

// 删除索引
// db.AGCData_Curr.dropIndex("idx_ProjectID");

// 查看索引
// db.AGCData_Curr.getIndexes();








db.createCollection("AGCData_CurrYear");

db.AGCData_CurrYear.insert(
   	{	
		_id:1, 
		ClltIDDay: 0,             	
		ClltDaytime: 1528183743111,         
		
		DeviceIndex: 0,         			
		
		DayTimes: 0, 					
		
		RegionID: 0,         			
		ProjectID: 0,         		
		SectionID: 0,         		
		CollectID: 0,         		
		ComID: 0,         			
		Address: 0,         				
		
		SN: "",	            	
		
		StatusInt : 0,	          		  
		StatusDesc: "",			
		
		AgcSetting: 0.0,				
		SolarPower: 0.0,				
		SolarMaxPower: 0.0,						
	        
	    PowerLine: 0.0,				
	    RemoteLocal: 0	
	}
);
 
// 索引
db.AGCData_CurrYear.createIndex({"ClltIDDay":1, "ProjectID":1},{name:"idx_ClltIDDay_ProjectID"});

// 删除索引
// db.AGCData_CurrYear.dropIndex("idx_ClltIDDay_ProjectID");

// 查看索引
// db.AGCData_CurrYear.getIndexes();









db.createCollection("InverterOffLineData_Curr");

db.InverterOffLineData_Curr.insert(
	   	{	
		_id:1, 
		ClltIDDay: 0,             	
		ClltDaytime: 1528183743111,         
		
		DeviceIndex: 0,         			
		
		DayTimes: 0, 					
		
		RegionID: 0,         			
		ProjectID: 0,         		
		SectionID: 0,         		
		CollectID: 0,         		
		ComID: 0,         			
		Address: 0,         				
		
		SN: "",	            	
		
		StatusInt : 0,	          		  
		StatusDesc: "",			
		
		DCVoltage: 0.0,		
	    DCCurrent: 0.0,	    
	    DCVoltageB: 0.0,	    
	    DCCurrentB: 0.0,	    
	    DCVoltageC: 0.0,	    
	    DCCurrentC: 0.0,	      
	        
	    StandbyYearEnergy: 0.0,       
	    StandbyTotalEnergy: 0.0,       
	    StandbyTemperature: 0.0,        
	            
	    StandbyVoltage: 0.0,		    
	    StandbyCurrent: 0.0,	    
	    StandbyFrequency: 0.0,        
	    StandbyVoltageB: 0.0,		    
	    StandbyCurrentB: 0.0,		    
	    StandbyFrequencyB: 0.0,       
	    StandbyVoltageC: 0.0,		    
	    StandbyCurrentC: 0.0,		    
	    StandbyFrequencyC: 0.0,              
	    StandbyTotalOutputPower: 0.0,	  
	               
	    Voltage: 0.0,             
	    Current: 0.0,                   
	    Frequency: 0.0,             
	    OutputPower: 0.0,            
	    ROutputPower: 0.0,        
	    PowerFactor: 0.0,		    
	    VoltageB: 0.0,            
	    CurrentB: 0.0,                  
	    FrequencyB: 0.0,          
	    OutputPowerB: 0.0,         
	    ROutputPowerB: 0.0,        
	    PowerFactorB: 0.0,		   
	    VoltageC: 0.0,            
	    CurrentC: 0.0,                    
	    FrequencyC: 0.0,           
	    OutputPowerC: 0.0,        
	    ROutputPowerC: 0.0,               
	    PowerFactorC: 0.0,		
	        
	    TotalOutputPower: 0.0,        
	    RTotalOutputPower: 0.0,       
	        
	    TotalPowerFactor: 0.0,                   
	        
	    TodayEnergy: 0.0,             
	    TotalEnergy: 0.0,             
	        
	    NTodayEnergy: 0.0,            
	    NTotalEnergy: 0.0,            
	        
	    DCUnderVoltage: 0.0,          
	    DCOverVoltage: 0.0,           
	        
	    StandbyUnderVoltage: 0.0,     
	    StandbyOverVoltage: 0.0,              
	        
	    BtryVoltage: 0.0,		        
	    BtryCurrent: 0.0,		        
	    BtryOutputPower: 0.0,	          
	        
	    TemperatureInverter: 0.0,		    
	    TemperatureBooster: 0.0,		
	    TemperatureSink: 0.0	
	}
);
 
// 索引
db.InverterOffLineData_Curr.createIndex({"ProjectID":1},{name:"idx_ProjectID"});

// 删除索引
// db.InverterOffLineData_Curr.dropIndex("idx_ProjectID");

// 查看索引
// db.InverterOffLineData_Curr.getIndexes();







db.createCollection("InverterOffLineData_CurrYear");

db.InverterOffLineData_CurrYear.insert(
   	{	
		_id:1, 
		ClltIDDay: 0,             	
		ClltDaytime: 1528183743111,         
		
		DeviceIndex: 0,         			
		
		DayTimes: 0, 					
		
		RegionID: 0,         			
		ProjectID: 0,         		
		SectionID: 0,         		
		CollectID: 0,         		
		ComID: 0,         			
		Address: 0,         				
		
		SN: "",	            	
		
		StatusInt : 0,	          		  
		StatusDesc: "",			
		
		DCVoltage: 0.0,		
	    DCCurrent: 0.0,	    
	    DCVoltageB: 0.0,	    
	    DCCurrentB: 0.0,	    
	    DCVoltageC: 0.0,	    
	    DCCurrentC: 0.0,	     
	        
	    StandbyYearEnergy: 0.0,       
	    StandbyTotalEnergy: 0.0,       
	    StandbyTemperature: 0.0,        
	            
	    StandbyVoltage: 0.0,		    
	    StandbyCurrent: 0.0,	    
	    StandbyFrequency: 0.0,        
	    StandbyVoltageB: 0.0,		    
	    StandbyCurrentB: 0.0,		    
	    StandbyFrequencyB: 0.0,       
	    StandbyVoltageC: 0.0,		    
	    StandbyCurrentC: 0.0,		    
	    StandbyFrequencyC: 0.0,              
	    StandbyTotalOutputPower: 0.0,	  
	               
	    Voltage: 0.0,             
	    Current: 0.0,                   
	    Frequency: 0.0,             
	    OutputPower: 0.0,            
	    ROutputPower: 0.0,        
	    PowerFactor: 0.0,		    
	    VoltageB: 0.0,            
	    CurrentB: 0.0,                  
	    FrequencyB: 0.0,          
	    OutputPowerB: 0.0,         
	    ROutputPowerB: 0.0,        
	    PowerFactorB: 0.0,		   
	    VoltageC: 0.0,            
	    CurrentC: 0.0,                    
	    FrequencyC: 0.0,           
	    OutputPowerC: 0.0,        
	    ROutputPowerC: 0.0,               
	    PowerFactorC: 0.0,		
	        
	    TotalOutputPower: 0.0,        
	    RTotalOutputPower: 0.0,       
	        
	    TotalPowerFactor: 0.0,                   
	        
	    TodayEnergy: 0.0,             
	    TotalEnergy: 0.0,             
	        
	    NTodayEnergy: 0.0,            
	    NTotalEnergy: 0.0,            
	        
	    DCUnderVoltage: 0.0,          
	    DCOverVoltage: 0.0,           
	        
	    StandbyUnderVoltage: 0.0,     
	    StandbyOverVoltage: 0.0,              
	        
	    BtryVoltage: 0.0,		        
	    BtryCurrent: 0.0,		        
	    BtryOutputPower: 0.0,	          
	        
	    TemperatureInverter: 0.0,		    
	    TemperatureBooster: 0.0,		
	    TemperatureSink: 0.0	
	}
);
 
// 索引
db.InverterOffLineData_CurrYear.createIndex({"ClltIDDay":1, "ProjectID":1},{name:"idx_ClltIDDay_ProjectID"});

// 删除索引
// db.InverterOffLineData_CurrYear.dropIndex("idx_ClltIDDay_ProjectID");

// 查看索引
// db.InverterOffLineData_CurrYear.getIndexes();







db.createCollection("ChargeData_Curr");

db.ChargeData_Curr.insert(
   	{	
		_id:1, 
		ClltIDDay: 0,             	
		ClltDaytime: 1528183743111,         
		
		DeviceIndex: 0,         			
		
		DayTimes: 0, 					
		
		RegionID: 0,         			
		ProjectID: 0,         		
		SectionID: 0,         		
		CollectID: 0,         		
		ComID: 0,         			
		Address: 0,         				
		
		SN: "",	            	
		
		StatusInt : 0,	          		  
		StatusDesc: "",			
		
		GridVoltage: 0.0,		
	    GridCurrent: 0.0,	    
	    GridVoltageB: 0.0,	
	    GridCurrentB: 0.0,	
	    GridVoltageC: 0.0,	
	    GridCurrentC: 0.0,	   
	    PVYearEnergy: 0.0,    
	    PVTotalEnergy: 0.0,    
	    PVTemperature: 0.0,     
	            
	    PVVoltage: 0.0,		    
	    PVCurrent: 0.0,		    
	    PVInputPower: 0.0,       	
	    PVVoltageB: 0.0,		   
	    PVCurrentB: 0.0,		   
	    PVInputPowerB: 0.0,      	
	    PVVoltageC: 0.0,		    
	    PVCurrentC: 0.0,		    
	    PVInputPowerC: 0.0,      	        
	    PVTotalInputPower: 0.0,	  
	    AhPVTotal: 0.0,			  
	              
	    Voltage: 0.0,             
	    Current: 0.0,                   
	    Frequency: 0.0,             
	    OutputPower: 0.0,            
	    ROutputPower: 0.0,        
	    PowerFactor: 0.0,		    
	    VoltageB: 0.0,            
	    CurrentB: 0.0,                  
	    FrequencyB: 0.0,          
	    OutputPowerB: 0.0,         
	    ROutputPowerB: 0.0,        
	    PowerFactorB: 0.0,		  
	    VoltageC: 0.0,            
	    CurrentC: 0.0,                    
	    FrequencyC: 0.0,           
	    OutputPowerC: 0.0,        
	    ROutputPowerC: 0.0,               
	    PowerFactorC: 0.0,		
	        
	    TotalOutputPower: 0.0,    
	    RTotalOutputPower: 0.0,   
	    TotalPowerFactor: 0.0,               
	        
	    TodayEnergy: 0.0,         
	    TotalEnergy: 0.0,         
	        
	    NTodayEnergy: 0.0,        
	    NTotalEnergy: 0.0,        
	        
	    AhChargeReset: 0.0,        
	    AhChargeTotal: 0.0,        
	        
	    KwhChargeReset: 0.0,       
	    KwhChargeTotal: 0.0,               
	        
	    BtryVoltage: 0.0,		    
	    BtryCurrent: 0.0,	    	
	    BtryOutputPower: 0.0,	      
	    AhBtryTotal: 0.0,	    	  
	        
	    TemperatureBattery: 0.0,	    
	    TemperatureRTS: 0.0,		
	    TemperatureSink: 0.0
	}
);
 
// 索引
db.ChargeData_Curr.createIndex({"ProjectID":1},{name:"idx_ProjectID"});

// 删除索引
// db.ChargeData_Curr.dropIndex("idx_ProjectID");

// 查看索引
// db.ChargeData_Curr.getIndexes();









db.createCollection("ChargeData_CurrYear");

db.ChargeData_CurrYear.insert(
   	{	
		_id:1, 
		ClltIDDay: 0,             	
		ClltDaytime: 1528183743111,         
		
		DeviceIndex: 0,         			
		
		DayTimes: 0, 					
		
		RegionID: 0,         			
		ProjectID: 0,         		
		SectionID: 0,         		
		CollectID: 0,         		
		ComID: 0,         			
		Address: 0,         				
		
		SN: "",	            	
		
		StatusInt : 0,	          		  
		StatusDesc: "",			
		
		GridVoltage: 0.0,		
	    GridCurrent: 0.0,	    
	    GridVoltageB: 0.0,	
	    GridCurrentB: 0.0,	
	    GridVoltageC: 0.0,	
	    GridCurrentC: 0.0,	   
	    PVYearEnergy: 0.0,    
	    PVTotalEnergy: 0.0,    
	    PVTemperature: 0.0,     
	            
	    PVVoltage: 0.0,		    
	    PVCurrent: 0.0,		    
	    PVInputPower: 0.0,       	
	    PVVoltageB: 0.0,		    
	    PVCurrentB: 0.0,		    
	    PVInputPowerB: 0.0,      	
	    PVVoltageC: 0.0,		    
	    PVCurrentC: 0.0,		    
	    PVInputPowerC: 0.0,      	        
	    PVTotalInputPower: 0.0,	  
	    AhPVTotal: 0.0,			  
	              
	    Voltage: 0.0,             
	    Current: 0.0,                   
	    Frequency: 0.0,             
	    OutputPower: 0.0,            
	    ROutputPower: 0.0,        
	    PowerFactor: 0.0,		    
	    VoltageB: 0.0,            
	    CurrentB: 0.0,                  
	    FrequencyB: 0.0,          
	    OutputPowerB: 0.0,         
	    ROutputPowerB: 0.0,        
	    PowerFactorB: 0.0,		   
	    VoltageC: 0.0,            
	    CurrentC: 0.0,                    
	    FrequencyC: 0.0,           
	    OutputPowerC: 0.0,        
	    ROutputPowerC: 0.0,               
	    PowerFactorC: 0.0,		
	        
	    TotalOutputPower: 0.0,    
	    RTotalOutputPower: 0.0,   
	    TotalPowerFactor: 0.0,               
	        
	    TodayEnergy: 0.0,         
	    TotalEnergy: 0.0,         
	        
	    NTodayEnergy: 0.0,        
	    NTotalEnergy: 0.0,        
	        
	    AhChargeReset: 0.0,        
	    AhChargeTotal: 0.0,        
	        
	    KwhChargeReset: 0.0,       
	    KwhChargeTotal: 0.0,               
	        
	    BtryVoltage: 0.0,		    
	    BtryCurrent: 0.0,	    	
	    BtryOutputPower: 0.0,	      
	    AhBtryTotal: 0.0,	    	  
	        
	    TemperatureBattery: 0.0,	    
	    TemperatureRTS: 0.0,		
	    TemperatureSink: 0.0	
	}
);
 
// 索引
db.ChargeData_CurrYear.createIndex({"ClltIDDay":1, "ProjectID":1},{name:"idx_ClltIDDay_ProjectID"});

// 删除索引
// db.ChargeData_CurrYear.dropIndex("idx_ClltIDDay_ProjectID");

// 查看索引
// db.ChargeData_CurrYear.getIndexes();









db.createCollection("ChargeStationData_Curr");

db.ChargeStationData_Curr.insert(
   	{	
		_id:1, 
		ClltIDDay: 0,             	
		ClltDaytime: 1528183743111,         
		
		DeviceIndex: 0,         			
		
		DayTimes: 0, 					
		
		RegionID: 0,         			
		ProjectID: 0,         		
		SectionID: 0,         		
		CollectID: 0,         		
		ComID: 0,         			
		Address: 0,         				
		
		SN: "",	            	
		
		StatusInt : 0,	          		  
		StatusDesc: "",			
		
		StationType: 0,				
	        
	    GunID: 0,						        
		SOC: 0,						
		
		BMSNeedVoltage: 0.0,		  	
	    BMSNeedCurrent: 0.0,		 	
	    
	    Voltage: 0.0,		    		
	    Current: 0.0,		    		  
	    
	    BMSMeasureVoltage: 0.0,		
	    BMSMeasureCurrent: 0.0,		
	    
	    WhSum: 0.0,           		
	    ThisTimeCharge: 0.0, 			
	    ThisTimeMoney: 0.0, 			
	    
	    MaxVoltage: 0.0,				 
	    MaxTemperature: 0.0,			
	    
	    ThisTimeRemainderHour: 0.0,	
	    
	    GunID1: 0,				            
		SOC1: 0,						
		
		BMSNeedVoltage1: 0.0,		  	
	    BMSNeedCurrent1: 0.0,		 	
	    
	    Voltage1: 0.0,		    	
	    Current1: 0.0,		    	  
	    
	    BMSMeasureVoltage1: 0.0,		
	    BMSMeasureCurrent1: 0.0,		
	    
	    WhSum1: 0.0,           		
	    ThisTimeCharge1: 0.0, 			
	    ThisTimeMoney1: 0.0, 			
	    
	    MaxVoltage1: 0.0,				 
	    MaxTemperature1: 0.0,			
	    
	    ThisTimeRemainderHour1: 0.0	
	}
);
 
// 索引
db.ChargeStationData_Curr.createIndex({"ProjectID":1},{name:"idx_ProjectID"});

// 删除索引
// db.ChargeStationData_Curr.dropIndex("idx_ProjectID");

// 查看索引
// db.ChargeStationData_Curr.getIndexes();








db.createCollection("ChargeStationData_CurrYear");

db.ChargeStationData_CurrYear.insert(
   	{	
		_id:1, 
		ClltIDDay: 0,             	
		ClltDaytime: 1528183743111,         
		
		DeviceIndex: 0,         			
		
		DayTimes: 0, 					
		
		RegionID: 0,         			
		ProjectID: 0,         		
		SectionID: 0,         		
		CollectID: 0,         		
		ComID: 0,         			
		Address: 0,         				
		
		SN: "",	            	
		
		StatusInt : 0,	          		  
		StatusDesc: "",			
		
		StationType: 0,				
	        
	    GunID: 0,						        
		SOC: 0,						
		
		BMSNeedVoltage: 0.0,		  	
	    BMSNeedCurrent: 0.0,		 	
	    
	    Voltage: 0.0,		    		
	    Current: 0.0,		    		  
	    
	    BMSMeasureVoltage: 0.0,		
	    BMSMeasureCurrent: 0.0,		
	    
	    WhSum: 0.0,           		
	    ThisTimeCharge: 0.0, 			
	    ThisTimeMoney: 0.0, 			
	    
	    MaxVoltage: 0.0,				 
	    MaxTemperature: 0.0,			
	    
	    ThisTimeRemainderHour: 0.0,	
	    
	    GunID1: 0,				            
		SOC1: 0,						
		
		BMSNeedVoltage1: 0.0,		  	
	    BMSNeedCurrent1: 0.0,		 	
	    
	    Voltage1: 0.0,		    	
	    Current1: 0.0,		    	  
	    
	    BMSMeasureVoltage1: 0.0,		
	    BMSMeasureCurrent1: 0.0,		
	    
	    WhSum1: 0.0,           		
	    ThisTimeCharge1: 0.0, 			
	    ThisTimeMoney1: 0.0, 			
	    
	    MaxVoltage1: 0.0,				 
	    MaxTemperature1: 0.0,			
	    
	    ThisTimeRemainderHour1: 0.0	
	}
);
 
// 索引
db.ChargeStationData_CurrYear.createIndex({"ClltIDDay":1, "ProjectID":1},{name:"idx_ClltIDDay_ProjectID"});

// 删除索引
// db.ChargeStationData_CurrYear.dropIndex("idx_ClltIDDay_ProjectID");

// 查看索引
// db.ChargeStationData_CurrYear.getIndexes();







db.createCollection("ChargeStationRpt_CurrYear");

db.ChargeStationRpt_CurrYear.insert(
   	{	
		_id:1, 
		ClltIDDay: 0,             		
		
		DeviceIndex: 0,         			
		
		ClltIDYear: 0,             			
		ClltIDMonth: 0,             		
		ClltIDDayIndex: 0,             	
		
		RegionID: 0,         				
		ProjectID: 0,         			
		SectionID: 0,         			
		CollectID: 0,         			
		ComID: 0,         				
		Address: 0,         					
		
		WhSum: 0.0,           			
	    ThisTimeCharge: 0.0, 				
	    ThisTimeMoney: 0.0 	
	}
);
 
// 索引
db.ChargeStationRpt_CurrYear.createIndex({"ClltIDDay":1, "ProjectID":1},{name:"idx_ClltIDDay_ProjectID"});

// 删除索引
// db.ChargeStationRpt_CurrYear.dropIndex("idx_ClltIDDay_ProjectID");

// 查看索引
// db.ChargeStationRpt_CurrYear.getIndexes();







db.createCollection("TransformData_Curr");

db.TransformData_Curr.insert(
   	{	
		_id:1, 
		ClltIDDay: 0,             	
		ClltDaytime: 1528183743111,         
		
		DeviceIndex: 0,         			
		
		DayTimes: 0, 					
		
		RegionID: 0,         			
		ProjectID: 0,         		
		SectionID: 0,         		
		CollectID: 0,         		
		ComID: 0,         			
		Address: 0,         				
		
		SN: "",	            	
		
		StatusInt : 0,	          		  
		StatusDesc: "",			
		
		TemperatureA: 0.0,    		 
		TemperatureB: 0.0,    		 
	    TemperatureC: 0.0,    		 
	    TemperatureD: 0.0,    		 
	        
	    AlertBit: 0,                  
	    OverTemperatureBit: 0,        
	    OverTemperatureTripBit: 0,    
	    FanControlBit: 0,             
	    IronCoreOverTemperatureBit: 0 
	}
);
 
// 索引
db.TransformData_Curr.createIndex({"ProjectID":1},{name:"idx_ProjectID"});

// 删除索引
// db.TransformData_Curr.dropIndex("idx_ProjectID");

// 查看索引
// db.TransformData_Curr.getIndexes();







db.createCollection("TransformData_CurrYear");

db.TransformData_CurrYear.insert(
   	{	
		_id:1, 
		ClltIDDay: 0,             	
		ClltDaytime: 1528183743111,         
		
		DeviceIndex: 0,         			
		
		DayTimes: 0, 					
		
		RegionID: 0,         			
		ProjectID: 0,         		
		SectionID: 0,         		
		CollectID: 0,         		
		ComID: 0,         			
		Address: 0,         				
		
		SN: "",	            	
		
		StatusInt : 0,	          		  
		StatusDesc: "",			
		
		TemperatureA: 0.0,    		 
		TemperatureB: 0.0,    		 
	    TemperatureC: 0.0,    		 
	    TemperatureD: 0.0,    		 
	        
	    AlertBit: 0,                  
	    OverTemperatureBit: 0,        
	    OverTemperatureTripBit: 0,    
	    FanControlBit: 0,             
	    IronCoreOverTemperatureBit: 0 
	}
);
 
// 索引
db.TransformData_CurrYear.createIndex({"ClltIDDay":1, "ProjectID":1},{name:"idx_ClltIDDay_ProjectID"});

// 删除索引
// db.TransformData_CurrYear.dropIndex("idx_ClltIDDay_ProjectID");

// 查看索引
// db.TransformData_CurrYear.getIndexes();








// 创建视图
// db.createView("view_alertdata_curr","alertdata_curr",[{$lookup:{from:"deviceinfo",localField:"ProjectID",foreignField:"ProjectID",as:"dev_info"}},{$project:{"dev_info.ProjectID":1}}]);
db.createView("view_alertdata_curr","alertdata_curr",[{$lookup:{from:"deviceinfo",localField:"ProjectID",foreignField:"ProjectID",as:"dev_info"}},{$project:{"dev_info.ProjectID":1}}, {$lookup:{from:"deviceinfo",localField:"ComID",foreignField:"ComID",as:"dev_info1"}},{$project:{"dev_info1.ComID":1}}, {$lookup:{from:"deviceinfo",localField:"Address",foreignField:"Address",as:"dev_info2"}},{$project:{"dev_info2.Address":1}}]);


// db.createView("view_bmsdata_curr","bmsdata_curr",[{$lookup:{from:"deviceinfo",localField:"ProjectID",foreignField:"ProjectID",as:"dev_info"}},{$project:{"dev_info.ProjectID":1}}]);
db.createView("view_bmsdata_curr","bmsdata_curr",[{$lookup:{from:"deviceinfo",localField:"ProjectID",foreignField:"ProjectID",as:"dev_info"}},{$project:{"dev_info.ProjectID":1}}, {$lookup:{from:"deviceinfo",localField:"ComID",foreignField:"ComID",as:"dev_info1"}},{$project:{"dev_info1.ComID":1}}, {$lookup:{from:"deviceinfo",localField:"Address",foreignField:"Address",as:"dev_info2"}},{$project:{"dev_info2.Address":1}}]);


// db.createView("view_chargestationdata_curr","chargestationdata_curr",[{$lookup:{from:"deviceinfo",localField:"ProjectID",foreignField:"ProjectID",as:"dev_info"}},{$project:{"dev_info.ProjectID":1}}]);
db.createView("view_chargestationdata_curr","chargestationdata_curr",[{$lookup:{from:"deviceinfo",localField:"ProjectID",foreignField:"ProjectID",as:"dev_info"}},{$project:{"dev_info.ProjectID":1}}, {$lookup:{from:"deviceinfo",localField:"ComID",foreignField:"ComID",as:"dev_info1"}},{$project:{"dev_info1.ComID":1}}, {$lookup:{from:"deviceinfo",localField:"Address",foreignField:"Address",as:"dev_info2"}},{$project:{"dev_info2.Address":1}}]);


// db.createView("view_epmdata_curr","epmdata_curr",[{$lookup:{from:"deviceinfo",localField:"ProjectID",foreignField:"ProjectID",as:"dev_info"}},{$project:{"dev_info.ProjectID":1}}]);
db.createView("view_epmdata_curr","epmdata_curr",[{$lookup:{from:"deviceinfo",localField:"ProjectID",foreignField:"ProjectID",as:"dev_info"}},{$project:{"dev_info.ProjectID":1}}, {$lookup:{from:"deviceinfo",localField:"ComID",foreignField:"ComID",as:"dev_info1"}},{$project:{"dev_info1.ComID":1}}, {$lookup:{from:"deviceinfo",localField:"Address",foreignField:"Address",as:"dev_info2"}},{$project:{"dev_info2.Address":1}}]);


// db.createView("view_inverterdata_curr","inverterdata_curr",[{$lookup:{from:"deviceinfo",localField:"ProjectID",foreignField:"ProjectID",as:"dev_info"}},{$project:{"dev_info.ProjectID":1}}]);
db.createView("view_inverterdata_curr","inverterdata_curr",[{$lookup:{from:"deviceinfo",localField:"ProjectID",foreignField:"ProjectID",as:"dev_info"}},{$project:{"dev_info.ProjectID":1}}, {$lookup:{from:"deviceinfo",localField:"ComID",foreignField:"ComID",as:"dev_info1"}},{$project:{"dev_info1.ComID":1}}, {$lookup:{from:"deviceinfo",localField:"Address",foreignField:"Address",as:"dev_info2"}},{$project:{"dev_info2.Address":1}}]);


// db.createView("view_pcsdata_curr","pcsdata_curr",[{$lookup:{from:"deviceinfo",localField:"ProjectID",foreignField:"ProjectID",as:"dev_info"}},{$project:{"dev_info.ProjectID":1}}]);
db.createView("view_pcsdata_curr","pcsdata_curr",[{$lookup:{from:"deviceinfo",localField:"ProjectID",foreignField:"ProjectID",as:"dev_info"}},{$project:{"dev_info.ProjectID":1}}, {$lookup:{from:"deviceinfo",localField:"ComID",foreignField:"ComID",as:"dev_info1"}},{$project:{"dev_info1.ComID":1}}, {$lookup:{from:"deviceinfo",localField:"Address",foreignField:"Address",as:"dev_info2"}},{$project:{"dev_info2.Address":1}}]);








// 创建视图
create view view_alertdata_curr AS 
SELECT
alertdata_curr.IID AS IID,
alertdata_curr.ClltIDDay AS ClltIDDay,
alertdata_curr.ClltDaytime AS ClltDaytime,
alertdata_curr.DeviceIndex AS DeviceIndex,
alertdata_curr.DayTimes AS DayTimes,
alertdata_curr.ClltIDYear AS ClltIDYear,
alertdata_curr.ClltIDMonth AS ClltIDMonth,
alertdata_curr.ClltIDDayIndex AS ClltIDDayIndex,
alertdata_curr.RegionID AS RegionID,
alertdata_curr.ProjectID AS ProjectID,
alertdata_curr.SectionID AS SectionID,
alertdata_curr.CollectID AS CollectID,
alertdata_curr.ComID AS ComID,
alertdata_curr.Address AS Address,
alertdata_curr.StatusInt AS StatusInt,
alertdata_curr.StatusDesc AS StatusDesc,
deviceinfo.ComLinkType AS ComLinkType,
deviceinfo.DeviceName AS DeviceName,
deviceinfo.DeviceID AS DeviceID,
deviceinfo.Description AS Description,
deviceinfo.Caption AS Caption
from (alertdata_curr join deviceinfo on(((alertdata_curr.ProjectID = deviceinfo.ProjectID) and (alertdata_curr.ComID = deviceinfo.ComID) and (alertdata_curr.Address = deviceinfo.Address)))) ;







// 创建视图
create view view_bmsdata_curr AS 
SELECT
bmsdata_curr.IID AS IID,
bmsdata_curr.ClltIDDay AS ClltIDDay,
bmsdata_curr.ClltDaytime AS ClltDaytime,
bmsdata_curr.DeviceIndex AS DeviceIndex,
bmsdata_curr.DayTimes AS DayTimes,
bmsdata_curr.RegionID AS RegionID,
bmsdata_curr.ProjectID AS ProjectID,
bmsdata_curr.SectionID AS SectionID,
bmsdata_curr.CollectID AS CollectID,
bmsdata_curr.ComID AS ComID,
bmsdata_curr.Address AS Address,
bmsdata_curr.SN AS SN,
bmsdata_curr.StatusInt AS StatusInt,
bmsdata_curr.StatusDesc AS StatusDesc,
bmsdata_curr.Voltage AS Voltage,
bmsdata_curr.Current AS Current,
bmsdata_curr.SOC AS SOC,
bmsdata_curr.SOH AS SOH,
bmsdata_curr.TodayCharge AS TodayCharge,
bmsdata_curr.TodayDisCharge AS TodayDisCharge,
bmsdata_curr.TotalCharge AS TotalCharge,
bmsdata_curr.TotalDisCharge AS TotalDisCharge,
bmsdata_curr.ChargeCount AS ChargeCount,
bmsdata_curr.DisChargeCount AS DisChargeCount,
bmsdata_curr.MaxVoltage AS MaxVoltage,
bmsdata_curr.MaxVoltageGroup AS MaxVoltageGroup,
bmsdata_curr.MaxVoltageUnit AS MaxVoltageUnit,
bmsdata_curr.MinVoltage AS MinVoltage,
bmsdata_curr.MinVoltageGroup AS MinVoltageGroup,
bmsdata_curr.MinVoltageUnit AS MinVoltageUnit,
bmsdata_curr.MaxTemperature AS MaxTemperature,
bmsdata_curr.MaxTemperatureGroup AS MaxTemperatureGroup,
bmsdata_curr.MaxTemperatureUnit AS MaxTemperatureUnit,
bmsdata_curr.MinTemperature AS MinTemperature,
bmsdata_curr.MinTemperatureGroup AS MinTemperatureGroup,
bmsdata_curr.MinTemperatureUnit AS MinTemperatureUnit,
bmsdata_curr.GroupCount AS GroupCount,
bmsdata_curr.GroupUnitCount AS GroupUnitCount,
bmsdata_curr.ChargeMode AS ChargeMode,
bmsdata_curr.ConstantVoltage AS ConstantVoltage,
bmsdata_curr.ConstantCurrent AS ConstantCurrent,
bmsdata_curr.PurlingCurrent AS PurlingCurrent,
deviceinfo.ComLinkType AS ComLinkType,
deviceinfo.DeviceName AS DeviceName,
deviceinfo.DeviceID AS DeviceID,
deviceinfo.Description AS Description,
deviceinfo.Caption AS Caption
from (bmsdata_curr join deviceinfo on(((bmsdata_curr.ProjectID = deviceinfo.ProjectID) and (bmsdata_curr.ComID = deviceinfo.ComID) and (bmsdata_curr.Address = deviceinfo.Address)))) ;






// 创建视图
create view view_chargestationdata_curr AS 
SELECT
chargestationdata_curr.IID AS IID,
chargestationdata_curr.ClltIDDay AS ClltIDDay,
chargestationdata_curr.DeviceIndex AS DeviceIndex,
chargestationdata_curr.DayTimes AS DayTimes,
chargestationdata_curr.ClltDaytime AS ClltDaytime,
chargestationdata_curr.RegionID AS RegionID,
chargestationdata_curr.ProjectID AS ProjectID,
chargestationdata_curr.SectionID AS SectionID,
chargestationdata_curr.CollectID AS CollectID,
chargestationdata_curr.ComID AS ComID,
chargestationdata_curr.Address AS Address,
chargestationdata_curr.SN AS SN,
chargestationdata_curr.StatusInt AS StatusInt,
chargestationdata_curr.StatusDesc AS StatusDesc,
chargestationdata_curr.StationType AS StationType,
chargestationdata_curr.GunID AS GunID,
chargestationdata_curr.SOC AS SOC,
chargestationdata_curr.BMSNeedVoltage AS BMSNeedVoltage,
chargestationdata_curr.BMSNeedCurrent AS BMSNeedCurrent,
chargestationdata_curr.Voltage AS Voltage,
chargestationdata_curr.Current AS Current,
chargestationdata_curr.BMSMeasureVoltage AS BMSMeasureVoltage,
chargestationdata_curr.BMSMeasureCurrent AS BMSMeasureCurrent,
chargestationdata_curr.WhSum AS WhSum,
chargestationdata_curr.ThisTimeCharge AS ThisTimeCharge,
chargestationdata_curr.ThisTimeMoney AS ThisTimeMoney,
chargestationdata_curr.MaxVoltage AS MaxVoltage,
chargestationdata_curr.MaxTemperature AS MaxTemperature,
chargestationdata_curr.ThisTimeRemainderHour AS ThisTimeRemainderHour,
chargestationdata_curr.GunID1 AS GunID1,
chargestationdata_curr.SOC1 AS SOC1,
chargestationdata_curr.BMSNeedVoltage1 AS BMSNeedVoltage1,
chargestationdata_curr.BMSNeedCurrent1 AS BMSNeedCurrent1,
chargestationdata_curr.Voltage1 AS Voltage1,
chargestationdata_curr.Current1 AS Current1,
chargestationdata_curr.BMSMeasureVoltage1 AS BMSMeasureVoltage1,
chargestationdata_curr.BMSMeasureCurrent1 AS BMSMeasureCurrent1,
chargestationdata_curr.WhSum1 AS WhSum1,
chargestationdata_curr.ThisTimeCharge1 AS ThisTimeCharge1,
chargestationdata_curr.ThisTimeMoney1 AS ThisTimeMoney1,
chargestationdata_curr.MaxVoltage1 AS MaxVoltage1,
chargestationdata_curr.MaxTemperature1 AS MaxTemperature1,
chargestationdata_curr.ThisTimeRemainderHour1 AS ThisTimeRemainderHour1,
deviceinfo.ComLinkType AS ComLinkType,
deviceinfo.DeviceName AS DeviceName,
deviceinfo.DeviceID AS DeviceID,
deviceinfo.Description AS Description,
deviceinfo.Caption AS Caption
from (chargestationdata_curr join deviceinfo on(((chargestationdata_curr.ProjectID = deviceinfo.ProjectID) and (chargestationdata_curr.ComID = deviceinfo.ComID) and (chargestationdata_curr.Address = deviceinfo.Address))));






// 创建视图
create view view_epmdata_curr AS 
SELECT
epmdata_curr.IID AS IID,
epmdata_curr.ClltIDDay AS ClltIDDay,
epmdata_curr.ClltDaytime AS ClltDaytime,
epmdata_curr.DeviceIndex AS DeviceIndex,
epmdata_curr.DayTimes AS DayTimes,
epmdata_curr.RegionID AS RegionID,
epmdata_curr.ProjectID AS ProjectID,
epmdata_curr.SectionID AS SectionID,
epmdata_curr.CollectID AS CollectID,
epmdata_curr.ComID AS ComID,
epmdata_curr.Address AS Address,
epmdata_curr.SN AS SN,
epmdata_curr.StatusInt AS StatusInt,
epmdata_curr.StatusDesc AS StatusDesc,
epmdata_curr.Voltage1 AS Voltage1,
epmdata_curr.Voltage2 AS Voltage2,
epmdata_curr.Voltage3 AS Voltage3,
epmdata_curr.Current1 AS Current1,
epmdata_curr.Current2 AS Current2,
epmdata_curr.Current3 AS Current3,
epmdata_curr.Frequency1 AS Frequency1,
epmdata_curr.Frequency2 AS Frequency2,
epmdata_curr.Frequency3 AS Frequency3,
epmdata_curr.ActivePower1 AS ActivePower1,
epmdata_curr.ActivePower2 AS ActivePower2,
epmdata_curr.ActivePower3 AS ActivePower3,
epmdata_curr.Var1 AS Var1,
epmdata_curr.Var2 AS Var2,
epmdata_curr.Var3 AS Var3,
epmdata_curr.PowerFactor1 AS PowerFactor1,
epmdata_curr.PowerFactor2 AS PowerFactor2,
epmdata_curr.PowerFactor3 AS PowerFactor3,
epmdata_curr.VSum AS VSum,
epmdata_curr.ASum AS ASum,
epmdata_curr.WSum AS WSum,
epmdata_curr.VarSum AS VarSum,
epmdata_curr.PFSum AS PFSum,
epmdata_curr.WhSum AS WhSum,
epmdata_curr.VarhSum AS VarhSum,
epmdata_curr.WhNSum AS WhNSum,
epmdata_curr.VarhNSum AS VarhNSum,
epmdata_curr.Voltage1N AS Voltage1N,
epmdata_curr.Voltage2N AS Voltage2N,
epmdata_curr.Voltage3N AS Voltage3N,
epmdata_curr.THDV1 AS THDV1,
epmdata_curr.THDV2 AS THDV2,
epmdata_curr.THDV3 AS THDV3,
epmdata_curr.THDA1 AS THDA1,
epmdata_curr.THDA2 AS THDA2,
epmdata_curr.THDA3 AS THDA3,
epmdata_curr.WhToday AS WhToday,
epmdata_curr.VarhToday AS VarhToday,
epmdata_curr.WhNToday AS WhNToday,
epmdata_curr.VarhNToday AS VarhNToday,
epmdata_curr.WhJian AS WhJian,
epmdata_curr.WhFeng AS WhFeng,
epmdata_curr.WhPing AS WhPing,
epmdata_curr.WhGu AS WhGu,
epmdata_curr.WhNJian AS WhNJian,
epmdata_curr.WhNFeng AS WhNFeng,
epmdata_curr.WhNPing AS WhNPing,
epmdata_curr.WhNGu AS WhNGu,
epmdata_curr.WhLastMonth AS WhLastMonth,
epmdata_curr.WhLastMonthJian AS WhLastMonthJian,
epmdata_curr.WhLastMonthFeng AS WhLastMonthFeng,
epmdata_curr.WhLastMonthPing AS WhLastMonthPing,
epmdata_curr.WhLastMonthGu AS WhLastMonthGu,
epmdata_curr.WhNLastMonth AS WhNLastMonth,
epmdata_curr.WhNLastMonthJian AS WhNLastMonthJian,
epmdata_curr.WhNLastMonthFeng AS WhNLastMonthFeng,
epmdata_curr.WhNLastMonthPing AS WhNLastMonthPing,
epmdata_curr.WhNLastMonthGu AS WhNLastMonthGu,
epmdata_curr.DI0 AS DI0,
epmdata_curr.DI1 AS DI1,
epmdata_curr.DI2 AS DI2,
epmdata_curr.DI3 AS DI3,
epmdata_curr.DO0 AS DO0,
epmdata_curr.DO1 AS DO1,
epmdata_curr.DO2 AS DO2,
epmdata_curr.DO3 AS DO3,
deviceinfo.ComLinkType AS ComLinkType,
deviceinfo.DeviceName AS DeviceName,
deviceinfo.DeviceID AS DeviceID,
deviceinfo.Description AS Description,
deviceinfo.Caption AS Caption
from (epmdata_curr join deviceinfo on(((epmdata_curr.ProjectID = deviceinfo.ProjectID) and (epmdata_curr.ComID = deviceinfo.ComID) and (epmdata_curr.Address = deviceinfo.Address))));







// 创建视图
create view view_inverterdata_curr AS 
SELECT
inverterdata_curr.IID AS IID,
inverterdata_curr.ClltIDDay AS ClltIDDay,
inverterdata_curr.ClltDaytime AS ClltDaytime,
inverterdata_curr.DeviceIndex AS DeviceIndex,
inverterdata_curr.DayTimes AS DayTimes,
inverterdata_curr.RegionID AS RegionID,
inverterdata_curr.ProjectID AS ProjectID,
inverterdata_curr.SectionID AS SectionID,
inverterdata_curr.CollectID AS CollectID,
inverterdata_curr.ComID AS ComID,
inverterdata_curr.Address AS Address,
inverterdata_curr.SN AS SN,
inverterdata_curr.StatusInt AS StatusInt,
inverterdata_curr.StatusDesc AS StatusDesc,
inverterdata_curr.PVVoltage AS PVVoltage,
inverterdata_curr.PVCurrent AS PVCurrent,
inverterdata_curr.PVOutputPower AS PVOutputPower,
inverterdata_curr.PVVoltageB AS PVVoltageB,
inverterdata_curr.PVCurrentB AS PVCurrentB,
inverterdata_curr.PVOutputPowerB AS PVOutputPowerB,
inverterdata_curr.PVVoltageC AS PVVoltageC,
inverterdata_curr.PVCurrentC AS PVCurrentC,
inverterdata_curr.PVOutputPowerC AS PVOutputPowerC,
inverterdata_curr.PVTotalOutputPower AS PVTotalOutputPower,
inverterdata_curr.VoltageA AS VoltageA,
inverterdata_curr.CurrentA AS CurrentA,
inverterdata_curr.FrequencyA AS FrequencyA,
inverterdata_curr.OutputPowerA AS OutputPowerA,
inverterdata_curr.ROutputPowerA AS ROutputPowerA,
inverterdata_curr.PowerFactorA AS PowerFactorA,
inverterdata_curr.VoltageB AS VoltageB,
inverterdata_curr.CurrentB AS CurrentB,
inverterdata_curr.FrequencyB AS FrequencyB,
inverterdata_curr.OutputPowerB AS OutputPowerB,
inverterdata_curr.ROutputPowerB AS ROutputPowerB,
inverterdata_curr.PowerFactorB AS PowerFactorB,
inverterdata_curr.VoltageC AS VoltageC,
inverterdata_curr.CurrentC AS CurrentC,
inverterdata_curr.FrequencyC AS FrequencyC,
inverterdata_curr.OutputPowerC AS OutputPowerC,
inverterdata_curr.ROutputPowerC AS ROutputPowerC,
inverterdata_curr.PowerFactorC AS PowerFactorC,
inverterdata_curr.TotalOutputPower AS TotalOutputPower,
inverterdata_curr.RTotalOutputPower AS RTotalOutputPower,
inverterdata_curr.TotalPowerFactor AS TotalPowerFactor,
inverterdata_curr.TodayEnergy AS TodayEnergy,
inverterdata_curr.TotalEnergy AS TotalEnergy,
inverterdata_curr.NTodayEnergy AS NTodayEnergy,
inverterdata_curr.NTotalEnergy AS NTotalEnergy,
inverterdata_curr.RTodayEnergy AS RTodayEnergy,
inverterdata_curr.RTotalEnergy AS RTotalEnergy,
inverterdata_curr.NRTodayEnergy AS NRTodayEnergy,
inverterdata_curr.NRTotalEnergy AS NRTotalEnergy,
inverterdata_curr.BtryVoltage AS BtryVoltage,
inverterdata_curr.BtryCurrent AS BtryCurrent,
inverterdata_curr.BtryOutputPower AS BtryOutputPower,
inverterdata_curr.TemperatureInverter AS TemperatureInverter,
inverterdata_curr.TemperatureBooster AS TemperatureBooster,
inverterdata_curr.TemperatureSink AS TemperatureSink,
inverterdata_curr.TodayHour AS TodayHour,
inverterdata_curr.TotalHour AS TotalHour,
inverterdata_curr.AGCSetting AS AGCSetting,
inverterdata_curr.AVCSetting AS AVCSetting,
inverterdata_curr.GroundResistance AS GroundResistance,
inverterdata_curr.LeakCurrent AS LeakCurrent,
inverterdata_curr.StringVoltage1 AS StringVoltage1,
inverterdata_curr.StringCurrent1 AS StringCurrent1,
inverterdata_curr.StringVoltage2 AS StringVoltage2,
inverterdata_curr.StringCurrent2 AS StringCurrent2,
inverterdata_curr.StringVoltage3 AS StringVoltage3,
inverterdata_curr.StringCurrent3 AS StringCurrent3,
inverterdata_curr.StringVoltage4 AS StringVoltage4,
inverterdata_curr.StringCurrent4 AS StringCurrent4,
inverterdata_curr.StringVoltage5 AS StringVoltage5,
inverterdata_curr.StringCurrent5 AS StringCurrent5,
inverterdata_curr.StringVoltage6 AS StringVoltage6,
inverterdata_curr.StringCurrent6 AS StringCurrent6,
inverterdata_curr.StringVoltage7 AS StringVoltage7,
inverterdata_curr.StringCurrent7 AS StringCurrent7,
inverterdata_curr.StringVoltage8 AS StringVoltage8,
inverterdata_curr.StringCurrent8 AS StringCurrent8,
inverterdata_curr.StringVoltage9 AS StringVoltage9,
inverterdata_curr.StringCurrent9 AS StringCurrent9,
inverterdata_curr.StringVoltage10 AS StringVoltage10,
inverterdata_curr.StringCurrent10 AS StringCurrent10,
inverterdata_curr.StringVoltage11 AS StringVoltage11,
inverterdata_curr.StringCurrent11 AS StringCurrent11,
inverterdata_curr.StringVoltage12 AS StringVoltage12,
inverterdata_curr.StringCurrent12 AS StringCurrent12,
inverterdata_curr.StringVoltage13 AS StringVoltage13,
inverterdata_curr.StringCurrent13 AS StringCurrent13,
inverterdata_curr.StringVoltage14 AS StringVoltage14,
inverterdata_curr.StringCurrent14 AS StringCurrent14,
inverterdata_curr.StringVoltage15 AS StringVoltage15,
inverterdata_curr.StringCurrent15 AS StringCurrent15,
inverterdata_curr.StringVoltage16 AS StringVoltage16,
inverterdata_curr.StringCurrent16 AS StringCurrent16,
inverterdata_curr.StringVoltage17 AS StringVoltage17,
inverterdata_curr.StringCurrent17 AS StringCurrent17,
inverterdata_curr.StringVoltage18 AS StringVoltage18,
inverterdata_curr.StringCurrent18 AS StringCurrent18,
inverterdata_curr.StringVoltage19 AS StringVoltage19,
inverterdata_curr.StringCurrent19 AS StringCurrent19,
inverterdata_curr.StringVoltage20 AS StringVoltage20,
inverterdata_curr.StringCurrent20 AS StringCurrent20,
inverterdata_curr.StringVoltage21 AS StringVoltage21,
inverterdata_curr.StringCurrent21 AS StringCurrent21,
inverterdata_curr.StringVoltage22 AS StringVoltage22,
inverterdata_curr.StringCurrent22 AS StringCurrent22,
inverterdata_curr.StringVoltage23 AS StringVoltage23,
inverterdata_curr.StringCurrent23 AS StringCurrent23,
inverterdata_curr.StringVoltage24 AS StringVoltage24,
inverterdata_curr.StringCurrent24 AS StringCurrent24,
inverterdata_curr.MotherLineVoltage AS MotherLineVoltage,
inverterdata_curr.GroudVoltage AS GroudVoltage,
inverterdata_curr.NGroudVoltage AS NGroudVoltage,
inverterdata_curr.DesignPower AS DesignPower,
inverterdata_curr.VoltageAB AS VoltageAB,
inverterdata_curr.VoltageBC AS VoltageBC,
inverterdata_curr.VoltageCA AS VoltageCA,
inverterdata_curr.SystemTime AS SystemTime,
inverterdata_curr.Efficiency AS Efficiency,
inverterdata_curr.PowerOnSecond AS PowerOnSecond,
inverterdata_curr.PowerOffSecond AS PowerOffSecond,
inverterdata_curr.MPPT1 AS MPPT1,
inverterdata_curr.MPPT2 AS MPPT2,
inverterdata_curr.MPPT3 AS MPPT3,
inverterdata_curr.MPPT4 AS MPPT4,
deviceinfo.ComLinkType AS ComLinkType,
deviceinfo.DeviceName AS DeviceName,
deviceinfo.DeviceID AS DeviceID,
deviceinfo.Description AS Description,
deviceinfo.Caption AS Caption
FROM ( inverterdata_curr JOIN deviceinfo ON ((( inverterdata_curr.ProjectID = deviceinfo.ProjectID ) AND ( inverterdata_curr.ComID = deviceinfo.ComID ) AND ( inverterdata_curr.Address = deviceinfo.Address ))));








// 创建视图
create view view_pcsdata_curr AS 
SELECT
pcsdata_curr.IID AS IID,
pcsdata_curr.ClltIDDay AS ClltIDDay,
pcsdata_curr.ClltDaytime AS ClltDaytime,
pcsdata_curr.DeviceIndex AS DeviceIndex,
pcsdata_curr.DayTimes AS DayTimes,
pcsdata_curr.RegionID AS RegionID,
pcsdata_curr.ProjectID AS ProjectID,
pcsdata_curr.SectionID AS SectionID,
pcsdata_curr.CollectID AS CollectID,
pcsdata_curr.ComID AS ComID,
pcsdata_curr.Address AS Address,
pcsdata_curr.SN AS SN,
pcsdata_curr.StatusInt AS StatusInt,
pcsdata_curr.StatusDesc AS StatusDesc,
pcsdata_curr.Udc AS Udc,
pcsdata_curr.Idc AS Idc,
pcsdata_curr.Pdc AS Pdc,
pcsdata_curr.Ua AS Ua,
pcsdata_curr.Ub AS Ub,
pcsdata_curr.Uc AS Uc,
pcsdata_curr.Uab AS Uab,
pcsdata_curr.Ubc AS Ubc,
pcsdata_curr.Uca AS Uca,
pcsdata_curr.Ia AS Ia,
pcsdata_curr.Ib AS Ib,
pcsdata_curr.Ic AS Ic,
pcsdata_curr.P AS P,
pcsdata_curr.Q AS Q,
pcsdata_curr.S AS S,
pcsdata_curr.PF AS PF,
pcsdata_curr.F AS F,
pcsdata_curr.Udc1 AS Udc1,
pcsdata_curr.Idc1 AS Idc1,
pcsdata_curr.Pdc1 AS Pdc1,
pcsdata_curr.Ua1 AS Ua1,
pcsdata_curr.Ub1 AS Ub1,
pcsdata_curr.Uc1 AS Uc1,
pcsdata_curr.Uab1 AS Uab1,
pcsdata_curr.Ubc1 AS Ubc1,
pcsdata_curr.Uca1 AS Uca1,
pcsdata_curr.Ia1 AS Ia1,
pcsdata_curr.Ib1 AS Ib1,
pcsdata_curr.Ic1 AS Ic1,
pcsdata_curr.P1 AS P1,
pcsdata_curr.Q1 AS Q1,
pcsdata_curr.S1 AS S1,
pcsdata_curr.PF1 AS PF1,
pcsdata_curr.F1 AS F1,
pcsdata_curr.Udc2 AS Udc2,
pcsdata_curr.Idc2 AS Idc2,
pcsdata_curr.Pdc2 AS Pdc2,
pcsdata_curr.Ua2 AS Ua2,
pcsdata_curr.Ub2 AS Ub2,
pcsdata_curr.Uc2 AS Uc2,
pcsdata_curr.Uab2 AS Uab2,
pcsdata_curr.Ubc2 AS Ubc2,
pcsdata_curr.Uca2 AS Uca2,
pcsdata_curr.Ia2 AS Ia2,
pcsdata_curr.Ib2 AS Ib2,
pcsdata_curr.Ic2 AS Ic2,
pcsdata_curr.P2 AS P2,
pcsdata_curr.Q2 AS Q2,
pcsdata_curr.S2 AS S2,
pcsdata_curr.PF2 AS PF2,
pcsdata_curr.F2 AS F2,
pcsdata_curr.TodayHourCharge AS TodayHourCharge,
pcsdata_curr.YearyHourCharge AS YearyHourCharge,
pcsdata_curr.TotalHourCharge AS TotalHourCharge,
pcsdata_curr.TodayHourDisCharge AS TodayHourDisCharge,
pcsdata_curr.YearyHourDisCharge AS YearyHourDisCharge,
pcsdata_curr.TotalHourDisCharge AS TotalHourDisCharge,
pcsdata_curr.TodayEnergyCharge AS TodayEnergyCharge,
pcsdata_curr.YearEnergyCharge AS YearEnergyCharge,
pcsdata_curr.TotalEnergyCharge AS TotalEnergyCharge,
pcsdata_curr.TodayEnergyDisCharge AS TodayEnergyDisCharge,
pcsdata_curr.YearEnergyDisCharge AS YearEnergyDisCharge,
pcsdata_curr.TotalEnergyDisCharge AS TotalEnergyDisCharge,
pcsdata_curr.ChargeCount AS ChargeCount,
pcsdata_curr.DisChargeCount AS DisChargeCount,
pcsdata_curr.Temperature AS Temperature,
pcsdata_curr.GroundResistance AS GroundResistance,
pcsdata_curr.GroundResistanceN AS GroundResistanceN,
pcsdata_curr.LeakCurrent AS LeakCurrent,
pcsdata_curr.Mode AS Mode,
pcsdata_curr.ChargePower AS ChargePower,
pcsdata_curr.DisChargePower AS DisChargePower,
deviceinfo.ComLinkType AS ComLinkType,
deviceinfo.DeviceName AS DeviceName,
deviceinfo.DeviceID AS DeviceID,
deviceinfo.Description AS Description,
deviceinfo.Caption AS Caption
from (pcsdata_curr join deviceinfo on(((pcsdata_curr.ProjectID = deviceinfo.ProjectID) and (pcsdata_curr.ComID = deviceinfo.ComID) and (pcsdata_curr.Address = deviceinfo.Address)))) ;









// 测试代码

手动创建集合

db.createCollection("UserInfo1")



查询
db.getCollection("UserInfo1").find()

db.UserInfo1.find()


单条插入

db.UserInfo1.insert(
   {_id:1, name: "张三", age: 23}
);



insertOne用于单条添加
　　db.UserInfo1.insertOne(
   　　{_id:2, name: "张三1", age: 24}
  　 );


//insertMany用于批量添加
　　 db.UserInfo1.insertMany([
  　　 {_id:3, name: "张三2", age: 25},
  　　 {_id:4, name: "李四2", age: 26}
　　]);



命令：db.集合名称.save(document)
注：如果文档的_id已经存在则修改，如果_id不存在则添加

db.UserInfo1.save({_id:4, name: "李四3", age: 27})
db.UserInfo1.save({name: "李四3", age: 27})


查找 全部
db.UserInfo1.find()

查找name=李四
db.UserInfo1.find({"name":"李四"}) 

db.UserInfo1.find({"name":"李四3"}) 

查找name=李四3 age=26
db.UserInfo1.find({"name":"李四3", "age":26}) 


$gt（大于）比较操作符
db.UserInfo1.find({"age":{$gt:26}})  



== 等于条件查询
db.UserInfo1.find({<field1>: <value1>,<field2>: <value2>, ... })  
db.UserInfo1.find({"onumber":"002"})  


>
$gt（大于）、$gte（大于或等于）、 $lt（小于）、 $lte（小于或等于）

$gt（大于）比较操作符
db.UserInfo1.find({"onumber":{$gt:"003"}})  

>=
$gte（大于或等于）与 $lte（小于或等于）联合查询并指定返回字段（通过第二参数）
db.UserInfo1.find({"onumber":{$gte:"002",$lte:"003"}},{"onumber":1,"cname":1})  


//更新 全部 不插入
db.UserInfo1.update({'name':'李四3'},{$set:{'name':'李四4'}},false,true );



//删除
//删除person集合中name字段的值等于ryan的所有文档。

db.UserInfo1.remove({"name":"张三"});



//db.UserInfo1.createIndex({"UserID":1},{name:"idx_UserInfo1"});










//  中文乱码测试 
create table UserInfo1 
(
    IID bigint not null primary key,	                // ID 
    UserName: "",           		// 姓名  
    Password: "",           		// 密码 
    age: 0	
);

insert into UserInfo1 values(1,'利州','123',18);
insert into UserInfo1 values(2,'allen','密码',28);

select * from UserInfo1;




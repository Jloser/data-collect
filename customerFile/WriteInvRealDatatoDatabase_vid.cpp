

/**********************************************************************************
功能：		inverter状态
			1: 文件名称 2009_datafile
            2: 文件格式: 
                状态    Day,Time,Type,COM,Address,Device,Status,EnergyToday,EnergyLife,OutputPower,CO2Reduction,
            3: 自动增加\r\n   
            4: 文件前10240为日期 位置头说明, 10240后才是数据
            5: 文件头格式 
            	2010-01-01 23456;\r\n
            
            注意：这里为了避免频繁写文件，一次读入所有未保存数据，然后再写文件。必须保证一次读入数据不能太大
            
输入参数：	CString strDataDir     数据文件所在目录
           	int iDayTimes			采集次数
			CString strDay			日期
			CString strTime			时间
            
           		
输出参数：	
作者：		李周
时间：		2008-04-28
**********************************************************************************/
void CCollectData::WriteInvRealDatatoDatabase_vid(char *pchDataBuff, int iDataBuffLen)
{
    CString strFile="";
    CString strLine="";
    CString strValue="";
	CString strYearMonth="";	
	CString strTempMonth="";	
    CString strDay="";
    CString strTime="";
    CString strTempDay="";  
	CString strTemp="";
	CString strTT="";
	CString strTT1="";
	CString strTT2="";
    CString strMsg="";
	CString strMsg1="";
	CString strMsg2="";    
    int 	iTempDayTimes=-1;
    STRC_DATAINV    *pInv=NULL;
	STRC_DATAINV    *qInv=NULL;	

	//CFile			file;
	unsigned int    uiFizeSize=0;
	
	CString 		strTempDataDir="";	
	BOOL            blnWrite=FALSE;

	//CLog			objLog;	
	//char 			chPosi[32];
	//int 			i=0;
	__int64			qwFileSize=0;
	
	//CFile			file;	
	CStdioFile 		file;
	CFileFind fFind;
	BOOL blnExist = FALSE;
	//CString strFile="";
	
	int iProjectID=-1;
	int iCollectID=-1;
	int iProjectIDOld=-1;
	int iCollectIDOld=-1;
	CString strDayOld="";
	CFolderEx objFolder;

	HRESULT     hr; 
    BOOL        blnConn=FALSE;
    
    int       iTempDay=0;
    STRC_DATABASE *p=NULL;
    STRC_DATABASE *q=NULL;
    
    int         iMaxID=-1;
    __int64		qwMaxID=0;	
    __int64		qwTemp=0;	
    BOOL        blnFund=FALSE;	
    
    //_bstr_t     bstrConnect;    
    //_bstr_t     strConnect;
	CString     strSQL="";	
	CString     strSQLS="";	
	CString     strSQLI="";	
	CString     strSQLIYear="";	
	CString     strSQLU="";	
	BOOL 		blnOK=FALSE;
	CString     strYear="";	
	CString     strMonth="";
	int     iYear=0;
	int     iMonth=0;
	int     iDay=0;
	
	CString strDatabaseType="";
	CString strServer="";
	int     iPort=-1;
	int     iBusiType=-1;
	CString strServiceName="";
    CString strDatabaseName="";
    CString strDSN="";
    CString strUID="";
    CString strPWD="";
    CString strCharset="";
    int     iDatabaseGroupID=-1;
	int     iDatabaseUnitID=-1;
	
	int     iDeviceID=-1;
	int     iDeviceIndex=0;
	
	CTime   time;
    CString strDay1 = "";    
    CString strTime1 = ""; 

	CString mSecond;
	CString strMSecond;
    int nMinSecond=0;
	struct _timeb timebuffer;

	int iCount=0;
	int iLastIndex=0;
	
	MYSQL_RES* result=NULL;
	int		res=0;
	long	count_res=0;
	
	char    chTemp[254];
    int     iTemp=-1;
    float   fltTemp=0.0;
    //CString strTemp="";
    
	int iLen=0;

	int iDayTimes=0;
    //CString strDay;	       				
    //CString strTime;	       			
    			
    //int iProjectID;
    //int iCollectID;
    			
    int iCom=0;
	int iAddress=0;
	    	        	
	int iRegionID=0; 
	//int iProjectID; 
	int iSectionID=0; 
	//int iCollectID; 
	//int iCom; 
	//int iAddress; 
						    			
	CString strSN="";
	int iStatus=0;    		 
	CString strStatus="";
	    			    	
	float fltPVVoltage=0.0;
	float fltPVCurrent=0.0;
	float fltPVOutputPower=0.0;
	float fltPVVoltageB=0.0;
	float fltPVCurrentB=0.0;
	float fltPVOutputPowerB=0.0;
	float fltPVVoltageC=0.0;
	float fltPVCurrentC=0.0;
	float fltPVOutputPowerC=0.0;       
	float fltPVTotalOutputPower=0.0;
				               
	float fltVoltage=0.0;
	float fltCurrent=0.0;     
	float fltFrequency=0.0; 
	float fltOutputPower=0.0; 
	float fltROutputPower=0.0;
	float fltPowerFactor=0.0;
	float fltVoltageB=0.0;
	float fltCurrentB=0.0;     
	float fltFrequencyB=0.0;
	float fltOutputPowerB=0.0;
	float fltROutputPowerB=0.0;
	float fltPowerFactorB=0.0;
	float fltVoltageC=0.0;
	float fltCurrentC=0.0;       
	float fltFrequencyC=0.0;
	float fltOutputPowerC=0.0;
	float fltROutputPowerC=0.0;        
	float fltPowerFactorC=0.0;
				        
	float fltTotalOutputPower=0.0;
	float fltRTotalOutputPower=0.0;
	float fltTotalPowerFactor=0.0;       
				        
	float fltTodayEnergy=0.0;
	float fltTotalEnergy=0.0;
				        
	float fltNTodayEnergy=0.0;
	float fltNTotalEnergy=0.0;
				        
	float fltRTodayEnergy=0.0;
	float fltRTotalEnergy=0.0;
				        
	float fltNRTodayEnergy=0.0;
	float fltNRTotalEnergy=0.0;      
				        
	float fltBtryVoltage=0.0;
	float fltBtryCurrent=0.0;
	float fltBtryOutputPower=0.0; 
				        
	float fltTemperatureInverter=0.0; 
	float fltTemperatureBooster=0.0;
	float fltTemperatureSink=0.0;      
				        
	float fltTodayHour=0.0;
	float fltTotalHour=0.0;
				        
	float fltAGCSetting=0.0;
	float fltAVCSetting=0.0;
				        
	float fltGroundResistance=0.0;
	float fltLeakCurrent=0.0;
				                
	float fltStringVoltage1=0.0;
	float fltStringCurrent1=0.0;    
	float fltStringVoltage2=0.0;
	float fltStringCurrent2=0.0;
	float fltStringVoltage3=0.0;
	float fltStringCurrent3=0.0;
	float fltStringVoltage4=0.0;
	float fltStringCurrent4=0.0;
	float fltStringVoltage5=0.0;
	float fltStringCurrent5=0.0;
	float fltStringVoltage6=0.0;
	float fltStringCurrent6=0.0;
	float fltStringVoltage7=0.0;
	float fltStringCurrent7=0.0;
	float fltStringVoltage8=0.0;
	float fltStringCurrent8=0.0;               
				        
	float fltStringVoltage9=0.0;
	float fltStringCurrent9=0.0;
	float fltStringVoltage10=0.0;
	float fltStringCurrent10=0.0;
	float fltStringVoltage11=0.0;
	float fltStringCurrent11=0.0;
	float fltStringVoltage12=0.0;
	float fltStringCurrent12=0.0;
	float fltStringVoltage13=0.0;
	float fltStringCurrent13=0.0;
	float fltStringVoltage14=0.0;
	float fltStringCurrent14=0.0;
	float fltStringVoltage15=0.0;
	float fltStringCurrent15=0.0;
	float fltStringVoltage16=0.0;
	float fltStringCurrent16=0.0;
	float fltStringVoltage17=0.0;
	float fltStringCurrent17=0.0;
	float fltStringVoltage18=0.0;
	float fltStringCurrent18=0.0;
	float fltStringVoltage19=0.0;
	float fltStringCurrent19=0.0;
	float fltStringVoltage20=0.0;
	float fltStringCurrent20=0.0;
	float fltStringVoltage21=0.0;
	float fltStringCurrent21=0.0;
	float fltStringVoltage22=0.0;
	float fltStringCurrent22=0.0;
	float fltStringVoltage23=0.0;
	float fltStringCurrent23=0.0;
	float fltStringVoltage24=0.0;
	float fltStringCurrent24=0.0;

	float fltMotherLineVoltage=0.0;
				        
	float fltGroudVoltage=0.0;
	float fltNGroudVoltage=0.0;
				        
	float fltDesignPower=0.0;
				        
	float fltVoltageAB=0.0;
	float fltVoltageBC=0.0;
	float fltVoltageCA=0.0;      
				        
	int iSystemTime=0;
	int iEfficiency=0;	
	int iPowerOnSecond=0;	
	int iPowerOffSecond=0;
	float fltMPPT1=0.0;
	float fltMPPT2=0.0;
	float fltMPPT3=0.0;
	float fltMPPT4=0.0;
	
	blnWrite=FALSE;
	strValue="";
	
	//函数调用
	G_WriteFunctionCall(GetCurrentThreadId(), "WriteInvRealDatatoDatabase_vid");
		
	//首先得到主框架		
	CMainFrame *pMainFrame=(CMainFrame *)AfxGetApp()->m_pMainWnd;		
		
	
	//得到数据库信息
    strDatabaseType=g_objSysDatabase.GetDatabaseType_str();
	strServer=g_objSysDatabase.GetServer_str();
	iPort=g_objSysDatabase.GetPort_i();
	iBusiType=g_objSysDatabase.GetBusiType_i();
	strServiceName=g_objSysDatabase.GetServiceName_str();
    strDatabaseName=g_objSysDatabase.GetDatabaseName_str();
    strDSN=g_objSysDatabase.GetDSN_str();
    strUID=g_objSysDatabase.GetUID_str();
    strPWD=g_objSysDatabase.GetPWD_str();
    strCharset=g_objSysDatabase.GetCharset_str();
    iDatabaseGroupID=g_objSysDatabase.GetDatabaseGroupID_i();
	iDatabaseUnitID=g_objSysDatabase.GetDatabaseUnitID_i();
	
	
	COleVariant vtOptional((long)DISP_E_PARAMNOTFOUND,VT_ERROR);
	

	strMsg1 = "WriteInvRealDatatoDatabase_vid 开始写数据库";
	strMsg2 = "WriteInvRealDatatoDatabase_vid Begin Write Database";
	strMsg = G_GetMsgLangFormat(strMsg1, strMsg2);
	g_objMsg.CMSG_SetMsgQue_vid(strMsg,_COLOR_BLACK, -1, -1, -1, -1, -1, -1, enuLogDatabaseAction);	
	if (NULL!=pMainFrame)
	{
		pMainFrame->PostMessage(_MSG_ID_OUTPUTDYM_I,0,0);
	}

	//临界区锁定
	//g_crtsecDataBase.Lock();


	try                                                                                                                                         
    { 	
        //开始事务
        //pConnection->BeginTrans();
        
		//--开启事务
		int ret = mysql_query(&g_Mysql, "start transaction");		//开启一次事务 start transaction
		//--设置事务为手动提交
		ret = mysql_query(&g_Mysql, "SET AUTOCOMMIT=0");			//SET AUTOCOMMIT=0 手动commit   SET AUTOCOMMIT=0  自动commit

			
		iCount=0;
        iLastIndex=0;
        
		//从最后开始往前 找没有保存到数据库的
		//pInv = m_pstrcInvEnd;
		//while (NULL!=pInv)
  //  	{	
		//	//未保存的 
  //  		if (TRUE==pInv->blnIsDBSave)
		//	{
		//		//已经保存
		//		break;
		//	}
		//	
		//	//下一节点
		//	qInv = pInv->pre;
		//	pInv = qInv;
		//}

		//if (NULL==pInv)
		//{
		//	pInv = m_pstrcInvHead;
		//}
		
		//得到数据
		iLen = 0;

		// 从 pchDataBuff+iLen 拷贝四个字节 到 iTemp
        memcpy(&iTemp, pchDataBuff+iLen, 4);
        iDayTimes=iTemp;
		iLen += 4;	

		// 将已chTemp 的前 254 个字节的值设为值 0x00。
		memset(chTemp, 0x00, 254);
        memcpy(chTemp, pchDataBuff+iLen, 10);        		       	    
        strDay = chTemp;
        strDay.Trim();		
		iLen += 10;	
				
		memset(chTemp, 0x00, 254);
        memcpy(chTemp, pchDataBuff+iLen, 8);        		       	    
        strTime = chTemp;
        strTime.Trim();		
		iLen += 8;	
			    
        memcpy(&iTemp, pchDataBuff+iLen, 4);
        iRegionID=iTemp;
		iLen += 4;	
				
        memcpy(&iTemp, pchDataBuff+iLen, 4);
        iProjectID=iTemp;
		iLen += 4;		
				
        memcpy(&iTemp, pchDataBuff+iLen, 4);
        iSectionID=iTemp;
		iLen += 4;	
				
        memcpy(&iTemp, pchDataBuff+iLen, 4);
        iCollectID=iTemp;
		iLen += 4;	
				
        memcpy(&iTemp, pchDataBuff+iLen, 4);
        iCom=iTemp;
		iLen += 4;	
				
        memcpy(&iTemp, pchDataBuff+iLen, 4);
        iAddress=iTemp;
		iLen += 4;	
        	    
        memset(chTemp, 0x00, 254);
        memcpy(chTemp, pchDataBuff+iLen, 32);        		       	    
        strSN = chTemp;
        strSN.Trim();		
		iLen += 32;	
				
        memcpy(&iTemp, pchDataBuff+iLen, 4);
        iStatus=iTemp;
		iLen += 4;	
	   			
	   	memset(chTemp, 0x00, 254);
        memcpy(chTemp, pchDataBuff+iLen, 128);        		       	    
        strStatus = chTemp;
        strStatus.Trim();		
		iLen += 128;
	   			
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltPVVoltage=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltPVCurrent=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltPVOutputPower=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltPVVoltageB=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltPVCurrentB=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltPVOutputPowerB=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltPVVoltageC=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltPVCurrentC=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltPVOutputPowerC=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltPVTotalOutputPower=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltVoltage=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltCurrent=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltFrequency=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltOutputPower=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltROutputPower=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltPowerFactor=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltVoltageB=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltCurrentB=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltFrequencyB=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltOutputPowerB=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltROutputPowerB=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltPowerFactorB=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltVoltageC=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltCurrentC=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltFrequencyC=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltOutputPowerC=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltROutputPowerC=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltPowerFactorC=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltTotalOutputPower=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltRTotalOutputPower=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltTotalPowerFactor=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltTodayEnergy=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltTotalEnergy=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltNTodayEnergy=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltNTotalEnergy=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltRTodayEnergy=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltRTotalEnergy=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltNRTodayEnergy=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltNRTotalEnergy=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltBtryVoltage=fltTemp;
		iLen += 4;
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltBtryCurrent=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltBtryOutputPower=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltTemperatureInverter=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltTemperatureBooster=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltTemperatureSink=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltTodayHour=fltTemp;
		iLen += 4;					
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltTotalHour=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltAGCSetting=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltAVCSetting=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltGroundResistance=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltLeakCurrent=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltStringVoltage1=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltStringCurrent1=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltStringVoltage2=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltStringCurrent2=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltStringVoltage3=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltStringCurrent3=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltStringVoltage4=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltStringCurrent4=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltStringVoltage5=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltStringCurrent5=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltStringVoltage6=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltStringCurrent6=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltStringVoltage7=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltStringCurrent7=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltStringVoltage8=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltStringCurrent8=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltStringVoltage9=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltStringCurrent9=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltStringVoltage10=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltStringCurrent10=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltStringVoltage11=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltStringCurrent11=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltStringVoltage12=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltStringCurrent12=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltStringVoltage13=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltStringCurrent13=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltStringVoltage14=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltStringCurrent14=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltStringVoltage15=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltStringCurrent15=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltStringVoltage16=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltStringCurrent16=fltTemp;
		iLen += 4;	

		memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltStringVoltage17=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltStringCurrent17=fltTemp;
		iLen += 4;	

		memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltStringVoltage18=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltStringCurrent18=fltTemp;
		iLen += 4;	

		memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltStringVoltage19=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltStringCurrent19=fltTemp;
		iLen += 4;	

		memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltStringVoltage20=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltStringCurrent20=fltTemp;
		iLen += 4;	

		memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltStringVoltage21=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltStringCurrent21=fltTemp;
		iLen += 4;	

		memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltStringVoltage22=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltStringCurrent22=fltTemp;
		iLen += 4;	

		memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltStringVoltage23=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltStringCurrent23=fltTemp;
		iLen += 4;	

		memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltStringVoltage24=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltStringCurrent24=fltTemp;
		iLen += 4;	
		
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltMotherLineVoltage=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltGroudVoltage=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltNGroudVoltage=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltDesignPower=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltVoltageAB=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltVoltageBC=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltVoltageCA=fltTemp;
		iLen += 4;	
				
        memcpy(&iTemp, pchDataBuff+iLen, 4);
        iSystemTime=iTemp;
		iLen += 4;	
				
        memcpy(&iTemp, pchDataBuff+iLen, 4);
        iEfficiency=iTemp;
		iLen += 4;	
				
        memcpy(&iTemp, pchDataBuff+iLen, 4);
        iPowerOnSecond=iTemp;
		iLen += 4;	
				
        memcpy(&iTemp, pchDataBuff+iLen, 4);
        iPowerOffSecond=iTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltMPPT1=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltMPPT2=fltTemp;
		iLen += 4;	
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltMPPT3=fltTemp;
		iLen += 4;		
				
        memcpy(&fltTemp, pchDataBuff+iLen, 4);
        fltMPPT4=fltTemp;
		iLen += 4;	
        

		if (iDataBuffLen!=iLen)
		{
			strMsg1 = "WriteInvRealDatatoDatabase_vid 包长度不符 退出";
			strMsg2 = "WriteInvRealDatatoDatabase_vid 包长度不符 退出";
			strMsg = G_GetMsgLangFormat(strMsg1, strMsg2);
			g_objMsg.CMSG_SetMsgQue_vid(strMsg,_COLOR_BLACK, -1, -1, -1, -1, -1, -1, enuLogDatabaseAction);	
			if (NULL!=pMainFrame)
			{
				pMainFrame->PostMessage(_MSG_ID_OUTPUTDYM_I,0,0);
			}

			return;

		}//if (iDataBuffLen!=iLen)
		
    	//pInv = m_pstrcInvHead;
		//while (NULL!=pInv)
    	{	
    		//比当前采集次数老的, 未保存的 
    		//if (TRUE==pInv->blnIsFinish && FALSE==pInv->blnIsDBSave)
    		{
    			//blnIsDBSave = TRUE;	
    			
    			iTempDayTimes=iDayTimes;
    			strDay = strDay;	       				//天2009-01-01  
    			strTime = strTime;	       			//12:13:20
    			strYearMonth = strDay.Left(7);	        	//月2009-01
    		    
    			iProjectID=iProjectID;
    			iCollectID=iCollectID;
                
                strTemp = strDay;
    			strTemp.Replace("-", "");
    			iTempDay =  atoi(strTemp); // 日期转数字 20090101
    			
    			//年
				strYear = strDay.Left(4);
				iYear = atoi(strYear);
				
				//月
				strMonth = strDay.Mid(5,2);
				iMonth = atoi(strMonth);
				
				//日
				strTemp = strDay.Right(2);
				iDay = atoi(strTemp);
	
				//得到时间
				time = CTime::GetCurrentTime();
				strDay1 =  time.Format("%Y-%m-%d");        //2009-12-10
				strTime1 =  time.Format("%H:%M:%S");        //12:12:13   
								
				//显示毫秒        		    
				_ftime(&timebuffer);
				strMSecond.Format("%d",timebuffer.millitm);
				if (1==strMSecond.GetLength())
				{
					strMSecond = "00" + strMSecond;
				}
				else if (2==strMSecond.GetLength())
				{
					strMSecond = "0" + strMSecond;
				}
				//9223372036854775807
				//11 190702163856667 1
				
				strTT.Format("%d", iDatabaseGroupID);
				strTT1.Format("%d", iDatabaseUnitID);
				strTT2.Format("%d", iLastIndex);
				
				strDay1 = strDay1.Right(8);
				
				strTemp = strTT + strTT1 + strDay1 + strTime1 + strMSecond + strTT2;
				strTemp.Replace("-", "");
				strTemp.Replace(":", "");
				qwMaxID = _atoi64(strTemp);     //1010 20190702163856
				
				if (0==iLastIndex)
				{
					iLastIndex = 1;
				}
				else
				{
					iLastIndex = 0;
				}
				
				strTT.Format("%d", iCom);
				strTT1.Format("%d", iAddress);
				if (strTT1.GetLength()<2)
				{
					strTT1 = "0" + strTT1;
				}
				strTemp = strTT + strTT1;
				iDeviceIndex = atoi(strTemp);
				
				
				//表 InverterData_Curr
				//判断是否存在  	
				strSQLS="";
    			strSQLS = strSQLS + "select ProjectID, ComID, Address from InverterData_Curr"; 
				strSQLS = strSQLS + " where "; 	
				strTemp.Format(" ProjectID=%d", iProjectID);
				strSQLS = strSQLS + strTemp;
				strTemp.Format(" and ComID=%d", iCom);
				strSQLS = strSQLS + strTemp;
				strTemp.Format(" and Address=%d", iAddress);
				strSQLS = strSQLS + strTemp;

				//新增加
				strSQLI="";
	    		strSQLI = strSQLI + "insert into InverterData_Curr"; 
				strSQLI = strSQLI + " (IID,";  				
				strSQLI = strSQLI + " ClltIDDay,"; 	
				strSQLI = strSQLI + " ClltDaytime,"; 	
				strSQLI = strSQLI + " DeviceIndex,";
				strSQLI = strSQLI + " DayTimes,"; 				
				strSQLI = strSQLI + " RegionID,"; 
				strSQLI = strSQLI + " ProjectID,"; 
				strSQLI = strSQLI + " SectionID,"; 
				strSQLI = strSQLI + " CollectID,"; 
				strSQLI = strSQLI + " ComID,"; 
				strSQLI = strSQLI + " Address,"; 						
				
				strSQLI = strSQLI + " SN,"; 
				strSQLI = strSQLI + " StatusInt,"; 
				strSQLI = strSQLI + " StatusDesc,"; 
					
				strSQLI = strSQLI + " PVVoltage,"; 
				strSQLI = strSQLI + " PVCurrent,"; 
				strSQLI = strSQLI + " PVOutputPower,"; 
				strSQLI = strSQLI + " PVVoltageB,"; 
				strSQLI = strSQLI + " PVCurrentB,"; 
				strSQLI = strSQLI + " PVOutputPowerB,"; 
				strSQLI = strSQLI + " PVVoltageC,"; 
				strSQLI = strSQLI + " PVCurrentC,"; 
				strSQLI = strSQLI + " PVOutputPowerC,";        
				strSQLI = strSQLI + " PVTotalOutputPower,"; 
				               
				strSQLI = strSQLI + " VoltageA,"; 
				strSQLI = strSQLI + " CurrentA,"; 
				strSQLI = strSQLI + " FrequencyA,"; 
				strSQLI = strSQLI + " OutputPowerA,"; 
				strSQLI = strSQLI + " ROutputPowerA,"; 
				strSQLI = strSQLI + " PowerFactorA,"; 
				strSQLI = strSQLI + " VoltageB,"; 
				strSQLI = strSQLI + " CurrentB,";      
				strSQLI = strSQLI + " FrequencyB,"; 
				strSQLI = strSQLI + " OutputPowerB,"; 
				strSQLI = strSQLI + " ROutputPowerB,"; 
				strSQLI = strSQLI + " PowerFactorB,"; 
				strSQLI = strSQLI + " VoltageC,"; 
				strSQLI = strSQLI + " CurrentC,";    
				strSQLI = strSQLI + " FrequencyC,"; 
				strSQLI = strSQLI + " OutputPowerC,"; 
				strSQLI = strSQLI + " ROutputPowerC,";      
				strSQLI = strSQLI + " PowerFactorC,"; 
				        
				strSQLI = strSQLI + " TotalOutputPower,"; 
				strSQLI = strSQLI + " RTotalOutputPower,"; 
				strSQLI = strSQLI + " TotalPowerFactor,";           
				        
				strSQLI = strSQLI + " TodayEnergy,"; 
				strSQLI = strSQLI + " TotalEnergy,"; 
				        
				strSQLI = strSQLI + " NTodayEnergy,"; 
				strSQLI = strSQLI + " NTotalEnergy,"; 
				        
				strSQLI = strSQLI + " RTodayEnergy,"; 
				strSQLI = strSQLI + " RTotalEnergy,"; 
				        
				strSQLI = strSQLI + " NRTodayEnergy,"; 
				strSQLI = strSQLI + " NRTotalEnergy,";    
				        
				strSQLI = strSQLI + " BtryVoltage,"; 
				strSQLI = strSQLI + " BtryCurrent,"; 
				strSQLI = strSQLI + " BtryOutputPower,";  
				        
				strSQLI = strSQLI + " TemperatureInverter,"; 
				strSQLI = strSQLI + " TemperatureBooster,"; 
				strSQLI = strSQLI + " TemperatureSink,";       
				        
				strSQLI = strSQLI + " TodayHour,"; 
				strSQLI = strSQLI + " TotalHour,"; 
				        
				strSQLI = strSQLI + " AGCSetting,"; 
				strSQLI = strSQLI + " AVCSetting,"; 
				        
				strSQLI = strSQLI + " GroundResistance,"; 
				strSQLI = strSQLI + " LeakCurrent,"; 
				       
				strSQLI = strSQLI + " StringVoltage1,"; 
				strSQLI = strSQLI + " StringCurrent1,";         
				strSQLI = strSQLI + " StringVoltage2,"; 
				strSQLI = strSQLI + " StringCurrent2,"; 
				strSQLI = strSQLI + " StringVoltage3,"; 
				strSQLI = strSQLI + " StringCurrent3,"; 
				strSQLI = strSQLI + " StringVoltage4,"; 
				strSQLI = strSQLI + " StringCurrent4,"; 
				strSQLI = strSQLI + " StringVoltage5,"; 
				strSQLI = strSQLI + " StringCurrent5,"; 
				strSQLI = strSQLI + " StringVoltage6,"; 
				strSQLI = strSQLI + " StringCurrent6,"; 
				strSQLI = strSQLI + " StringVoltage7,"; 
				strSQLI = strSQLI + " StringCurrent7,"; 
				strSQLI = strSQLI + " StringVoltage8,"; 
				strSQLI = strSQLI + " StringCurrent8,"; 
				        
				strSQLI = strSQLI + " StringVoltage9,"; 
				strSQLI = strSQLI + " StringCurrent9,"; 
				strSQLI = strSQLI + " StringVoltage10,"; 
				strSQLI = strSQLI + " StringCurrent10,"; 
				strSQLI = strSQLI + " StringVoltage11,"; 
				strSQLI = strSQLI + " StringCurrent11,"; 
				strSQLI = strSQLI + " StringVoltage12,"; 
				strSQLI = strSQLI + " StringCurrent12,"; 
				strSQLI = strSQLI + " StringVoltage13,"; 
				strSQLI = strSQLI + " StringCurrent13,"; 
				strSQLI = strSQLI + " StringVoltage14,"; 
				strSQLI = strSQLI + " StringCurrent14,"; 
				strSQLI = strSQLI + " StringVoltage15,"; 
				strSQLI = strSQLI + " StringCurrent15,"; 
				strSQLI = strSQLI + " StringVoltage16,"; 
				strSQLI = strSQLI + " StringCurrent16,"; 				
				strSQLI = strSQLI + " StringVoltage17,"; 
				strSQLI = strSQLI + " StringCurrent17,"; 
				strSQLI = strSQLI + " StringVoltage18,"; 
				strSQLI = strSQLI + " StringCurrent18,"; 
				strSQLI = strSQLI + " StringVoltage19,"; 
				strSQLI = strSQLI + " StringCurrent19,"; 
				strSQLI = strSQLI + " StringVoltage20,"; 
				strSQLI = strSQLI + " StringCurrent20,"; 
				strSQLI = strSQLI + " StringVoltage21,"; 
				strSQLI = strSQLI + " StringCurrent21,"; 
				strSQLI = strSQLI + " StringVoltage22,"; 
				strSQLI = strSQLI + " StringCurrent22,"; 
				strSQLI = strSQLI + " StringVoltage23,"; 
				strSQLI = strSQLI + " StringCurrent23,"; 
				strSQLI = strSQLI + " StringVoltage24,"; 
				strSQLI = strSQLI + " StringCurrent24,"; 

				strSQLI = strSQLI + " MotherLineVoltage,"; 
				        
				strSQLI = strSQLI + " GroudVoltage,"; 
				strSQLI = strSQLI + " NGroudVoltage,"; 
				        
				strSQLI = strSQLI + " DesignPower,"; 
				        
				strSQLI = strSQLI + " VoltageAB,"; 
				strSQLI = strSQLI + " VoltageBC,"; 
				strSQLI = strSQLI + " VoltageCA,";   
				        
				strSQLI = strSQLI + " SystemTime,"; 
				strSQLI = strSQLI + " Efficiency,"; 		
				strSQLI = strSQLI + " PowerOnSecond,"; 	
				strSQLI = strSQLI + " PowerOffSecond,"; 
				strSQLI = strSQLI + " MPPT1,"; 
				strSQLI = strSQLI + " MPPT2,"; 
				strSQLI = strSQLI + " MPPT3,"; 
				strSQLI = strSQLI + " MPPT4)"; 
		
			    
				//一行记录    
	    		strLine.Format(" values (%I64d,%d,'%s',%d,%d,%d,%d,%d,%d,%d,%d,'%s',%d,'%s',%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%d,%d,%d,%d,%.2f,%.2f,%.2f,%.2f)",
	    	        qwMaxID,
	    	        iTempDay,	
					(strDay + " " + strTime),
	    	        iDeviceIndex,	

	    	        iDayTimes, 
	    	        	
	    	        iRegionID, 
					iProjectID, 
					iSectionID, 
					iCollectID, 
					iCom, 
					iAddress, 
						    			
	    			strSN,
	    			iStatus,    		 
	    			strStatus,
	    			    	
	    			fltPVVoltage,
				    fltPVCurrent,
				    fltPVOutputPower/1000,
				    fltPVVoltageB,
				    fltPVCurrentB,
				    fltPVOutputPowerB/1000,
				    fltPVVoltageC,
				    fltPVCurrentC,
				    fltPVOutputPowerC/1000,       
				    fltPVTotalOutputPower/1000,
				               
				    fltVoltage,
				    fltCurrent,     
				    fltFrequency, 
				    fltOutputPower/1000, 
				    fltROutputPower/1000,
				    fltPowerFactor,
				    fltVoltageB,
				    fltCurrentB,     
				    fltFrequencyB,
				    fltOutputPowerB/1000,
				    fltROutputPowerB/1000,
				    fltPowerFactorB,
				    fltVoltageC,
				    fltCurrentC,       
				    fltFrequencyC,
				    fltOutputPowerC/1000,
				    fltROutputPowerC/1000,        
				    fltPowerFactorC,
				        
				    fltTotalOutputPower/1000,
				    fltRTotalOutputPower/1000,
				    fltTotalPowerFactor,       
				        
				    fltTodayEnergy/1000,
				    fltTotalEnergy/1000,
				        
				    fltNTodayEnergy/1000,
				    fltNTotalEnergy/1000,
				        
				    fltRTodayEnergy/1000,
				    fltRTotalEnergy/1000,
				        
				    fltNRTodayEnergy/1000,
				    fltNRTotalEnergy/1000,      
				        
				    fltBtryVoltage,
				    fltBtryCurrent,
				    fltBtryOutputPower/1000, 
				        
				    fltTemperatureInverter, 
				    fltTemperatureBooster,
				    fltTemperatureSink,      
				        
				    fltTodayHour,
				    fltTotalHour,
				        
				    fltAGCSetting,
				    fltAVCSetting,
				        
				    fltGroundResistance,
				    fltLeakCurrent,
				                
				    fltStringVoltage1,
				    fltStringCurrent1,    
				    fltStringVoltage2,
				    fltStringCurrent2,
				    fltStringVoltage3,
				    fltStringCurrent3,
				    fltStringVoltage4,
				    fltStringCurrent4,
				    fltStringVoltage5,
				    fltStringCurrent5,
				    fltStringVoltage6,
				    fltStringCurrent6,
				    fltStringVoltage7,
				    fltStringCurrent7,
				    fltStringVoltage8,
				    fltStringCurrent8,               
				        
				    fltStringVoltage9,
				    fltStringCurrent9,
				    fltStringVoltage10,
				    fltStringCurrent10,
				    fltStringVoltage11,
				    fltStringCurrent11,
				    fltStringVoltage12,
				    fltStringCurrent12,
				    fltStringVoltage13,
				    fltStringCurrent13,
				    fltStringVoltage14,
				    fltStringCurrent14,
				    fltStringVoltage15,
				    fltStringCurrent15,
				    fltStringVoltage16,
				    fltStringCurrent16,
				    
					fltStringVoltage17,
				    fltStringCurrent17,
					fltStringVoltage18,
				    fltStringCurrent18,
					fltStringVoltage19,
				    fltStringCurrent19,
					fltStringVoltage20,
				    fltStringCurrent20,
					fltStringVoltage21,
				    fltStringCurrent21,
					fltStringVoltage22,
				    fltStringCurrent22,
					fltStringVoltage23,
				    fltStringCurrent23,
					fltStringVoltage24,
				    fltStringCurrent24,

				    fltMotherLineVoltage,
				        
				    fltGroudVoltage,
				    fltNGroudVoltage,
				        
				    fltDesignPower/1000,
				        
				    fltVoltageAB,
				    fltVoltageBC,
				    fltVoltageCA,      
				        
				    iSystemTime,
					iEfficiency,	
					iPowerOnSecond,	
					iPowerOffSecond,
					fltMPPT1/1000,
					fltMPPT2/1000,
					fltMPPT3/1000,
					fltMPPT4/1000
	    			    );	   
	    			    
	    		strSQLI = strSQLI + strLine;		    			
	    		
	    		
	    		//更新 InverterData_Curr  
				strSQLU="";
				strSQLU = strSQLU + "Update InverterData_Curr set "; 
				strTemp.Format(" ClltIDDay=%d", iTempDay); 
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,ClltDaytime='%s'", (strDay + " " + strTime));
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,DayTimes=%d", iDayTimes); 
				strSQLU = strSQLU + strTemp;			
				strTemp.Format(" ,RegionID=%d", iRegionID); 
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,ProjectID=%d", iProjectID); 
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,SectionID=%d", iSectionID); 
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,CollectID=%d", iCollectID); 
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,ComID=%d", iCom); 
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,Address=%d", iAddress); 				
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,SN='%s'", strSN);
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,StatusInt=%d", iStatus); 
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,StatusDesc='%s'", strStatus);
				strSQLU = strSQLU + strTemp;	
				strTemp.Format(" ,PVVoltage=%f", fltPVVoltage);
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,PVCurrent=%f", fltPVCurrent);
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,PVOutputPower=%f", fltPVOutputPower/1000);
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,PVVoltageB=%f", fltPVVoltageB);
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,PVCurrentB=%f", fltPVCurrentB);
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,PVOutputPowerB=%f", fltPVOutputPowerB/1000);
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,PVVoltageC=%f", fltPVVoltageC);
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,PVCurrentC=%f", fltPVCurrentC);
				strSQLU = strSQLU + strTemp; 
				strTemp.Format(" ,PVOutputPowerC=%f", fltPVOutputPowerC/1000);
				strSQLU = strSQLU + strTemp;       
				strTemp.Format(" ,PVTotalOutputPower=%f", fltPVTotalOutputPower/1000);
				strSQLU = strSQLU + strTemp;               
				strTemp.Format(" ,VoltageA=%f", fltVoltage);
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,CurrentA=%f", fltCurrent);
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,FrequencyA=%f", fltFrequency);
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,OutputPowerA=%f", fltOutputPower/1000);
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,ROutputPowerA=%f", fltROutputPower/1000);
				strSQLU = strSQLU + strTemp; 
				strTemp.Format(" ,PowerFactorA=%f", fltPowerFactor);
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,VoltageB=%f", fltVoltageB);
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,CurrentB=%f", fltCurrentB);
				strSQLU = strSQLU + strTemp;     
				strTemp.Format(" ,FrequencyB=%f", fltFrequencyB);
				strSQLU = strSQLU + strTemp; 
				strTemp.Format(" ,OutputPowerB=%f", fltOutputPowerB/1000);
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,ROutputPowerB=%f", fltROutputPowerB/1000);
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,PowerFactorB=%f", fltPowerFactorB);
				strSQLU = strSQLU + strTemp; 
				strTemp.Format(" ,VoltageC=%f", fltVoltageC);
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,CurrentC=%f", fltCurrentC);
				strSQLU = strSQLU + strTemp;  
				strTemp.Format(" ,FrequencyC=%f", fltFrequencyC);
				strSQLU = strSQLU + strTemp; 
				strTemp.Format(" ,OutputPowerC=%f", fltOutputPowerC/1000);
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,ROutputPowerC=%f", fltROutputPowerC/1000);
				strSQLU = strSQLU + strTemp;      
				strTemp.Format(" ,PowerFactorC=%f", fltPowerFactorC);
				strSQLU = strSQLU + strTemp;        
				strTemp.Format(" ,TotalOutputPower=%f", fltTotalOutputPower/1000);
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,RTotalOutputPower=%f", fltRTotalOutputPower/1000);
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,TotalPowerFactor=%f", fltTotalPowerFactor);      
				strSQLU = strSQLU + strTemp;        
				strTemp.Format(" ,TodayEnergy=%f", fltTodayEnergy/1000);
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,TotalEnergy=%f", fltTotalEnergy/1000);
				strSQLU = strSQLU + strTemp;				        
				strTemp.Format(" ,NTodayEnergy=%f", fltNTodayEnergy/1000);
				strSQLU = strSQLU + strTemp; 
				strTemp.Format(" ,NTotalEnergy=%f", fltNTotalEnergy/1000);
				strSQLU = strSQLU + strTemp;        
				strTemp.Format(" ,RTodayEnergy=%f", fltRTodayEnergy/1000);
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,RTotalEnergy=%f", fltRTotalEnergy/1000);
				strSQLU = strSQLU + strTemp;        
				strTemp.Format(" ,NRTodayEnergy=%f", fltNRTodayEnergy/1000);
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,NRTotalEnergy=%f", fltNRTotalEnergy/1000);
				strSQLU = strSQLU + strTemp;        
				strTemp.Format(" ,BtryVoltage=%f", fltBtryVoltage);
				strSQLU = strSQLU + strTemp; 
				strTemp.Format(" ,BtryCurrent=%f", fltBtryCurrent);
				strSQLU = strSQLU + strTemp; 
				strTemp.Format(" ,BtryOutputPower=%f", fltBtryOutputPower/1000); 
				strSQLU = strSQLU + strTemp;        
				strTemp.Format(" ,TemperatureInverter=%f", fltTemperatureInverter);
				strSQLU = strSQLU + strTemp; 
				strTemp.Format(" ,TemperatureBooster=%f", fltTemperatureBooster);
				strSQLU = strSQLU + strTemp; 
				strTemp.Format(" ,TemperatureSink=%f", fltTemperatureSink);    
				strSQLU = strSQLU + strTemp;        
				strTemp.Format(" ,TodayHour=%f", fltTodayHour);
				strSQLU = strSQLU + strTemp; 
				strTemp.Format(" ,TotalHour=%f", fltTotalHour);
				strSQLU = strSQLU + strTemp;        
				strTemp.Format(" ,AGCSetting=%f", fltAGCSetting);
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,AVCSetting=%f", fltAVCSetting);
				strSQLU = strSQLU + strTemp;        
				strTemp.Format(" ,GroundResistance=%f", fltGroundResistance);
				strSQLU = strSQLU + strTemp; 
				strTemp.Format(" ,LeakCurrent=%f", fltLeakCurrent);
				strSQLU = strSQLU + strTemp;       
				strTemp.Format(" ,StringVoltage1=%f", fltStringVoltage1);
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,StringCurrent1=%f", fltStringCurrent1);
				strSQLU = strSQLU + strTemp;       
				strTemp.Format(" ,StringVoltage2=%f", fltStringVoltage2);
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,StringCurrent2=%f", fltStringCurrent2);
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,StringVoltage3=%f", fltStringVoltage3);
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,StringCurrent3=%f", fltStringCurrent3);
				strSQLU = strSQLU + strTemp; 
				strTemp.Format(" ,StringVoltage4=%f", fltStringVoltage4); 
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,StringCurrent4=%f", fltStringCurrent4);
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,StringVoltage5=%f", fltStringVoltage5); 
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,StringCurrent5=%f", fltStringCurrent5);
				strSQLU = strSQLU + strTemp; 
				strTemp.Format(" ,StringVoltage6=%f", fltStringVoltage6);
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,StringCurrent6=%f", fltStringCurrent6);
				strSQLU = strSQLU + strTemp; 
				strTemp.Format(" ,StringVoltage7=%f", fltStringVoltage7);
				strSQLU = strSQLU + strTemp; 
				strTemp.Format(" ,StringCurrent7=%f", fltStringCurrent7);
				strSQLU = strSQLU + strTemp; 
				strTemp.Format(" ,StringVoltage8=%f", fltStringVoltage8);
				strSQLU = strSQLU + strTemp; 
				strTemp.Format(" ,StringCurrent8=%f", fltStringCurrent8);
				strSQLU = strSQLU + strTemp;       
				strTemp.Format(" ,StringVoltage9=%f", fltStringVoltage9);
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,StringCurrent9=%f", fltStringCurrent9);
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,StringVoltage10=%f", fltStringVoltage10);
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,StringCurrent10=%f", fltStringCurrent10);
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,StringVoltage11=%f", fltStringVoltage11);
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,StringCurrent11=%f", fltStringCurrent11);
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,StringVoltage12=%f", fltStringVoltage12);
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,StringCurrent12=%f", fltStringCurrent12);
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,StringVoltage13=%f", fltStringVoltage13);
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,StringCurrent13=%f", fltStringCurrent13);
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,StringVoltage14=%f", fltStringVoltage14);
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,StringCurrent14=%f", fltStringCurrent14);
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,StringVoltage15=%f", fltStringVoltage15);
				strSQLU = strSQLU + strTemp; 
				strTemp.Format(" ,StringCurrent15=%f", fltStringCurrent15);
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,StringVoltage16=%f", fltStringVoltage16);
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,StringCurrent16=%f", fltStringCurrent16);
				strSQLU = strSQLU + strTemp; 
				strTemp.Format(" ,StringVoltage17=%f", fltStringVoltage17);
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,StringCurrent17=%f", fltStringCurrent17);
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,StringVoltage18=%f", fltStringVoltage18);
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,StringCurrent18=%f", fltStringCurrent18);
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,StringVoltage19=%f", fltStringVoltage19);
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,StringCurrent19=%f", fltStringCurrent19);
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,StringVoltage20=%f", fltStringVoltage20);
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,StringCurrent20=%f", fltStringCurrent20);
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,StringVoltage21=%f", fltStringVoltage21);
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,StringCurrent21=%f", fltStringCurrent21);
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,StringVoltage22=%f", fltStringVoltage22);
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,StringCurrent22=%f", fltStringCurrent22);
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,StringVoltage23=%f", fltStringVoltage23);
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,StringCurrent23=%f", fltStringCurrent23);
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,StringVoltage24=%f", fltStringVoltage24);
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,StringCurrent24=%f", fltStringCurrent24);
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,MotherLineVoltage=%f", fltMotherLineVoltage);
				strSQLU = strSQLU + strTemp;       
				strTemp.Format(" ,GroudVoltage=%f", fltGroudVoltage);
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,NGroudVoltage=%f", fltNGroudVoltage);
				strSQLU = strSQLU + strTemp;        
				strTemp.Format(" ,DesignPower=%f", fltDesignPower/1000);
				strSQLU = strSQLU + strTemp;        
				strTemp.Format(" ,VoltageAB=%f", fltVoltageAB);
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,VoltageBC=%f", fltVoltageBC);
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,VoltageCA=%f", fltVoltageCA); 
				strSQLU = strSQLU + strTemp;        
				strTemp.Format(" ,SystemTime=%d", iSystemTime); 
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,Efficiency=%d", iEfficiency); 
				strSQLU = strSQLU + strTemp;		
				strTemp.Format(" ,PowerOnSecond=%d", iPowerOnSecond); 
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,PowerOffSecond=%d", iPowerOffSecond); 
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,MPPT1=%f", fltMPPT1/1000);
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,MPPT2=%f", fltMPPT2/1000);
				strSQLU = strSQLU + strTemp; 
				strTemp.Format(" ,MPPT3=%f", fltMPPT3/1000);
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,MPPT4=%f", fltMPPT4/1000);
				strSQLU = strSQLU + strTemp; 

				strSQLU = strSQLU + " where "; 	
				strTemp.Format(" ProjectID=%d", iProjectID);
				strSQLU = strSQLU + strTemp; 
				strTemp.Format(" and ComID=%d", iCom);
				strSQLU = strSQLU + strTemp; 
				strTemp.Format(" and Address=%d", iAddress);
				strSQLU = strSQLU + strTemp; 

				
				//不存在，新增, 存在 则更新
				strSQL = "if NOT exists(" + strSQLS + ") " + strSQLI + " else " + strSQLU;
									
				if (0==strDatabaseType.Compare("sqlserver")) 
				{
					g_pConnection->Execute(_bstr_t(strSQL),&vtOptional,-1);	
				}
				else if (0==strDatabaseType.Compare("mysql")) 
				{
					//查询成功，返回0 
					if(0==mysql_query(&g_Mysql,(char*)(LPCTSTR)strSQLS))
					{   
						result = mysql_store_result(&g_Mysql);	 
						count_res = mysql_num_rows(result);		
						if (count_res>0)
						{
							//update
							res = mysql_query(&g_Mysql,(char*)(LPCTSTR)strSQLU);
						}
						else
						{
							//insert
							res = mysql_query(&g_Mysql,(char*)(LPCTSTR)strSQLI);
						}

						if(NULL!=result) 
						{
							mysql_free_result(result); //释放结果资源
						}

					}	
					else
					{
						g_blnDBIsError = TRUE; 
					}							
				}	
													
				iCount++;
													
					
													
				
				//表 InverterData_2019
				//判断是否存在  				
				strSQLS="";
    			strSQLS = strSQLS + "select ClltIDDay, ProjectID, ComID, Address, DayTimes from InverterData_";  
				//strSQLS = strSQLS + strYear; 
				strSQLS = strSQLS + "CurrYear"; 
				strSQLS = strSQLS + " where "; 				
				strTemp.Format(" ClltIDDay=%d", iTempDay);
				strSQLS = strSQLS + strTemp; 				
				strTemp.Format(" and ProjectID=%d", iProjectID);
				strSQLS = strSQLS + strTemp;  				
				strTemp.Format(" and ComID=%d", iCom);
				strSQLS = strSQLS + strTemp;	
				strTemp.Format(" and Address=%d", iAddress);
				strSQLS = strSQLS + strTemp;
				strTemp.Format(" and DayTimes=%d", iDayTimes);
				strSQLS = strSQLS + strTemp; 
				
				//新增加
				//Data_Curr -> Data_2019
				
				//strTT = "Data_" + strYear; 
				strTT = "Data_CurrYear";
				strSQLI.Replace("Data_Curr", strTT);
					
					    			
    			//不存在 则增加	
    			strSQL = "if NOT exists(" + strSQLS + ") " + strSQLI;
    			
				if (0==strDatabaseType.Compare("sqlserver")) 
				{
					g_pConnection->Execute(_bstr_t(strSQL),&vtOptional,-1);	
				}
				else if (0==strDatabaseType.Compare("mysql")) 
				{
					//查询成功，返回0 
					if(0==mysql_query(&g_Mysql,(char*)(LPCTSTR)strSQLS))
					{   
						result = mysql_store_result(&g_Mysql);	 
						count_res = mysql_num_rows(result);		
						if (count_res>0)
						{
							//update
							res = mysql_query(&g_Mysql,(char*)(LPCTSTR)strSQLU);
						}
						else
						{
							//insert
							res = mysql_query(&g_Mysql,(char*)(LPCTSTR)strSQLI);
						}

						if(NULL!=result) 
						{
							mysql_free_result(result); //释放结果资源
						}

					}	
					else
					{
						g_blnDBIsError = TRUE; 
					}							
				}	
				
				iCount++;
					
				
				    			
    			//表 InverterRpt_2019
				//判断是否存在  				
				strSQLS="";
    			strSQLS = strSQLS + "select ClltIDDay, ProjectID, ComID, Address from InverterRpt_";  
				//strSQLS = strSQLS + strYear; 
				strSQLS = strSQLS + "CurrYear"; 
				strSQLS = strSQLS + " where "; 				
				strTemp.Format(" ClltIDDay=%d", iTempDay);
				strSQLS = strSQLS + strTemp; 				
				strTemp.Format(" and ProjectID=%d", iProjectID);
				strSQLS = strSQLS + strTemp;  				
				strTemp.Format(" and ComID=%d", iCom);
				strSQLS = strSQLS + strTemp;
				strTemp.Format(" and Address=%d", iAddress);
				strSQLS = strSQLS + strTemp;
				
				//新增
				strSQLI="";
		 		strSQLI = strSQLI + "insert into InverterRpt_"; 
		 		//strSQLI = strSQLI + strYear; 
				strSQLI = strSQLI + "CurrYear";
				strSQLI = strSQLI + " (IID,";  				
				strSQLI = strSQLI + " ClltIDDay,"; 		
				strSQLI = strSQLI + " DeviceIndex,"; 
				strSQLI = strSQLI + " ClltIDYear,";	
				strSQLI = strSQLI + " ClltIDMonth,";	
				strSQLI = strSQLI + " ClltIDDayIndex,";  
				strSQLI = strSQLI + " RegionID,"; 
				strSQLI = strSQLI + " ProjectID,"; 
				strSQLI = strSQLI + " SectionID,"; 
				strSQLI = strSQLI + " CollectID,"; 
				strSQLI = strSQLI + " ComID,"; 
				strSQLI = strSQLI + " Address,";					
				strSQLI = strSQLI + " TodayEnergy,";	
				strSQLI = strSQLI + " TotalEnergy)";

				//一行记录    
		 		strLine.Format(" values (%I64d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%f,%f)",
		 	        qwMaxID,
		 	        iTempDay,	
		 	        iDeviceIndex,
		 	        iYear,	
		 	        iMonth,	
		 	        iDay,	
		 	        iRegionID, 
					iProjectID, 
					iSectionID, 
					iCollectID, 
					iCom, 
					iAddress, 
				    fltTodayEnergy/1000,
				    fltTotalEnergy/1000
		 			    );	  
		 			
		 		strSQLI = strSQLI + strLine;	

				
				//更新 InverterRpt_2019  
				strSQLU="";
		 		strSQLU = strSQLU + "Update InverterRpt_"; 	
		 		//strSQLU = strSQLU + strYear; 	 
				strSQLU = strSQLU + "CurrYear";	 
		 		strSQLU = strSQLU + " set"; 	
				strTemp.Format(" TodayEnergy=%f", fltTodayEnergy/1000);
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" ,TotalEnergy=%f", fltTotalEnergy/1000);
				strSQLU = strSQLU + strTemp;	
					   			
				strSQLU = strSQLU + " where "; 				
				strTemp.Format(" ClltIDDay=%d", iTempDay);
				strSQLU = strSQLU + strTemp; 				
				strTemp.Format(" and ProjectID=%d", iProjectID);
				strSQLU = strSQLU + strTemp;  				
				strTemp.Format(" and ComID=%d", iCom);
				strSQLU = strSQLU + strTemp;
				strTemp.Format(" and Address=%d", iAddress);
				strSQLU = strSQLU + strTemp;
				

				//不存在，新增, 存在 则更新
				strSQL = "if NOT exists(" + strSQLS + ") " + strSQLI + " else " + strSQLU;

				if (0==strDatabaseType.Compare("sqlserver")) 
				{
					g_pConnection->Execute(_bstr_t(strSQL),&vtOptional,-1);	
				}
				else if (0==strDatabaseType.Compare("mysql")) 
				{
					//查询成功，返回0 
					if(0==mysql_query(&g_Mysql,(char*)(LPCTSTR)strSQLS))
					{   
						result = mysql_store_result(&g_Mysql);	 
						count_res = mysql_num_rows(result);		
						if (count_res>0)
						{
							//update
							res = mysql_query(&g_Mysql,(char*)(LPCTSTR)strSQLU);
						}
						else
						{
							//insert
							res = mysql_query(&g_Mysql,(char*)(LPCTSTR)strSQLI);
						}

						if(NULL!=result) 
						{
							mysql_free_result(result); //释放结果资源
						}

					}	
					else
					{
						g_blnDBIsError = TRUE; 
					}							
				}			

				iCount++;
				
				
				//更新设备状态
				WriteDeviceStatustoDatabase_vid(strDay, strTime, iRegionID, iProjectID, iSectionID,iCollectID,iCom,iAddress,iDayTimes, iStatus,strStatus, fltTotalOutputPower/1000, fltTodayEnergy/1000, fltNTodayEnergy/1000, 0.0, 0.0, 0.0);


    		} //if (TRUE==pInv->blnIsFinish && FALSE==pInv->blnIsDBSave)
    		
    		//下一节点
			//qInv = pInv->next;
			//pInv = qInv;			
		}//while (NULL!=pInv)	
		
		
		//结束事务
        //pConnection->CommitTrans();
        
		//执行commit，手动提交事务
		ret = mysql_query(&g_Mysql, "COMMIT"); //提交

		strMsg1.Format("WriteInvRealDatatoDatabase_vid 写数据库成功: %d",  iCount);
		strMsg2.Format("WriteInvRealDatatoDatabase_vid Write Database Successfull %d",  iCount);
		strMsg = G_GetMsgLangFormat(strMsg1, strMsg2);
		g_objMsg.CMSG_SetMsgQue_vid(strMsg,_COLOR_BLACK, -1, -1, -1, -1, -1, -1, enuLogDatabaseAction);	
		if (NULL!=pMainFrame)
		{
			pMainFrame->PostMessage(_MSG_ID_OUTPUTDYM_I,0,0);
		}

	}//try
    catch (_com_error &e)                                           
    {         
        //回滚事务
        //pConnection->RollbackTrans();
        
		//执行rollback，回滚事务
		int ret = mysql_query(&g_Mysql, "ROLLBACK");			
		if (0!=ret) 
		{
			strTemp = mysql_error(&g_Mysql);

		}

		g_blnDBIsError = TRUE;

		strTemp = e.ErrorMessage();

		strMsg1.Format("WriteInvRealDatatoDatabase_vid 数据库操作错误: %s",  strTemp);
		strMsg2.Format("WriteInvRealDatatoDatabase_vid Database error: %s",  strTemp);
		strMsg = G_GetMsgLangFormat(strMsg1, strMsg2);
		g_objMsg.CMSG_SetMsgQue_vid(strMsg,_COLOR_BLACK, -1, -1, -1, -1, -1, -1, enuLogDatabase);	
		if (NULL!=pMainFrame)
		{
			pMainFrame->PostMessage(_MSG_ID_OUTPUTDYM_I,0,0);
		}
		
    }//catch (_com_error &e)  
	
	//临界区解锁
	//g_crtsecDataBase.Unlock();
			
	return;		
}


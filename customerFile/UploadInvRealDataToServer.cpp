



//发送数据到M2W Platform服务器
//发送数据前，必须发送采集器编号=MachineID
//包格式
//0x11 0x12 type Len0 Len2 Len3 Len4 content CRCL CRCH 0x19 0x18
//type 逆变器 电表      	    
BOOL CCollectData::UploadInvRealDataToServer(STRC_M2WSERVER *pM2WMonitor)
{
    CString strMsg1 = "";
	CString strMsg2 = "";
	CString strMsg = "";

	STRC_COMDEV 	*p=NULL;
	STRC_COMDEV 	*q=NULL; 
	STRC_COMLINKDEV *pComLink=NULL;
	STRC_COMLINKDEV *qComLink=NULL;
	CString 		strLinkDevID="";
	int             iDayTimes=0;
	int				iDevCount=0;
	int				iRunDev=0;
	int				iCom=-1;
	int				iAddress=-1;
	int				iProjectID=-1; // 项目ID
	int             iSectionID=-1; // 区域ID
	int				iCollectID=-1; //采集器ID
	int				iComCount=0;
	BOOL			blnNewCom=FALSE;
	int             iTempStatus=-1;	

	int     	iCount=0;   
	BOOL		blnIsFinish=FALSE;
	int			iAlert=0;
	int         iStatus=-1;	
	char        chDataBuff[IOCP_BUFFERSIZE_I];
	int         iDataLen=0;	
	char        chBusiPack[IOCP_BUFFERSIZE_I];
	int         iBusiPackLen=0;
	int			iBusiType=0;
	
	int         iTemp=0;
	char        chTest[8];
	int         iLen=0;
	unsigned short  usCRC=0;
    unsigned char   chCRC[4];
    
	CString         strServerAddress="";
	UINT            uiServerPort=0;
	char            chServerIP[16];  	
	CString         strServerType="";
    CString         strServerUser="";
    CString         strServerPassword="";
    CString         strServerPublicKey="";
    CString         strServerPrivateKey="";
    CString strProjectNo="";
    CString strGatewayID="";
    CString strCollectNo="";
    
	CTime   	time;
	CString 	strDay = "";	
	CString 	strTime = ""; 
    CString     strAllOutputPower="";                
    CString     strTemperature="";
	CString     strTemp="";
	CString     strTT="";	
	int			i=0;
	int			j=0;
	int			m=0;
	int			n=0;
	float       fltTemp=0.0;
	CString     strLine="";
	CString     strValue="";
	BOOL        blnSend=FALSE;
	BOOL        blnIOCPSend=FALSE;
	BOOL        blnIOCPContinueSend=FALSE;
	int         iRt=0;
	int		iCloseRt = 0;
	BOOL    blnCRCOK=FALSE;
    //int     iInputCom=0;
    //int     iInputAddress=0;
    int     iTempProjectID=0;
    int     iTempCollectID=0;
    char    chTemp[254];
    CString strTempIP="";
    int     iTempPort=0;
    CString strTempCollectNo="";
    BOOL	blnFundData=FALSE;
    BOOL	blnFundProjectID=FALSE;
    int     iTempLen=0;
    
    STRC_COLLECTOR *pCollector=NULL;
    STRC_COLLECTOR *qCollector=NULL;
    
    STRC_DATAINV    *pInv=NULL;
	STRC_DATAINV    *qInv=NULL;
	STRC_DATARELAY    *pRemoteTest=NULL;
	STRC_DATARELAY    *qRemoteTest=NULL;
	STRC_DATARELAY    *pRemoteSignal=NULL;
	STRC_DATARELAY    *qRemoteSignal=NULL;
	STRC_DATARELAY    *pRemoteControl=NULL;
	STRC_DATARELAY    *qRemoteControl=NULL;
	STRC_DATARELAY    *pRemotePulse=NULL;
	STRC_DATARELAY    *qRemotePulse=NULL;
	
	STRC_DATAEMS   *pEMSTmpr=NULL;
	STRC_DATAEMS   *qEMSTmpr=NULL;
	STRC_DATAEMS   *pEMSPVTmpr=NULL;
	STRC_DATAEMS   *qEMSPVTmpr=NULL;
	STRC_DATAEMS   *pEMSIrrd=NULL;
	STRC_DATAEMS   *qEMSIrrd=NULL;
	STRC_DATAEMS   *pEMSWdSpd=NULL;
	STRC_DATAEMS   *qEMSWdSpd=NULL;
	STRC_DATAEMS   *pEMSWdDirect=NULL;
	STRC_DATAEMS   *qEMSWdDirect=NULL;
	STRC_DATATRANSFORM   *pTransform=NULL;
	STRC_DATATRANSFORM   *qTransform=NULL;
	STRC_DATAEPM	*pEPM=NULL;
	STRC_DATAEPM	*qEPM=NULL;
	STRC_DATAPVS	*pPVS=NULL;
	STRC_DATAPVS	*qPVS=NULL;
	STRC_DATADCCABINET	*pDCCabinet=NULL;
	STRC_DATADCCABINET	*qDCCabinet=NULL;
	STRC_DATAIO	*pIO=NULL;
	STRC_DATAIO	*qIO=NULL;
	STRC_DATALED    *pLED=NULL;
	STRC_DATALED    *qLED=NULL;
	STRC_DATAALERT  *pAlert=NULL;
	STRC_DATAALERT  *qAlert=NULL;	
	STRC_DATAEMS   *pHotMeter=NULL;
	STRC_DATAEMS   *qHotMeter=NULL;
    STRC_DATAEMS   *pWaterMeter=NULL;
	STRC_DATAEMS   *qWaterMeter=NULL;
	
	STRC_DATAINVOFFLINE    *pInvOffline=NULL;
	STRC_DATAINVOFFLINE    *qInvOffline=NULL;
	STRC_DATACHARGE    *pCharge=NULL;
	STRC_DATACHARGE    *qCharge=NULL;    
    
    STRC_DATACONTRARY    *pContrary=NULL;
	STRC_DATACONTRARY    *qContrary=NULL;
	
	int iTempIndex=0;
	
	BOOL blnTCPOK=FALSE;    
    int  iTCPCount=0;

	CComm_Comm		objCom_Com; 
	CString strData="";
	CSysTCPIPProtocol objTCPIP;
	
	
	//int iDayTimes=0;
    //CString strDay;	       				
    //CString strTime;	       			
    
    int iRegionID=0; 
	//int iProjectID; 
	//int iSectionID=0; 
	//int iCollectID; 
	//int iCom=0;
	//int iAddress=0;
						    			
	CString strSN="";
	//int iStatus=0;    		 
	CString strStatus="";
	    			    	
	float fltPVVoltage=0.0;
	float fltPVCurrent=0.0;
	float fltPVOutputPower=0.0;
	float fltPVVoltageB=0.0;
	float fltPVCurrentB=0.0;
	float fltPVOutputPowerB=0.0;
	float fltPVVoltageC=0.0;
	float fltPVCurrentC=0.0;
	float fltPVOutputPowerC=0.0;       
	float fltPVTotalOutputPower=0.0;
				               
	float fltVoltage=0.0;
	float fltCurrent=0.0;     
	float fltFrequency=0.0; 
	float fltOutputPower=0.0; 
	float fltROutputPower=0.0;
	float fltPowerFactor=0.0;
	float fltVoltageB=0.0;
	float fltCurrentB=0.0;     
	float fltFrequencyB=0.0;
	float fltOutputPowerB=0.0;
	float fltROutputPowerB=0.0;
	float fltPowerFactorB=0.0;
	float fltVoltageC=0.0;
	float fltCurrentC=0.0;       
	float fltFrequencyC=0.0;
	float fltOutputPowerC=0.0;
	float fltROutputPowerC=0.0;        
	float fltPowerFactorC=0.0;
				        
	float fltTotalOutputPower=0.0;
	float fltRTotalOutputPower=0.0;
	float fltTotalPowerFactor=0.0;       
				        
	float fltTodayEnergy=0.0;
	float fltTotalEnergy=0.0;
				        
	float fltNTodayEnergy=0.0;
	float fltNTotalEnergy=0.0;
				        
	float fltRTodayEnergy=0.0;
	float fltRTotalEnergy=0.0;
				        
	float fltNRTodayEnergy=0.0;
	float fltNRTotalEnergy=0.0;      
				        
	float fltBtryVoltage=0.0;
	float fltBtryCurrent=0.0;
	float fltBtryOutputPower=0.0; 
				        
	float fltTemperatureInverter=0.0; 
	float fltTemperatureBooster=0.0;
	float fltTemperatureSink=0.0;      
				        
	float fltTodayHour=0.0;
	float fltTotalHour=0.0;
				        
	float fltAGCSetting=0.0;
	float fltAVCSetting=0.0;
				        
	float fltGroundResistance=0.0;
	float fltLeakCurrent=0.0;
				                
	float fltStringVoltage1=0.0;
	float fltStringCurrent1=0.0;    
	float fltStringVoltage2=0.0;
	float fltStringCurrent2=0.0;
	float fltStringVoltage3=0.0;
	float fltStringCurrent3=0.0;
	float fltStringVoltage4=0.0;
	float fltStringCurrent4=0.0;
	float fltStringVoltage5=0.0;
	float fltStringCurrent5=0.0;
	float fltStringVoltage6=0.0;
	float fltStringCurrent6=0.0;
	float fltStringVoltage7=0.0;
	float fltStringCurrent7=0.0;
	float fltStringVoltage8=0.0;
	float fltStringCurrent8=0.0;               
				        
	float fltStringVoltage9=0.0;
	float fltStringCurrent9=0.0;
	float fltStringVoltage10=0.0;
	float fltStringCurrent10=0.0;
	float fltStringVoltage11=0.0;
	float fltStringCurrent11=0.0;
	float fltStringVoltage12=0.0;
	float fltStringCurrent12=0.0;
	float fltStringVoltage13=0.0;
	float fltStringCurrent13=0.0;
	float fltStringVoltage14=0.0;
	float fltStringCurrent14=0.0;
	float fltStringVoltage15=0.0;
	float fltStringCurrent15=0.0;
	float fltStringVoltage16=0.0;
	float fltStringCurrent16=0.0;				        
	float fltStringVoltage17=0.0;
	float fltStringCurrent17=0.0;
	float fltStringVoltage18=0.0;
	float fltStringCurrent18=0.0;
	float fltStringVoltage19=0.0;
	float fltStringCurrent19=0.0;
	float fltStringVoltage20=0.0;
	float fltStringCurrent20=0.0;
	float fltStringVoltage21=0.0;
	float fltStringCurrent21=0.0;
	float fltStringVoltage22=0.0;
	float fltStringCurrent22=0.0;
	float fltStringVoltage23=0.0;
	float fltStringCurrent23=0.0;
	float fltStringVoltage24=0.0;
	float fltStringCurrent24=0.0;

	float fltMotherLineVoltage=0.0;
				        
	float fltGroudVoltage=0.0;
	float fltNGroudVoltage=0.0;
				        
	float fltDesignPower=0.0;
				        
	float fltVoltageAB=0.0;
	float fltVoltageBC=0.0;
	float fltVoltageCA=0.0;      
				        
	int iSystemTime=0;
	int iEfficiency=0;	
	int iPowerOnSecond=0;	
	int iPowerOffSecond=0;
	float fltMPPT1=0.0;
	float fltMPPT2=0.0;
	float fltMPPT3=0.0;
	float fltMPPT4=0.0;


	//得到时间
   	time = CTime::GetCurrentTime();
 	strDay =  time.Format("%Y-%m-%d");        //2009-12-10
	strTime =  time.Format("%H:%M:%S");        //12:12:13 
	
   	//UploadRealW 配置
	strServerAddress = pM2WMonitor->strM2WUploadRealServerAddress;      
    uiServerPort = pM2WMonitor->iM2WUploadRealServerPort;    
    strServerUser = pM2WMonitor->strM2WServerUser;    
    strServerPassword = pM2WMonitor->strM2WServerPassword;    
    strServerPublicKey = pM2WMonitor->strM2WServerAESKey;    
    strServerPrivateKey = pM2WMonitor->strM2WServerAESIV;  
         
    
    //函数调用
	G_WriteFunctionCall(GetCurrentThreadId(), "UploadInvRealDataToServer");
	
    try
    {
        //首先得到主框架		
    	CMainFrame *pMainFrame=(CMainFrame *)AfxGetApp()->m_pMainWnd;	
       	
		//2. 逆变器
		pInv = m_pstrcInvHead;  
	 	while (NULL!=pInv)
	 	{
	   		if (TRUE==pInv->blnIsFinish && FALSE==pInv->blnIsUploadReal)
	   		{
				pInv->blnIsUploadReal = TRUE;

	   			iDayTimes=pInv->iDayTimes;
			    strDay=pInv->strDay;	       				
			    strTime=pInv->strTime;	       			
			    
			    iRegionID=pInv->iRegionID; 
				iProjectID=pInv->iProjectID; 
				iSectionID=pInv->iSectionID; 
				iCollectID=pInv->iCollectID; 
				iCom=pInv->iCom;
				iAddress=pInv->iAddress;
									    			
				strSN=pInv->strSN;
				iStatus=pInv->iStatus;    		 
				strStatus=pInv->strStatus;
	
	   			fltPVVoltage=pInv->fltPVVoltage;
				fltPVCurrent=pInv->fltPVCurrent;
				fltPVOutputPower=pInv->fltPVOutputPower;
				fltPVVoltageB=pInv->fltPVVoltageB;
				fltPVCurrentB=pInv->fltPVCurrentB;
				fltPVOutputPowerB=pInv->fltPVOutputPowerB;
				fltPVVoltageC=pInv->fltPVVoltageC;
				fltPVCurrentC=pInv->fltPVCurrentC;
				fltPVOutputPowerC=pInv->fltPVOutputPowerC;       
				fltPVTotalOutputPower=pInv->fltPVTotalOutputPower;
				               
				fltVoltage=pInv->fltVoltage;
				fltCurrent=pInv->fltCurrent;     
				fltFrequency=pInv->fltFrequency; 
				fltOutputPower=pInv->fltOutputPower; 
				fltROutputPower=pInv->fltROutputPower;
				fltPowerFactor=pInv->fltPowerFactor;
				fltVoltageB=pInv->fltVoltageB;
				fltCurrentB=pInv->fltCurrentB;     
				fltFrequencyB=pInv->fltFrequencyB;
				fltOutputPowerB=pInv->fltOutputPowerB;
				fltROutputPowerB=pInv->fltROutputPowerB;
				fltPowerFactorB=pInv->fltPowerFactorB;
				fltVoltageC=pInv->fltVoltageC;
				fltCurrentC=pInv->fltCurrentC;       
				fltFrequencyC=pInv->fltFrequencyC;
				fltOutputPowerC=pInv->fltOutputPowerC;
				fltROutputPowerC=pInv->fltROutputPowerC;        
				fltPowerFactorC=pInv->fltPowerFactorC;
				        
				fltTotalOutputPower=pInv->fltTotalOutputPower;
				fltRTotalOutputPower=pInv->fltRTotalOutputPower;
				fltTotalPowerFactor=pInv->fltTotalPowerFactor;       
				        
				fltTodayEnergy=pInv->fltTodayEnergy;
				fltTotalEnergy=pInv->fltTotalEnergy;
				        
				fltNTodayEnergy=pInv->fltNTodayEnergy;
				fltNTotalEnergy=pInv->fltNTotalEnergy;
				        
				fltRTodayEnergy=pInv->fltRTodayEnergy;
				fltRTotalEnergy=pInv->fltRTotalEnergy;
				        
				fltNRTodayEnergy=pInv->fltNRTodayEnergy;
				fltNRTotalEnergy=pInv->fltNRTotalEnergy;      
				        
				fltBtryVoltage=pInv->fltBtryVoltage;
				fltBtryCurrent=pInv->fltBtryCurrent;
				fltBtryOutputPower=pInv->fltBtryOutputPower; 
				        
				fltTemperatureInverter=pInv->fltTemperatureInverter; 
				fltTemperatureBooster=pInv->fltTemperatureBooster;
				fltTemperatureSink=pInv->fltTemperatureSink;      
				        
				fltTodayHour=pInv->fltTodayHour;
				fltTotalHour=pInv->fltTotalHour;
				        
				fltAGCSetting=pInv->fltAGCSetting;
				fltAVCSetting=pInv->fltAVCSetting;
				        
				fltGroundResistance=pInv->fltGroundResistance;
				fltLeakCurrent=pInv->fltLeakCurrent;
				                
				fltStringVoltage1=pInv->fltStringVoltage1;
				fltStringCurrent1=pInv->fltStringCurrent1;    
				fltStringVoltage2=pInv->fltStringVoltage2;
				fltStringCurrent2=pInv->fltStringCurrent2;
				fltStringVoltage3=pInv->fltStringVoltage3;
				fltStringCurrent3=pInv->fltStringCurrent3;
				fltStringVoltage4=pInv->fltStringVoltage4;
				fltStringCurrent4=pInv->fltStringCurrent4;
				fltStringVoltage5=pInv->fltStringVoltage5;
				fltStringCurrent5=pInv->fltStringCurrent5;
				fltStringVoltage6=pInv->fltStringVoltage6;
				fltStringCurrent6=pInv->fltStringCurrent6;
				fltStringVoltage7=pInv->fltStringVoltage7;
				fltStringCurrent7=pInv->fltStringCurrent7;
				fltStringVoltage8=pInv->fltStringVoltage8;
				fltStringCurrent8=pInv->fltStringCurrent8;               
				        
				fltStringVoltage9=pInv->fltStringVoltage9;
				fltStringCurrent9=pInv->fltStringCurrent9;
				fltStringVoltage10=pInv->fltStringVoltage10;
				fltStringCurrent10=pInv->fltStringCurrent10;
				fltStringVoltage11=pInv->fltStringVoltage11;
				fltStringCurrent11=pInv->fltStringCurrent11;
				fltStringVoltage12=pInv->fltStringVoltage12;
				fltStringCurrent12=pInv->fltStringCurrent12;
				fltStringVoltage13=pInv->fltStringVoltage13;
				fltStringCurrent13=pInv->fltStringCurrent13;
				fltStringVoltage14=pInv->fltStringVoltage14;
				fltStringCurrent14=pInv->fltStringCurrent14;
				fltStringVoltage15=pInv->fltStringVoltage15;
				fltStringCurrent15=pInv->fltStringCurrent15;
				fltStringVoltage16=pInv->fltStringVoltage16;
				fltStringCurrent16=pInv->fltStringCurrent16;
				fltStringVoltage17=pInv->fltStringVoltage17;
				fltStringCurrent17=pInv->fltStringCurrent17;
				fltStringVoltage18=pInv->fltStringVoltage18;
				fltStringCurrent18=pInv->fltStringCurrent18;
				fltStringVoltage19=pInv->fltStringVoltage19;
				fltStringCurrent19=pInv->fltStringCurrent19;
				fltStringVoltage20=pInv->fltStringVoltage20;
				fltStringCurrent20=pInv->fltStringCurrent20;
				fltStringVoltage21=pInv->fltStringVoltage21;
				fltStringCurrent21=pInv->fltStringCurrent21;
				fltStringVoltage22=pInv->fltStringVoltage22;
				fltStringCurrent22=pInv->fltStringCurrent22;
				fltStringVoltage23=pInv->fltStringVoltage23;
				fltStringCurrent23=pInv->fltStringCurrent23;
				fltStringVoltage24=pInv->fltStringVoltage24;
				fltStringCurrent24=pInv->fltStringCurrent24;
				fltMotherLineVoltage=pInv->fltMotherLineVoltage;
				        
				fltGroudVoltage=pInv->fltGroudVoltage;
				fltNGroudVoltage=pInv->fltNGroudVoltage;
				        
				fltDesignPower=pInv->fltDesignPower;
				        
				fltVoltageAB=pInv->fltVoltageAB;
				fltVoltageBC=pInv->fltVoltageBC;
				fltVoltageCA=pInv->fltVoltageCA;      
				        
				iSystemTime=pInv->iSystemTime;
				iEfficiency=pInv->iEfficiency;	
				iPowerOnSecond=pInv->iPowerOnSecond;	
				iPowerOffSecond=pInv->iPowerOffSecond;
				fltMPPT1=pInv->fltMPPT1;
				fltMPPT2=pInv->fltMPPT2;
				fltMPPT3=pInv->fltMPPT3;
				fltMPPT4=pInv->fltMPPT4;
								
				
	   			//先清空
    			memset(chDataBuff, 0x00, IOCP_BUFFERSIZE_I); 
    	     	iLen = 0;    	     	    	        
    	        
    	        iTemp=iDayTimes;
        	    memcpy(chDataBuff+iLen, &iTemp, 4);
				iLen += 4;	
			    
			    strTemp=strDay;
        	    memcpy(chDataBuff+iLen, strTemp, 10);
				iLen += 10;	
				       			
			    strTemp=strTime;
        	    memcpy(chDataBuff+iLen, strTemp, 8);
				iLen += 8;	
				
			    iTemp=iRegionID;
        	    memcpy(chDataBuff+iLen, &iTemp, 4);
				iLen += 4;	
				
				iTemp=iProjectID;
        	    memcpy(chDataBuff+iLen, &iTemp, 4);
				iLen += 4;	 
								
				iTemp=iSectionID;
        	    memcpy(chDataBuff+iLen, &iTemp, 4);
				iLen += 4;	
				
				iTemp=iCollectID;
        	    memcpy(chDataBuff+iLen, &iTemp, 4);
				iLen += 4;	
				
				iTemp=iCom;
        	    memcpy(chDataBuff+iLen, &iTemp, 4);
				iLen += 4;	
				
				iTemp=iAddress;
        	    memcpy(chDataBuff+iLen, &iTemp, 4);
				iLen += 4;	
				
				strTemp=strSN;
        		iTempLen = strTemp.GetLength();								
        		if (iTempLen>0 && iTempLen<32)
        		{
        			memcpy(chDataBuff+iLen, strTemp, iTempLen);
        		}
        		else if (iTempLen>=32)
        		{
        			strTT = strTemp.Left(32);
                    memcpy(chDataBuff+iLen, strTT, 32);
        		}	
        	    iLen += 32;	
        	    
				iTemp=iStatus;
        	    memcpy(chDataBuff+iLen, &iTemp, 4);
				iLen += 4;	
	   			
	   			strTemp=strStatus;
        		iTempLen = strTemp.GetLength();								
        		if (iTempLen>0 && iTempLen<128)
        		{
        			memcpy(chDataBuff+iLen, strTemp, iTempLen); 
        		}
        		else if (iTempLen>=128)
        		{
        			strTT = strTemp.Left(128);
                    memcpy(chDataBuff+iLen, strTT, 128); 
        		}	
	   			iLen += 128;
	   			
	   			fltTemp=fltPVVoltage;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	
				
				fltTemp=fltPVCurrent;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	
				
				fltTemp=fltPVOutputPower;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	
				
				fltTemp=fltPVVoltageB;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	
				
				fltTemp=fltPVCurrentB;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	
				
				fltTemp=fltPVOutputPowerB;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	
				
				fltTemp=fltPVVoltageC;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	
				
				fltTemp=fltPVCurrentC;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	
				
				fltTemp=fltPVOutputPowerC;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	     
				
				fltTemp=fltPVTotalOutputPower;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	   
				
				fltTemp=fltVoltage;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	
				
				fltTemp=fltCurrent;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	    
				
				fltTemp=fltFrequency;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	 
				
				fltTemp=fltOutputPower;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	
				
				fltTemp=fltROutputPower;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	
				
				fltTemp=fltPowerFactor;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	
				
				fltTemp=fltVoltageB;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	
				
				fltTemp=fltCurrentB;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	  
				
				fltTemp=fltFrequencyB;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	
				
				fltTemp=fltOutputPowerB;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	
				
				fltTemp=fltROutputPowerB;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	
				
				fltTemp=fltPowerFactorB;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	
				
				fltTemp=fltVoltageC;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	
				
				fltTemp=fltCurrentC;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	      
				
				fltTemp=fltFrequencyC;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	
				
				fltTemp=fltOutputPowerC;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	
				
				fltTemp=fltROutputPowerC;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	        
				
				fltTemp=fltPowerFactorC;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	        
				
				fltTemp=fltTotalOutputPower;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	
				
				fltTemp=fltRTotalOutputPower;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	
				  
				fltTemp=fltTotalPowerFactor;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	        
				
				fltTemp=fltTodayEnergy;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	
				
				fltTemp=fltTotalEnergy;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	        
				
				fltTemp=fltNTodayEnergy;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	
				
				fltTemp=fltNTotalEnergy;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	        
				
				fltTemp=fltRTodayEnergy;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	
				
				fltTemp=fltRTotalEnergy;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	        
				
				fltTemp=fltNRTodayEnergy;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	
				    
				fltTemp=fltNRTotalEnergy;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	        
				
				fltTemp=fltBtryVoltage;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	
				
				fltTemp=fltBtryCurrent;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	
				
				fltTemp=fltBtryOutputPower;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	        
				
				fltTemp=fltTemperatureInverter;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	
				
				fltTemp=fltTemperatureBooster;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	
				   
				fltTemp=fltTemperatureSink;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	        
				
				fltTemp=fltTodayHour;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	
				
				fltTemp=fltTotalHour;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	        
				
				fltTemp=fltAGCSetting;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	
				
				fltTemp=fltAVCSetting;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	        
				
				fltTemp=fltGroundResistance;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	
				
				fltTemp=fltLeakCurrent;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	                
				
				fltTemp=fltStringVoltage1;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	
				
				fltTemp=fltStringCurrent1;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	  
				
				fltTemp=fltStringVoltage2;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	  
				
				fltTemp=fltStringCurrent2;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	  
				
				fltTemp=fltStringVoltage3;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	  
				
				fltTemp=fltStringCurrent3;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	  
				
				fltTemp=fltStringVoltage4;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	  
				
				fltTemp=fltStringCurrent4;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	  
				
				fltTemp=fltStringVoltage5;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	  
				
				fltTemp=fltStringCurrent5;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	  
				
				fltTemp=fltStringVoltage6;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	  
				
				fltTemp=fltStringCurrent6;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	  
				
				fltTemp=fltStringVoltage7;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	  
				
				fltTemp=fltStringCurrent7;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	  
				
				fltTemp=fltStringVoltage8;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	  
				  
				fltTemp=fltStringCurrent8;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	
				 
				fltTemp=fltStringVoltage9;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	  
				
				fltTemp=fltStringCurrent9;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	  
				
				fltTemp=fltStringVoltage10;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	  
				
				fltTemp=fltStringCurrent10;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	  
				
				fltTemp=fltStringVoltage11;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	  
				
				fltTemp=fltStringCurrent11;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	  
				
				fltTemp=fltStringVoltage12;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	  
				
				fltTemp=fltStringCurrent12;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	  
				 
				fltTemp=fltStringVoltage13;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	  
				
				fltTemp=fltStringCurrent13;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	  
				 
				fltTemp=fltStringVoltage14;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	  
				
				fltTemp=fltStringCurrent14;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	  
				  
				fltTemp=fltStringVoltage15;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	  
				
				fltTemp=fltStringCurrent15;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	  
				
				fltTemp=fltStringVoltage16;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	  
				
				fltTemp=fltStringCurrent16;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	

				fltTemp=fltStringVoltage17;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	  
				
				fltTemp=fltStringCurrent17;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	  

				fltTemp=fltStringVoltage18;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	  
				
				fltTemp=fltStringCurrent18;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	  

				fltTemp=fltStringVoltage19;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	  
				
				fltTemp=fltStringCurrent19;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	  

				fltTemp=fltStringVoltage20;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	  
				
				fltTemp=fltStringCurrent20;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	  

				fltTemp=fltStringVoltage21;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	  
				
				fltTemp=fltStringCurrent21;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	  

				fltTemp=fltStringVoltage22;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	  
				
				fltTemp=fltStringCurrent22;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	  

				fltTemp=fltStringVoltage23;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	  
				
				fltTemp=fltStringCurrent23;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	  

				fltTemp=fltStringVoltage24;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	  
				
				fltTemp=fltStringCurrent24;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	  
				
				fltTemp=fltMotherLineVoltage;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	
				
				fltTemp=fltGroudVoltage;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	  
				
				fltTemp=fltNGroudVoltage;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	
				
				fltTemp=fltDesignPower;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	
				
				fltTemp=fltVoltageAB;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	  
				
				fltTemp=fltVoltageBC;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	  
				
				fltTemp=fltVoltageCA;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	
				 
				iTemp=iSystemTime;
        	    memcpy(chDataBuff+iLen, &iTemp, 4);
				iLen += 4;	  
				 
				iTemp=iEfficiency;
        	    memcpy(chDataBuff+iLen, &iTemp, 4);
				iLen += 4;	
				  
				iTemp=iPowerOnSecond;
        	    memcpy(chDataBuff+iLen, &iTemp, 4);
				iLen += 4;	
				  
				iTemp=iPowerOffSecond;
        	    memcpy(chDataBuff+iLen, &iTemp, 4);
				iLen += 4;	
				
				fltTemp=fltMPPT1;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	
				  
				fltTemp=fltMPPT2;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	
				
				fltTemp=fltMPPT3;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	
								
				fltTemp=fltMPPT4;
        	    memcpy(chDataBuff+iLen, &fltTemp, 4);
				iLen += 4;	
    	    	        
    	    	    	    
    	    	//上传
    	    	//业务包格式   
				memset(chBusiPack, 0x00, IOCP_BUFFERSIZE_I);
				iBusiPackLen=0;	
				
				iBusiType = _TCPIP_MWPACKTYPEINVERTER_I;
																		
				objTCPIP.M2WGetSendPack_vid(chBusiPack, &iBusiPackLen, IOCP_BUFFERSIZE_I, iBusiType, chDataBuff, iLen);
				
				//发送字符串Hex格式
				strData = objCom_Com.CComm_ChartoHexStr_str(chBusiPack,iBusiPackLen);
                
				//发送, 强制发送 IOCP_BUFFERSIZE_I 长度，避免组包
               	if (TRUE==pM2WMonitor->blnM2WClientConnect && TRUE==pM2WMonitor->blnM2WClientOK)
                {
                    //发送 2048 的整数倍，避免组包和粘包, 而不是发送实际长度
                                    						        
                    //3次收发不成功 退出
                    blnTCPOK=FALSE;    
                                    						                                        						        
                    for (iTCPCount=0; iTCPCount<_TCPIP_REPEATECOUNT_I; iTCPCount++)
                    {        
                        //系统停止
						if (FALSE==g_blnIsProgramRun)
						{
							break;
						}                                                            
                                                                                                                                    
						//接收线程退出信号
						if (FALSE==pM2WMonitor->blnthrdM2WClientWriteIsRunning)
						{
							break;
						}
                                                            
                        if (FALSE==pM2WMonitor->blnM2WClientOK)
                        {
                            break;
                        }
                                        			                    
                                        			                    
                        //每次发送前 等待一会
                        if (iTCPCount>0)
                        {
							Sleep(1000);
                        }
                        else
                        {
                            Sleep(30);
                        }        
                                            						            
                        iRt = send(pM2WMonitor->sktM2WClient,chBusiPack,iBusiPackLen,0);  
        
                        if (SOCKET_ERROR==iRt)
                        {
                            blnTCPOK=FALSE;
                            pM2WMonitor->blnM2WClientOK = FALSE;                               					                
                                        					                
                            //发送失败 String strDevID, int iDayTimes
                            strMsg1.Format("UploadRealS 发送数据失败 %s:%d %d %s", strServerAddress, uiServerPort,WSAGetLastError(), strData);
                            strMsg2.Format("UploadRealS Send Data Failed %s:%d %d %s", strServerAddress, uiServerPort, WSAGetLastError(), strData);
                            strMsg = G_GetMsgLangFormat(strMsg1, strMsg2);
                            g_objMsg.CMSG_SetMsgQue_vid(strMsg,_COLOR_BLACK, -1, -1, -1, -1, -1, -1, enuLogTCPIP); 
                            pMainFrame->PostMessage(_MSG_ID_OUTPUTDYM_I,0,0);                				
                        }
                        else
                        {
                            blnTCPOK=TRUE;
                                        										    
                            //发送成功
                            strMsg1.Format("UploadRealS 发送数据成功 %s:%d %d Com=%d Address=%d %s %s", strServerAddress, uiServerPort,iRt, -1, -1, strData, strData);
                            strMsg2.Format("UploadRealS Send Data Successfull %s:%d %d Com=%d Address=%d %s %s", strServerAddress, uiServerPort,iRt, -1, -1, strData, strData);
                            strMsg = G_GetMsgLangFormat(strMsg1, strMsg2);
                            g_objMsg.CMSG_SetMsgQue_vid(strMsg,_COLOR_BLACK, -1, -1, -1, -1, -1, -1, enuLogTCPIPAction); 
                            pMainFrame->PostMessage(_MSG_ID_OUTPUTDYM_I,0,0);
                        }
                        
                        if (TRUE==blnTCPOK)
                       	{
                       		//发送成功，退出
                       		break;
                       	}
                       	
					}//for (iTCPCount=0; iTCPCount<_TCPIP_REPEATECOUNT_I; iTCPCount++)
					
				}//if (TRUE==pM2WMonitor->blnM2WClientConnect && TRUE==pM2WMonitor->blnM2WClientOK)
    	    	           	   	         
    	  	}//if (TRUE==blnIsFinish)
	    		
	   		qInv = pInv->next;
	   		pInv = qInv;                            
		}//while (NULL!=pInv) 
			
    	
	}//try
	catch (...)
	{
		//G_WriteExceptionLog("CCollectData", "UploadInvRealDataToServer", "");
		AfxMessageBox("UploadInvRealDataToServer Error");
	}//try catch (...)
	
	return blnFundData;
}

